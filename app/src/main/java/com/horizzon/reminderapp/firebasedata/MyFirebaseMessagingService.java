package com.horizzon.reminderapp.firebasedata;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.PowerManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.horizzon.reminderapp.HomeActivity;
import com.horizzon.reminderapp.app.MyApplication;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.receiver.AlarmBroadCustReciver;
import com.horizzon.reminderapp.receiver.NotificationHelper;
import com.horizzon.reminderapp.retrofit.model.requestforotp.RegisterDevice;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;
import com.horizzon.reminderapp.service.SoundBg;
import com.horizzon.reminderapp.utility.DateUtilitys;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * MyFirebaseMessagingService receives notification Firebase Cloud Messaging Server
 */

@SuppressLint("MissingFirebaseInstanceTokenRefresh")
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    DateUtilitys dateUtl;
    String year, month, day, hours, min, sec;

    //*********** Called when the Notification is Received ********//
    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        Log.e("newToken", token);
        // Save FCM Token to sharedPreferences
        SharedPreferences sharedPreferences = getSharedPreferences("UserInfo", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("FCM_Token", token);
        editor.apply();
        RegisterDeviceForFCM(getApplicationContext(), "");
    }

    @SuppressLint("InvalidWakeLockTag")
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();
        if (!isScreenOn) {
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "MyLock");
            wl.acquire(10000);
            PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyCpuLock");
            wl_cpu.acquire(10000);
        }
        Intent notificationIntent = new Intent(getApplicationContext(), HomeActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        String body = remoteMessage.getNotification().getBody();
        Log.d("TAG", "onMessageReceived: body :" + body);

        if (!remoteMessage.getNotification().getIcon().equals("")) {
            String[] parts = remoteMessage.getNotification().getIcon().split(" ");
            String part1 = parts[0];
            String part2 = parts[1];
            String[] parts01 = part1.split("-");
            year = parts01[0];
            month = parts01[1];
            day = parts01[2];

            String[] time = part2.split(":");
            hours = time[0];
            min = time[1];
            sec = time[2];
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.MONTH, Integer.parseInt(month));
            cal.set(Calendar.YEAR, Integer.parseInt(year));
            cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(day));
            cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hours));
            cal.set(Calendar.MINUTE, Integer.parseInt(min));
            cal.set(Calendar.SECOND, 0);
            Calendar c = Calendar.getInstance();
            if (cal.compareTo(c) <= 0) {
                // Today Set time passed, count to tomorrow
                cal.add(Calendar.DATE, 1);
            }

            try {
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                r.play();
            } catch (Exception e) {
                e.printStackTrace();
            }
            startService(new Intent(getApplicationContext(), SoundBg.class));
            NotificationHelper.showNewNotification
                    (getApplicationContext(), notificationIntent, remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle(),
                            remoteMessage.getNotification().getImageUrl());
        } else {
            NotificationHelper.ChatNotifaction(getApplicationContext(), notificationIntent, remoteMessage.getNotification().getBody(),
                    remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getImageUrl());
        }


        /*
         * Set alarm
         * */
        //  new AlarmBroadCustReciver().setAlarm(getApplicationContext(), cal);

        /*
         * Play sound when notification came
         * */
        //playNotificationSound(getApplicationContext());
       /* if (MyApplication.wasInBackground) {
            startService(new Intent(getApplicationContext(), SoundBg.class));
            // playNotificationSound(getApplicationContext());
            Log.d("TAG", "onMessageReceived:background :: ");
        }*/


        //   NotificationHelper.showNewNotification(App.getContext(), null, getString(R.string.thank_you), "Your order has been placed successfully!!!");

    }

    public void playNotificationSound(Context context) {
        try {
            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            //  soundUri = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.tone);
            MediaPlayer player = MediaPlayer.create(context, soundUri);
            player.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void RegisterDeviceForFCM(final Context context, String SessId) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("UserInfo", MODE_PRIVATE);
        String token_FCM = sharedPreferences.getString("FCM_Token", "");
        String mobile = sharedPreferences.getString("mobile", "");
        String deviceId = sharedPreferences.getString("deviceId", "");
        String UId = sharedPreferences.getString("UId", "");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        if (mobile != null && deviceId != null) {
            Call<RegisterDevice> call = apiService.registerDeviceOnFCM(mobile, UId, SessId, deviceId, token_FCM);
            call.enqueue(new Callback<RegisterDevice>() {
                @Override
                public void onResponse(Call<RegisterDevice> call, Response<RegisterDevice> response) {
                    RegisterDevice registerDevice = response.body();
                    if (registerDevice.getStatusCode() == 200) {
                        Log.d("TAG", "onResponse: " + response.message());
                    }
                }

                @Override
                public void onFailure(Call<RegisterDevice> call, Throwable t) {
                    Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
                }
            });
        }

    }

}
