package com.horizzon.reminderapp;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CalenderActivity extends AppCompatActivity {
    ImageView imgBack, imgHome, imgCancel, imgYes;
    CalendarView calendar;
    int hour, min;
    TextView txtDateTime;
    String now, dayOfTheWeek, am_pm, month_name, cal_date;
    LinearLayout calLayout, repeatLayout;
    Calendar currentCalendar;
    long lastdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender);
        imgBack = (ImageView) findViewById(R.id.backBtn);
        imgHome = (ImageView) findViewById(R.id.homeBtn);
        imgYes = (ImageView) findViewById(R.id.imgYes);
        imgCancel = (ImageView) findViewById(R.id.imgCancel);
        txtDateTime = (TextView) findViewById(R.id.txtDateTime);
        calendar = (CalendarView)findViewById(R.id.calendarView1);
        calLayout = (LinearLayout) findViewById(R.id.calLayout);
        repeatLayout = (LinearLayout) findViewById(R.id.repeatLayout);

        Calendar c = Calendar.getInstance();
        min = c.get(Calendar.MINUTE);
        hour=c.get(Calendar.HOUR);

        if(hour == 0) {
            hour = 12;
        }

        final DateFormat df = new SimpleDateFormat("MMM dd yyyy");
        now = df.format(new Date());
        dayOfTheWeek = (String) android.text.format.DateFormat.format("EEEE", new Date());

        if(c.get(Calendar.AM_PM) == Calendar.AM)
        {
            am_pm="AM";
        }
        if(c.get(Calendar.AM_PM) == Calendar.PM)
        {
            am_pm="PM";
        }
        cal_date = now.toUpperCase()+", "+dayOfTheWeek.substring(0,3).toUpperCase()+", "+format2LenStr(hour)+":"+format2LenStr(min)+" "+am_pm;
        txtDateTime.setText(cal_date);

        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
                // TODO Auto-generated method stub
//                String cdate = dayOfMonth + "-" + (month + 1) + "-" + year;
                lastdate = view.getDate();
                currentCalendar = new GregorianCalendar();
                currentCalendar.setTimeInMillis(lastdate);
                dayOfTheWeek = (String) android.text.format.DateFormat.format("EEEE", currentCalendar.getTime());

                now = df.format(currentCalendar.getTime());
                cal_date = now.toUpperCase() + ", " + dayOfTheWeek.substring(0, 3).toUpperCase()+ ", " +format2LenStr(hour) + ":" + format2LenStr(min) + " " + am_pm;
                txtDateTime.setText(cal_date);
            }
        });

        imgYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentMessage = new Intent(CalenderActivity.this, DateTimeActivity.class);
                // put the message in Intent
                intentMessage.putExtra("cal_date", cal_date);
                intentMessage.putExtra("cal", lastdate);
                // Set The Result in Intent
                setResult(3, intentMessage);
                // finish The activity
                finish();
            }
        });

        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(CalenderActivity.this, DateTimeActivity.class);
                startActivity(in);
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(CalenderActivity.this, DateTimeActivity.class);
                startActivity(in);
            }
        });

        imgHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(CalenderActivity.this, HomeActivity.class);
                startActivity(in);
            }
        });

        repeatLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(CalenderActivity.this, DateTimeRepeatActivity.class);
                startActivityForResult(in, 2);
            }
        });
    }

    public static String format2LenStr(int num) {
        return (num < 10) ? "0" + num : String.valueOf(num);
    }
}
