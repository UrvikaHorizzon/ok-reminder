package com.horizzon.reminderapp.utility;

/**
 * Created by Horizzon Admin on 2/28/2017.
 */
public class SubtaskItem {
    public String subtask_name;

    public String getSubtask_name() {
        return subtask_name;
    }

    public void setSubtask_name(String subtask_name) {
        this.subtask_name = subtask_name;
    }
}
