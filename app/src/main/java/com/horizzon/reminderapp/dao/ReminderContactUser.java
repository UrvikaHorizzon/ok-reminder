package com.horizzon.reminderapp.dao;

/**
 * Created by Abhijit on 3/15/2017.
 */

public class ReminderContactUser {
    String id;
    String sesssionId;
    String iUserId;
    String name;
    String number;
    String photo="";
    Boolean foraction = false;
    String consel;

    public ReminderContactUser() {
    }

    public ReminderContactUser(String id, String sesssionId, String iUserId, String name, String number, String photo, Boolean foraction, String consel) {
        this.id = id;
        this.sesssionId = sesssionId;
        this.iUserId = iUserId;
        this.name = name;
        this.number = number;
        this.photo = photo;
        this.foraction = foraction;
        this.consel = consel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSesssionId() {
        return sesssionId;
    }

    public void setSesssionId(String sesssionId) {
        this.sesssionId = sesssionId;
    }

    public String getiUserId() {
        return iUserId;
    }

    public void setiUserId(String iUserId) {
        this.iUserId = iUserId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Boolean getForaction() {
        return foraction;
    }

    public void setForaction(Boolean foraction) {
        this.foraction = foraction;
    }

    public String getConsel() {
        return consel;
    }

    public void setConsel(String consel) {
        this.consel = consel;
    }
}
