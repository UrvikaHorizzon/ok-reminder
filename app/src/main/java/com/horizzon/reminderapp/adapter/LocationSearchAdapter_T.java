package com.horizzon.reminderapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.dao.LocationSearch;

import java.util.List;


public class LocationSearchAdapter_T extends ArrayAdapter<LocationSearch> {

    private Context mContext;
    private List<LocationSearch> locationSearchList;
    private LayoutInflater mInflater;

    public LocationSearchAdapter_T(Context context, List<LocationSearch> locationSearch) {
        super(context,0, locationSearch);
        this.mContext = context;
        this.locationSearchList = locationSearch;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return locationSearchList.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.location_autoc_lay, null);
            holder = new ViewHolder();
            holder.mainText = (TextView) convertView.findViewById(R.id.mainText);
            holder.secondaryText = (TextView) convertView.findViewById(R.id.secondaryText);
            holder.description = (TextView) convertView.findViewById(R.id.description);
            holder.addtext = (ImageView) convertView.findViewById(R.id.addtext);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        LocationSearch glossary = locationSearchList.get(position);
        holder.mainText.setText(glossary.getMainText());
        holder.secondaryText.setText(glossary.getSecondaryText());
        holder.description.setText(glossary.getDescription());
        return convertView;
    }

    private static class ViewHolder {

        TextView mainText,secondaryText,description;
        ImageView addtext;
    }

}
