package com.horizzon.reminderapp.handler;

/**
 * Created by Abhijit Sojitra on 9/3/17.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.horizzon.reminderapp.dao.ReminderLocation;
import com.horizzon.reminderapp.data.Common;

import java.util.ArrayList;
import java.util.HashMap;

public class CRLocationDbHandler {


    // table name
    public static final String TABLE_NAME = "cr_location";

    public static final String[] COLUMNS = {
            "Id",
            "sesssionId",
            "iUserId",
            "locationFor",
            "locationDetail",
            "lat",
            "lon",
            "areaFor",
            "areaSize",
            "areaUnit",
            "status"
    };

    DatabaseHandler dbh;
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
            + "(" + COLUMNS[0] + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMNS[1] + " TEXT,"
            + COLUMNS[2] + " TEXT,"
            + COLUMNS[3] + " TEXT,"
            + COLUMNS[4] + " TEXT,"
            + COLUMNS[5] + " TEXT,"
            + COLUMNS[6] + " TEXT,"
            + COLUMNS[7] + " TEXT,"
            + COLUMNS[8] + " TEXT,"
            + COLUMNS[9] + " TEXT,"
            + COLUMNS[10] + " TEXT"
            + ");";

    public CRLocationDbHandler(Context context) {
        dbh = new DatabaseHandler(context);
    }


    public long addData(String[] c) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        for (int j = 1; j < COLUMNS.length; j++) {
            Common.DisplayLog("columnNames", COLUMNS[j] + " => " + c[j]);
            values.put(COLUMNS[j], c[j]);
        }

        long id = db.insert(TABLE_NAME, null, values);
        db.close();
        return id;
    }

    public ArrayList<HashMap<String, String>> getAllData(int id) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> hashmap;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " ORDER BY " + COLUMNS[0] + " DESC ";

        Cursor cursor = db.rawQuery(query_select, null);
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                hashmap = new HashMap<String, String>();
                for (int j = 0; j < columnNames.length; j++) {
                    /*if (ConstantData.log_enable) {
						Log.e("columnNames", columnNames[j]);
						Log.e("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
					}*/
                    hashmap.put(COLUMNS[j], cursor.getString(j));
                }
                list.add(hashmap);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public HashMap<String, String> getTableSingleDate(String sesid) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        HashMap<String, String> hashmap = null;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "=" + sesid + " ORDER BY " + COLUMNS[0] + " DESC";

        Cursor cursor = db.rawQuery(query_select, null);
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                hashmap = new HashMap<String, String>();
                for (int j = 0; j < columnNames.length; j++) {
                    /*if (ConstantData.log_enable) {
						Log.e("columnNames", columnNames[j]);
						Log.e("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
					}*/
                    hashmap.put(COLUMNS[j], cursor.getString(j));
                }

            } while (cursor.moveToNext());
        }
        cursor.close();

        return hashmap;
    }


    public ReminderLocation getTableSingleDateWihtDao(String sesid) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ReminderLocation ReminderLocation = null;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "='" + sesid + "' ORDER BY " + COLUMNS[0] + " ASC";
        Common.DisplayLog("query_select", query_select);
        Cursor cursor = db.rawQuery(query_select, null);

        Common.DisplayLog("cursor.getCount()", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            Common.DisplayLog("cursor", "yes");
            String[] columnNames = cursor.getColumnNames();
            do {
                ReminderLocation = new ReminderLocation(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10));
                for (int j = 0; j < columnNames.length; j++) {
                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
                }
            } while (cursor.moveToNext());
        }
        cursor.close();

        return ReminderLocation;
    }

    public void resetTempdata(String sessid) {
        SQLiteDatabase db = dbh.getWritableDatabase();
        db.execSQL("delete from " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = '" + sessid + "'");
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }


    public void resetTable() {
        SQLiteDatabase db = dbh.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.execSQL("delete from " + TABLE_NAME);
        db.execSQL("delete from sqlite_sequence where name='" + TABLE_NAME + "'");
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }

    public int getDataCount(String sessid) {
        SQLiteDatabase db = dbh.getReadableDatabase();
        String statusstr = "";

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = '" + sessid + "' " + statusstr;

        Cursor cursor = db.rawQuery(query_select, null);
        int totcount = cursor.getCount();
        cursor.close();

        return totcount;
    }
}
