package com.horizzon.reminderapp.service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;

import androidx.annotation.Nullable;

public class SoundBg extends Service {
    Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
    MediaPlayer player;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    public void onCreate() {
        player = MediaPlayer.create(getApplicationContext(), soundUri);
        player.setLooping(true); //set looping
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        player.start();
        return Service.START_NOT_STICKY;
    }

}
