package com.horizzon.reminderapp.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpdateAddress {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Datum {

        @SerializedName("Id")
        @Expose
        private String id;
        @SerializedName("iUserId")
        @Expose
        private String iUserId;
        @SerializedName("vAddressFor")
        @Expose
        private String vAddressFor;
        @SerializedName("tAddress")
        @Expose
        private String tAddress;
        @SerializedName("vLat")
        @Expose
        private String vLat;
        @SerializedName("vLong")
        @Expose
        private String vLong;
        @SerializedName("dCreatedDateTime")
        @Expose
        private String dCreatedDateTime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIUserId() {
            return iUserId;
        }

        public void setIUserId(String iUserId) {
            this.iUserId = iUserId;
        }

        public String getVAddressFor() {
            return vAddressFor;
        }

        public void setVAddressFor(String vAddressFor) {
            this.vAddressFor = vAddressFor;
        }

        public String getTAddress() {
            return tAddress;
        }

        public void setTAddress(String tAddress) {
            this.tAddress = tAddress;
        }

        public String getVLat() {
            return vLat;
        }

        public void setVLat(String vLat) {
            this.vLat = vLat;
        }

        public String getVLong() {
            return vLong;
        }

        public void setVLong(String vLong) {
            this.vLong = vLong;
        }

        public String getDCreatedDateTime() {
            return dCreatedDateTime;
        }

        public void setDCreatedDateTime(String dCreatedDateTime) {
            this.dCreatedDateTime = dCreatedDateTime;
        }

    }
}
