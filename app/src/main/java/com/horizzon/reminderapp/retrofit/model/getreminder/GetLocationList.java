
package com.horizzon.reminderapp.retrofit.model.getreminder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetLocationList {

    @SerializedName("iId")
    @Expose
    private String iId;
    @SerializedName("vSesssionId")
    @Expose
    private String vSesssionId;
    @SerializedName("iUserId")
    @Expose
    private String iUserId;
    @SerializedName("vLocationFor")
    @Expose
    private String vLocationFor;
    @SerializedName("tLocationDetail")
    @Expose
    private String tLocationDetail;
    @SerializedName("vLat")
    @Expose
    private String vLat;
    @SerializedName("vLon")
    @Expose
    private String vLon;
    @SerializedName("vAreaFor")
    @Expose
    private String vAreaFor;
    @SerializedName("vAreaSize")
    @Expose
    private String vAreaSize;
    @SerializedName("vAreaUnit")
    @Expose
    private String vAreaUnit;
    @SerializedName("iStatus")
    @Expose
    private String iStatus;
    @SerializedName("dCreatedDateTime")
    @Expose
    private String dCreatedDateTime;

    public String getIId() {
        return iId;
    }

    public void setIId(String iId) {
        this.iId = iId;
    }

    public String getVSesssionId() {
        return vSesssionId;
    }

    public void setVSesssionId(String vSesssionId) {
        this.vSesssionId = vSesssionId;
    }

    public String getIUserId() {
        return iUserId;
    }

    public void setIUserId(String iUserId) {
        this.iUserId = iUserId;
    }

    public String getVLocationFor() {
        return vLocationFor;
    }

    public void setVLocationFor(String vLocationFor) {
        this.vLocationFor = vLocationFor;
    }

    public String getTLocationDetail() {
        return tLocationDetail;
    }

    public void setTLocationDetail(String tLocationDetail) {
        this.tLocationDetail = tLocationDetail;
    }

    public String getVLat() {
        return vLat;
    }

    public void setVLat(String vLat) {
        this.vLat = vLat;
    }

    public String getVLon() {
        return vLon;
    }

    public void setVLon(String vLon) {
        this.vLon = vLon;
    }

    public String getVAreaFor() {
        return vAreaFor;
    }

    public void setVAreaFor(String vAreaFor) {
        this.vAreaFor = vAreaFor;
    }

    public String getVAreaSize() {
        return vAreaSize;
    }

    public void setVAreaSize(String vAreaSize) {
        this.vAreaSize = vAreaSize;
    }

    public String getVAreaUnit() {
        return vAreaUnit;
    }

    public void setVAreaUnit(String vAreaUnit) {
        this.vAreaUnit = vAreaUnit;
    }

    public String getIStatus() {
        return iStatus;
    }

    public void setIStatus(String iStatus) {
        this.iStatus = iStatus;
    }

    public String getDCreatedDateTime() {
        return dCreatedDateTime;
    }

    public void setDCreatedDateTime(String dCreatedDateTime) {
        this.dCreatedDateTime = dCreatedDateTime;
    }

}
