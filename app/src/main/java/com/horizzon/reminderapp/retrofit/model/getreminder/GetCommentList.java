
package com.horizzon.reminderapp.retrofit.model.getreminder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCommentList {

    @SerializedName("iId")
    @Expose
    private String iId;
    @SerializedName("vSesssionId")
    @Expose
    private String vSesssionId;
    @SerializedName("iUserId")
    @Expose
    private String iUserId;
    @SerializedName("tComment")
    @Expose
    private String tComment;
    @SerializedName("tCommentFor")
    @Expose
    private String tCommentFor;
    @SerializedName("iStatus")
    @Expose
    private String iStatus;
    @SerializedName("dCreatedDateTime")
    @Expose
    private String dCreatedDateTime;

    public String getIId() {
        return iId;
    }

    public void setIId(String iId) {
        this.iId = iId;
    }

    public String getVSesssionId() {
        return vSesssionId;
    }

    public void setVSesssionId(String vSesssionId) {
        this.vSesssionId = vSesssionId;
    }

    public String getIUserId() {
        return iUserId;
    }

    public void setIUserId(String iUserId) {
        this.iUserId = iUserId;
    }

    public String getTComment() {
        return tComment;
    }

    public void setTComment(String tComment) {
        this.tComment = tComment;
    }

    public String gettCommentFor() {
        return tCommentFor;
    }

    public void settCommentFor(String tCommentFor) {
        this.tCommentFor = tCommentFor;
    }

    public String getIStatus() {
        return iStatus;
    }

    public void setIStatus(String iStatus) {
        this.iStatus = iStatus;
    }

    public String getDCreatedDateTime() {
        return dCreatedDateTime;
    }

    public void setDCreatedDateTime(String dCreatedDateTime) {
        this.dCreatedDateTime = dCreatedDateTime;
    }

}
