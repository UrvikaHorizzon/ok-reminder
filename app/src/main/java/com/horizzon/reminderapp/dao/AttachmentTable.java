package com.horizzon.reminderapp.dao;

/**
 * Created by horizzon on 3/27/2017.
 */

public class AttachmentTable {
    String Id;String sesssionId;

    String iUserId;
    String attachFor;
    String detail;
    String status;
    String createdData;

    public AttachmentTable() {
    }

    public AttachmentTable(String id, String sesssionId, String iUserId, String attachFor, String detail, String status, String createdData) {
        Id = id;
        this.sesssionId = sesssionId;
        this.iUserId = iUserId;
        this.attachFor = attachFor;
        this.detail = detail;
        this.status = status;
        this.createdData = createdData;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getSesssionId() {
        return sesssionId;
    }

    public void setSesssionId(String sesssionId) {
        this.sesssionId = sesssionId;
    }

    public String getiUserId() {
        return iUserId;
    }

    public void setiUserId(String iUserId) {
        this.iUserId = iUserId;
    }

    public String getAttachFor() {
        return attachFor;
    }

    public void setAttachFor(String attachFor) {
        this.attachFor = attachFor;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedData() {
        return createdData;
    }

    public void setCreatedData(String createdData) {
        this.createdData = createdData;
    }
}
