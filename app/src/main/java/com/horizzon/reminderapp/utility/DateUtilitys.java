package com.horizzon.reminderapp.utility;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by horizzon on 4/4/2017.
 */

public class DateUtilitys {

    private Date CurselDate;
    private String defaultDateTIMEFormat = "yyyy-MM-dd HH:mm:ss";
    private String defaultDateFormat = "yyyy-MM-dd";
    private String defaultTimeFormat = "HH:mm:ss";
    private String year = "";
    private String month = "";
    private String day = "";
    private String weekDay = "";
    private String fullweekDay = "";
    private String hour = "";
    private String min = "";
    private String ses = "";
    private String ms = "";
    private String ampm = "";

    public DateUtilitys() {
    }

    public DateUtilitys(Date dateStr, boolean fromWheel) {
        CurselDate = dateStr;
        setTitleFormet(dateStr);
    }

    public DateUtilitys(String dateStr, String formatfrom, String format) {

        try {
            String d = FormetDate(dateStr, formatfrom, format);
        } catch (ParseException e) {
//            e.printStackTrace();
            year = "";
            month = "";
            day = "";
            weekDay = "";
            hour = "";
            min = "";
            ses = "";
            ms = "";
            ampm = "";
        }

        Date date = new Date();
        String strDateFormat = "dd-MMM-yyyy HH:mm:ss a";
        SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
        System.out.println("Date with time and AM/PM field : " + sdf.format(date));
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(String weekDay) {
        this.weekDay = weekDay;
    }

    public String getFullweekDay() {
        return fullweekDay;
    }

    public void setFullweekDay(String fullweekDay) {
        this.fullweekDay = fullweekDay;
    }

    public String getHour() {
        return hour;
    }

    public String get12Hour() {
        return (String) android.text.format.DateFormat.format("hh", CurselDate);
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getSes() {
        return ses;
    }

    public void setSes(String ses) {
        this.ses = ses;
    }

    public String getMs() {
        return ms;
    }

    public void setMs(String ms) {
        this.ms = ms;
    }

    public String getAmpm() {
        return ampm;
    }

    public void setAmpm(String ampm) {
        this.ampm = ampm;
    }


    public String getforDisplayTitle() {
        return month + " " + day + " " + year + ", " + weekDay + ", " + hour + ":" + min + " " + ampm;
    }


    public void setTitleFormet(Date dateStr) {
        CurselDate = dateStr;
        year = (String) android.text.format.DateFormat.format("yyyy", dateStr);
        month = (String) android.text.format.DateFormat.format("MMM", dateStr);
        day = (String) android.text.format.DateFormat.format("dd", dateStr);
        weekDay = (String) android.text.format.DateFormat.format("E", dateStr);
        fullweekDay = (String) android.text.format.DateFormat.format("EEEE", dateStr);
        hour = (String) android.text.format.DateFormat.format("hh", dateStr);
        min = (String) android.text.format.DateFormat.format("mm", dateStr);
        ses = (String) android.text.format.DateFormat.format("ss", dateStr);
        ms = (String) android.text.format.DateFormat.format("SS", dateStr);
        ampm = (String) android.text.format.DateFormat.format("a", dateStr);
    }

    public String setforDisplayTitle(Date dateStr) {
        CurselDate = dateStr;
        year = (String) android.text.format.DateFormat.format("yyyy", dateStr);
        month = (String) android.text.format.DateFormat.format("MMM", dateStr);
        day = (String) android.text.format.DateFormat.format("dd", dateStr);
        weekDay = (String) android.text.format.DateFormat.format("E", dateStr);
        fullweekDay = (String) android.text.format.DateFormat.format("EEEE", dateStr);
        hour = (String) android.text.format.DateFormat.format("hh", dateStr);
        min = (String) android.text.format.DateFormat.format("mm", dateStr);
        ses = (String) android.text.format.DateFormat.format("ss", dateStr);
        ms = (String) android.text.format.DateFormat.format("SS", dateStr);
        ampm = (String) android.text.format.DateFormat.format("a", dateStr);

        return getforDisplayTitle();
    }

    public String setforDisplayTitleForRep(Date dateStr) {
        CurselDate = dateStr;
        year = (String) android.text.format.DateFormat.format("yyyy", dateStr);
        month = (String) android.text.format.DateFormat.format("MMM", dateStr);
        day = (String) android.text.format.DateFormat.format("dd", dateStr);
        weekDay = (String) android.text.format.DateFormat.format("E", dateStr);
        fullweekDay = (String) android.text.format.DateFormat.format("EEEE", dateStr);
        hour = (String) android.text.format.DateFormat.format("hh", dateStr);
        min = (String) android.text.format.DateFormat.format("mm", dateStr);
        ses = (String) android.text.format.DateFormat.format("ss", dateStr);
        ms = (String) android.text.format.DateFormat.format("SS", dateStr);
        ampm = (String) android.text.format.DateFormat.format("a", dateStr);

        return month + " " + day + " " + year;
    }

    public String getDateForStore(Date dateStr) {
        CurselDate = dateStr;
        return (String) android.text.format.DateFormat.format(defaultDateFormat, dateStr);
    }

    public String getTimeForStore(Date dateStr) {
        CurselDate = dateStr;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateStr);
        calendar.set(Calendar.SECOND, 0);
        dateStr = calendar.getTime();
        return (String) android.text.format.DateFormat.format(defaultTimeFormat, dateStr);
    }

    public String FormetDate(String DateStr, String formatfrom, String format) throws java.text.ParseException {
        //String startDateStr = "2016/1/10";
        //java.text.SimpleDateFormat ps = new java.text.SimpleDateFormat("yyyy/MM/dd");
        java.text.SimpleDateFormat ps = new java.text.SimpleDateFormat(formatfrom);

        //java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("MM-dd-yyyy EEE");
        java.text.SimpleDateFormat df = new java.text.SimpleDateFormat(format);

        java.util.Date startDate = ps.parse(DateStr);
        CurselDate = startDate;
        year = "";
        month = "";
        day = "";
        weekDay = "";
        hour = "";
        min = "";
        ses = "";
        ms = "";
        ampm = "";

        //out.println(startDate);
        return df.format(startDate);
    }

    public Date FormetDate(String DateStr) {
        Date dateStr = new Date();
        if (DateStr != null) {
            DateFormat formatter = new SimpleDateFormat(defaultDateTIMEFormat);
            try {
                dateStr = (Date) formatter.parse(DateStr.toString());
            } catch (ParseException e) {
                DateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                dateStr = new Date();
                try {
                    dateStr = (Date) formatter1.parse(DateStr.toString());
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
            }
            CurselDate = dateStr;
            year = (String) android.text.format.DateFormat.format("yyyy", dateStr);
            month = (String) android.text.format.DateFormat.format("MM", dateStr);
            day = (String) android.text.format.DateFormat.format("dd", dateStr);
            weekDay = (String) android.text.format.DateFormat.format("E", dateStr);
            fullweekDay = (String) android.text.format.DateFormat.format("EEEE", dateStr);
            hour = (String) android.text.format.DateFormat.format("HH", dateStr);
            min = (String) android.text.format.DateFormat.format("mm", dateStr);
            ses = (String) android.text.format.DateFormat.format("ss", dateStr);
            ms = (String) android.text.format.DateFormat.format("SS", dateStr);
            ampm = (String) android.text.format.DateFormat.format("a", dateStr);

            //out.println(startDate);
            return dateStr;
        } else {
            return dateStr;
        }
    }

    public static String timeAgo(String DateStr) {

        String defaultDateTIMEFormat = "yyyy-MM-dd HH:mm:ss";
        DateFormat formatter = new SimpleDateFormat(defaultDateTIMEFormat);
        Date dateStr = new Date();
        try {
            dateStr = (Date) formatter.parse(DateStr.toString());
        } catch (ParseException e) {
            DateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            dateStr = new Date();
            try {
                dateStr = (Date) formatter1.parse(DateStr.toString());
            } catch (ParseException e1) {
                e1.printStackTrace();
            }
        }

        long seleiTime = dateStr.getTime();
        Date cdate = new Date();
        long cTime = cdate.getTime();

        Object[][] time_formats = {{60.0, "seconds", 1.0}, // 60
                {120.0, "1 minute ago", "1 minute from now"}, // 60*2
                {3600.0, "minutes", 60.0}, // 60*60, 60
                {7200.0, "1 hour ago", "1 hour from now"}, // 60*60*2
                {86400.0, "hours", 3600.0}, // 60*60*24, 60*60
                {172800.0, "Yesterday", "Tomorrow"}, // 60*60*24*2
                {604800.0, "days", 86400.0}, // 60*60*24*7, 60*60*24
                {1209600.0, "Last week", "Next week"}, // 60*60*24*7*4*2
                {2419200.0, "weeks", 604800.0}, // 60*60*24*7*4, 60*60*24*7
                {4838400.0, "Last month", "Next month"}, // 60*60*24*7*4*2
                {29030400.0, "months", 2419200.0}, // 60*60*24*7*4*12,
                // 60*60*24*7*4
                {58060800.0, "Last year", "Next year"}, // 60*60*24*7*4*12*2

                {2903040000.0, "years", 29030400.0}, // 60*60*24*7*4*12*100,// 60*60*24*7*4*12
                {5806080000.0, "Last century", "Next century"}, //// 60*60*24*7*4*12*100*2
                {58060800000.0, "centuries", 2903040000.0} //// 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100

//				{ (60 * 60 * 24 * 7 * 4 * 12 * 100), "years", 29030400 }, // 60*60*24*7*4*12*100,// 60*60*24*7*4*12
//				{ (60 * 60 * 24 * 7 * 4 * 12 * 100 * 2), "Last century", "Next century" }, // 60*60*24*7*4*12*100*2
//				{ (60 * 60 * 24 * 7 * 4 * 12 * 100 * 20), "centuries", (60 * 60 * 24 * 7 * 4 * 12 * 100) } // 60*60*24*7*4*12*100*20,
                // 60*60*24*7*4*12*100
        };

        long seconds = (cTime - seleiTime) / 1000;
        String token = "ago";
        int list_choice = 1;

//		System.out.println("" + seleiTime);
//		System.out.println("" + seconds);

        if (seconds == 0) {
            return "Just now";
        }
        if (seconds < 0) {
            seconds = Math.abs(seconds);
            token = "from now";
            list_choice = 2;
        }
        int i = 0;
        Object[] format;
        while (i < time_formats.length) {
            format = time_formats[i++];
            //System.out.println(seconds +" < "+format[0]);
            if (Double.valueOf(seconds) < Double.valueOf((Double) format[0])) {
                if (format[2] instanceof String) {
                    return (String) format[list_choice];
                } else {
                    return ((int) Math.floor(Double.valueOf(seconds) / Double.valueOf((Double) format[2]))) + " " + (String) format[1] + " " + token;
                }
            }
            i++;
        }

        return String.valueOf(seleiTime);
    }

}
