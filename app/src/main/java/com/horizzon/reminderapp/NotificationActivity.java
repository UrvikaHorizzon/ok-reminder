package com.horizzon.reminderapp;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.horizzon.reminderapp.adapter.DrawerItemCustomAdapter;
import com.horizzon.reminderapp.adapter.ReminderListAdapter;
import com.horizzon.reminderapp.dao.CReminder;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.CreateReminderDbHandler;
import com.horizzon.reminderapp.utility.ObjectDrawerItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class NotificationActivity extends AppCompatActivity {

    String comeForm = "";
    TextView pageTitle;
    ImageView imgBack, imgBtnOpt;
    RecyclerView listView;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    protected ListView mDrawerList;
    ObjectDrawerItem[] drawerItem;
    //    ArrayList<CommentItem> list;
//    NotificationCustomAdapter cAdapter;
    ArrayList<CReminder> list;
    ReminderListAdapter cAdapter;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

//        pageTitle = (TextView) findViewById(R.id.pageTitle);
        if (getIntent().getStringExtra("comeForm") != null) {
            comeForm = getIntent().getStringExtra("comeForm");
//            pageTitle.setText(comeForm);
        }

        imgBack = (ImageView) findViewById(R.id.backBtn);
        imgBtnOpt = (ImageView) findViewById(R.id.imgOpt);
        listView = (RecyclerView) findViewById(R.id.list);
        list = new ArrayList<CReminder>();

        fab = (FloatingActionButton) findViewById(R.id.fab);

        final Animation zoomin = AnimationUtils.loadAnimation(this, R.anim.zzomin);
        final Animation zoomout = AnimationUtils.loadAnimation(this, R.anim.zoomout);

        fab.setAnimation(zoomin);
        fab.setAnimation(zoomout);

        zoomin.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                fab.startAnimation(zoomout);
            }
        });

        zoomout.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                fab.startAnimation(zoomin);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                Intent in = new Intent(NotificationActivity.this, CreateReminderActivity.class);
                startActivity(in);
            }
        });

        SetData(comeForm);
//        cAdapter = new NotificationCustomAdapter(this, list);
        /*Collections.sort(list, new Comparator<CReminder>() {
            @Override
            public int compare(CReminder cReminder, CReminder t1) {
                return cReminder.getId().compareTo(t1.getId());
            }
        });*/
        Collections.reverse(list);
        cAdapter = new ReminderListAdapter(NotificationActivity.this, list, "inbox");
        LinearLayoutManager lLayout = new LinearLayoutManager(getApplicationContext());
        listView.setLayoutManager(lLayout);
        listView.setAdapter(cAdapter);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // list the drawer items
        drawerItem = new ObjectDrawerItem[1];

        drawerItem[0] = new ObjectDrawerItem(R.drawable.settingicon, "Settings");
//        drawerItem[1] = new ObjectDrawerItem(R.drawable.syncicon, "Sync Now");

        // Pass the folderData to our ListView adapter
        final DrawerItemCustomAdapter adapter1 = new DrawerItemCustomAdapter(this, R.layout.list_item_row, drawerItem);

        // Set the adapter for the list view
        mDrawerList.setAdapter(adapter1);
        // set the item click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // for app icon control for nav drawer
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imgBtnOpt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        });
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            selectItem(position);
        }
    }

    private void SetData(String comeForm) {


        CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getApplicationContext());


        HashMap<String, String> cond = new HashMap<>();
        if (comeForm.equals("NOTIFICATIONS")) {
//            cond.put("", "");
        } else if (comeForm.equals("SHARED")) {
            cond.put("reminderFor", "share");
        } else if (comeForm.equals("SENT")) {
            cond.put("reminderFor", "send");
        }
//        ArrayList<CReminder> cReminder = crdbh.getAllDataWithSearch(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), comeForm, cond);

        list = crdbh.getAllDataForNotification(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), comeForm, cond);

    }

}
