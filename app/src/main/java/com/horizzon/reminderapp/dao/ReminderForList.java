package com.horizzon.reminderapp.dao;

/**
 * Created by horizzon on 6/9/2017.
 */

public class ReminderForList {
    private String iId;
    private String iUserId;
    private String vSesssionId;
    private String vViewed;
    private String vCreatedBy;
    private String vUpdatedBy;
    private String tUpdateNote;
    private String vReminderStatus;

    public ReminderForList(String iId, String iUserId, String vSesssionId, String vViewed, String vCreatedBy, String vUpdatedBy, String tUpdateNote, String vReminderStatus) {
        this.iId = iId;
        this.iUserId = iUserId;
        this.vSesssionId = vSesssionId;
        this.vViewed = vViewed;
        this.vCreatedBy = vCreatedBy;
        this.vUpdatedBy = vUpdatedBy;
        this.tUpdateNote = tUpdateNote;
        this.vReminderStatus = vReminderStatus;
    }

    public String getiId() {
        return iId;
    }

    public void setiId(String iId) {
        this.iId = iId;
    }

    public String getiUserId() {
        return iUserId;
    }

    public void setiUserId(String iUserId) {
        this.iUserId = iUserId;
    }

    public String getvSesssionId() {
        return vSesssionId;
    }

    public void setvSesssionId(String vSesssionId) {
        this.vSesssionId = vSesssionId;
    }

    public String getvViewed() {
        return vViewed;
    }

    public void setvViewed(String vViewed) {
        this.vViewed = vViewed;
    }

    public String getvCreatedBy() {
        return vCreatedBy;
    }

    public void setvCreatedBy(String vCreatedBy) {
        this.vCreatedBy = vCreatedBy;
    }

    public String getvUpdatedBy() {
        return vUpdatedBy;
    }

    public void setvUpdatedBy(String vUpdatedBy) {
        this.vUpdatedBy = vUpdatedBy;
    }

    public String gettUpdateNote() {
        return tUpdateNote;
    }

    public void settUpdateNote(String tUpdateNote) {
        this.tUpdateNote = tUpdateNote;
    }

    public String getvReminderStatus() {
        return vReminderStatus;
    }

    public void setvReminderStatus(String vReminderStatus) {
        this.vReminderStatus = vReminderStatus;
    }
}
