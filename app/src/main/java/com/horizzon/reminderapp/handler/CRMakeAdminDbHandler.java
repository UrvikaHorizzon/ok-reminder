package com.horizzon.reminderapp.handler;

/**
 * Created by Abhijit Sojitra on 9/3/17.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.horizzon.reminderapp.dao.ReminderMakeAdmin;
import com.horizzon.reminderapp.data.Common;

import java.util.ArrayList;

public class CRMakeAdminDbHandler {


    // table name
    public static final String TABLE_NAME = "cr_makeadmin";

    public static final String[] COLUMNS = {
            "Id",
            "sesssionId",
            "userId",
            "extra"
    };

    DatabaseHandler dbh;
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
            + "(" + COLUMNS[0] + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMNS[1] + " TEXT,"
            + COLUMNS[2] + " TEXT,"
            + COLUMNS[3] + " TEXT"
            + ");";

    public CRMakeAdminDbHandler(Context context) {
        dbh = new DatabaseHandler(context);
    }


    public long addData(String[] c) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        for (int j = 1; j < COLUMNS.length; j++) {
            Common.DisplayLog("columnNames", COLUMNS[j] + " => " + c[j]);
            values.put(COLUMNS[j], c[j]);
        }

//        long id = db.insert(TABLE_NAME, null, values);
        long id;
        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "='" + c[0] + "' AND " + COLUMNS[2] + "='" + c[1] + "'";

        Cursor cursor = db.rawQuery(query_select, null);
        if (cursor.getCount() <= 0) {
            id = db.insert(TABLE_NAME, null, values);
        } else {
            cursor.moveToFirst();
            id = cursor.getLong(0);
        }
        db.close();
        return id;
    }

    public ArrayList<ReminderMakeAdmin> getAllCurrentSessDataWithType(String sesid) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<ReminderMakeAdmin> list = new ArrayList<ReminderMakeAdmin>();
        ReminderMakeAdmin ReminderMakeAdmin;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "='" + sesid + "' ORDER BY " + COLUMNS[0] + " ASC";
        Common.DisplayLog("query_select", query_select + "");
        Cursor cursor = db.rawQuery(query_select, null);
        Common.DisplayLog("", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                ReminderMakeAdmin = new ReminderMakeAdmin(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));
                for (int j = 0; j < columnNames.length; j++) {

                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));

                }
                list.add(ReminderMakeAdmin);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public ReminderMakeAdmin getTableSingleDateWihtDao(String sesid, String uId) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ReminderMakeAdmin ReminderMakeAdmin = null;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "='" + sesid + "' AND " + COLUMNS[2] + "='" + uId + "' ORDER BY " + COLUMNS[0] + " ASC";
        Common.DisplayLog("query_select", query_select);
        Cursor cursor = db.rawQuery(query_select, null);

        Common.DisplayLog("cursor.getCount()", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            Common.DisplayLog("cursor", "yes");
            String[] columnNames = cursor.getColumnNames();
            do {
                ReminderMakeAdmin = new ReminderMakeAdmin(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));
                for (int j = 0; j < columnNames.length; j++) {
                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
                }
            } while (cursor.moveToNext());
        }
        cursor.close();

        return ReminderMakeAdmin;
    }

    public void resetTempdata(String sessid, String uid) {
        SQLiteDatabase db = dbh.getWritableDatabase();
        Common.DisplayLog("", "delete from " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = '" + sessid + "' AND " + COLUMNS[2] + " = '" + uid + "'");
        db.execSQL("delete from " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = '" + sessid + "' AND " + COLUMNS[2] + " = '" + uid + "'");
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }


    public void resetTable() {
        SQLiteDatabase db = dbh.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.execSQL("delete from " + TABLE_NAME);
        db.execSQL("delete from sqlite_sequence where name='" + TABLE_NAME + "'");
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }

    public int getDataCount(String sessid) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = '" + sessid + "' ";

        Cursor cursor = db.rawQuery(query_select, null);
        int totcount = cursor.getCount();
        cursor.close();

        return totcount;
    }

}
