
package com.horizzon.reminderapp.retrofit.model.getreminder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetUsersboxsList {

    @SerializedName("iId")
    @Expose
    private String iId;
    @SerializedName("iUserId")
    @Expose
    private String iUserId;
    @SerializedName("vBoxName")
    @Expose
    private String vBoxName;
    @SerializedName("vBoxType")
    @Expose
    private String vBoxType;
    @SerializedName("dCreatedDateTime")
    @Expose
    private String dCreatedDateTime;

    public String getIId() {
        return iId;
    }

    public void setIId(String iId) {
        this.iId = iId;
    }

    public String getIUserId() {
        return iUserId;
    }

    public void setIUserId(String iUserId) {
        this.iUserId = iUserId;
    }

    public String getVBoxName() {
        return vBoxName;
    }

    public void setVBoxName(String vBoxName) {
        this.vBoxName = vBoxName;
    }

    public String getVBoxType() {
        return vBoxType;
    }

    public void setVBoxType(String vBoxType) {
        this.vBoxType = vBoxType;
    }

    public String getDCreatedDateTime() {
        return dCreatedDateTime;
    }

    public void setDCreatedDateTime(String dCreatedDateTime) {
        this.dCreatedDateTime = dCreatedDateTime;
    }

}
