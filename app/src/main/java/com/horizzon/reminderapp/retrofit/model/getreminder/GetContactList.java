
package com.horizzon.reminderapp.retrofit.model.getreminder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetContactList {

    @SerializedName("iId")
    @Expose
    private String iId;
    @SerializedName("vSesssionId")
    @Expose
    private String vSesssionId;
    @SerializedName("iUserId")
    @Expose
    private String iUserId;
    @SerializedName("vName")
    @Expose
    private String vName;
    @SerializedName("vNumber")
    @Expose
    private String vNumber;
    @SerializedName("vPhoto")
    @Expose
    private String vPhoto;
    @SerializedName("vForaction")
    @Expose
    private String vForaction;
    @SerializedName("vConsel")
    @Expose
    private String vConsel;
    @SerializedName("dCreatedDateTime")
    @Expose
    private String dCreatedDateTime;

    public String getIId() {
        return iId;
    }

    public void setIId(String iId) {
        this.iId = iId;
    }

    public String getVSesssionId() {
        return vSesssionId;
    }

    public void setVSesssionId(String vSesssionId) {
        this.vSesssionId = vSesssionId;
    }

    public String getIUserId() {
        return iUserId;
    }

    public void setIUserId(String iUserId) {
        this.iUserId = iUserId;
    }

    public String getVName() {
        return vName;
    }

    public void setVName(String vName) {
        this.vName = vName;
    }

    public String getVNumber() {
        return vNumber;
    }

    public void setVNumber(String vNumber) {
        this.vNumber = vNumber;
    }

    public String getVPhoto() {
        return vPhoto;
    }

    public void setVPhoto(String vPhoto) {
        this.vPhoto = vPhoto;
    }

    public String getVForaction() {
        return vForaction;
    }

    public void setVForaction(String vForaction) {
        this.vForaction = vForaction;
    }

    public String getVConsel() {
        return vConsel;
    }

    public void setVConsel(String vConsel) {
        this.vConsel = vConsel;
    }

    public String getDCreatedDateTime() {
        return dCreatedDateTime;
    }

    public void setDCreatedDateTime(String dCreatedDateTime) {
        this.dCreatedDateTime = dCreatedDateTime;
    }

}
