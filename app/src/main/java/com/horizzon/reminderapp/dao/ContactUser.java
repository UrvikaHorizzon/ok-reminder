package com.horizzon.reminderapp.dao;

import java.io.Serializable;

/**
 * Created by Abhijit on 3/15/2017.
 */

public class ContactUser implements Serializable {
    String id;
    String sesssionId;
    String name;
    String number;
    String photo="";
    Boolean foraction = false;

    public ContactUser() {
    }

    public ContactUser(String id, String sesssionId, String name, String number, String photo, Boolean foraction) {
        this.id = id;
        this.sesssionId = sesssionId;
        this.name = name;
        this.number = number;
        this.photo = (photo!=null)?photo:"";
        this.foraction = foraction;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSesssionId() {
        return sesssionId;
    }

    public void setSesssionId(String sesssionId) {
        this.sesssionId = sesssionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Boolean getForaction() {
        return foraction;
    }

    public void setForaction(Boolean foraction) {
        this.foraction = foraction;
    }
}
