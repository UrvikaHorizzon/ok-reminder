package com.horizzon.reminderapp.retrofit.rest;

import com.horizzon.reminderapp.dao.DetailsCommnetsModel;
import com.horizzon.reminderapp.model.DeactivUser;
import com.horizzon.reminderapp.model.FolderName;
import com.horizzon.reminderapp.model.OtherUserList;
import com.horizzon.reminderapp.model.UpdateUserProfile;
import com.horizzon.reminderapp.retrofit.model.AddAddress;
import com.horizzon.reminderapp.retrofit.model.ChatNotifactionList;
import com.horizzon.reminderapp.retrofit.model.DeleteAddress;
import com.horizzon.reminderapp.retrofit.model.GetAllAddress;
import com.horizzon.reminderapp.retrofit.model.GetFolderNameList;
import com.horizzon.reminderapp.retrofit.model.PostChatNotifactionList;
import com.horizzon.reminderapp.retrofit.model.ReminderCount;
import com.horizzon.reminderapp.retrofit.model.SessionIdList;
import com.horizzon.reminderapp.retrofit.model.UpdateAddress;
import com.horizzon.reminderapp.retrofit.model.checkcontact.CheckContact;
import com.horizzon.reminderapp.retrofit.model.getRFolderFilter.RFolderFilter;
import com.horizzon.reminderapp.retrofit.model.getreminder.ReminderList;
import com.horizzon.reminderapp.retrofit.model.getreminderforlist.GetReminderForList;
import com.horizzon.reminderapp.retrofit.model.location.Nearbysearch;
import com.horizzon.reminderapp.retrofit.model.makeadmin.MakeAdmin;
import com.horizzon.reminderapp.retrofit.model.reminder.Reminder;
import com.horizzon.reminderapp.retrofit.model.requestforotp.CheckUserExist;
import com.horizzon.reminderapp.retrofit.model.requestforotp.RegisterDevice;
import com.horizzon.reminderapp.retrofit.model.requestforotp.RequestForOtp;
import com.horizzon.reminderapp.retrofit.model.usersboxs.UsersBoxs;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by horizzon on 5/5/2017.
 */

public interface ApiInterface {
    @Multipart
    @POST("user/requestForOtp")
    Call<RequestForOtp> getRequestForOtp(@Part("vUContryNumber") RequestBody vUContryNumber,
                                         @Part("vUNumber") RequestBody vUNumber,
                                         @Part("vUName") RequestBody vUName,
                                         @Part("vLastName") RequestBody vLastName,
                                         @Part MultipartBody.Part image

    );
//

    @Multipart
    @POST("reminder/checkmobile")
    Call<CheckUserExist> getUserData(@Part("vMobile") RequestBody vUNumber

    );

    @Multipart
    @POST("user/UserUpdateProfile")
    Call<UpdateUserProfile> postUpdateProfile(@Part("vUContryNumber") RequestBody vUContryNumber,
                                              @Part("iUserId") RequestBody iUserId,
                                              @Part("vUNumber") RequestBody vUNumber,
                                              @Part("vUName") RequestBody vUName,
                                              @Part("vUHiddenPhoto") RequestBody vUHiddenPhoto,
                                              @Part MultipartBody.Part image

    );


    //

    @POST("user/UserDeactive")
    @FormUrlEncoded
    Call<DeactivUser> UserDeactive(@Field("iUserId") String iUserId);


    @POST("user/DeviceRegistration")
    @FormUrlEncoded
    Call<RegisterDevice> registerDeviceOnFCM(@Field("vMobile") String vMobile,
                                             @Field("iUserId") String iUserId,
                                             @Field("vSesssionId") String vSesssionId,
                                             @Field("vDeviceId") String vDeviceId,
                                             @Field("vDeviceToken") String vDeviceToken);


    @POST("user/checkContact")
    @FormUrlEncoded
    Call<CheckContact> getCheckContact(@Field("vContact[]") String[] vContact);

    @GET("nearbysearch/json")
    Call<Nearbysearch> getNearbysearchDetails(@Query("key") String key, @Query("location") String location, @Query("rankby") String rankby, @Query("keyword") String keyword);


    /*
     * Add folder name in param
     * */
    @POST("reminder/addReminder")
    @FormUrlEncoded
    Call<Reminder> addReminder(@Field("vSesssionId") String vSesssionId,
                               @Field("iUserId") String iUserId,
                               @Field("tSubject") String tSubject,
                               @Field("iPriority") String iPriority,
                               @Field("vReminderFor") String vReminderFor,
                               @Field("iStatus") String iStatus,
                               @Field("vLockStatus") String vLockStatus,
                               @Field("vUserFolder") String vUserFolder,
                               @Field("tExtra") String tExtra);


    @POST("reminder/chatMessage")
    @FormUrlEncoded
    Call<PostChatNotifactionList> chatNotifactionListPost(@Field("vSesssionId") String vSesssionId,
                                                          @Field("sentby_userid") String sentby_userid,
                                                          @Field("sentto_userid") String sentto_userid,
                                                          @Field("sentby_mobile") String sentby_mobile,
                                                          @Field("sentto_mobile[]") ArrayList<String> sentto_mobile,
                                                          @Field("message") String message,
                                                          @Field("chattype") String chattype
    );

    @GET("reminder/noofReminder")
    Call<ReminderCount> checkRemider(@Query("iUserId") String iUserId);

    @GET("reminder/getuserFolder")
    Call<GetFolderNameList> getFolderNameList(@Query("iUserId") String iUserId);


    @GET("reminder/GetUserSessionData")
    Call<SessionIdList> getSessionIdList(@Query("iUserId") String iUserId);


    @GET("reminder/getChatMessage")
    Call<ChatNotifactionList> getChatNotifactionList(@Query("mobile") String iUserId);


    @POST("reminder/createFolder")
    @FormUrlEncoded
    Call<FolderName> createFolder(@Field("iUserId") String iUserId,
                                  @Field("vFoldername") String vFoldername
    );

    @POST("user/OtherUsersDetail")
    @FormUrlEncoded
    Call<OtherUserList> otherUserList(@Field("iUserId") String iUserId);


    @POST("reminder/editReminder")
    @FormUrlEncoded
    Call<Reminder> editReminder(@Field("vSesssionId") String vSesssionId, @Field("iUserId") String iUserId, @Field("tSubject") String tSubject, @Field("iPriority") String iPriority, @Field("vReminderFor") String vReminderFor, @Field("iStatus") String iStatus, @Field("vLockStatus") String vLockStatus, @Field("tExtra") String tExtra);

    @POST("reminder/addContact")
    @FormUrlEncoded
    Call<Reminder> addContact(@Field("vSesssionId") String vSesssionId,
                              @Field("iUserId") String iUserId,
                              @Field("vName") String vName,
                              @Field("vNumber") String vNumber,
                              @Field("vPhoto") String vPhoto,
                              @Field("vForaction") Boolean vForaction,
                              @Field("vConsel") String vConsel);

    @POST("reminder/removeContact")
    @FormUrlEncoded
    Call<Reminder> removeContact(@Field("vSesssionId") String vSesssionId);

    @POST("reminder/addDatetime")
    @FormUrlEncoded
    Call<Reminder> addDatetime(@Field("vSesssionId") String vSesssionId, @Field("iUserId") String iUserId, @Field("vDtFor") String vDtFor, @Field("vStartDate") String vStartDate, @Field("vStartTime") String vStartTime, @Field("vEndDate") String vEndDate, @Field("vEndTime") String vEndTime, @Field("tDays") String tDays, @Field("iStatus") String iStatus);

    @POST("reminder/editDatetime")
    @FormUrlEncoded
    Call<Reminder> editDatetime(@Field("vSesssionId") String vSesssionId, @Field("iUserId") String iUserId, @Field("vDtFor") String vDtFor, @Field("vStartDate") String vStartDate, @Field("vStartTime") String vStartTime, @Field("vEndDate") String vEndDate, @Field("vEndTime") String vEndTime, @Field("tDays") String tDays, @Field("iStatus") String iStatus);

    @POST("reminder/addLocation")
    @FormUrlEncoded
    Call<Reminder> addLocation(@Field("vSesssionId") String vSesssionId,
                               @Field("iUserId") String iUserId, @Field("vLocationFor") String vLocationFor,
                               @Field("tLocationDetail") String tLocationDetail, @Field("vLat") String vLat,
                               @Field("vLon") String vLon, @Field("vAreaFor") String vAreaFor,
                               @Field("vAreaSize") String vAreaSize, @Field("vAreaUnit") String vAreaUnit,
                               @Field("iStatus") String iStatus);

    @POST("reminder/editLocation")
    @FormUrlEncoded
    Call<Reminder> editLocation(@Field("vSesssionId") String vSesssionId, @Field("iUserId") String iUserId, @Field("vLocationFor") String vLocationFor, @Field("tLocationDetail") String tLocationDetail, @Field("vLat") String vLat, @Field("vLon") String vLon, @Field("vAreaFor") String vAreaFor, @Field("vAreaSize") String vAreaSize, @Field("vAreaUnit") String vAreaUnit, @Field("iStatus") String iStatus);

    @POST("reminder/addComment")
    @FormUrlEncoded
    Call<Reminder> addComment(@Field("vSesssionId") String vSesssionId,
                              @Field("iUserId") String iUserId,
                              @Field("tComment") String tComment,
                              @Field("iStatus") String iStatus,
                              @Field("tCommentFor") String tCommentFor);

    @POST("reminder/addAttachment")
    @FormUrlEncoded
    Call<Reminder> addAttachment(@Field("vSesssionId") String vSesssionId, @Field("iUserId") String iUserId, @Field("vAttachFor") String vAttachFor, @Field("tDetail") String tDetail, @Field("iStatus") String iStatus);

    @POST("reminder/editAttachment")
    @FormUrlEncoded
    Call<Reminder> editAttachment(@Field("iId") String iId, @Field("vSesssionId") String vSesssionId, @Field("iUserId") String iUserId, @Field("vAttachFor") String vAttachFor, @Field("tDetail") String tDetail, @Field("iStatus") String iStatus);

    @POST("reminder/removeAttachment")
    @FormUrlEncoded
    Call<Reminder> removeAttachment(@Field("iId") String iId, @Field("vSesssionId") String vSesssionId);

    @POST("reminder/addImageAttachment")
    @Multipart
    Call<Reminder> addImageAttachment(@Part("vSesssionId") RequestBody vSesssionId, @Part("iUserId") RequestBody iUserId, @Part("vAttachFor") RequestBody vAttachFor, @Part MultipartBody.Part tDetail, @Part("iStatus") RequestBody iStatus);

    @POST("reminder/getListOfReminder")
    @FormUrlEncoded
    Call<ReminderList> getListOfReminder(@Field("iUserId") String iUserId);

    @POST("reminder/getReminderForList")
    @FormUrlEncoded
    Call<GetReminderForList> getReminderForList(@Field("iUserId") String iUserId);


    @POST("user/GetUserAddressDetail")
    @FormUrlEncoded
    Call<GetAllAddress> getForListAddress(@Field("iUserId") String iUserId);


    @POST("reminder/updateReminderStatus")
    @FormUrlEncoded
    Call<Reminder> updateReminderStatus(@Field("iUserId") String iUserId, @Field("vSesssionId") String vSesssionId, @Field("vReminderStatus") String vReminderStatus);

    @POST("reminder/addMakeAdmin")
    @FormUrlEncoded
    Call<Reminder> addMakeAdmin(@Field("vSesssionId") String vSesssionId, @Field("vUserId") String vUserId, @Field("tExtra") String tExtra);

    @POST("reminder/removeMakeAdmin")
    @FormUrlEncoded
    Call<Reminder> removeMakeAdmin(@Field("vSesssionId") String vSesssionId, @Field("vUserId") String vUserId);

    @POST("reminder/getMakeAdmin")
    @FormUrlEncoded
    Call<MakeAdmin> getMakeAdmin(@Field("iUserId") String iUserId);

    @POST("reminder/addBox")
    @FormUrlEncoded
    Call<Reminder> addBox(@Field("iUserId") String iUserId, @Field("vBoxName") String vBoxName, @Field("vBoxType") String vBoxType);

    @POST("reminder/editBox")
    @FormUrlEncoded
    Call<Reminder> editBox(@Field("iId") String iId, @Field("iUserId") String iUserId, @Field("vBoxName") String vBoxName, @Field("vBoxType") String vBoxType);

    @POST("reminder/removeBox")
    @FormUrlEncoded
    Call<Reminder> removeBox(@Field("iId") String iId, @Field("iUserId") String iUserId);

    @POST("reminder/getBoxs")
    @FormUrlEncoded
    Call<UsersBoxs> getBoxs(@Field("iUserId") String iUserId);

    @POST("reminder/addRFolderFilter")
    @FormUrlEncoded
    Call<Reminder> addRFolderFilter(@Field("iUserId") String iUserId, @Field("vSesssionId") String vSesssionId, @Field("iFolderId") String iFolderId);

    @POST("reminder/getRFolderFilter")
    @FormUrlEncoded
    Call<RFolderFilter> getRFolderFilter(@Field("iUserId") String iUserId);

    @GET("reminder/getChatList")
    Call<DetailsCommnetsModel> getCommentList(@Query("vSesssionId") String vSesssionId);

    @POST("user/AddUserAddress")
    @FormUrlEncoded
    Call<AddAddress> addAddress(@Field("iUserId") String iUserId,
                                @Field("vAddressFor") String vAddressFor,
                                @Field("tAddress") String tAddress,
                                @Field("vLat") double vLat,
                                @Field("vLong") double vLong);
    //                                @Field("dCreatedDateTime") String dCreatedDateTime

    @POST("user/UserAddressUpdate")
    @FormUrlEncoded
    Call<UpdateAddress> updateAddress(@Field("hiddenId") String hiddenId,
                                      @Field("iUserId") String iUserId,
                                      @Field("vAddressFor") String vAddressFor,
                                      @Field("tAddress") String tAddress,
                                      @Field("vLat") double vLat,
                                      @Field("vLong") double vLong);

    @POST("user/DeleteUserAddress")
    @FormUrlEncoded
    Call<DeleteAddress> deleteAddress(@Field("Id") String hiddenId);
}