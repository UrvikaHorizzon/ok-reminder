package com.horizzon.reminderapp.app;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by horizzon on 1/18/2017.
 */

public class MyRegButton extends androidx.appcompat.widget.AppCompatButton{
    public MyRegButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(),"fonts/arial.ttf"));
    }
}
