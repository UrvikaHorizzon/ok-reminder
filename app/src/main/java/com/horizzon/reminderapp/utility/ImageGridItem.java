package com.horizzon.reminderapp.utility;

import android.graphics.Bitmap;

/**
 * Created by Horizzon Admin on 3/1/2017.
 */
public class ImageGridItem {
    Bitmap img_attach;

    public Bitmap getImg_attach() {
        return img_attach;
    }

    public void setImg_attach(Bitmap img_attach) {
        this.img_attach = img_attach;
    }
}
