package com.horizzon.reminderapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.horizzon.reminderapp.helper.LoopScrollListener;
import com.horizzon.reminderapp.helper.LoopView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DateTimeRepeatActivity extends AppCompatActivity implements View.OnTouchListener{
    public LoopView custom_picker_year, custom_picker_day, custom_picker_am_pm, everyday_picker_year, everyday_picker_day, everyday_ampm, everywday_picker_year, everywday_picker_day, everywday_am_pm, everyweek_picker_year, everyweek_picker_day, everyweek_picker_am_pm;
    private int yearPos1 = 0, dayPos1 = 0, ampmPos1 = 0, yearPos2 = 0, dayPos2 = 0, ampmPos2 = 0, yearPos3 = 0, dayPos3 = 0, ampmPos3 = 0, yearPos4 = 0, dayPos4 = 0, ampmPos4 = 0;
    List<String> yearList1 = new ArrayList();
    List<String> dayList1 = new ArrayList();
    List<String> ampmList1 = new ArrayList();
    List<String> yearList2 = new ArrayList();
    List<String> dayList2 = new ArrayList();
    List<String> ampmList2 = new ArrayList();
    List<String> yearList3 = new ArrayList();
    List<String> dayList3 = new ArrayList();
    List<String> ampmList3 = new ArrayList();
    List<String> yearList4 = new ArrayList();
    List<String> dayList4 = new ArrayList();
    List<String> ampmList4 = new ArrayList();
    ScrollView repscrollView;
    LinearLayout customLayout, startenddateTimeLayout, startDateLayout, endDateLayout, everydayLayout, everyweekdayLayout, everyweekLayout, everymonthLayout,calLayout, repeatLayout, repoutLayout;
    RadioGroup repeatradioGroup;
    CheckBox cb1, cb2, cb3, cb4, cb5, cb6, cb7;
    public LoopView repstartendpicker_year, repstartendpicker_month,repstartendpicker_day;
    public TextView txtStart, txtEnd;
    private int minYear = 1990; // min year
    private int maxYear = 2550; // max year
    private int yearPos = 0, monthPos = 0, dayPos = 0;
    ImageView imgBack, imgHome, imgCancel, imgYes;
    List<String> yearList = new ArrayList();
    List<String> monthList = new ArrayList();
    List<String> dayList = new ArrayList();
    public int repsesrept_flag = 1;
    public int repsesback_flag = 1;
    public String start_date, end_date, custom_date, month_name, today_date;
    SimpleDateFormat dateFormatter;
    SingleDateAndTimePicker everymonth_day_picker, startdate_day_picker, enddate_day_picker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_time_repeat);

        repstartendpicker_year = (LoopView) findViewById(R.id.repstartendpicker_year);
        repstartendpicker_month = (LoopView) findViewById(R.id.repstartendpicker_month);
        repstartendpicker_day = (LoopView) findViewById(R.id.repstartendpicker_day);
        custom_picker_year = (LoopView) findViewById(R.id.custom_picker_year);
        custom_picker_day = (LoopView) findViewById(R.id.custom_picker_day);
        custom_picker_am_pm = (LoopView) findViewById(R.id.custom_picker_am_pm);
        everyday_picker_year = (LoopView) findViewById(R.id.everyday_picker_year);
        everyday_picker_day = (LoopView) findViewById(R.id.everyday_picker_day);
        everyday_ampm = (LoopView) findViewById(R.id.everyday_ampm);
        everywday_picker_year = (LoopView) findViewById(R.id.everywday_picker_year);
        everywday_picker_day = (LoopView) findViewById(R.id.everywday_picker_day);
        everywday_am_pm = (LoopView) findViewById(R.id.everywday_am_pm);
        everyweek_picker_year = (LoopView) findViewById(R.id.everyweek_picker_year);
        everyweek_picker_day = (LoopView) findViewById(R.id.everyweek_picker_day);
        everyweek_picker_am_pm = (LoopView) findViewById(R.id.everyweek_picker_am_pm);
        repscrollView = (ScrollView) findViewById(R.id.repscrollView);
        customLayout = (LinearLayout) findViewById(R.id.customLayout);
        startenddateTimeLayout = (LinearLayout) findViewById(R.id.startenddateTimeLayout);
        startDateLayout = (LinearLayout) findViewById(R.id.startDateLayout);
        endDateLayout = (LinearLayout) findViewById(R.id.endDateLayout);
        repoutLayout = (LinearLayout) findViewById(R.id.repoutLayout);
        everydayLayout = (LinearLayout) findViewById(R.id.everydayLayout);
        everyweekdayLayout = (LinearLayout) findViewById(R.id.everywdayLayout);
        everyweekLayout = (LinearLayout) findViewById(R.id.everyweekLayout);
        everymonthLayout = (LinearLayout) findViewById(R.id.everymonthLayout);
        repeatradioGroup = (RadioGroup) findViewById(R.id.repeatradioGroup);
        imgBack = (ImageView) findViewById(R.id.backBtn);
        imgHome = (ImageView) findViewById(R.id.homeBtn);
        imgYes = (ImageView) findViewById(R.id.imgYes);
        imgCancel = (ImageView) findViewById(R.id.imgCancel);
        calLayout = (LinearLayout) findViewById(R.id.calLayout);
        repeatLayout = (LinearLayout) findViewById(R.id.repeatLayout);

        txtStart = (TextView) findViewById(R.id.txtStart);
        txtEnd = (TextView) findViewById(R.id.txtEnd);

        everymonth_day_picker = (SingleDateAndTimePicker) findViewById(R.id.everymonth_day_picker);
        startdate_day_picker = (SingleDateAndTimePicker) findViewById(R.id.startdate_day_picker);
        enddate_day_picker = (SingleDateAndTimePicker) findViewById(R.id.enddate_day_picker);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        everymonth_day_picker.selectDate(calendar);
        startdate_day_picker.selectDate(calendar);
        enddate_day_picker.selectDate(calendar);

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        today_date = dateFormatter.format(new Date());

        final DateFormat df = new SimpleDateFormat("MMM dd yyyy");

        cb1 = (CheckBox) findViewById(R.id.chk);
        cb2 = (CheckBox) findViewById(R.id.chk1);
        cb3 = (CheckBox) findViewById(R.id.chk2);
        cb4 = (CheckBox) findViewById(R.id.chk3);
        cb5 = (CheckBox) findViewById(R.id.chk4);
        cb6 = (CheckBox) findViewById(R.id.chk5);
        cb7 = (CheckBox) findViewById(R.id.chk6);

        setSelectedDate(dateFormatter.format(new Date()));
        int year = minYear + yearPos;
        int month = monthPos + 1;
        int day = dayPos + 1;

        if (month == 1) {
            month_name = "Jan";
        } else if (month == 2) {
            month_name = "Feb";
        } else if (month == 3) {
            month_name = "March";
        } else if (month == 4) {
            month_name = "April";
        } else if (month == 5) {
            month_name = "May";
        } else if (month == 6) {
            month_name = "June";
        } else if (month == 7) {
            month_name = "July";
        } else if (month == 8) {
            month_name = "Aug";
        } else if (month == 9) {
            month_name = "Sep";
        } else if (month == 10) {
            month_name = "Oct";
        } else if (month == 11) {
            month_name = "Nov";
        } else if (month == 12) {
            month_name = "Dec";
        }
        StringBuffer sb = new StringBuffer();
        sb.append(month_name);
        sb.append(" ");
        sb.append(format2LenStr(day));
        sb.append(" ");
        sb.append(String.valueOf(year));
        start_date = sb.toString();

        calLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(DateTimeRepeatActivity.this, CalenderActivity.class);
                startActivityForResult(in, 3);
            }
        });

        repstartendpicker_year.setLoopListener(new LoopScrollListener() {
            @Override
            public void onItemSelect(int item) {
                yearPos = item;
                repeatinitDayPickerView();
            }
        });
        repstartendpicker_month.setLoopListener(new LoopScrollListener() {
            @Override
            public void onItemSelect(int item) {
                monthPos = item;
                repeatinitDayPickerView();
            }
        });
        repstartendpicker_day.setLoopListener(new LoopScrollListener() {
            @Override
            public void onItemSelect(int item) {
                dayPos = item;
            }
        });

        repeatinitPickerViews(); // init year and month loop view
        repeatinitDayPickerView(); //init day loop view


        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(DateTimeRepeatActivity.this, DateTimeActivity.class);
                startActivity(in);
            }
        });

        imgHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(DateTimeRepeatActivity.this, HomeActivity.class);
                startActivity(in);
            }
        });

        repoutLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (repsesback_flag == 2) {
                    repscrollView.setVisibility(View.VISIBLE);
                    startenddateTimeLayout.setVisibility(View.GONE);
                    customLayout.setVisibility(View.GONE);
                    startDateLayout.setBackgroundColor(ContextCompat.getColor(DateTimeRepeatActivity.this, R.color.transparent));
                    endDateLayout.setBackgroundColor(ContextCompat.getColor(DateTimeRepeatActivity.this, R.color.transparent));
                    repsesback_flag = 1;
                } else if (repsesback_flag == 4) {
                    repscrollView.setVisibility(View.VISIBLE);
                    startenddateTimeLayout.setVisibility(View.GONE);
                    customLayout.setVisibility(View.GONE);
                    startDateLayout.setBackgroundColor(ContextCompat.getColor(DateTimeRepeatActivity.this, R.color.transparent));
                    endDateLayout.setBackgroundColor(ContextCompat.getColor(DateTimeRepeatActivity.this, R.color.transparent));
                    repsesback_flag = 1;
                }
                return false;
            }
        });

        imgYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (repsesback_flag == 2) {
                    startDateLayout.setBackgroundColor(ContextCompat.getColor(DateTimeRepeatActivity.this, R.color.transparent));
                    endDateLayout.setBackgroundColor(ContextCompat.getColor(DateTimeRepeatActivity.this, R.color.transparent));
                    int year = minYear + yearPos;
                    int month = monthPos + 1;
                    int day = dayPos + 1;

                    if (month == 1) {
                        month_name = "Jan";
                    } else if (month == 2) {
                        month_name = "Feb";
                    } else if (month == 3) {
                        month_name = "March";
                    } else if (month == 4) {
                        month_name = "April";
                    } else if (month == 5) {
                        month_name = "May";
                    } else if (month == 6) {
                        month_name = "June";
                    } else if (month == 7) {
                        month_name = "July";
                    } else if (month == 8) {
                        month_name = "Aug";
                    } else if (month == 9) {
                        month_name = "Sep";
                    } else if (month == 10) {
                        month_name = "Oct";
                    } else if (month == 11) {
                        month_name = "Nov";
                    } else if (month == 12) {
                        month_name = "Dec";
                    }
                    StringBuffer sb = new StringBuffer();
                    sb.append(month_name);
                    sb.append(" ");
                    sb.append(format2LenStr(day));
                    sb.append(" ");
                    sb.append(String.valueOf(year));
                    String temp_date = year + "-" + format2LenStr(month) + "-" + format2LenStr(day);
                    start_date = sb.toString();
                    if (temp_date.equals(today_date)) {
                        txtStart.setText("Today");
                    } else {
                        txtStart.setText(start_date);
                    }
                    startDateLayout.setBackgroundColor(ContextCompat.getColor(DateTimeRepeatActivity.this, R.color.transparent));
                    repscrollView.setVisibility(View.VISIBLE);
                    startenddateTimeLayout.setVisibility(View.GONE);
                    customLayout.setVisibility(View.GONE);
                    repsesback_flag = 1;
                } else if (repsesback_flag == 4) {
                    startDateLayout.setBackgroundColor(ContextCompat.getColor(DateTimeRepeatActivity.this, R.color.transparent));
                    endDateLayout.setBackgroundColor(ContextCompat.getColor(DateTimeRepeatActivity.this, R.color.transparent));
                    int year = minYear + yearPos;
                    int month = monthPos + 1;
                    int day = dayPos + 1;

                    if (month == 1) {
                        month_name = "Jan";
                    } else if (month == 2) {
                        month_name = "Feb";
                    } else if (month == 3) {
                        month_name = "March";
                    } else if (month == 4) {
                        month_name = "April";
                    } else if (month == 5) {
                        month_name = "May";
                    } else if (month == 6) {
                        month_name = "June";
                    } else if (month == 7) {
                        month_name = "July";
                    } else if (month == 8) {
                        month_name = "Aug";
                    } else if (month == 9) {
                        month_name = "Sep";
                    } else if (month == 10) {
                        month_name = "Oct";
                    } else if (month == 11) {
                        month_name = "Nov";
                    } else if (month == 12) {
                        month_name = "Dec";
                    }
                    StringBuffer sb = new StringBuffer();
                    sb.append(month_name);
                    sb.append(" ");
                    sb.append(format2LenStr(day));
                    sb.append(" ");
                    sb.append(String.valueOf(year));
                    String temp_date = year + "-" + format2LenStr(month) + "-" + format2LenStr(day);
                    end_date = sb.toString();
                    if (temp_date.equals(today_date)) {
                        txtEnd.setText("Today");
                    } else {
                        txtEnd.setText(end_date);
                    }
                    endDateLayout.setBackgroundColor(ContextCompat.getColor(DateTimeRepeatActivity.this, R.color.transparent));
                    repscrollView.setVisibility(View.VISIBLE);
                    startenddateTimeLayout.setVisibility(View.GONE);
                    customLayout.setVisibility(View.GONE);
                    repsesback_flag = 1;
                } else if (repsesback_flag == 3) {
                    String rept_time = "";
                    int day = dayPos1 + 1;
                    if (yearPos1 == 0) {
                        rept_time = "Days";
                    } else if (yearPos1 == 1) {
                        rept_time = "Weeks";
                    } else if (yearPos1 == 2) {
                        rept_time = "Months";
                    } else if (yearPos1 == 3) {
                        rept_time = "Years";
                    }
                    String rept_str = "Every " + day + " " + rept_time;
                    Intent intentMessage = new Intent();
                    // put the message in Intent
                    intentMessage.putExtra("repsesrept_flag", 5);
                    intentMessage.putExtra("start_date", start_date);
                    intentMessage.putExtra("rept_str", rept_str);
                    // Set The Result in Intent
                    setResult(2, intentMessage);
                    // finish The activity
                    finish();
                } else {
                    int r = repeatradioGroup.getCheckedRadioButtonId();

                    if (r != -1) {
                        RadioButton rb = (RadioButton) findViewById(r);
                        if (rb.getText().equals("Everyday")) {
                            repsesrept_flag = 1;
                        } else if (rb.getText().equals("Every Weekday")) {
                            repsesrept_flag = 2;
                        } else if (rb.getText().equals("Every Week")) {
                            repsesrept_flag = 3;
                        } else if (rb.getText().equals("Every Month")) {
                            repsesrept_flag = 4;
                        } else if (rb.getText().equals("Custom")) {
                            repsesrept_flag = 5;
                        }
                    }
                    Intent intentMessage = new Intent();
                    // put the message in Intent
                    intentMessage.putExtra("repsesrept_flag", repsesrept_flag);
                    intentMessage.putExtra("start_date", start_date);
                    // Set The Result in Intent
                    setResult(2, intentMessage);
                    // finish The activity
                    finish();
                    repsesback_flag = 1;
                }
            }
        });

        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (repsesback_flag == 1) {
                    onBackPressed();
                } else if (repsesback_flag == 2) {
                    repscrollView.setVisibility(View.VISIBLE);
                    startenddateTimeLayout.setVisibility(View.GONE);
                    customLayout.setVisibility(View.GONE);
                    startDateLayout.setBackgroundColor(ContextCompat.getColor(DateTimeRepeatActivity.this, R.color.transparent));
                    endDateLayout.setBackgroundColor(ContextCompat.getColor(DateTimeRepeatActivity.this, R.color.transparent));
                    repsesback_flag = 1;
                } else if (repsesback_flag == 3) {
                    repscrollView.setVisibility(View.VISIBLE);
                    startenddateTimeLayout.setVisibility(View.GONE);
                    customLayout.setVisibility(View.GONE);
                    repsesback_flag = 1;
                } else if (repsesback_flag == 4) {
                    repscrollView.setVisibility(View.VISIBLE);
                    startenddateTimeLayout.setVisibility(View.GONE);
                    customLayout.setVisibility(View.GONE);
                    startDateLayout.setBackgroundColor(ContextCompat.getColor(DateTimeRepeatActivity.this, R.color.transparent));
                    endDateLayout.setBackgroundColor(ContextCompat.getColor(DateTimeRepeatActivity.this, R.color.transparent));
                    repsesback_flag = 1;
                }
            }
        });

        startDateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repsesback_flag = 2;
                endDateLayout.setBackgroundColor(Color.parseColor("#ccffffff"));
                startDateLayout.setBackgroundColor(ContextCompat.getColor(DateTimeRepeatActivity.this, R.color.transparent));
                repscrollView.setVisibility(View.GONE);
                startenddateTimeLayout.setVisibility(View.VISIBLE);
                customLayout.setVisibility(View.GONE);
                enddate_day_picker.setVisibility(View.GONE);
                startdate_day_picker.setVisibility(View.VISIBLE);
            }
        });

        endDateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repsesback_flag = 4;
                endDateLayout.setBackgroundColor(ContextCompat.getColor(DateTimeRepeatActivity.this, R.color.transparent));
                startDateLayout.setBackgroundColor(Color.parseColor("#ccffffff"));
                repscrollView.setVisibility(View.GONE);
                startenddateTimeLayout.setVisibility(View.VISIBLE);
                customLayout.setVisibility(View.GONE);

                startdate_day_picker.setVisibility(View.GONE);
                enddate_day_picker.setVisibility(View.VISIBLE);
            }
        });

        cb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    cb1.setTextColor(Color.parseColor("#1ba1e2"));
                } else {
                    cb1.setTextColor(Color.parseColor("#90000000"));
                }
            }
        });

        cb2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    cb2.setTextColor(Color.parseColor("#1ba1e2"));
                } else {
                    cb2.setTextColor(Color.parseColor("#90000000"));
                }
            }
        });

        cb3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    cb3.setTextColor(Color.parseColor("#1ba1e2"));
                } else {
                    cb3.setTextColor(Color.parseColor("#90000000"));
                }
            }
        });

        cb4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    cb4.setTextColor(Color.parseColor("#1ba1e2"));
                } else {
                    cb4.setTextColor(Color.parseColor("#90000000"));
                }
            }
        });

        cb5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    cb5.setTextColor(Color.parseColor("#1ba1e2"));
                } else {
                    cb5.setTextColor(Color.parseColor("#90000000"));
                }
            }
        });

        cb6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    cb6.setTextColor(Color.parseColor("#1ba1e2"));
                } else {
                    cb6.setTextColor(Color.parseColor("#90000000"));
                }
            }
        });

        cb7.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    cb7.setTextColor(Color.parseColor("#1ba1e2"));
                } else {
                    cb7.setTextColor(Color.parseColor("#90000000"));
                }
            }
        });

        repeatradioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                RadioButton rb = (RadioButton) findViewById(checkedId);
                if (rb.getText().equals("Everyday")) {
                    everydayLayout.setVisibility(View.VISIBLE);
                    everyweekdayLayout.setVisibility(View.GONE);
                    everymonthLayout.setVisibility(View.GONE);
                    everyweekLayout.setVisibility(View.GONE);
                    customLayout.setVisibility(View.GONE);
                } else if (rb.getText().equals("Every Weekday")) {
                    everydayLayout.setVisibility(View.GONE);
                    everyweekdayLayout.setVisibility(View.VISIBLE);
                    everymonthLayout.setVisibility(View.GONE);
                    everyweekLayout.setVisibility(View.GONE);
                    customLayout.setVisibility(View.GONE);
                } else if (rb.getText().equals("Every Week")) {
                    everydayLayout.setVisibility(View.GONE);
                    everyweekdayLayout.setVisibility(View.GONE);
                    everymonthLayout.setVisibility(View.GONE);
                    everyweekLayout.setVisibility(View.VISIBLE);
                    customLayout.setVisibility(View.GONE);
                } else if (rb.getText().equals("Every Month")) {
                    everydayLayout.setVisibility(View.GONE);
                    everyweekdayLayout.setVisibility(View.GONE);
                    everymonthLayout.setVisibility(View.VISIBLE);
                    everyweekLayout.setVisibility(View.GONE);
                    customLayout.setVisibility(View.GONE);
                } else if (rb.getText().equals("Custom")) {
                    everydayLayout.setVisibility(View.GONE);
                    everyweekdayLayout.setVisibility(View.GONE);
                    everymonthLayout.setVisibility(View.GONE);
                    everyweekLayout.setVisibility(View.GONE);
                    customLayout.setVisibility(View.VISIBLE);
                    repsesback_flag = 3;
                }
            }
        });

        custom_picker_year.setLoopListener(new LoopScrollListener() {
            @Override
            public void onItemSelect(int item) {
                yearPos1 = item;
            }
        });

        custom_picker_day.setLoopListener(new LoopScrollListener() {
            @Override
            public void onItemSelect(int item) {
                dayPos1 = item;
            }
        });

        custom_picker_am_pm.setLoopListener(new LoopScrollListener() {
            @Override
            public void onItemSelect(int item) {
                ampmPos1 = item;
            }
        });

        everyday_picker_year.setLoopListener(new LoopScrollListener() {
            @Override
            public void onItemSelect(int item) {
                yearPos2 = item;
            }
        });

        everyday_picker_day.setLoopListener(new LoopScrollListener() {
            @Override
            public void onItemSelect(int item) {
                dayPos2 = item;
            }
        });

        everyday_ampm.setLoopListener(new LoopScrollListener() {
            @Override
            public void onItemSelect(int item) {
                ampmPos2 = item;
            }
        });

        everywday_picker_year.setLoopListener(new LoopScrollListener() {
            @Override
            public void onItemSelect(int item) {
                yearPos3 = item;
            }
        });

        everywday_picker_day.setLoopListener(new LoopScrollListener() {
            @Override
            public void onItemSelect(int item) {
                dayPos3 = item;
            }
        });

        everywday_am_pm.setLoopListener(new LoopScrollListener() {
            @Override
            public void onItemSelect(int item) {
                ampmPos3 = item;
            }
        });

        everyweek_picker_year.setLoopListener(new LoopScrollListener() {
            @Override
            public void onItemSelect(int item) {
                yearPos4 = item;
            }
        });

        everyweek_picker_day.setLoopListener(new LoopScrollListener() {
            @Override
            public void onItemSelect(int item) {
                dayPos4 = item;
            }
        });

        everyweek_picker_am_pm.setLoopListener(new LoopScrollListener() {
            @Override
            public void onItemSelect(int item) {
                ampmPos4 = item;
            }
        });

        initPickerViews1(); // init year and month loop view
        initPickerViews2();
        initPickerViews3();
        initPickerViews4();
        repeatinitPickerViews();
    }

    private void initPickerViews1() {
        yearList1.add("00");
        yearList1.add("05");
        yearList1.add("10");
        yearList1.add("15");
        yearList1.add("20");
        yearList1.add("25");
        yearList1.add("30");
        yearList1.add("35");
        yearList1.add("40");
        yearList1.add("45");
        yearList1.add("50");
        yearList1.add("55");

        custom_picker_year.setDataList((ArrayList) yearList1);
        custom_picker_year.setInitPosition(yearPos1);

        ampmList1.add("AM");
        ampmList1.add("PM");

        custom_picker_am_pm.setDataList((ArrayList) ampmList1);
        custom_picker_am_pm.setInitPosition(ampmPos1);

        for (int i = 0; i < 12; i++) {
            dayList1.add(format2LenStr(i + 1));
        }

        custom_picker_day.setDataList((ArrayList) dayList1);
        custom_picker_day.setInitPosition(dayPos1);
    }

    private void initPickerViews2() {
        yearList2.add("00");
        yearList2.add("05");
        yearList2.add("10");
        yearList2.add("15");
        yearList2.add("20");
        yearList2.add("25");
        yearList2.add("30");
        yearList2.add("35");
        yearList2.add("40");
        yearList2.add("45");
        yearList2.add("50");
        yearList2.add("55");

        everyday_picker_year.setDataList((ArrayList) yearList2);
        everyday_picker_year.setInitPosition(yearPos2);

        ampmList2.add("AM");
        ampmList2.add("PM");

        everyday_ampm.setDataList((ArrayList) ampmList2);
        everyday_ampm.setInitPosition(ampmPos2);

        for (int i = 0; i < 12; i++) {
            dayList2.add(format2LenStr(i + 1));
        }

        everyday_picker_day.setDataList((ArrayList) dayList2);
        everyday_picker_day.setInitPosition(dayPos2);
    }

    private void initPickerViews3() {
        yearList3.add("00");
        yearList3.add("05");
        yearList3.add("10");
        yearList3.add("15");
        yearList3.add("20");
        yearList3.add("25");
        yearList3.add("30");
        yearList3.add("35");
        yearList3.add("40");
        yearList3.add("45");
        yearList3.add("50");
        yearList3.add("55");

        everywday_picker_year.setDataList((ArrayList) yearList3);
        everywday_picker_year.setInitPosition(yearPos3);

        ampmList3.add("AM");
        ampmList3.add("PM");

        everywday_am_pm.setDataList((ArrayList) ampmList3);
        everywday_am_pm.setInitPosition(ampmPos3);

        for (int i = 0; i < 12; i++) {
            dayList3.add(format2LenStr(i + 1));
        }

        everywday_picker_day.setDataList((ArrayList) dayList3);
        everywday_picker_day.setInitPosition(dayPos3);
    }

    private void initPickerViews4() {
        yearList4.add("00");
        yearList4.add("05");
        yearList4.add("10");
        yearList4.add("15");
        yearList4.add("20");
        yearList4.add("25");
        yearList4.add("30");
        yearList4.add("35");
        yearList4.add("40");
        yearList4.add("45");
        yearList4.add("50");
        yearList4.add("55");

        everyweek_picker_year.setDataList((ArrayList) yearList4);
        everyweek_picker_year.setInitPosition(yearPos4);

        ampmList4.add("AM");
        ampmList4.add("PM");

        everyweek_picker_am_pm.setDataList((ArrayList) ampmList4);
        everyweek_picker_am_pm.setInitPosition(ampmPos4);

        for (int i = 0; i < 12; i++) {
            dayList4.add(format2LenStr(i + 1));
        }

        everyweek_picker_day.setDataList((ArrayList) dayList4);
        everyweek_picker_day.setInitPosition(dayPos4);
    }

    private void repeatinitPickerViews() {
        int yearCount = maxYear - minYear;

        for (int i = 0; i < yearCount; i++) {
            yearList.add(format2LenStr(minYear + i));
        }

        monthList.add("January");
        monthList.add("February");
        monthList.add("March");
        monthList.add("April");
        monthList.add("May");
        monthList.add("June");
        monthList.add("July");
        monthList.add("August");
        monthList.add("Sepetember");
        monthList.add("October");
        monthList.add("November");
        monthList.add("December");

        repstartendpicker_year.setDataList((ArrayList) yearList);
        repstartendpicker_year.setInitPosition(yearPos);

        repstartendpicker_month.setDataList((ArrayList) monthList);
        repstartendpicker_month.setInitPosition(monthPos);
    }

    /**
     * Init day item
     */
    private void repeatinitDayPickerView() {
        int dayMaxInMonth;
        Calendar calendar = Calendar.getInstance();
        dayList = new ArrayList<String>();

        calendar.set(Calendar.YEAR, minYear + yearPos);
        calendar.set(Calendar.MONTH, monthPos);

        //get max day in month
        dayMaxInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        for (int i = 0; i < dayMaxInMonth; i++) {
            dayList.add(format2LenStr(i + 1));
        }

        repstartendpicker_day.setDataList((ArrayList) dayList);
        repstartendpicker_day.setInitPosition(dayPos);
    }

    public void setSelectedDate(String dateStr) {
        if (!TextUtils.isEmpty(dateStr)) {
            long milliseconds = getLongFromyyyyMMdd(dateStr);
            Calendar calendar = Calendar.getInstance(Locale.getDefault());

            if (milliseconds != -1) {
                calendar.setTimeInMillis(milliseconds);
                yearPos = calendar.get(Calendar.YEAR) - minYear;
                monthPos = calendar.get(Calendar.MONTH);
                dayPos = calendar.get(Calendar.DAY_OF_MONTH) - 1;
            }
        }
    }

    public static long getLongFromyyyyMMdd(String date) {
        SimpleDateFormat mFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date parse = null;
        try {
            parse = mFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (parse != null) {
            return parse.getTime();
        } else {
            return -1;
        }
    }

    public static String format2LenStr(int num) {
        return (num < 10) ? "0" + num : String.valueOf(num);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        int id = v.getId();
        if(id==R.id.everyday_picker_day){


        }else if(id==R.id.everyday_picker_day){

        }

        return false;
    }
}
