package com.horizzon.reminderapp.adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.horizzon.reminderapp.DetailViewActivity;
import com.horizzon.reminderapp.InboxDetailActivity;
import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.ReminderDetailActivity;
import com.horizzon.reminderapp.dao.AttachmentTable;
import com.horizzon.reminderapp.dao.ReminderUser;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.RUserDbHandler;
import com.horizzon.reminderapp.helper.Clickable;
import com.horizzon.reminderapp.utility.DateUtilitys;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by horizzon on 4/1/2017.
 */

public class ImageAttachmentAdapter extends RecyclerView.Adapter<ImageAttachmentAdapter.MyViewHolder> {

    Activity context;
    ArrayList<AttachmentTable> list;
    Clickable object;
    String comefrom, action;
    RUserDbHandler rudbh;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView del_icon, imgView, user_image;
        public TextView imgText;
        public LinearLayout contacInfoLay;

        public MyViewHolder(View view) {
            super(view);
            del_icon = (ImageView) view.findViewById(R.id.del_icon);
            imgView = (ImageView) view.findViewById(R.id.imgView);
            user_image = (ImageView) view.findViewById(R.id.user_image);
            imgText = (TextView) view.findViewById(R.id.imgText);
            contacInfoLay = (LinearLayout) view.findViewById(R.id.contacInfoLay);
        }
    }


    public ImageAttachmentAdapter(Activity context, ArrayList<AttachmentTable> list, Clickable object, String comefrom, String action) {
        this.context = context;
        this.list = list;
        this.object = object;
        this.comefrom = comefrom;
        this.action = action;
        rudbh = new RUserDbHandler(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_grid_images, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final AttachmentTable at = list.get(position);
        Common.DisplayLog("at.getDetail()", at.getDetail());

        if (object != null && action.equals("add")) {
            File imgFile = new File(at.getDetail());
            Common.DisplayLog("imgFile", imgFile + "");
//        holder.imgView.setImageBitmap(myBitmap);
//        Picasso.with(context)
            Glide.with(context)
                    .load(imgFile)
                    .placeholder(R.drawable.defaultimg)
                    .error(R.drawable.defaultimg)
                    .into(holder.imgView);
            holder.del_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ImageView btn = (ImageView) v;
                    int pos = position;
                    object.OnClick(Integer.parseInt(at.getId()));
                }
            });
        } else {
            Picasso.with(context)
                    .load(at.getDetail())
                    .placeholder(R.drawable.defaultimg)
                    .error(R.drawable.defaultimg)
                    .into(holder.imgView);
            holder.del_icon.setVisibility(View.GONE);
            if (action.equals("edit")) {
                holder.del_icon.setVisibility(View.VISIBLE);
                holder.del_icon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        object.OnClick(Integer.parseInt(at.getId()));
                    }
                });
            }

            if (action.equals("edit") && !Common.getSharedPreferences(context, "userId", "0").equals(at.getiUserId())) {
                holder.del_icon.setOnClickListener(null);
                holder.del_icon.setVisibility(View.GONE);
            }


            if (comefrom.equals("InboxDetailActivity")) {
                holder.contacInfoLay.setVisibility(View.VISIBLE);

//                LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,130);
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) holder.imgView.getLayoutParams();
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                holder.imgView.setLayoutParams(params);


                ReminderUser reminderUser = rudbh.getTableSingleDateWihtDao(Common.getSharedPreferences(context, "userId", "0"));

                String imgpath = ((reminderUser.getuPhoto().equals("")) ? "defaultimg" : reminderUser.getuPhoto()) + "";

                Picasso.with(context)
                        .load(imgpath)
                        .placeholder(R.drawable.profilepic)
                        .error(R.drawable.profilepic)
                        .into(holder.user_image);

                String uname = (reminderUser.getuName().equals("")) ? reminderUser.getuNumber() : reminderUser.getuName();
                holder.imgText.setText(uname + " / " + DateUtilitys.timeAgo(at.getCreatedData()));
            } else {
                holder.contacInfoLay.setVisibility(View.GONE);
            }

            holder.imgView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Common.DisplayLog("comefrom : ", comefrom);
                    if (comefrom.equals("ReminderDetailActivity")) {
                        ReminderDetailActivity _ac = (ReminderDetailActivity) context;
                        _ac.getExtendedViewPager(position);
                    }
                    if (comefrom.equals("DetailViewActivity")) {
                        DetailViewActivity _ac = (DetailViewActivity) context;
                        _ac.getExtendedViewPager(_ac, position);
                    }
                    if (comefrom.equals("InboxDetailActivity")) {
                        InboxDetailActivity _ac = (InboxDetailActivity) context;
                        _ac.getExtendedViewPager(position);
                    }
                    if (comefrom.equals("DetailAttachmentTabFragment")) {
//                        DetailViewActivity _ac = (DetailViewActivity) context;
//                        _ac.getExtendedViewPager(_ac, position);
                        object.OnClick(position);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
