package com.horizzon.reminderapp.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import com.google.android.material.snackbar.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.horizzon.reminderapp.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Common {
    public static void ShowToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static void ShowSnackbar(View v, String msg) {
        /*View snackbarView = v;
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setMaxLines(5);*/
        Snackbar.make(v, msg, Snackbar.LENGTH_LONG).show();
    }

    public static void ShowSnackbarWithAction(View rootView, String msg, final View.OnClickListener listener) {

        Snackbar.make(rootView, msg, Snackbar.LENGTH_INDEFINITE)
                .setAction("Reload", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onClick(v);
                    }
                }).show();

    }

    public static void DisplayLog(String tag, String msg) {
        if (ConstantData.log_enable) {
            Log.e(tag, msg);
//            System.out.println(tag+"----"+ msg);
        }
    }

    public static boolean hasInternetConnection(Context context) {
        boolean isConnected = false;
        // if connectivity object is available
        ConnectivityManager con_manager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (con_manager.getActiveNetworkInfo() != null) {
            // if network is available
            if (con_manager.getActiveNetworkInfo().isAvailable()) {
                // if connected
                if (con_manager.getActiveNetworkInfo().isConnected()) {
                    // yep... there is connectivity
                    isConnected = true;
                }
            }
        }
        if(!isConnected)
        {
            noInternetMessage(context);
        }
        return isConnected;
    }

    public static boolean hasInternetConnection(Context context,boolean dosum) {
        boolean isConnected = false;
        // if connectivity object is available
        ConnectivityManager con_manager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (con_manager.getActiveNetworkInfo() != null) {
            // if network is available
            if (con_manager.getActiveNetworkInfo().isAvailable()) {
                // if connected
                if (con_manager.getActiveNetworkInfo().isConnected()) {
                    // yep... there is connectivity
                    isConnected = true;
                }
            }
        }
        if(!isConnected && dosum)
        {
            noInternetMessage(context);
        }
        return isConnected;
    }

    // show something if there is no internet connectivity
    public static void noInternetMessage(Context context) {

        Toast.makeText(context, "Internet connection error. Please try after sometime.",
                Toast.LENGTH_SHORT).show();

        try {
//            MainActivity.networkerrorlayer(context);
        } catch (Exception e) {
        }
        /*AlertDialog.Builder alertbox = new AlertDialog.Builder(context);
        alertbox.setMessage("Internet connection error. Please try after sometime.");
        alertbox.setNeutralButton("Ok", new
                DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
        alertbox.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface arg0) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        alertbox.show();*/
    }

    public static void noInternetMessageSnackbar(View rootView, final View.OnClickListener listener) {

        if (rootView != null) {
            Snackbar.make(rootView, "Internet connection error. Please try after sometime.", Snackbar.LENGTH_LONG)
                    .setAction("Reload", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            listener.onClick(v);
                        }
                    }).show();
        }

    }

    // validation for proper title between a-z and 0-9
    public static boolean Is_Valid_Name(EditText edt)
            throws NumberFormatException {
        if (edt.getText().toString().length() == 0) {
            edt.setError("Field is required.");
            return false;
        } else if (edt.getText().toString().length() <= 0) {
            edt.setError("Field is required.");
            return false;
        } else if (!edt.getText().toString().matches("^[a-zA-Z0-9 ]*$")) {
            edt.setError("Accept Alphabets And Numbers Only.");
            return false;
        } else {
            return true;
        }

    }

    public static boolean Is_Valid_Text(EditText edt)
            throws NumberFormatException {
        if (edt.getText().toString().length() == 0) {
            edt.setError("Field is required.");
            return false;
        } else if (edt.getText().toString().length() <= 0) {
            edt.setError("Field is required.");
            return false;
        } else if (!edt.getText().toString().matches("^[A-Za-z0-9\\s,\\.\\-\\:]*$")) {
            edt.setError("Some special character not allowed.");
            return false;
        } else {
            return true;
        }

    }

    // validation for proper name between a-z
    public static boolean Is_Valid_Person_Name(EditText edt)
            throws NumberFormatException {
        if (edt.getText().toString().length() == 0) {
            edt.setError("Field is required.");
            return false;
        } else if (edt.getText().toString().length() <= 0) {
            edt.setError("Field is required.");
            return false;
        } else if (!edt.getText().toString().matches("[a-zA-Z ]+")) {
            edt.setError("Accept Alphabets Only.");
            return false;
        } else {
            return true;
        }

    }

    // validation for proper number between min-value and max-value
    public static boolean Is_Valid_Sign_Number_Validation(int MinLen,
                                                          int MaxLen, EditText edt) throws NumberFormatException {
        if (edt.getText().toString().length() <= 0) {
            edt.setError("Field is required.");
            return false;
        } else if (!edt.getText().toString().matches("^[0-9]*$")) {
            edt.setError("Accept Number Only.");
            return false;
        } else if (edt.getText().toString().length() < MinLen) {
            edt.setError("Minimum length " + MinLen);
            return false;

        } else if (edt.getText().toString().length() > MaxLen) {
            edt.setError("Maximum length " + MaxLen);
            return false;

        } else {
            return true;

        }

    }

    // validation for proper email
    public static boolean Is_Valid_Only_Email(EditText edt) {
        if (!edt.getText().toString().trim().equalsIgnoreCase("") && !android.util.Patterns.EMAIL_ADDRESS.matcher(
                edt.getText().toString()).matches()) {
            edt.setError("Invalid Email Address");
            return false;
        } else {
            return true;
        }
    }

    public static boolean Is_Valid_Email(EditText edt) {
        if (edt.getText().toString() == null) {
            edt.setError("Invalid Email Address");
            return false;
        } else if (edt.getText().toString().length() <= 0) {
            edt.setError("Invalid Email Address");
            return false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(
                edt.getText().toString()).matches()) {
            edt.setError("Invalid Email Address");
            return false;
        } else {
            return true;
        }
    }


    public static String FormetDate(String DateStr, String formatfrom, String format) throws java.text.ParseException {
        //String startDateStr = "2016/1/10";
        //java.text.SimpleDateFormat ps = new java.text.SimpleDateFormat("yyyy/MM/dd");
        java.text.SimpleDateFormat ps = new java.text.SimpleDateFormat(formatfrom);

        //java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("MM-dd-yyyy EEE");
        java.text.SimpleDateFormat df = new java.text.SimpleDateFormat(format);

        java.util.Date startDate = ps.parse(DateStr);

        //out.println(startDate);
        return df.format(startDate);
    }

    public static String getSQLDateTime() {
        java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        java.util.Date date = new java.util.Date();
        return dateFormat.format(date);
    }

    public static String formatToYesterdayOrToday(String date) throws ParseException {
        Date dateTime = new SimpleDateFormat("EEE hh:mma MMM d, yyyy").parse(date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateTime);
        Calendar today = Calendar.getInstance();
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        DateFormat timeFormatter = new SimpleDateFormat("hh:mma");

        if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
            return "Today " + timeFormatter.format(dateTime);
        } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
            return "Yesterday " + timeFormatter.format(dateTime);
        } else {
            return date;
        }
    }

    public static void createSharedPreferences(Context _c, String key, String val) {
        SharedPreferences GS_preferences = _c.getSharedPreferences(_c.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = GS_preferences.edit();
        editor.putString(key, val);
        editor.commit();
    }

    public static String getSharedPreferences(Context _c, String key, String defaultVal) {
        SharedPreferences sharedPref = _c.getSharedPreferences(_c.getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        return sharedPref.getString(key, defaultVal);
    }
}
