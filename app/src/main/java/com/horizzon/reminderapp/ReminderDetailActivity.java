package com.horizzon.reminderapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.horizzon.reminderapp.adapter.CommentAdapter;
import com.horizzon.reminderapp.adapter.ImageAttachmentAdapter;
import com.horizzon.reminderapp.dao.AttachmentTable;
import com.horizzon.reminderapp.dao.CReminder;
import com.horizzon.reminderapp.dao.ReminderComment;
import com.horizzon.reminderapp.dao.ReminderContactUser;
import com.horizzon.reminderapp.dao.ReminderDateTime;
import com.horizzon.reminderapp.dao.ReminderForList;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.CRAttachmentDbHandler;
import com.horizzon.reminderapp.handler.CRCommentDbHandler;
import com.horizzon.reminderapp.handler.CRContactDbHandler;
import com.horizzon.reminderapp.handler.CRDateTimeDbHandler;
import com.horizzon.reminderapp.handler.CReminderForListDbHandler;
import com.horizzon.reminderapp.handler.CreateReminderDbHandler;
import com.horizzon.reminderapp.helper.ExtendedViewPager;
import com.horizzon.reminderapp.helper.SpaceItemDecoration;
import com.horizzon.reminderapp.helper.TouchImageAdapter;
import com.horizzon.reminderapp.receiver.AlarmBroadCustReciver;
import com.horizzon.reminderapp.retrofit.model.reminder.Reminder;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;
import com.horizzon.reminderapp.utility.DateUtilitys;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.core.content.PermissionChecker.PERMISSION_GRANTED;

public class ReminderDetailActivity extends AppCompatActivity {

    String SessionId, inFrom;
    TextView conName, rSub, rDateTime, rday, commentcount, attachcount;
    ImageView imgBack, imgHome, con_image1, con_image2, con_image3, imgPriority1, imgPriority2, imgPriority3, tabcomment, tabattach, btn_reject, btn_edit, btn_accept;
    RecyclerView commentList;
    ArrayList<ReminderComment> commentArrayList;
    CommentAdapter commentAdapter;
    LinearLayout tabcommentcontent, tabattachcontent;
    ScrollView mainscroll;
    final int callbackId = 42;
    LinearLayout subtaskLayout, maincontentlay, pafeviewerlay, notesLayout, imagesLayout, subtasklistLayout, notlistLayout, contactMainLayout, contactLayout;
    ArrayList<AttachmentTable> imagesList;
    ImageAttachmentAdapter imgAdapter;
    RecyclerView imglistView;
    CRAttachmentDbHandler cradbh;
    ExtendedViewPager miimgpwEViewPager;
    int backstate = 0;
    ProgressDialog pd;
    public static final String NOTIFICATION_CHANNEL_ID = "10001";
    private final static String default_notification_channel_id = "default";
    int mMonth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_detail);

//        SessionId = Common.getSharedPreferences(getApplicationContext(), "SessionId", "");
        checkPermission(callbackId, android.Manifest.permission.READ_CALENDAR, android.Manifest.permission.WRITE_CALENDAR);
        SessionId = getIntent().getStringExtra("SessionId");
        if (savedInstanceState != null && (SessionId == null || SessionId.equals(""))) {
            SessionId = savedInstanceState.getString("SessionId");
        }

        cradbh = new CRAttachmentDbHandler(getApplicationContext());

        inFrom = "done";

        imgBack = (ImageView) findViewById(R.id.backBtn);
        imgHome = (ImageView) findViewById(R.id.homeBtn);
        btn_reject = (ImageView) findViewById(R.id.btn_reject);
        btn_edit = (ImageView) findViewById(R.id.btn_edit);
        btn_accept = (ImageView) findViewById(R.id.btn_accept);


        conName = (TextView) findViewById(R.id.conName);
        rSub = (TextView) findViewById(R.id.rSub);
        rDateTime = (TextView) findViewById(R.id.rDateTime);
        rday = (TextView) findViewById(R.id.rday);
        commentcount = (TextView) findViewById(R.id.commentcount);
        attachcount = (TextView) findViewById(R.id.attachcount);

        con_image1 = (ImageView) findViewById(R.id.con_image1);
        con_image2 = (ImageView) findViewById(R.id.con_image2);
        con_image3 = (ImageView) findViewById(R.id.con_image3);
        imgPriority1 = (ImageView) findViewById(R.id.imgPriority1);
        imgPriority2 = (ImageView) findViewById(R.id.imgPriority2);
        imgPriority3 = (ImageView) findViewById(R.id.imgPriority3);
        tabcomment = (ImageView) findViewById(R.id.tabcomment);
        tabattach = (ImageView) findViewById(R.id.tabattach);

        mainscroll = (ScrollView) findViewById(R.id.mainscroll);
        tabcommentcontent = (LinearLayout) findViewById(R.id.tabcommentcontent);
        tabattachcontent = (LinearLayout) findViewById(R.id.tabattachcontent);

        maincontentlay = (LinearLayout) findViewById(R.id.maincontentlay);
        pafeviewerlay = (LinearLayout) findViewById(R.id.pafeviewerlay);

        pafeviewerlay.setVisibility(View.GONE);
        maincontentlay.setVisibility(View.VISIBLE);

        imgPriority1.setVisibility(View.GONE);
        imgPriority2.setVisibility(View.GONE);
        imgPriority3.setVisibility(View.GONE);

        commentList = (RecyclerView) findViewById(R.id.commentList);


        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(ReminderDetailActivity.this, InboxActivity.class);
                startActivity(in);

            }
        });

        imgHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(ReminderDetailActivity.this, HomeActivity.class);
                startActivity(in);

            }
        });

        tabcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tabcomment.setImageResource(R.drawable.ic_rdetailcommentw);
                tabattach.setImageResource(R.drawable.ic_rdetailattachb);
                tabcommentcontent.setVisibility(View.VISIBLE);
                tabattachcontent.setVisibility(View.GONE);
                getComment(SessionId);
            }
        });
        tabattach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tabcomment.setImageResource(R.drawable.ic_rdetailcommentb);
                tabattach.setImageResource(R.drawable.ic_rdetailattachw);
                tabcommentcontent.setVisibility(View.GONE);
                tabattachcontent.setVisibility(View.VISIBLE);
                getAttachData(SessionId);
            }
        });
        btn_reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateReminderStatus(SessionId, "reject");
            }
        });
        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ReminderDetailActivity.this, EditReminderActivity.class);
                in.putExtra("SessionId", SessionId);
                startActivity(in);
            }
        });
        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                updateReminderStatus(SessionId, "accept");
            }
        });

        subtasklistLayout = (LinearLayout) findViewById(R.id.subtasklistLayout);
        notlistLayout = (LinearLayout) findViewById(R.id.notlistLayout);
        contactMainLayout = (LinearLayout) findViewById(R.id.contactMainLayout);
        contactLayout = (LinearLayout) findViewById(R.id.contactLayout);

        subtaskLayout = (LinearLayout) findViewById(R.id.subtaskLayout);
        notesLayout = (LinearLayout) findViewById(R.id.notesLayout);
        imagesLayout = (LinearLayout) findViewById(R.id.imagesLayout);

        imglistView = (RecyclerView) findViewById(R.id.imglistView);


    }

    private void checkPermission(int callbackId, String... permissionsId) {
        boolean permissions = true;
        for (String p : permissionsId) {
            permissions = permissions && ContextCompat.checkSelfPermission(this, p) == PERMISSION_GRANTED;
        }

        if (!permissions)
            ActivityCompat.requestPermissions(this, permissionsId, callbackId);
    }

    public void dataonNotifaction(String SessId) {

        /*SessionIDModel sessionIDModel = new SessionIDModel(getDateTimeList.getIId(), getDateTimeList.getVSesssionId());
        sessionIDModel.setId(getDateTimeList.getIId());
        sessionIDModel.setSesssionId(getDateTimeList.getVSesssionId());
        sessionIDList.add(sessionIDModel);
        setList(sessionIDList);*/

        CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getApplicationContext());
        CReminder cReminder = crdbh.getTableSingleDateWihtDao(SessId);

        CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(getApplicationContext());
        ReminderDateTime rdt = crdtdbh.getTableSingleDateWihtDao(SessId);
        Common.DisplayLog("crdtdbh.getDataCount(SessionId)", crdtdbh.getDataCount(SessId) + "");
        DateUtilitys dateUtl = new DateUtilitys();
        Date date;
        if (crdtdbh.getDataCount(SessId) > 0) {
            date = dateUtl.FormetDate(rdt.getStartDate() + " " + rdt.getStartTime());
        }
        mMonth = Integer.parseInt(dateUtl.getMonth());
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, --mMonth);
        cal.set(Calendar.YEAR, Integer.parseInt(dateUtl.getYear()));
        cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateUtl.getDay()));
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(dateUtl.getHour()));
        cal.set(Calendar.MINUTE, Integer.parseInt(dateUtl.getMin()));
        cal.set(Calendar.SECOND, 0);
        Calendar c = Calendar.getInstance();
        if (cal.compareTo(c) <= 0) {
            // Today Set time passed, count to tomorrow
            cal.add(Calendar.DATE, 1);
        }
        long currentTime = c.getTimeInMillis();
        long diffTime = cal.getTimeInMillis() - currentTime;

        SharedPreferences sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putLong("time", diffTime);
        Log.d("TAG", "onResponse:SessId :: " + diffTime);
        editor.commit();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("SessionId", getIntent().getStringExtra("SessionId"));
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadSavedData(SessionId);
        getComment(SessionId);
        getAttachData(SessionId);
    }

    @Override
    public void onBackPressed() {

        if (backstate > 0) {
            pafeviewerlay.setVisibility(View.GONE);
            maincontentlay.setVisibility(View.VISIBLE);
            backstate = 0;
        } else {
            super.onBackPressed();
        }
    }

    public void updateReminderStatus(final String SessId, String status) {

        /*** update Reminder Status ****/
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        CReminderForListDbHandler crfldbh = new CReminderForListDbHandler(getApplicationContext());

        ReminderForList rfl = crfldbh.getTableSingleDateWihtDao(SessId, Common.getSharedPreferences(getApplicationContext(), "userId", "0"));

        if (Common.hasInternetConnection(getApplicationContext())) {
            if (rfl != null) {

                crfldbh.updataReminderStatus(rfl.getiUserId(), rfl.getvSesssionId(), status);
                pd = new ProgressDialog(ReminderDetailActivity.this);
                pd.setMessage("Loading...");
                pd.setIndeterminate(true);
                pd.setCancelable(false);
                pd.show();
                Call<Reminder> callDateTime = apiService.updateReminderStatus(rfl.getiUserId(), rfl.getvSesssionId(), status);
                callDateTime.enqueue(new Callback<Reminder>() {
                    @Override
                    public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                        int statusCode = response.code();
                        /*SharedPreferences sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("SessId", SessId);
                        Log.d("TAG", "onResponse:SessId :: " + SessId);
                        editor.commit();*/
                        Common.DisplayLog("response.body()", response.body().toString());
                        boolean status = response.body().getStatus();
                        Common.DisplayLog("status", status + "");
                        Common.DisplayLog("getMessage", response.body().getMessage() + "");

                        if (status) {
                            //dataonNotifaction(SessId);
                        } else {
                            Common.ShowToast(getApplicationContext(), response.body().getMessage());

                        }
                        pd.dismiss();
                        imgBack.performClick();

                    }

                    @Override
                    public void onFailure(Call<Reminder> call, Throwable t) {

                        Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
                        pd.dismiss();
                        imgBack.performClick();
                    }


                });
            } else {
                Log.d("TAG", "updateReminderStatus: ");
            }
        } else {
            Log.d("TAG", "updateReminderStatus: ");
        }
    }

    public void loadSavedData(String SessId) {


        Common.DisplayLog("", "loadSavedData " + SessId);
        CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getApplicationContext());
//        RContactDbHandler rcdbh = new RContactDbHandler(getApplicationContext());


        CReminder cReminder = crdbh.getTableSingleDateWihtDao(SessId);
        if (cReminder != null) {
            Log.d("TAG", "loadSavedData: reminderDetails :" + cReminder);
            Common.DisplayLog("cReminder.getSubject()", cReminder.getSubject() + "");
            // set Subject
            rSub.setText(cReminder.getSubject());
            Log.d("TAG", "loadSavedData: subj :" + cReminder.getSubject());
            // set Priority
            if (cReminder.getPriority().equals("1")) {
                imgPriority1.setVisibility(View.VISIBLE);
            } else if (cReminder.getPriority().equals("2")) {
                imgPriority1.setVisibility(View.VISIBLE);
                imgPriority2.setVisibility(View.VISIBLE);
            } else if (cReminder.getPriority().equals("3")) {
                imgPriority1.setVisibility(View.VISIBLE);
                imgPriority2.setVisibility(View.VISIBLE);
                imgPriority3.setVisibility(View.VISIBLE);
            }

            // set Contact
            CRContactDbHandler crcdbh = new CRContactDbHandler(getApplicationContext());
            ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllCurrentSessContactData(SessId, "contact");
//        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllContactData();

            Common.DisplayLog("remindercontactuser", remindercontactuser.size() + "");
            String tempToText = "";
            int i = 0;
            for (i = 0; i < remindercontactuser.size(); i++) {

                LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = vi.inflate(R.layout.fgcontact_view, null);

                if (i < 1) {
                    tempToText += remindercontactuser.get(i).getName() + ",";
                }

                TextView textView = (TextView) v.findViewById(R.id.conName);
                textView.setText(remindercontactuser.get(i).getName());

                ImageView imageView = (ImageView) v.findViewById(R.id.con_image);


                String imgpath = ((remindercontactuser.get(i).getPhoto().equals("")) ? "defaultimg" : remindercontactuser.get(i).getPhoto()) + "";
                if (i == 0 && !imgpath.equals("defaultimg")) {
                    Picasso.with(getApplicationContext())
                            .load(imgpath)
                            .placeholder(R.drawable.defaultusericon)
                            .error(R.drawable.defaultusericon)
                            .into(con_image1);
                }/*else{
                con_image1.setVisibility(View.GONE);
            }*/
                if (i == 1 && !imgpath.equals("defaultimg")) {
                    Picasso.with(getApplicationContext())
                            .load(imgpath)
                            .placeholder(R.drawable.defaultusericon)
                            .error(R.drawable.defaultusericon)
                            .into(con_image2);
                }/*else{
                con_image2.setVisibility(View.GONE);
            }*/
                if (i == 2 && !imgpath.equals("defaultimg")) {
                    Picasso.with(getApplicationContext())
                            .load(imgpath)
                            .placeholder(R.drawable.defaultusericon)
                            .error(R.drawable.defaultusericon)
                            .into(con_image3);
                }/*else{
                con_image3.setVisibility(View.GONE);
            }*/
            }

            tempToText = (tempToText != null && !tempToText.equals("")) ? tempToText.substring(0, tempToText.length() - 1) : "";
            if (i > 2) {
                tempToText = tempToText.substring(6) + " & " + (i - 1) + " others";
            }

            conName.setText(tempToText);

            // set Attachment
            CRAttachmentDbHandler cradbh = new CRAttachmentDbHandler(getApplicationContext());
            Common.DisplayLog("cradbh.getDataCount(SessId)", cradbh.getDataCount(SessId, "done") + "");
            attachcount.setText(cradbh.getDataCount(SessId, "done") + "");

            // set Comment

            CRCommentDbHandler crcodbh = new CRCommentDbHandler(getApplicationContext());
            Common.DisplayLog("crcodbh.getDataCount(SessionId)", crcodbh.getDataCount(SessId) + "");
            commentcount.setText(crcodbh.getDataCount(SessId) + "");
            if (crcodbh.getDataCount(SessId) > 0) {
                tabcomment.performClick();
            }

            // set Date Time
            CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(getApplicationContext());
            ReminderDateTime rdt = crdtdbh.getTableSingleDateWihtDao(SessId);
            Common.DisplayLog("crdtdbh.getDataCount(SessionId)", crdtdbh.getDataCount(SessId) + "");


            DateUtilitys dateUtl = new DateUtilitys();

            Date date;
            if (crdtdbh.getDataCount(SessId) > 0) {
                date = dateUtl.FormetDate(rdt.getStartDate() + " " + rdt.getStartTime());
                Log.d("TAG", "loadSavedData: date :" + rdt.getStartDate());
                Log.d("TAG", "loadSavedData: time : " + rdt.getStartTime());
            } else {
                Common.DisplayLog("cr.getCreateddate()", cReminder.getCreateddate() + "");
                date = dateUtl.FormetDate(cReminder.getCreateddate());

            }
            dateUtl.setforDisplayTitle(date);
            rDateTime.setText(dateUtl.getMonth() + " " + dateUtl.getDay() + " " + dateUtl.getYear() + ", " + dateUtl.getHour() + ":" + dateUtl.getMin() + " " + dateUtl.getAmpm());
            rday.setText(dateUtl.getFullweekDay());

            CReminderForListDbHandler crfldbh = new CReminderForListDbHandler(getApplicationContext());
            ReminderForList rfl = crfldbh.getTableSingleDateWihtDao(SessId, Common.getSharedPreferences(getApplicationContext(), "userId", "0"));
            if (rfl != null) {
                if (rfl.getvReminderStatus().equals("accept") || rfl.getvReminderStatus().equals("reject")) {
                    btn_reject.setVisibility(View.GONE);
                    btn_accept.setVisibility(View.GONE);
                    btn_edit.setVisibility(View.GONE);
                }
            }
        }

    }

    private void getComment(String SessId) {

        CRCommentDbHandler crcdbh = new CRCommentDbHandler(getApplicationContext());
        commentArrayList = crcdbh.getAllCurrentSessDataWithType(SessId);
        Common.DisplayLog("commentArrayList count", commentArrayList.size() + "");

        commentList.setAdapter(null);
//                        commentAdapter = new ImageGridCustomAdapter(AttachmentActivity.this, commentArrayList, AttachmentActivity.this);
        commentAdapter = new CommentAdapter(ReminderDetailActivity.this, commentArrayList, "view");
        LinearLayoutManager lLayout = new LinearLayoutManager(getApplicationContext());
//                        lLayout.setStackFromEnd(true);
//                        lLayout.setReverseLayout(true);
        commentList.setLayoutManager(lLayout);
        commentList.setAdapter(commentAdapter);

//        commentAdapter.notifyDataSetChanged();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        commentList.scrollToPosition(commentArrayList.size() - 1);

                    }
                },
                200);

    }

    public void getAttachData(String SessId) {


        Common.DisplayLog("", "loadSavedData " + SessId);

        //load Subtask

        subtasklistLayout.removeAllViews();
        ArrayList<AttachmentTable> lodsubt = cradbh.getAllCurrentSessDataWithType(SessId, "subtask", inFrom);

        if (lodsubt.size() > 0) {
            subtaskLayout.setVisibility(View.VISIBLE);
            for (int i = 0; i < lodsubt.size(); i++) {
                subtasklistLayout.addView(createEditView(lodsubt.get(i).getAttachFor(), lodsubt.get(i).getId()));

            }

        }

        //load nots
        notlistLayout.removeAllViews();
        ArrayList<AttachmentTable> lodnote = cradbh.getAllCurrentSessDataWithType(SessId, "note", inFrom);
        if (lodnote.size() > 0) {
            notesLayout.setVisibility(View.VISIBLE);

            Common.DisplayLog("", "imgNoteEdit clicked");
            notlistLayout.addView(createEditView(lodnote.get(0).getAttachFor(), lodnote.get(0).getId()));
        }

        //load image layout
        getAttachImages();


        //load Contact
        getAttachContact();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        mainscroll.requestFocus();

                    }
                },
                500);

    }

    private View createEditView(final String attfor, String preId) {
//        mContainerView = (LinearLayout)findViewById(R.id.parentView);
        Common.DisplayLog("preId ", preId);

        final String lastid = preId;

        final AttachmentTable at = cradbh.getTableSingleDateWihtDao(SessionId, lastid);
        Common.DisplayLog("lastid ", lastid + "");
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View myView = inflater.inflate(R.layout.attachment_edit_row, null);
        myView.setTag(lastid);
        final CheckBox attselchk = (CheckBox) myView.findViewById(R.id.attselchk);


        attselchk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                attselchk.setChecked(false);
            }
        });
        attselchk.setChecked(false);
        if (attfor.equals("note")) {
            attselchk.setVisibility(View.GONE);
        }

        final LinearLayout editcontanor = (LinearLayout) myView.findViewById(R.id.editcontanor);
        final TextView texv = (TextView) myView.findViewById(R.id.textChange);
        String st = at.getDetail().replace("\\n", "\n").replace("\n", "\r\n");
//        texv.setVisibility(View.GONE);

        texv.setText(st);

//        ed.setText(preId);
        final Button textEdit = (Button) myView.findViewById(R.id.textEdit);
        final Button textDelete = (Button) myView.findViewById(R.id.textDelete);

        textEdit.setVisibility(View.GONE);
        textDelete.setVisibility(View.GONE);

        if (preId.equals("")) {
//            textEdit.performClick();
        } else if (attfor.equals("note")) {

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {

                            final EditText texv1 = new EditText(getApplicationContext()); // Pass it an Activity or Context
                            texv1.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                            texv1.setGravity(Gravity.CENTER | Gravity.LEFT);
                            texv1.setBackgroundResource(0);
                            final float scale = getResources().getDisplayMetrics().density;
                            int lr = (int) (26 * scale + 0.5f);
                            int tb = (int) (10 * scale + 0.5f);
                            texv1.setPadding(lr, tb, lr, tb);
                            texv1.setTextColor(Color.parseColor("#adadad"));
                            texv1.setHintTextColor(Color.parseColor("#adadad"));
                            texv1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                            editcontanor.removeAllViews();
                            editcontanor.addView(texv1);
                            texv1.setText(at.getDetail());
                            Common.DisplayLog("texv1", texv1.getText().toString());
                            /*text text*/

                            String edst = texv1.getText().toString().trim();
                            final TextView texv2 = new TextView(getApplicationContext()); // Pass it an Activity or Context
                            texv2.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                            texv2.setGravity(Gravity.CENTER | Gravity.LEFT);
                            texv2.setBackgroundResource(0);
                            texv2.setPadding(lr, tb, lr, tb);
                            texv2.setTextColor(Color.parseColor("#adadad"));
                            texv2.setHintTextColor(Color.parseColor("#adadad"));
                            texv2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                            editcontanor.removeAllViews();
                            editcontanor.addView(texv2);
                            texv2.setText(edst);

                        }
                    },
                    1000);
        }

        return myView;
    }

    private void getAttachImages() {

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        imagesList = cradbh.getAllCurrentSessDataWithType(SessionId, "image", inFrom);
                        Common.DisplayLog("imagesList count", imagesList.size() + "");
                        if (imagesList.size() > 0) {
                            imagesLayout.setVisibility(View.VISIBLE);
                        } else {
                            imagesLayout.setVisibility(View.GONE);
                        }
                        imglistView.setAdapter(null);
                        imgAdapter = new ImageAttachmentAdapter(ReminderDetailActivity.this, imagesList, null, "ReminderDetailActivity", "detail");
                        LinearLayoutManager lLayout = new LinearLayoutManager(ReminderDetailActivity.this, LinearLayoutManager.HORIZONTAL, false);
                        imglistView.addItemDecoration(new SpaceItemDecoration(0, 5, 0, 0));
                        imglistView.setHasFixedSize(true);
                        imglistView.setLayoutManager(lLayout);
                        imglistView.setAdapter(imgAdapter);

                        String[] imgLists = new String[imagesList.size() + 1];
                        for (int i = 0; i < imagesList.size(); i++) {
                            imgLists[i] = imagesList.get(i).getDetail();
                        }

                        miimgpwEViewPager = (ExtendedViewPager) findViewById(R.id.miimgpwEViewPager);
                        miimgpwEViewPager.setAdapter(new TouchImageAdapter(ReminderDetailActivity.this, imgLists, 0));

                    }
                },
                200);

    }

    public void getExtendedViewPager(int pos) {
        ArrayList<AttachmentTable> imagesLists = cradbh.getAllCurrentSessDataWithType(SessionId, "image", inFrom);
        String[] imgLists = new String[imagesLists.size()];
        for (int i = 0; i < imagesLists.size(); i++) {
            imgLists[i] = imagesLists.get(i).getDetail();
        }

        miimgpwEViewPager = (ExtendedViewPager) findViewById(R.id.miimgpwEViewPager);
        miimgpwEViewPager.setAdapter(new TouchImageAdapter(ReminderDetailActivity.this, imgLists, 0));
        miimgpwEViewPager.setCurrentItem(pos);
        maincontentlay.setVisibility(View.GONE);
        pafeviewerlay.setVisibility(View.VISIBLE);
        backstate = 1;
    }

    private void getAttachContact() {
        contactMainLayout.setVisibility(View.GONE);

        contactLayout.removeAllViews();
        final CRContactDbHandler crcdbh = new CRContactDbHandler(getApplicationContext());
        ArrayList<AttachmentTable> remindercontactuser = cradbh.getAllCurrentSessDataWithType(SessionId, "contact", inFrom);


        Common.DisplayLog("remindercontactuser", remindercontactuser.size() + "");
        if (remindercontactuser.size() > 0) {
            contactMainLayout.setVisibility(View.VISIBLE);
            int i = 0;
            for (i = 0; i < remindercontactuser.size(); i++) {

                final AttachmentTable rcu = remindercontactuser.get(i);


                LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = vi.inflate(R.layout.attachcontact_view, null);

                TextView conName = (TextView) v.findViewById(R.id.conName);

                ImageView imageView = (ImageView) v.findViewById(R.id.con_image);
                LinearLayout btncall = (LinearLayout) v.findViewById(R.id.btncall);
                LinearLayout btntask = (LinearLayout) v.findViewById(R.id.btntask);
                LinearLayout btnsave = (LinearLayout) v.findViewById(R.id.btnsave);
                Button conDelete = (Button) v.findViewById(R.id.conDelete);
                conDelete.setVisibility(View.GONE);

                try {
                    final JSONObject json = new JSONObject(rcu.getDetail());
                    Common.DisplayLog("json.toString()", json.toString() + "");
                    conName.setText(json.getString("name").toString());
//                    String imgpath = getRealPathFromURI(Uri.parse(((json.getString("photo").toString().equals("")) ? "defaultimg" : json.getString("photo").toString()) + ""));
                    String imgpath = ((json.getString("photo").toString().equals("")) ? "defaultimg" : json.getString("photo").toString()) + "";
                    final String number = json.getString("number").toString();
                    Common.DisplayLog("imgpath", imgpath + "");
                    Common.DisplayLog("json.getString('photo').toString()", json.getString("photo").toString() + "");
                    Picasso.with(getApplicationContext())
                            .load(imgpath)
                            .placeholder(R.drawable.defaultusericon)
                            .error(R.drawable.defaultusericon)
                            .into(imageView);

                    btncall.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            try {
                                intent.setData(Uri.parse("tel:" + json.getString("number").toString()));
                                startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    btntask.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                            try {
                                sendIntent.setData(Uri.parse("sms:" + json.getString("number").toString()));
                                startActivity(sendIntent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    btnsave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                            try {
                                Intent intent = new Intent(Intent.ACTION_INSERT);
                                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                                intent.putExtra(ContactsContract.Intents.Insert.NAME, json.getString("name").toString());
                                intent.putExtra(ContactsContract.Intents.Insert.PHONE, json.getString("number").toString());
                                int PICK_CONTACT = 100;
                                startActivityForResult(intent, PICK_CONTACT);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    });

                    contactLayout.addView(v);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }

    }

    public void setBrodCastForReminder(String SessId) {


        Common.DisplayLog("", "loadSavedData " + SessId);
        CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getApplicationContext());
//        RContactDbHandler rcdbh = new RContactDbHandler(getApplicationContext());


        CReminder cReminder = crdbh.getTableSingleDateWihtDao(SessId);


        Common.DisplayLog("cReminder.getSubject()", cReminder.getSubject() + "");
        // set Subject
        rSub.setText(cReminder.getSubject());

        // set Priority
        if (cReminder.getPriority().equals("1")) {
            imgPriority1.setVisibility(View.VISIBLE);
        } else if (cReminder.getPriority().equals("2")) {
            imgPriority1.setVisibility(View.VISIBLE);
            imgPriority2.setVisibility(View.VISIBLE);
        } else if (cReminder.getPriority().equals("3")) {
            imgPriority1.setVisibility(View.VISIBLE);
            imgPriority2.setVisibility(View.VISIBLE);
            imgPriority3.setVisibility(View.VISIBLE);
        }


        // set Contact
        CRContactDbHandler crcdbh = new CRContactDbHandler(getApplicationContext());
        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllCurrentSessContactData(SessId, "contact");
//        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllContactData();

        Common.DisplayLog("remindercontactuser", remindercontactuser.size() + "");


        // set Attachment
        CRAttachmentDbHandler cradbh = new CRAttachmentDbHandler(getApplicationContext());
        Common.DisplayLog("cradbh.getDataCount(SessId)", cradbh.getDataCount(SessId, "done") + "");

        // set Comment

        CRCommentDbHandler crcodbh = new CRCommentDbHandler(getApplicationContext());
        Common.DisplayLog("crcodbh.getDataCount(SessionId)", crcodbh.getDataCount(SessId) + "");

        // set Date Time
        CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(getApplicationContext());
        ReminderDateTime rdt = crdtdbh.getTableSingleDateWihtDao(SessId);
        Common.DisplayLog("crdtdbh.getDataCount(SessionId)", crdtdbh.getDataCount(SessId) + "");


        DateUtilitys dateUtl = new DateUtilitys();

        Date date;
        if (crdtdbh.getDataCount(SessId) > 0) {
            date = dateUtl.FormetDate(rdt.getStartDate() + " " + rdt.getStartTime());
        } else {
            Common.DisplayLog("cr.getCreateddate()", cReminder.getCreateddate() + "");
            date = dateUtl.FormetDate(cReminder.getCreateddate());

        }
        dateUtl.setforDisplayTitle(date);
        rDateTime.setText(dateUtl.getMonth() + " " + dateUtl.getDay() + " " + dateUtl.getYear() + ", " + dateUtl.getHour() + ":" + dateUtl.getMin() + " " + dateUtl.getAmpm());

        Log.d("TAG", "setBrodCastForReminder: " + dateUtl.getMonth() + " " + dateUtl.getDay() + " " + dateUtl.getYear() + ", " + dateUtl.getHour() + ":" + dateUtl.getMin() + " " + dateUtl.getAmpm());
        rday.setText(dateUtl.getFullweekDay());

        CReminderForListDbHandler crfldbh = new CReminderForListDbHandler(getApplicationContext());
        ReminderForList rfl = crfldbh.getTableSingleDateWihtDao(SessId, Common.getSharedPreferences(getApplicationContext(), "userId", "0"));
        if (rfl != null) {
            if (rfl.getvReminderStatus().equals("accept")) {


                /***** SET BROADCAST FOR REMINDER ******/

                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.MONTH, 12);
                cal.set(Calendar.YEAR, Integer.parseInt(dateUtl.getYear()));
                cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateUtl.getDay()));
                cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(dateUtl.getHour()));
                cal.set(Calendar.MINUTE, Integer.parseInt(dateUtl.getMin()));

                Common.DisplayLog("timeStamp", "Month: " + dateUtl.getMonth()
                        + " Year: " + dateUtl.getYear()
                        + " Day: " + dateUtl.getDay()
                        + " Hour: " + dateUtl.getHour()
                        + " Year: " + dateUtl.getYear());

                String timeStamp = new SimpleDateFormat("ddMMHHmmss").format(new Date());
                Common.DisplayLog("timeStamp", ": " + timeStamp);
                int requestCode = Integer.parseInt(timeStamp);

                Intent myIntent1 = new Intent(ReminderDetailActivity.this, AlarmBroadCustReciver.class);
                myIntent1.putExtra("SesssionId", cReminder.getSesssionId());
                myIntent1.putExtra("Subject", cReminder.getSubject());
                PendingIntent pendingIntent1 = PendingIntent.getBroadcast(ReminderDetailActivity.this, requestCode, myIntent1,
                        PendingIntent.FLAG_UPDATE_CURRENT);

                AlarmManager alarmManager1 = (AlarmManager) getSystemService(ALARM_SERVICE);
                // alarmManager1.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent1);
                if (Build.VERSION.SDK_INT < 19) {
                    alarmManager1.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent1);
                } else {
                    alarmManager1.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent1);
                }

            }
        }

    }


}
