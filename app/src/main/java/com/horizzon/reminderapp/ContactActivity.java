package com.horizzon.reminderapp;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;

import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AlphabetIndexer;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.horizzon.reminderapp.adapter.ContactListAdapter;
import com.horizzon.reminderapp.adapter.ContactNumListAdapter;
import com.horizzon.reminderapp.dao.CReminder;
import com.horizzon.reminderapp.dao.ContactUser;
import com.horizzon.reminderapp.dao.ReminderContactUser;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.CRAttachmentDbHandler;
import com.horizzon.reminderapp.handler.CRContactDbHandler;
import com.horizzon.reminderapp.handler.CreateReminderDbHandler;
import com.horizzon.reminderapp.handler.RContactDbHandler;
import com.horizzon.reminderapp.helper.Glossary;
import com.horizzon.reminderapp.model.OtherUserList;
import com.horizzon.reminderapp.retrofit.model.reminder.Reminder;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactActivity extends AppCompatActivity {
    protected static final String TAG = "MainActivity";
    private LinearLayout mIndexerLayout;
    private ListView mListView;
    private FrameLayout mTitleLayout;
    private TextView mTitleText;
    private RelativeLayout mSectionToastLayout;
    private TextView mSectionToastText;
    private ArrayList<Glossary> glossaries = new ArrayList<Glossary>();
    private String alphabet = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private AlphabetIndexer mIndexer;

    private int lastSelectedPosition = -1;
    Button btnDone, allcont, alluser;
    GradientDrawable gd;
    EditText edtSearch;
    ImageView imgBack, imgHome;

    private static final int REQUEST_CODE_PICK_CONTACTS = 1;
    private Uri uriContact;
    private String contactID;
    RContactDbHandler rcdbh;
    CRContactDbHandler crcdbh;
    ArrayList<ContactUser> contactuser;
    ContactNumListAdapter cnAdapter;
    private ContactListAdapter mAdapter;
    private String cuncar = "0";
    int moveto = 1;
    String consel = "contact";
    String SessionId, action;
    boolean curectTab = false;
    LinearLayout selcontmainlayout, selcontlayout;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        if (getIntent().getStringExtra("moveto") != null) {
            moveto = Integer.parseInt(getIntent().getStringExtra("moveto"));
            if (moveto == 1) {
                consel = "contact";
            } else {
                consel = "attachment";
            }
        }


        action = getIntent().getStringExtra("action");
        if (action == null || action.equals("")) {
            action = "";
        }


        SessionId = Common.getSharedPreferences(getApplicationContext(), "SessionId", "");
        rcdbh = new RContactDbHandler(getApplicationContext());
        crcdbh = new CRContactDbHandler(getApplicationContext());

        imgBack = (ImageView) findViewById(R.id.backBtn);
        imgHome = (ImageView) findViewById(R.id.homeBtn);

        btnDone = (Button) findViewById(R.id.btnDone);
        allcont = (Button) findViewById(R.id.allcont);
        alluser = (Button) findViewById(R.id.alluser);
        edtSearch = (EditText) findViewById(R.id.edtSearch);


//        retrieveFullContactDetail();
        createContactView();
//        initView();

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onBackPressed();
                if (action.equals("edit")) {
                    onBackPressed();
                } else {
                    gobacktocr(moveto);
                }

            }
        });

        imgHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ContactActivity.this, HomeActivity.class);
                startActivity(in);
            }
        });

        allcont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createContactView();
                allcont.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_send));
                allcont.setTextColor(Color.parseColor("#ffffff"));

                alluser.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_device));
                alluser.setTextColor(Color.parseColor("#1ba1e2"));
                curectTab = false;
                cnAdapter.filterContactUser(false);
                cnAdapter.filter(edtSearch.getText().toString(), false);

            }
        });

        alluser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                allcont.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_send_white));
                allcont.setTextColor(Color.parseColor("#1ba1e2"));

                alluser.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_device_blue));
                alluser.setTextColor(Color.parseColor("#ffffff"));
                curectTab = true;
                getUserList();

            }
        });


        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                crcdbh.updataConSel(SessionId, consel);
                if (consel.equals("attachment")) {
                    CRAttachmentDbHandler cradbh = new CRAttachmentDbHandler(getApplicationContext());
//                    cradbh.resetTempdata(SessionId, consel);
                    cradbh.resetTempdata(SessionId, "contact");

                    ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllCurrentSessContactData(SessionId, "attachment");

                    Common.DisplayLog("remindercontactuser", remindercontactuser.size() + "");

                    int i = 0;
                    for (i = 0; i < remindercontactuser.size(); i++) {

                        final ReminderContactUser rcu = remindercontactuser.get(i);
                        JSONObject json = new JSONObject();
                        try {
                            json.put("name", rcu.getName().toString() + "");
                            json.put("number", rcu.getNumber().toString() + "");
                            Common.DisplayLog("rcu.getPhoto().toString()", ((rcu.getPhoto().toString() == null) ? "null" : "") + "");
                            json.put("photo", rcu.getPhoto().toString() + "");
//                            json.put("photo", "");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Common.DisplayLog("json.toString()", json.toString() + "");
                        String[] COLUMN = {
                                "",
                                SessionId,
                                Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                                "contact",
                                json.toString(),
                                "false",
                                Common.getSQLDateTime()
                        };

                        String selId = String.valueOf(cradbh.addData(COLUMN));
                        Common.DisplayLog("selId", selId + "");
                    }

                }

                if (action.equals("edit")) {
                    SavedEditDataOnServer(SessionId);
                    onBackPressed();
                } else {

                    gobacktocr(moveto);
                }
            }
        });

    }

    public void getUserList() {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<OtherUserList> call = apiService.otherUserList(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));
        call.enqueue(new Callback<OtherUserList>() {
            @Override
            public void onResponse(Call<OtherUserList> call, Response<OtherUserList> response) {
                if (response.body() != null) {
                    OtherUserList otherUserList = response.body();
                    mListView = (ListView) findViewById(R.id.contacts_list);
                    mAdapter = new ContactListAdapter(otherUserList.getData(),getApplicationContext(),ContactActivity.this,"contact" );
                    mListView.setAdapter(mAdapter);
                    mListView.setOnScrollListener(mOnScrollListener);
                    mIndexerLayout.setOnTouchListener(mOnTouchListener);
                    mAdapter.filterContactUser(false);
                    mAdapter.filter(edtSearch.getText().toString(), true);

                    Cursor cursor = rcdbh.getAllContactCursor();
                    mIndexer = new AlphabetIndexer(cursor, 1, alphabet);
                    startManagingCursor(cursor);
                    mAdapter.setIndexer(mIndexer);

                    cursor.close();
                   // mAdapter.setIndexer(mIndexer);
                    /*mIndexer = new AlphabetIndexer(cursor, 1, alphabet);
                    mAdapter.setIndexer(mIndexer);*/
                }
            }

            @Override
            public void onFailure(Call<OtherUserList> call, Throwable t) {

                Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
//                    pd.dismiss();

            }
        });
    }

    public void setselconlay() {
//        Common.ShowToast(getApplicationContext(), "Cteate selected view");
        selcontmainlayout = (LinearLayout) findViewById(R.id.selcontmainlayout);
        selcontlayout = (LinearLayout) findViewById(R.id.selcontlayout);
        selcontmainlayout.setVisibility(View.GONE);
        selcontlayout.removeAllViews();
        // set Contact
        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllselContactData(SessionId, consel);
//        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllContactData();

        Common.DisplayLog("remindercontactuser", remindercontactuser.size() + "");
        String tempToText = "";
        int i = 0;
        for (i = 0; i < remindercontactuser.size(); i++) {


            selcontmainlayout.setVisibility(View.VISIBLE);

            LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = vi.inflate(R.layout.selectedcontact_view, null);

            if (i < 2) {
                tempToText += remindercontactuser.get(i).getName() + ",";
            }

            Button conremove = (Button) v.findViewById(R.id.conremove);
            TextView textView = (TextView) v.findViewById(R.id.conName);
            textView.setText(remindercontactuser.get(i).getName());

            ImageView imageView = (ImageView) v.findViewById(R.id.con_image);

            String imgpath = ((remindercontactuser.get(i).getPhoto().equals("")) ? "defaultimg" : remindercontactuser.get(i).getPhoto()) + "";

            Picasso.with(getApplicationContext())
                    .load(imgpath)
                    .placeholder(R.drawable.defaultusericon)
                    .error(R.drawable.defaultusericon)
                    .into(imageView);
            final String number = remindercontactuser.get(i).getNumber();
            conremove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cnAdapter.removeselnumber(number);
                    mAdapter.removeselnumber(number);
                    setselconlay();
                }
            });
            selcontlayout.addView(v);
        }

    }

    private void gobacktocr(int movefrom) {
        Intent in = null;
        if (movefrom == 1) {
            in = new Intent(getApplicationContext(), CreateReminderActivity.class);
            in.putExtra("inFrom", "cont");
        } else {
            in = new Intent(getApplicationContext(), AttachmentActivity.class);
        }
        in.putExtra("newrem", "no");
        startActivity(in);

    }

    private String getSortKey(String sortKeyString) {
        String key = sortKeyString.substring(0, 1).toUpperCase();
        if (key.matches("[A-Z]")) {
            return key;
        }
        return "#";
    }

    private AbsListView.OnScrollListener mOnScrollListener = new AbsListView.OnScrollListener() {

        private int lastFirstVisibleItem = -1;

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == SCROLL_STATE_IDLE) {
                //mIndexerLayout.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                //mIndexerLayout.setBackgroundResource(R.drawable.letterslist_bg);
            } else {
                //mIndexerLayout.setBackgroundResource(R.drawable.letterslist_bg);
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                             int totalItemCount) {
//            Common.DisplayLog(TAG, "onScroll()-->firstVisibleItem=" + firstVisibleItem);
//            Common.DisplayLog("alphabet.charAt(sectionPosition)", String.valueOf(cnAdapter.getCarPosition(firstVisibleItem, false)) + "");
            mTitleText.setText(String.valueOf(cnAdapter.getCarPosition(firstVisibleItem, false)));
//            mTitleText.setText(String.valueOf(mAdapter.getCarPosition(firstVisibleItem, false)));
        }

    };

    private View.OnTouchListener mOnTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            float alphabetHeight = mIndexerLayout.getHeight();
            float y = event.getY();
            int sectionPosition = (int) ((y / alphabetHeight) / (1f / 27f));
            if (sectionPosition < 0) {
                sectionPosition = 0;
            } else if (sectionPosition > 26) {
                sectionPosition = 26;
            }
            if (lastSelectedPosition != sectionPosition) {
                if (-1 != lastSelectedPosition) {
                    ((TextView) mIndexerLayout.getChildAt(lastSelectedPosition)).setBackgroundColor(getResources().getColor(android.R.color.transparent));
                }
                lastSelectedPosition = sectionPosition;
            }
            String sectionLetter = String.valueOf(alphabet.charAt(sectionPosition));
//            int position = mIndexer.getPositionForSection(sectionPosition);
            int position = Integer.parseInt(cnAdapter.getCarPosition(sectionPosition, true));
            int position1 = Integer.parseInt(mAdapter.getCarPosition(sectionPosition, true));
            Common.DisplayLog("alphabet.charAt(sectionPosition)", alphabet.charAt(sectionPosition) + "");
            mTitleText.setText(String.valueOf(alphabet.charAt(sectionPosition)));
            Common.DisplayLog("OnTouchListener position ", position + "");
            TextView textView = (TextView) mIndexerLayout.getChildAt(sectionPosition);
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
//                    mIndexerLayout.setBackgroundResource(R.drawable.letterslist_bg);
//                    textView.setBackgroundColor(getResources().getColor(R.color.letter_bg_color));
                    mSectionToastLayout.setVisibility(View.VISIBLE);
                    mSectionToastText.setText(sectionLetter);
                    mListView.smoothScrollToPositionFromTop(position, 0, 1);
                    mListView.smoothScrollToPositionFromTop(position1, 0, 1);
                    break;
                case MotionEvent.ACTION_MOVE:
//                    mIndexerLayout.setBackgroundResource(R.drawable.letterslist_bg);
//                    textView.setBackgroundColor(getResources().getColor(R.color.letter_bg_color));
                    mSectionToastLayout.setVisibility(View.VISIBLE);
                    mSectionToastText.setText(sectionLetter);
                    mListView.smoothScrollToPositionFromTop(position, 0, 1);
                    mListView.smoothScrollToPositionFromTop(position1, 0, 1);
                    break;
                case MotionEvent.ACTION_UP:
                    //mIndexerLayout.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                    mSectionToastLayout.setVisibility(View.GONE);
                default:
                    mSectionToastLayout.setVisibility(View.GONE);
                    break;
            }
            return true;
        }

    };


    private void retrieveFullContactDetail() {
        Common.DisplayLog("", "retrieveFullContactDetail() = ok");
        rcdbh.resetTable();
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

        ContentResolver contactResolver = getContentResolver();

//        Cursor allCursor = contactResolver.query(uri, null, null, null, null);
        Cursor allCursor = contactResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        Common.DisplayLog("", "allCursor.getCount() = " + allCursor.getCount());
        if (allCursor != null && allCursor.moveToFirst()) {

            while (allCursor.moveToNext()) {
                String id = allCursor.getString(allCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                String name = allCursor.getString(allCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String phoneNumber = allCursor.getString(allCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                String EmailAddr = allCursor.getString(allCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA2));
                String image_thumb = allCursor.getString(allCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI));
                Common.DisplayLog("", "id = " + id + " name = " + name + " phoneNumber = " + phoneNumber + " EmailAddr = " + EmailAddr + " image_thumb = " + image_thumb);
                String[] COLUMN = {
                        "",
                        "",
                        name,
                        phoneNumber,
                        image_thumb,
                        "false"
                };
                rcdbh.addData(COLUMN);
            }


            allCursor.close();
        }

        createContactView();
    }


    private void createContactView() {


        rcdbh = new RContactDbHandler(getApplicationContext());
        crcdbh = new CRContactDbHandler(getApplicationContext());

        crcdbh.resetTempContdata(SessionId);

        contactuser = new ArrayList<ContactUser>();
        mIndexerLayout = (LinearLayout) findViewById(R.id.indexer_layout);
        mListView = (ListView) findViewById(R.id.contacts_list);
        mTitleLayout = (FrameLayout) findViewById(R.id.title_layout);
        mTitleText = (TextView) findViewById(R.id.title_text);
        mSectionToastLayout = (RelativeLayout) findViewById(R.id.section_toast_layout);
        mSectionToastText = (TextView) findViewById(R.id.section_toast_text);

        for (int i = 0; i < alphabet.length(); i++) {
            TextView letterTextView = new TextView(this);
            letterTextView.setText(alphabet.charAt(i) + "");
            letterTextView.setTextSize(13f);
            letterTextView.setTextColor(getResources().getColor(R.color.colorPrimary));
            letterTextView.setGravity(Gravity.CENTER);
            letterTextView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/arialbd.ttf"));
            ViewGroup.LayoutParams params = new LinearLayout.LayoutParams(28, 0, 1.0f);
            letterTextView.setLayoutParams(params);
            letterTextView.setPadding(2, 0, 1, 0);
            mIndexerLayout.addView(letterTextView);
//            mIndexerLayout.setBackgroundResource(R.drawable.letterslist_bg);
        }

        contactuser = rcdbh.getAllContactData();

        Cursor cursor = rcdbh.getAllContactCursor();
        cnAdapter = new ContactNumListAdapter(contactuser, getApplicationContext(), ContactActivity.this, consel);
        startManagingCursor(cursor);
        mIndexer = new AlphabetIndexer(cursor, 1, alphabet);
        cnAdapter.setIndexer(mIndexer);




        cursor.close();

        if (contactuser != null && contactuser.size() > 0) {
            mListView.setAdapter(cnAdapter);
            mListView.setOnScrollListener(mOnScrollListener);
            mIndexerLayout.setOnTouchListener(mOnTouchListener);
        }

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                cnAdapter.filter(s.toString(), curectTab);
//                cnAdapter.filterContactUser(curectTab);

            }
        });
        setselconlay();

    }

    @Override
    public void onBackPressed() {
        if (action.equals("edit")) {
            super.onBackPressed();
        } else {
            gobacktocr(moveto);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (pd != null) {
            pd.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (pd != null) {
            pd.dismiss();
        }
    }

    public void SavedEditDataOnServer(String SessId) {


        /***  Save main reminder ****/

        if (Common.hasInternetConnection(getApplicationContext())) {

            removeDataOnServer(SessId);

            Common.DisplayLog("", "loadSavedData " + SessId);

            Common.DisplayLog("SessId", SessId + "");

            if (pd != null) {
                pd.dismiss();
            }
            pd = new ProgressDialog(ContactActivity.this);
            pd.setMessage("Loading...");
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


            /*** Location ****/
            CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getApplicationContext());
            CReminder cReminder = crdbh.getTableSingleDateWihtDao(SessId);
            CRContactDbHandler crcdbh = new CRContactDbHandler(getApplicationContext());
//            if (cReminder.getReminderFor().equals("send"))
            {

                ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllCurrentSessContactData(SessId, "contact");

                Common.DisplayLog("remindercontactuser", remindercontactuser.size() + "");
                if (remindercontactuser.size() <= 0) {
                    pd.dismiss();
                }
                String tempToText = "";
                int i = 0;
                for (i = 0; i < remindercontactuser.size(); i++) {

                    Call<Reminder> callContact = apiService.addContact(SessId, remindercontactuser.get(i).getiUserId(), remindercontactuser.get(i).getName(), remindercontactuser.get(i).getNumber(), remindercontactuser.get(i).getPhoto(), remindercontactuser.get(i).getForaction(), remindercontactuser.get(i).getConsel());
                    callContact.enqueue(new Callback<Reminder>() {
                        @Override
                        public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                            int statusCode = response.code();
                            Common.DisplayLog("response.body()", response.body().toString());
                            boolean status = response.body().getStatus();
                            Common.DisplayLog("status", status + "");
                            Common.DisplayLog("getMessage", response.body().getMessage() + "");
                            if (status) {

//                                Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                            } else {
                                Common.ShowToast(getApplicationContext(), response.body().getMessage());

                            }
                            pd.dismiss();

                        }

                        @Override
                        public void onFailure(Call<Reminder> call, Throwable t) {
                            Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
                            pd.dismiss();
                        }


                    });


                }


            }
        }
    }

    public void removeDataOnServer(String SessId) {


        /***  Save main reminder ****/

        if (Common.hasInternetConnection(getApplicationContext())) {


            Common.DisplayLog("", "loadSavedData " + SessId);

            Common.DisplayLog("SessId", SessId + "");
            if (pd != null) {
                pd.dismiss();
            }
            pd = new ProgressDialog(ContactActivity.this);
            pd.setMessage("Loading...");
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


            /*** Location ****/

            Call<Reminder> callContact = apiService.removeContact(SessId);
            callContact.enqueue(new Callback<Reminder>() {
                @Override
                public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                    int statusCode = response.code();
                    Common.DisplayLog("response.body()", response.body().toString());
                    boolean status = response.body().getStatus();
                    Common.DisplayLog("status", status + "");
                    Common.DisplayLog("getMessage", response.body().getMessage() + "");
                    if (status) {

//                                Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                    } else {
                        Common.ShowToast(getApplicationContext(), response.body().getMessage());

                    }
                    pd.dismiss();

                }

                @Override
                public void onFailure(Call<Reminder> call, Throwable t) {
                    Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
                    pd.dismiss();
                }


            });

        }
    }

}
