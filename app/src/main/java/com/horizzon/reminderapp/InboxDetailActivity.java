package com.horizzon.reminderapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.horizzon.reminderapp.adapter.CommentAdapter;
import com.horizzon.reminderapp.adapter.ImageAttachmentAdapter;
import com.horizzon.reminderapp.adapter.InboxDetailContactBoxListAdapter;
import com.horizzon.reminderapp.dao.AttachmentTable;
import com.horizzon.reminderapp.dao.CReminder;
import com.horizzon.reminderapp.dao.ReminderComment;
import com.horizzon.reminderapp.dao.ReminderContactUser;
import com.horizzon.reminderapp.dao.ReminderDateTime;
import com.horizzon.reminderapp.dao.ReminderUser;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.CRAttachmentDbHandler;
import com.horizzon.reminderapp.handler.CRCommentDbHandler;
import com.horizzon.reminderapp.handler.CRContactDbHandler;
import com.horizzon.reminderapp.handler.CRDateTimeDbHandler;
import com.horizzon.reminderapp.handler.CreateReminderDbHandler;
import com.horizzon.reminderapp.handler.RUserDbHandler;
import com.horizzon.reminderapp.helper.ExtendedViewPager;
import com.horizzon.reminderapp.helper.SpaceItemDecoration;
import com.horizzon.reminderapp.helper.TouchImageAdapter;
import com.horizzon.reminderapp.utility.DateUtilitys;
import com.horizzon.reminderapp.utility.GridSpacingItemDecoration;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class InboxDetailActivity extends AppCompatActivity {

    String SessionId, comeForm, inFrom, action;
    TextView conName, rSub, rDateTime, rday, commentcount, attachcount, userName;
    ImageView imgBack, imgHome, user_image, con_image2, con_image3, imgPriority1, imgPriority2, imgPriority3, tabcomment, tabattach, btn_reject, btn_edit, btn_accept;
    RecyclerView commentList;
    ArrayList<ReminderComment> commentArrayList;
    CommentAdapter commentAdapter;
    LinearLayout tabcommentcontent, tabattachcontent;
    ScrollView mainscroll;
    InboxDetailContactBoxListAdapter inboxDetailContactBoxListAdapter;

    LinearLayout subtaskLayout, maincontentlay, pafeviewerlay, notesLayout, imagesLayout, subtasklistLayout, notlistLayout, contactMainLayout, contactLayout;
    ArrayList<AttachmentTable> imagesList;
    ImageAttachmentAdapter imgAdapter;
    RecyclerView imglistView, participantslist;
    CRAttachmentDbHandler cradbh;
    ExtendedViewPager miimgpwEViewPager;
    public ExpandableRelativeLayout conttoolsexpandable;
    int backstate = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox_detail);


        action = getIntent().getStringExtra("action");
        if (action == null || action.equals("")) {
            action = "";
        }

        SessionId = getIntent().getStringExtra("SessionId");
        if (savedInstanceState != null && (SessionId == null || SessionId.equals(""))) {
            SessionId = savedInstanceState.getString("SessionId");
        }

        inFrom = "done";
        if (getIntent().getStringExtra("comeForm") != null) {
            comeForm = getIntent().getStringExtra("comeForm");
        }

        cradbh = new CRAttachmentDbHandler(getApplicationContext());

        imgBack = (ImageView) findViewById(R.id.backBtn);
        imgHome = (ImageView) findViewById(R.id.homeBtn);

        conName = (TextView) findViewById(R.id.conName);
        rSub = (TextView) findViewById(R.id.rSub);
        rDateTime = (TextView) findViewById(R.id.rDateTime);

        user_image = (ImageView) findViewById(R.id.user_image);
        userName = (TextView) findViewById(R.id.userName);

        maincontentlay = (LinearLayout) findViewById(R.id.maincontentlay);
        pafeviewerlay = (LinearLayout) findViewById(R.id.pafeviewerlay);

        pafeviewerlay.setVisibility(View.GONE);
        maincontentlay.setVisibility(View.VISIBLE);

        subtasklistLayout = (LinearLayout) findViewById(R.id.subtasklistLayout);
        notlistLayout = (LinearLayout) findViewById(R.id.notlistLayout);
        contactMainLayout = (LinearLayout) findViewById(R.id.contactMainLayout);
        contactLayout = (LinearLayout) findViewById(R.id.contactLayout);

        subtaskLayout = (LinearLayout) findViewById(R.id.subtaskLayout);
        notesLayout = (LinearLayout) findViewById(R.id.notesLayout);
        imagesLayout = (LinearLayout) findViewById(R.id.imagesLayout);

        imglistView = (RecyclerView) findViewById(R.id.imglistView);
        participantslist = (RecyclerView) findViewById(R.id.participantslist);

        commentList = (RecyclerView) findViewById(R.id.commentList);

        conttoolsexpandable = (ExpandableRelativeLayout) findViewById(R.id.conttoolsexpandable);
        if(conttoolsexpandable.isExpanded()){
            conttoolsexpandable.collapse();
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent in = new Intent(InboxDetailActivity.this, AllActivity.class);
//                startActivity(in);
                onBackPressed();
            }
        });

        imgHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(InboxDetailActivity.this, HomeActivity.class);
                startActivity(in);

            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("SessionId", getIntent().getStringExtra("SessionId"));
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadSavedData(SessionId);
        getAttachData(SessionId);
        getComment(SessionId);
    }


    @Override
    public void onBackPressed() {

        if (backstate > 0) {
            pafeviewerlay.setVisibility(View.GONE);
            maincontentlay.setVisibility(View.VISIBLE);
            backstate = 0;
        } else {
            super.onBackPressed();
        }
    }


    public void loadSavedData(String SessId) {


        Common.DisplayLog("", "loadSavedData " + SessId);
        CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getApplicationContext());
//        RContactDbHandler rcdbh = new RContactDbHandler(getApplicationContext());


        CReminder cReminder = crdbh.getTableSingleDateWihtDao(SessId);


        Common.DisplayLog("cReminder.getSubject()", cReminder.getSubject() + "");
        // set Subject
        rSub.setText(cReminder.getSubject());

        // set Priority
        /*if (cReminder.getPriority().equals("1")) {
            imgPriority1.setVisibility(View.VISIBLE);
        } else if (cReminder.getPriority().equals("2")) {
            imgPriority1.setVisibility(View.VISIBLE);
            imgPriority2.setVisibility(View.VISIBLE);
        } else if (cReminder.getPriority().equals("3")) {
            imgPriority1.setVisibility(View.VISIBLE);
            imgPriority2.setVisibility(View.VISIBLE);
            imgPriority3.setVisibility(View.VISIBLE);
        }*/


        // set Contact
        CRContactDbHandler crcdbh = new CRContactDbHandler(getApplicationContext());
        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllCurrentSessContactData(SessId, "contact");
//        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllContactData();

        participantslist.setAdapter(null);
        inboxDetailContactBoxListAdapter = new InboxDetailContactBoxListAdapter(InboxDetailActivity.this, remindercontactuser, "InboxDetailActivity");
        LinearLayoutManager lLayout = new GridLayoutManager(InboxDetailActivity.this, 4);
//        imglistView.addItemDecoration(new SpaceItemDecoration(0, 5, 0, 0));
//        imglistView.setHasFixedSize(true);
        imglistView.addItemDecoration(new GridSpacingItemDecoration(4, 5, false));
        imglistView.setHasFixedSize(true);
        participantslist.setLayoutManager(lLayout);
        participantslist.setAdapter(inboxDetailContactBoxListAdapter);
        Common.DisplayLog("remindercontactuser", remindercontactuser.size() + "");

        RUserDbHandler rudbh = new RUserDbHandler(getApplicationContext());
        ReminderUser reminderUser = rudbh.getTableSingleDateWihtDao(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));

        userName.setText(reminderUser.getuName());
        String imgpath = ((reminderUser.getuPhoto().equals("")) ? "defaultimg" : reminderUser.getuPhoto()) + "";

        Picasso.with(getApplicationContext())
                .load(imgpath)
                .placeholder(R.drawable.profilepic)
                .error(R.drawable.profilepic)
                .into(user_image);


        // set Date Time
        CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(getApplicationContext());
        ReminderDateTime rdt = crdtdbh.getTableSingleDateWihtDao(SessId);
        Common.DisplayLog("crdtdbh.getDataCount(SessionId)", crdtdbh.getDataCount(SessId) + "");


        DateUtilitys dateUtl = new DateUtilitys();

        Date date;
        if (crdtdbh.getDataCount(SessId) > 0) {
            date = dateUtl.FormetDate(rdt.getStartDate() + " " + rdt.getStartTime());
        } else {
            Common.DisplayLog("cr.getCreateddate()", cReminder.getCreateddate() + "");
            date = dateUtl.FormetDate(cReminder.getCreateddate());

        }
        dateUtl.setforDisplayTitle(date);
        rDateTime.setText(dateUtl.getforDisplayTitle());


    }

    public void getAttachData(String SessId) {


        Common.DisplayLog("", "loadSavedData " + SessId);

        //load Subtask

        subtasklistLayout.removeAllViews();

        ArrayList<AttachmentTable> lodsubt = cradbh.getAllCurrentSessDataWithType(SessId, "subtask", inFrom);

        if (lodsubt.size() > 0) {
            subtaskLayout.setVisibility(View.VISIBLE);
            for (int i = 0; i < lodsubt.size(); i++) {
                subtasklistLayout.addView(createEditView(lodsubt.get(i).getAttachFor(), lodsubt.get(i).getId()));

            }

        }

        //load nots
        notlistLayout.removeAllViews();
        ArrayList<AttachmentTable> lodnote = cradbh.getAllCurrentSessDataWithType(SessId, "note", inFrom);
        if (lodnote.size() > 0) {
            notesLayout.setVisibility(View.VISIBLE);

            Common.DisplayLog("", "imgNoteEdit clicked");
            notlistLayout.addView(createEditView(lodnote.get(0).getAttachFor(), lodnote.get(0).getId()));
        }

        //load image layout
        getAttachImages();


        //load Contact
        getAttachContact();

        /*new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        mainscroll.requestFocus();

                    }
                },
                500);*/

    }

    private View createEditView(final String attfor, String preId) {
//        mContainerView = (LinearLayout)findViewById(R.id.parentView);
        Common.DisplayLog("preId ", preId);

        final String lastid = preId;

        final AttachmentTable at = cradbh.getTableSingleDateWihtDao(SessionId, lastid);
        Common.DisplayLog("lastid ", lastid + "");
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View myView = inflater.inflate(R.layout.attachment_edit_row, null);
        myView.setTag(lastid);
        final CheckBox attselchk = (CheckBox) myView.findViewById(R.id.attselchk);


        attselchk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                attselchk.setChecked(false);
            }
        });
        attselchk.setChecked(false);
        if (attfor.equals("note")) {
            attselchk.setVisibility(View.GONE);
        }

        final LinearLayout editcontanor = (LinearLayout) myView.findViewById(R.id.editcontanor);
        final TextView texv = (TextView) myView.findViewById(R.id.textChange);
        String st = at.getDetail().replace("\\n", "\n").replace("\n", "\r\n");
//        texv.setVisibility(View.GONE);

        texv.setText(st);

//        ed.setText(preId);
        final Button textEdit = (Button) myView.findViewById(R.id.textEdit);
        final Button textDelete = (Button) myView.findViewById(R.id.textDelete);

        textEdit.setVisibility(View.GONE);
        textDelete.setVisibility(View.GONE);

        if (preId.equals("")) {
//            textEdit.performClick();
        } else if (attfor.equals("note")) {

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {

                            final EditText texv1 = new EditText(getApplicationContext()); // Pass it an Activity or Context
                            texv1.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                            texv1.setGravity(Gravity.CENTER | Gravity.LEFT);
                            texv1.setBackgroundResource(0);
                            final float scale = getResources().getDisplayMetrics().density;
                            int lr = (int) (26 * scale + 0.5f);
                            int tb = (int) (10 * scale + 0.5f);
                            texv1.setPadding(lr, tb, lr, tb);
                            texv1.setTextColor(Color.parseColor("#adadad"));
                            texv1.setHintTextColor(Color.parseColor("#adadad"));
                            texv1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                            editcontanor.removeAllViews();
                            editcontanor.addView(texv1);
                            texv1.setText(at.getDetail());
                            Common.DisplayLog("texv1", texv1.getText().toString());
                            /*text text*/

                            String edst = texv1.getText().toString().trim();
                            final TextView texv2 = new TextView(getApplicationContext()); // Pass it an Activity or Context
                            texv2.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                            texv2.setGravity(Gravity.CENTER | Gravity.LEFT);
                            texv2.setBackgroundResource(0);
                            texv2.setPadding(lr, tb, lr, tb);
                            texv2.setTextColor(Color.parseColor("#adadad"));
                            texv2.setHintTextColor(Color.parseColor("#adadad"));
                            texv2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                            editcontanor.removeAllViews();
                            editcontanor.addView(texv2);
                            texv2.setText(edst);

                        }
                    },
                    1000);
        }

        return myView;
    }

    private void getAttachImages() {

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        imagesList = cradbh.getAllCurrentSessDataWithType(SessionId, "image", inFrom);
                        Common.DisplayLog("imagesList count", imagesList.size() + "");
                        if (imagesList.size() > 0) {
                            imagesLayout.setVisibility(View.VISIBLE);
                        } else {
                            imagesLayout.setVisibility(View.GONE);
                        }
                        imglistView.setAdapter(null);
                        imgAdapter = new ImageAttachmentAdapter(InboxDetailActivity.this, imagesList, null, "InboxDetailActivity", "detail");
//                        LinearLayoutManager lLayout = new LinearLayoutManager(InboxDetailActivity.this, LinearLayoutManager.HORIZONTAL, false);
                        LinearLayoutManager lLayout = new LinearLayoutManager(InboxDetailActivity.this);
                        imglistView.addItemDecoration(new SpaceItemDecoration(0, 0, 10, 0));
                        imglistView.setHasFixedSize(true);
                        imglistView.setLayoutManager(lLayout);
                        imglistView.setAdapter(imgAdapter);

                        String[] imgLists = new String[imagesList.size() + 1];
                        for (int i = 0; i < imagesList.size(); i++) {
                            imgLists[i] = imagesList.get(i).getDetail();
                        }

                        miimgpwEViewPager = (ExtendedViewPager) findViewById(R.id.miimgpwEViewPager);
                        miimgpwEViewPager.setAdapter(new TouchImageAdapter(InboxDetailActivity.this, imgLists, 0));

                    }
                },
                200);

    }

    public void getExtendedViewPager(int pos) {
        ArrayList<AttachmentTable> imagesLists = cradbh.getAllCurrentSessDataWithType(SessionId, "image", inFrom);
        String[] imgLists = new String[imagesLists.size()];
        for (int i = 0; i < imagesLists.size(); i++) {
            imgLists[i] = imagesLists.get(i).getDetail();
        }

        miimgpwEViewPager = (ExtendedViewPager) findViewById(R.id.miimgpwEViewPager);
        miimgpwEViewPager.setAdapter(new TouchImageAdapter(InboxDetailActivity.this, imgLists, 0));
        miimgpwEViewPager.setCurrentItem(pos);
        maincontentlay.setVisibility(View.GONE);
        pafeviewerlay.setVisibility(View.VISIBLE);
        backstate = 1;
    }

    private void getAttachContact() {
        contactMainLayout.setVisibility(View.GONE);

        contactLayout.removeAllViews();
        final CRContactDbHandler crcdbh = new CRContactDbHandler(getApplicationContext());
        ArrayList<AttachmentTable> remindercontactuser = cradbh.getAllCurrentSessDataWithType(SessionId, "contact", inFrom);


        Common.DisplayLog("remindercontactuser", remindercontactuser.size() + "");
        if (remindercontactuser.size() > 0) {
            contactMainLayout.setVisibility(View.VISIBLE);
            int i = 0;
            for (i = 0; i < remindercontactuser.size(); i++) {

                final AttachmentTable rcu = remindercontactuser.get(i);


                LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = vi.inflate(R.layout.attachcontact_view, null);

                TextView conName = (TextView) v.findViewById(R.id.conName);

                ImageView imageView = (ImageView) v.findViewById(R.id.con_image);
                LinearLayout btncall = (LinearLayout) v.findViewById(R.id.btncall);
                LinearLayout btntask = (LinearLayout) v.findViewById(R.id.btntask);
                LinearLayout btnsave = (LinearLayout) v.findViewById(R.id.btnsave);
                Button conDelete = (Button) v.findViewById(R.id.conDelete);
                conDelete.setVisibility(View.GONE);

                try {
                    final JSONObject json = new JSONObject(rcu.getDetail());
                    Common.DisplayLog("json.toString()", json.toString() + "");
                    conName.setText(json.getString("name").toString());
//                    String imgpath = getRealPathFromURI(Uri.parse(((json.getString("photo").toString().equals("")) ? "defaultimg" : json.getString("photo").toString()) + ""));
                    String imgpath = ((json.getString("photo").toString().equals("")) ? "defaultimg" : json.getString("photo").toString()) + "";
                    final String number = json.getString("number").toString();
                    Common.DisplayLog("imgpath", imgpath + "");
                    Common.DisplayLog("json.getString('photo').toString()", json.getString("photo").toString() + "");
                    Picasso.with(getApplicationContext())
                            .load(imgpath)
                            .placeholder(R.drawable.defaultusericon)
                            .error(R.drawable.defaultusericon)
                            .into(imageView);

                    btncall.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            try {
                                intent.setData(Uri.parse("tel:" + json.getString("number").toString()));
                                startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    btntask.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                            try {
                                sendIntent.setData(Uri.parse("sms:" + json.getString("number").toString()));
                                startActivity(sendIntent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    btnsave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                            try {
                                Intent intent = new Intent(Intent.ACTION_INSERT);
                                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                                intent.putExtra(ContactsContract.Intents.Insert.NAME, json.getString("name").toString());
                                intent.putExtra(ContactsContract.Intents.Insert.PHONE, json.getString("number").toString());
                                int PICK_CONTACT = 100;
                                startActivityForResult(intent, PICK_CONTACT);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    });

                    contactLayout.addView(v);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }

    }

    private void getComment(String SessId) {

        CRCommentDbHandler crcdbh = new CRCommentDbHandler(getApplicationContext());
        commentArrayList = crcdbh.getAllCurrentSessDataWithType(SessId);
        Common.DisplayLog("commentArrayList count", commentArrayList.size() + "");

        commentList.setAdapter(null);
//                        commentAdapter = new ImageGridCustomAdapter(AttachmentActivity.this, commentArrayList, AttachmentActivity.this);
        commentAdapter = new CommentAdapter(InboxDetailActivity.this, commentArrayList, "add");
        LinearLayoutManager lLayout = new LinearLayoutManager(getApplicationContext());
        commentList.setHasFixedSize(true);
        commentList.setLayoutManager(lLayout);
        commentList.setAdapter(commentAdapter);

        /*new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        commentList.scrollToPosition(commentArrayList.size() - 1);

                    }
                },
                200);*/

    }

}
