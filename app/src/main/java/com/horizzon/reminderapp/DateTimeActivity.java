package com.horizzon.reminderapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.aigestudio.wheelpicker.WheelPicker;
import com.aigestudio.wheelpicker.widgets.WheelDayPicker;
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.horizzon.reminderapp.dao.CReminder;
import com.horizzon.reminderapp.dao.ReminderDateTime;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.CRDateTimeDbHandler;
import com.horizzon.reminderapp.handler.CreateReminderDbHandler;
import com.horizzon.reminderapp.helper.CustomScrollView;
import com.horizzon.reminderapp.helper.dateandtimepicker.DateandTimePicker;
import com.horizzon.reminderapp.helper.dateandtimepicker.TimePicker;
import com.horizzon.reminderapp.retrofit.model.reminder.Reminder;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;
import com.horizzon.reminderapp.utility.DateUtilitys;
import com.horizzon.reminderapp.utility.OnSwipeTouchListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DateTimeActivity extends AppCompatActivity implements View.OnClickListener {

    private int minYear = 1990; // min year
    private int maxYear = 2550; // max year
    private int yearPos = 0;
    private int monthPos = 0;
    private int dayPos = 0;
    private String dtFor = "common", commonFor = "";
    private String selstartDate = "";
    private String selendDate = "";
    private String selstartTime = "";
    private String selendTime = "";
    private String seldays = "";


    ImageView imgBack, imgHome, imgCancel, imgYes, imgRepeat, imgCal;
    TextView txtDateTime, txtRepeat, txtRept, txtCal;
    LinearLayout calLayout, repeatLayout, reptLayout, datLayout;
    LinearLayout commonRedioLayout, commonMainLayout, calenderMainLayout, repeatMainLayout;
    int hour, min;

    List<String> yearList = new ArrayList();
    List<String> monthList = new ArrayList();
    List<String> dayList = new ArrayList();
    RadioGroup commonradioGroup;
    String now;
    SimpleDateFormat dateFormatter;
    DateandTimePicker singleDateAndTimePicker;
    DateUtilitys dateUtl;

    Calendar calendarmonth, repeatercalendar;
    TextView calmonth;
    ImageView prevMonth, nextMonth;
    GridView gridcalender;
    GridCellAdapter calendaradapter;
    int day, month, year;
    boolean rettocal = false;

    private String SessionId, action;
    private CRDateTimeDbHandler crdtdbh;

    /*** Repeat ***/
    String repeatFor;
    CustomScrollView repscrollView;
    LinearLayout customLayout, startenddateTimeLayout, startDateLayout, endDateLayout, everydayLayout, everyweekdayLayout, everyweekLayout, repoutLayout;
    FrameLayout everymonthLayout;
    RadioGroup repeatradioGroup;
    RadioButton everydayradbtn, everywdayradbtn, everyweekradbtn, everymonthradbtn, customradbtn;
    CheckBox cb1, cb2, cb3, cb4, cb5, cb6, cb7;
    public TextView txtStartDate, txtEndDate;
    public int repsesback_flag = 1;
    WheelDayPicker everymonth_day_loopview;
    int everymonth_day_val;

    private String[] weekdayList = {"false", "false", "false", "false", "false", "false", "false"};
    //    SimpleDateFormat dateFormatter;
    SingleDateAndTimePicker everymonth_day_picker;
    DateandTimePicker startdate_day_picker, enddate_day_picker;
    TimePicker everyday_time_picker, everywday_time_picker, custom_time_picker, everyweek_time_picker, everymonth_time_picker;

    ProgressDialog pd;

    /*******/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_time);

        SessionId = Common.getSharedPreferences(getApplicationContext(), "SessionId", "");
        crdtdbh = new CRDateTimeDbHandler(getApplicationContext());
        dateUtl = new DateUtilitys();

        action = getIntent().getStringExtra("action");
        if (action == null || action.equals("")) {
            action = "";
        }


        imgBack = (ImageView) findViewById(R.id.backBtn);
        imgHome = (ImageView) findViewById(R.id.homeBtn);
        imgYes = (ImageView) findViewById(R.id.imgYes);
        imgCancel = (ImageView) findViewById(R.id.imgCancel);
        imgRepeat = (ImageView) findViewById(R.id.imgRepeat);
        imgCal = (ImageView) findViewById(R.id.imgCal);
        txtDateTime = (TextView) findViewById(R.id.txtDateTime);
        txtRepeat = (TextView) findViewById(R.id.txtRepeat);
        txtRept = (TextView) findViewById(R.id.txtRept);
        txtCal = (TextView) findViewById(R.id.txtCal);
        calLayout = (LinearLayout) findViewById(R.id.calLayout);
        repeatLayout = (LinearLayout) findViewById(R.id.repeatLayout);
        reptLayout = (LinearLayout) findViewById(R.id.reptLayout);
        datLayout = (LinearLayout) findViewById(R.id.datLayout);
        commonradioGroup = (RadioGroup) findViewById(R.id.commonradioGroup);


        /**main layout**/
        commonRedioLayout = (LinearLayout) findViewById(R.id.commonRedioLayout);
        commonMainLayout = (LinearLayout) findViewById(R.id.commonMainLayout);
        repeatMainLayout = (LinearLayout) findViewById(R.id.repeatMainLayout);
        calenderMainLayout = (LinearLayout) findViewById(R.id.calenderMainLayout);

        repeatMainLayout.setVisibility(View.GONE);
        calenderMainLayout.setVisibility(View.GONE);
        commonRedioLayout.setVisibility(View.VISIBLE);
        commonMainLayout.setVisibility(View.VISIBLE);

        /*** Repeart Start***/


        repscrollView = (CustomScrollView) findViewById(R.id.repscrollView);
        customLayout = (LinearLayout) findViewById(R.id.customLayout);
        startenddateTimeLayout = (LinearLayout) findViewById(R.id.startenddateTimeLayout);
        startDateLayout = (LinearLayout) findViewById(R.id.startDateLayout);
        endDateLayout = (LinearLayout) findViewById(R.id.endDateLayout);
        repoutLayout = (LinearLayout) findViewById(R.id.repoutLayout);
        everydayLayout = (LinearLayout) findViewById(R.id.everydayLayout);
        everyweekdayLayout = (LinearLayout) findViewById(R.id.everywdayLayout);
        everyweekLayout = (LinearLayout) findViewById(R.id.everyweekLayout);
        everymonthLayout = (FrameLayout) findViewById(R.id.everymonthLayout);
        repeatradioGroup = (RadioGroup) findViewById(R.id.repeatradioGroup);
        everydayradbtn = (RadioButton) findViewById(R.id.everydayradbtn);
        everywdayradbtn = (RadioButton) findViewById(R.id.everywdayradbtn);
        everyweekradbtn = (RadioButton) findViewById(R.id.everyweekradbtn);
        everymonthradbtn = (RadioButton) findViewById(R.id.everymonthradbtn);
        customradbtn = (RadioButton) findViewById(R.id.customradbtn);
        everymonth_day_loopview = (WheelDayPicker) findViewById(R.id.everymonth_day_loopview);


        txtStartDate = (TextView) findViewById(R.id.txtStartDate);
        txtEndDate = (TextView) findViewById(R.id.txtEndDate);

        everymonth_day_picker = (SingleDateAndTimePicker) findViewById(R.id.everymonth_day_picker);
        startdate_day_picker = (DateandTimePicker) findViewById(R.id.startdate_day_picker);
        enddate_day_picker = (DateandTimePicker) findViewById(R.id.enddate_day_picker);
        startdate_day_picker.setPickerLayout("date");
        enddate_day_picker.setPickerLayout("date");


        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        everymonth_day_picker.selectDate(calendar);
        startdate_day_picker.selectDate(calendar);
        enddate_day_picker.selectDate(calendar);

        cb1 = (CheckBox) findViewById(R.id.chk);
        cb2 = (CheckBox) findViewById(R.id.chk1);
        cb3 = (CheckBox) findViewById(R.id.chk2);
        cb4 = (CheckBox) findViewById(R.id.chk3);
        cb5 = (CheckBox) findViewById(R.id.chk4);
        cb6 = (CheckBox) findViewById(R.id.chk5);
        cb7 = (CheckBox) findViewById(R.id.chk6);


        everyday_time_picker = (TimePicker) findViewById(R.id.everyday_time_picker);
        everywday_time_picker = (TimePicker) findViewById(R.id.everywday_time_picker);
        custom_time_picker = (TimePicker) findViewById(R.id.custom_time_picker);
        everyweek_time_picker = (TimePicker) findViewById(R.id.everyweek_time_picker);
        everymonth_time_picker = (TimePicker) findViewById(R.id.everymonth_time_picker);
        everymonth_time_picker.setSelectorLayout(false);


        /*** Repeart End***/


//        dateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm a");
        Calendar c = Calendar.getInstance();
        min = c.get(Calendar.MINUTE);
        hour = c.get(Calendar.HOUR);

        if (hour == 0) {
            hour = 12;
        }

        singleDateAndTimePicker = (DateandTimePicker) findViewById(R.id.single_day_picker);

        final DateFormat df = new SimpleDateFormat("MMM dd yyyy");
        now = df.format(new Date());
//        dayOfTheWeek = (String) android.text.format.DateFormat.format("EEEE", new Date());


//        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        singleDateAndTimePicker.selectDate(calendar);
        Common.DisplayLog("calendar", dateFormatter.format(calendar.getTime()) + "");
        txtDateTime.setText(dateUtl.setforDisplayTitle(singleDateAndTimePicker.getDate()));

        singleDateAndTimePicker.setListener(new DateandTimePicker.Listener() {
            @Override
            public void onDateChanged(String displayed, Date date) {
                {
//                    if (enable_flag == 3)
                    {
                        txtCal.setTextColor(Color.parseColor("#adadad"));
                        imgCal.setImageResource(R.drawable.calicon);
                    }
                    Common.DisplayLog("date", date + "");
                    now = df.format(date);
                    DateUtilitys dateUtl1 = new DateUtilitys(date, true);
                    Common.DisplayLog("fulldate ", dateUtl1.getforDisplayTitle() + "");
                    txtDateTime.setText(dateUtl.setforDisplayTitle(date));
                }
            }
        });
        /*singleDateAndTimePicker.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Common.DisplayLog("touch date",singleDateAndTimePicker.getDate()+"");
                txtDateTime.setText(dateUtl.setforDisplayTitle(singleDateAndTimePicker.getDate()));
                return false;
            }
        });*/
        commonMainLayout.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {
            public void onSwipeTop() {
                Common.DisplayLog("touch date", singleDateAndTimePicker.getDate() + "");
                txtDateTime.setText(dateUtl.setforDisplayTitle(singleDateAndTimePicker.getDate()));
            }

            public void onSwipeRight() {
                Common.DisplayLog("touch date", singleDateAndTimePicker.getDate() + "");
                txtDateTime.setText(dateUtl.setforDisplayTitle(singleDateAndTimePicker.getDate()));
            }

            public void onSwipeLeft() {
                Common.DisplayLog("touch date", singleDateAndTimePicker.getDate() + "");
                txtDateTime.setText(dateUtl.setforDisplayTitle(singleDateAndTimePicker.getDate()));
            }

            public void onSwipeBottom() {
                Common.DisplayLog("touch date", singleDateAndTimePicker.getDate() + "");
            }

        });
        commonradioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                RadioButton rb = (RadioButton) findViewById(checkedId);
                commonFor = rb.getText().toString();
                if (rb.getText().equals("Today")) {
                    now = df.format(new Date());
                    Calendar calendar = Calendar.getInstance();
                    Common.DisplayLog("calendar", dateFormatter.format(calendar.getTime()) + "");
                    singleDateAndTimePicker.selectDate(calendar);
//                    initPickerViews(); // init year and month loop view
//                    initDayPickerView(); //init day loop view
                    txtDateTime.setText(dateUtl.setforDisplayTitle(singleDateAndTimePicker.getDate()));
                } else if (rb.getText().equals("Tomorrow")) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.DAY_OF_YEAR, 1);
                    calendar.set(Calendar.HOUR_OF_DAY, 9);
                    calendar.set(Calendar.MINUTE, 00);
                    calendar.set(Calendar.SECOND, 0);
                    singleDateAndTimePicker.selectDate(calendar);
                    now = df.format(calendar.getTime());
//                    setSelectedDate(dateFormatter.format(calendar.getTime()));
//                    initPickerViews(); // init year and month loop view
//                    initDayPickerView(); //init day loop view
                    txtDateTime.setText(dateUtl.setforDisplayTitle(singleDateAndTimePicker.getDate()));
                } else if (rb.getText().equals("Next Week")) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.DAY_OF_YEAR, 7);
                    calendar.set(Calendar.HOUR_OF_DAY, 9);
                    calendar.set(Calendar.MINUTE, 00);
                    calendar.set(Calendar.SECOND, 0);
                    singleDateAndTimePicker.selectDate(calendar);
                    now = df.format(calendar.getTime());
//                    setSelectedDate(dateFormatter.format(calendar.getTime()));
//                    initPickerViews(); // init year and month loop view
//                    initDayPickerView(); //init day loop view
                    txtDateTime.setText(dateUtl.setforDisplayTitle(singleDateAndTimePicker.getDate()));
                }
            }
        });

        imgYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Common.DisplayLog("repeatFor", repeatFor + "");
//                dtFor = "common";
                if (repsesback_flag != 2 && repsesback_flag != 4) {
                    if (dtFor.equals("common")) {
                        selstartDate = dateUtl.getDateForStore(singleDateAndTimePicker.getDate());
                        selendDate = dateUtl.getDateForStore(singleDateAndTimePicker.getDate());
                        selstartTime = dateUtl.getTimeForStore(singleDateAndTimePicker.getDate());
                        selendTime = dateUtl.getTimeForStore(singleDateAndTimePicker.getDate());
                        seldays = commonFor;
                    } else if (dtFor.equals("repeat")) {

//                        if(!repeatFor.equals(""))
                        {
                            long diff = enddate_day_picker.getDate().getTime() - startdate_day_picker.getDate().getTime();
                            int numOfDays = (int) (diff / (1000 * 60 * 60 * 24));
                            Common.DisplayLog("numOfDays", numOfDays + "");
                            if (numOfDays < 0) {
                                Common.ShowToast(getApplicationContext(), "Please select proper start date and end date");
                                return;
                            }
                            selstartDate = dateUtl.getDateForStore(startdate_day_picker.getDate());
                            selendDate = dateUtl.getDateForStore(enddate_day_picker.getDate());
                    /*if (repeatFor.equals("startend")) {
                        selstartTime = dateUtl.getTimeForStore(startdate_day_picker.getDate());
                        selendTime = dateUtl.getTimeForStore(enddate_day_picker.getDate());
                    } else*/
                            {
                                selstartTime = dateUtl.getTimeForStore(repeatercalendar.getTime());
                                selendTime = dateUtl.getTimeForStore(repeatercalendar.getTime());
                            }
                            seldays = "";
                            JSONArray jsonarray = new JSONArray();
                            //[1,2,1,] etc..
                            for (int i = 0; i < weekdayList.length; i++) {
                                jsonarray.put(weekdayList[i]);
                            }
                            System.out.println("Prints the Json Array data :" + jsonarray.toString());
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("repeatFor", repeatFor);
                                if (repeatFor.equals("Every Month")) {
                                    jsonObject.put("seldays", everymonth_day_val + "");
                                    selstartTime = dateUtl.getTimeForStore(everymonth_time_picker.getDate());
                                } else {
                                    jsonObject.put("seldays", jsonarray);
                                }
                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            System.out.println("Prints the Json Object data :" + jsonObject.toString());
                            seldays = jsonObject.toString();
                        }
                        /*else{
                            Common.ShowToast(getApplicationContext(),"Please select time");
                        }*/
                    } else if (dtFor.equals("calender")) {

                        dtFor = "common";

                        selstartDate = dateUtl.getDateForStore(calendarmonth.getTime());
                        selendDate = dateUtl.getDateForStore(calendarmonth.getTime());
                        selstartTime = dateUtl.getTimeForStore(calendarmonth.getTime());
                        selendTime = dateUtl.getTimeForStore(calendarmonth.getTime());
                        seldays = "";

                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(calendarmonth.getTime());
                        calendar.set(Calendar.HOUR_OF_DAY, 9);
                        calendar.set(Calendar.MINUTE, 00);
                        calendar.set(Calendar.SECOND, 0);
                        singleDateAndTimePicker.selectDate(calendar);

                        repeatMainLayout.setVisibility(View.GONE);
                        calenderMainLayout.setVisibility(View.GONE);
                        commonRedioLayout.setVisibility(View.VISIBLE);
                        commonMainLayout.setVisibility(View.VISIBLE);
                        datLayout.setVisibility(View.VISIBLE);

                        return;


                    }

                    String[] COLUMN = {
                            "",
                            SessionId,
                            Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                            dtFor,
                            selstartDate,
                            selstartTime,
                            selendDate,
                            selendTime,
                            selstartDate
                    };
                    //seldays
                    String selId = String.valueOf(crdtdbh.saveData(COLUMN));
//                Toast.makeText(getApplicationContext(), "selId = = " + selId, Toast.LENGTH_LONG).show();
//                Toast.makeText(getApplicationContext(), selstartDate, Toast.LENGTH_LONG).show();
//                Toast.makeText(getApplicationContext(), selstartTime, Toast.LENGTH_LONG).show();
                    if (action.equals("edit")) {
                        SavedEditDataOnServer(SessionId);
                    } else {
                        gobacktocr();
                    }
                } else {
                    repsesback_flag = 1;
                    repeatLayout.performClick();
                }
            }
        });

        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (repsesback_flag == 2 || repsesback_flag == 4) {


                    repscrollView.setVisibility(View.GONE);
                    startenddateTimeLayout.setVisibility(View.GONE);
                    customLayout.setVisibility(View.GONE);
                    startDateLayout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                    endDateLayout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                    imgCal.setImageResource(R.drawable.calicon);
                    imgRepeat.setImageResource(R.drawable.repeaticon);

                    repeatMainLayout.setVisibility(View.GONE);
                    calenderMainLayout.setVisibility(View.GONE);
                    commonRedioLayout.setVisibility(View.VISIBLE);
                    commonMainLayout.setVisibility(View.VISIBLE);
                    datLayout.setVisibility(View.VISIBLE);

                    repsesback_flag = 1;
                } else if (rettocal) {
                    dtFor = "common";
                    repeatMainLayout.setVisibility(View.GONE);
                    calenderMainLayout.setVisibility(View.GONE);
                    commonRedioLayout.setVisibility(View.VISIBLE);
                    commonMainLayout.setVisibility(View.VISIBLE);
                    datLayout.setVisibility(View.VISIBLE);

                    imgCal.setImageResource(R.drawable.calicon);
                    imgRepeat.setImageResource(R.drawable.repeaticon);
                    rettocal = false;
                } else {
                    crdtdbh.resetTempdata(SessionId);
                    if (action.equals("edit")) {
                        onBackPressed();
                    } else {
                        gobacktocr();
                    }
                }
            }
        });

        repeatLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent in = new Intent(DateTimeActivity.this, DateTimeRepeatActivity.class);
                startActivityForResult(in, 2);*/
                repoutLayout.performClick();
                repscrollView.setVisibility(View.VISIBLE);
                datLayout.setVisibility(View.GONE);
                commonRedioLayout.setVisibility(View.GONE);
                commonMainLayout.setVisibility(View.GONE);
                repeatMainLayout.setVisibility(View.VISIBLE);
                calenderMainLayout.setVisibility(View.GONE);

                imgCal.setImageResource(R.drawable.calicon);
                imgRepeat.setImageResource(R.drawable.repeaticonblue);


                startenddateTimeLayout.setVisibility(View.GONE);
                customLayout.setVisibility(View.GONE);
                startDateLayout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                endDateLayout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));


                rettocal = true;
                dtFor = "repeat";
            }
        });

        calLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent in = new Intent(DateTimeActivity.this, CalenderActivity.class);
                startActivityForResult(in, 3);*/
                commonRedioLayout.setVisibility(View.GONE);
                commonMainLayout.setVisibility(View.GONE);
                repeatMainLayout.setVisibility(View.GONE);
                calenderMainLayout.setVisibility(View.VISIBLE);
                datLayout.setVisibility(View.VISIBLE);

                imgRepeat.setImageResource(R.drawable.repeaticon);
                imgCal.setImageResource(R.drawable.caliconblue);
                rettocal = true;
                dtFor = "calender";
                gridcalender = (GridView) findViewById(R.id.gridcalender);
                gridcalender.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {
                    public void onSwipeTop() {

                    }

                    public void onSwipeRight() {
                        prevMonth.performClick();
                    }

                    public void onSwipeLeft() {
                        nextMonth.performClick();
                    }

                    public void onSwipeBottom() {

                    }

                });

                setGridCellAdapterToDate(day, month, year);
            }
        });


        /*** Calender ***/
        calmonth = (TextView) findViewById(R.id.calmonth);
        prevMonth = (ImageView) findViewById(R.id.prevMonth);
        nextMonth = (ImageView) findViewById(R.id.nextMonth);
        prevMonth.setOnClickListener(this);
        nextMonth.setOnClickListener(this);

        calendarmonth = Calendar.getInstance(Locale.getDefault());
        day = calendarmonth.get(Calendar.DAY_OF_MONTH);
        month = calendarmonth.get(Calendar.MONTH) + 1;
        year = calendarmonth.get(Calendar.YEAR);


        /*unused*/
        setSelectedDate(dateFormatter.format(new Date()));

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (action.equals("edit")) {
                    onBackPressed();
                } else {
                    gobacktocr();
                }
            }
        });

        imgHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent in = new Intent(DateTimeActivity.this, HomeActivity.class);
//                startActivity(in);
                onBackPressed();

            }
        });


        /***Repeat***/
        repeatercalendar = Calendar.getInstance(Locale.getDefault());
        repeatercalendar.setTime(new Date());
        initRepeatdate();
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadSavedData(SessionId);
    }

    private void initRepeatdate() {

        repeatFor = "";
        for (int i = 0; i < weekdayList.length; i++) {
            weekdayList[i] = "false";
            Common.DisplayLog("" + i, weekdayList[i]);
        }


        /***Every day***/

        everyday_time_picker.setListener(new TimePicker.Listener() {
            @Override
            public void onDateChanged(String displayed, Date date) {
                repeatercalendar.setTime(date);
                Common.DisplayLog("everyday_time_picker", dateUtl.setforDisplayTitle(date));
            }
        });


        /***Every Weekeday***/
        everywday_time_picker.setListener(new TimePicker.Listener() {
            @Override
            public void onDateChanged(String displayed, Date date) {
                repeatercalendar.setTime(date);
                Common.DisplayLog("everywday_time_picker", dateUtl.setforDisplayTitle(date));
            }
        });

        /***Every week***/
        everyweek_time_picker.setListener(new TimePicker.Listener() {
            @Override
            public void onDateChanged(String displayed, Date date) {
                repeatercalendar.setTime(date);
                Common.DisplayLog("everyweek_time_picker", dateUtl.setforDisplayTitle(date));
            }
        });

        /***Every month***/
        everymonth_day_picker.setListener(new SingleDateAndTimePicker.Listener() {
            @Override
            public void onDateChanged(String displayed, Date date) {

                repeatercalendar.setTime(date);
                Common.DisplayLog("everymonth_day_picker", dateUtl.setforDisplayTitle(date));

            }
        });


        dayList = new ArrayList<String>();
        for (int i = 1; i < 31; i++) {
            dayList.add(String.valueOf(i));
        }
        /*everymonth_day_loopview.setDataList((ArrayList) dayList);

        everymonth_day_loopview.setInitPosition(0);
        everymonth_day_loopview.setLoopListener(new LoopScrollListener() {
            @Override
            public void onItemSelect(int item) {
                everymonth_day_val = item;
                Common.DisplayLog("everymonth_day_val",everymonth_day_val+"");
            }
        });*/
        everymonth_day_loopview.setOnItemSelectedListener(new WheelPicker.OnItemSelectedListener() {
            @Override
            public void onItemSelected(WheelPicker picker, Object data, int position) {
                everymonth_day_val = Integer.parseInt(data.toString());
                Common.DisplayLog("everymonth_day_val", everymonth_day_val + "");
            }
        });

        everymonth_time_picker.setListener(new TimePicker.Listener() {
            @Override
            public void onDateChanged(String displayed, Date date) {
                Common.DisplayLog("everymonth_time_picker", dateUtl.setforDisplayTitle(date));

                repeatercalendar.setTime(date);
            }
        });

        /***Custom***/
        custom_time_picker.setListener(new TimePicker.Listener() {
            @Override
            public void onDateChanged(String displayed, Date date) {
                repeatercalendar.setTime(date);
                Common.DisplayLog("custom_time_picker", dateUtl.setforDisplayTitle(date));
            }
        });



        /*repoutLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (repsesback_flag == 2) {
                    repscrollView.setVisibility(View.VISIBLE);
                    startenddateTimeLayout.setVisibility(View.GONE);
                    customLayout.setVisibility(View.GONE);
                    startDateLayout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                    endDateLayout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                    repsesback_flag = 1;
                } else if (repsesback_flag == 4) {
                    repscrollView.setVisibility(View.VISIBLE);
                    startenddateTimeLayout.setVisibility(View.GONE);
                    customLayout.setVisibility(View.GONE);
                    startDateLayout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                    endDateLayout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                    repsesback_flag = 1;
                }
                return false;
            }
        });*/


        startDateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                repeatFor = "startend";
                repsesback_flag = 2;
                endDateLayout.setBackgroundColor(Color.parseColor("#ccffffff"));
                startDateLayout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                repscrollView.setVisibility(View.GONE);
                startenddateTimeLayout.setVisibility(View.VISIBLE);
                customLayout.setVisibility(View.GONE);
                enddate_day_picker.setVisibility(View.GONE);
                startdate_day_picker.setVisibility(View.VISIBLE);
            }
        });

        endDateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                repeatFor = "startend";
                repsesback_flag = 4;
                endDateLayout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                startDateLayout.setBackgroundColor(Color.parseColor("#ccffffff"));
                repscrollView.setVisibility(View.GONE);
                startenddateTimeLayout.setVisibility(View.VISIBLE);
                customLayout.setVisibility(View.GONE);

                startdate_day_picker.setVisibility(View.GONE);
                enddate_day_picker.setVisibility(View.VISIBLE);
            }
        });


        /***start date***/
        startdate_day_picker.setListener(new DateandTimePicker.Listener() {
            @Override
            public void onDateChanged(String displayed, Date date) {
                repeatercalendar.setTime(date);
                Common.DisplayLog("custom_time_picker", dateUtl.setforDisplayTitle(date));
                txtStartDate.setText(dateUtl.setforDisplayTitleForRep(date));
            }
        });

        /***end date***/
        enddate_day_picker.setListener(new DateandTimePicker.Listener() {
            @Override
            public void onDateChanged(String displayed, Date date) {
                repeatercalendar.setTime(date);
                Common.DisplayLog("custom_time_picker", dateUtl.setforDisplayTitle(date));
                txtEndDate.setText(dateUtl.setforDisplayTitleForRep(date));
            }
        });

        cb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                weekdayList[0] = String.valueOf(isChecked);
                if (isChecked) {
                    cb1.setTextColor(Color.parseColor("#1ba1e2"));
                } else {
                    cb1.setTextColor(Color.parseColor("#90000000"));
                }
            }
        });

        cb2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                weekdayList[1] = String.valueOf(isChecked);
                if (isChecked) {
                    cb2.setTextColor(Color.parseColor("#1ba1e2"));
                } else {
                    cb2.setTextColor(Color.parseColor("#90000000"));
                }
            }
        });

        cb3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                weekdayList[2] = String.valueOf(isChecked);
                if (isChecked) {
                    cb3.setTextColor(Color.parseColor("#1ba1e2"));
                } else {
                    cb3.setTextColor(Color.parseColor("#90000000"));
                }
            }
        });

        cb4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                weekdayList[3] = String.valueOf(isChecked);
                if (isChecked) {
                    cb4.setTextColor(Color.parseColor("#1ba1e2"));
                } else {
                    cb4.setTextColor(Color.parseColor("#90000000"));
                }
            }
        });

        cb5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                weekdayList[4] = String.valueOf(isChecked);
                if (isChecked) {
                    cb5.setTextColor(Color.parseColor("#1ba1e2"));
                } else {
                    cb5.setTextColor(Color.parseColor("#90000000"));
                }
            }
        });

        cb6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                weekdayList[5] = String.valueOf(isChecked);
                if (isChecked) {
                    cb6.setTextColor(Color.parseColor("#1ba1e2"));
                } else {
                    cb6.setTextColor(Color.parseColor("#90000000"));
                }
            }
        });

        cb7.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                weekdayList[6] = String.valueOf(isChecked);
                if (isChecked) {
                    cb7.setTextColor(Color.parseColor("#1ba1e2"));
                } else {
                    cb7.setTextColor(Color.parseColor("#90000000"));
                }
            }
        });


        repeatradioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                RadioButton rb = (RadioButton) findViewById(checkedId);
                repeatFor = rb.getText().toString();
                if (rb.getText().equals("Everyday")) {

                    for (int i = 0; i < weekdayList.length; i++) {
                        weekdayList[i] = "false";
                        Common.DisplayLog("" + i, weekdayList[i]);
                    }

                    repeatercalendar.setTime(startdate_day_picker.getDate());
                    everyday_time_picker.selectDate(repeatercalendar);

                    everydayLayout.setVisibility(View.VISIBLE);
                    everyweekdayLayout.setVisibility(View.GONE);
                    everymonthLayout.setVisibility(View.GONE);
                    everyweekLayout.setVisibility(View.GONE);
                    customLayout.setVisibility(View.GONE);
                } else if (rb.getText().equals("Every Weekday")) {

                    repeatercalendar.setTime(startdate_day_picker.getDate());
                    everywday_time_picker.selectDate(repeatercalendar);


                    for (int i = 0; i < weekdayList.length; i++) {
                        if (i == 0 || i == 6) {
                            weekdayList[i] = "false";
                        } else {
                            weekdayList[i] = "true";
                        }
                        Common.DisplayLog("" + i, weekdayList[i]);
                    }

                    everydayLayout.setVisibility(View.GONE);
                    everyweekdayLayout.setVisibility(View.VISIBLE);
                    everymonthLayout.setVisibility(View.GONE);
                    everyweekLayout.setVisibility(View.GONE);
                    customLayout.setVisibility(View.GONE);
                } else if (rb.getText().equals("Every Week")) {

                    repeatercalendar.setTime(startdate_day_picker.getDate());
                    everyweek_time_picker.selectDate(repeatercalendar);

                    for (int i = 0; i < weekdayList.length; i++) {
                        weekdayList[i] = "true";
                        Common.DisplayLog("" + i, weekdayList[i]);
                    }

                    everydayLayout.setVisibility(View.GONE);
                    everyweekdayLayout.setVisibility(View.GONE);
                    everymonthLayout.setVisibility(View.GONE);
                    everyweekLayout.setVisibility(View.VISIBLE);
                    customLayout.setVisibility(View.GONE);
                } else if (rb.getText().equals("Every Month")) {

                    repeatercalendar.setTime(everymonth_time_picker.getDate());
                    everymonth_time_picker.selectDate(repeatercalendar);
                    everymonth_day_picker.selectDate(repeatercalendar);

                    for (int i = 0; i < weekdayList.length; i++) {
                        weekdayList[i] = "false";
                        Common.DisplayLog("" + i, weekdayList[i]);
                    }

                    everydayLayout.setVisibility(View.GONE);
                    everyweekdayLayout.setVisibility(View.GONE);
                    everymonthLayout.setVisibility(View.VISIBLE);
                    everyweekLayout.setVisibility(View.GONE);
                    customLayout.setVisibility(View.GONE);
                } else if (rb.getText().equals("Custom")) {

                    repeatercalendar.setTime(startdate_day_picker.getDate());
                    custom_time_picker.selectDate(repeatercalendar);

                    CheckBox[] cbIds = {cb1, cb1, cb1, cb1, cb1, cb1, cb1};
                    for (int i = 0; i < weekdayList.length; i++) {
                        cbIds[i].setChecked(false);
                        weekdayList[i] = "false";
                        Common.DisplayLog("" + i, weekdayList[i]);
                    }

                    everydayLayout.setVisibility(View.GONE);
                    everyweekdayLayout.setVisibility(View.GONE);
                    everymonthLayout.setVisibility(View.GONE);
                    everyweekLayout.setVisibility(View.GONE);
                    customLayout.setVisibility(View.VISIBLE);
                    repsesback_flag = 3;
                }
            }
        });


    }


    public void setSelectedDate(String dateStr) {
        if (!TextUtils.isEmpty(dateStr)) {
            long milliseconds = getLongFromyyyyMMdd(dateStr);
            Calendar calendar = Calendar.getInstance(Locale.getDefault());

            if (milliseconds != -1) {
                calendar.setTimeInMillis(milliseconds);
                yearPos = calendar.get(Calendar.YEAR) - minYear;
                monthPos = calendar.get(Calendar.MONTH);
                dayPos = calendar.get(Calendar.DAY_OF_MONTH) - 1;
            }
        }
    }

    public static long getLongFromyyyyMMdd(String date) {
        SimpleDateFormat mFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date parse = null;
        try {
            parse = mFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (parse != null) {
            return parse.getTime();
        } else {
            return -1;
        }
    }

    public static String format2LenStr(int num) {
        return (num < 10) ? "0" + num : String.valueOf(num);
    }


    @Override
    public void onBackPressed() {

        if (action.equals("edit")) {
            super.onBackPressed();
        } else {
            gobacktocr();
        }
    }


    private void gobacktocr() {
        Intent in = null;
        in = new Intent(getApplicationContext(), CreateReminderActivity.class);
        in.putExtra("newrem", "no");
        startActivity(in);

    }

    @Override
    public void onClick(View v) {
        if (v == prevMonth) {
            if (month <= 1) {
                month = 12;
                year--;
            } else {
                month--;
            }

            Common.DisplayLog("", "Setting Prev Month in GridCellAdapter: " + "Month: "
                    + month + " Year: " + year);
            day = 0;
            setGridCellAdapterToDate(day, month, year);
            Animation RightSwipe = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.lefttoright);
            gridcalender.startAnimation(RightSwipe);
        }
        if (v == nextMonth) {
            if (month > 11) {
                month = 1;
                year++;
            } else {
                month++;
            }

            Common.DisplayLog("", "Setting Next Month in GridCellAdapter: " + "Month: "
                    + month + " Year: " + year);
            day = 0;
            setGridCellAdapterToDate(day, month, year);
            Animation LeftSwipe = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.righttoleftcal);
            gridcalender.startAnimation(LeftSwipe);
        }
    }


    public void SavedEditDataOnServer(String SessId) {


        /***  Save main reminder ****/
        CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getApplicationContext());
        CReminder cReminder = crdbh.getTableSingleDateWihtDao(SessId);

        if (Common.hasInternetConnection(getApplicationContext())) {
            crdbh.updataStatus(SessId, "done");
            Common.DisplayLog("", "loadSavedData " + SessId);

            cReminder = crdbh.getTableSingleDateWihtDao(SessId);

            Common.DisplayLog("SessId", SessId + "");
            pd = new ProgressDialog(DateTimeActivity.this);
            pd.setMessage("Loading...");
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


            /*** Date & Time ****/
            CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(getApplicationContext());

            ReminderDateTime rdt = crdtdbh.getTableSingleDateWihtDao(SessId);

            if (rdt != null) {
                Call<Reminder> callDateTime = apiService.editDatetime(rdt.getSesssionId(), rdt.getiUserId(), rdt.getDtFor(), rdt.getStartDate(), rdt.getStartTime(), rdt.getEndDate(), rdt.getEndTime(), rdt.getDays(), rdt.getStatus());
                callDateTime.enqueue(new Callback<Reminder>() {
                    @Override
                    public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                        int statusCode = response.code();
                        Common.DisplayLog("response.body()", response.body().toString());
                        boolean status = response.body().getStatus();
                        Common.DisplayLog("status", status + "");
                        Common.DisplayLog("getMessage", response.body().getMessage() + "");

                        if (status) {
                            pd.dismiss();
                            onBackPressed();
                                /*Intent in = new Intent(getApplicationContext(), HomeActivity.class);
                                startActivity(in);*/
                        } else {
                            Common.ShowToast(getApplicationContext(), response.body().getMessage());
                        }
                        pd.dismiss();

                    }

                    @Override
                    public void onFailure(Call<Reminder> call, Throwable t) {

                        Common.DisplayLog("fail", t.getMessage().toString());
                    }

                });
            } else {

            }
        }
    }


    private void setGridCellAdapterToDate(int day, int month, int year) {
        calendaradapter = new GridCellAdapter(getApplicationContext(),
                day, month, year);
        calendarmonth.set(year, month - 1, ((day > 0) ? day : calendarmonth.get(Calendar.DAY_OF_MONTH)));
        calmonth.setText(android.text.format.DateFormat.format("MMMM", calendarmonth.getTime()).toString());
        calendaradapter.notifyDataSetChanged();
        gridcalender.setAdapter(calendaradapter);
    }

    public void loadSavedData(String SessId) {


        Common.DisplayLog("", "loadSavedData " + SessId);
        Common.DisplayLog("", "crdtdbh.getDataCount(SessId) " + crdtdbh.getDataCount(SessId));

        if (crdtdbh.getDataCount(SessId) > 0) {
            ReminderDateTime rdt = crdtdbh.getTableSingleDateWihtDao(SessId);

            dtFor = rdt.getDtFor();
            selstartDate = rdt.getStartDate();
            selendDate = rdt.getEndDate();
            selstartTime = rdt.getStartTime();
            selendTime = rdt.getEndTime();
            seldays = rdt.getDays();
            if (dtFor.equals("common")) {


                Date date = dateUtl.FormetDate(selstartDate + " " + selstartTime);
                calendarmonth = Calendar.getInstance(Locale.getDefault());
                calendarmonth.setTime(date);

                Common.DisplayLog("dateUtl.getHour()", (Integer.parseInt(dateUtl.getHour())) + "");
                Common.DisplayLog("repeatercalendartemp", calendarmonth + "");

//                calendarmonth.set(Calendar.HOUR_OF_DAY, Integer.parseInt(dateUtl.getHour()));
//                calendarmonth.set(Calendar.HOUR, Integer.parseInt(dateUtl.get12Hour()));
                Common.DisplayLog("repeatercalendartemp 2", calendarmonth + "");
                singleDateAndTimePicker.selectDate(calendarmonth);
//                singleDateAndTimePicker.setHoursStep(Integer.parseInt(dateUtl.getHour())+1);
                txtDateTime.setText(dateUtl.setforDisplayTitle(singleDateAndTimePicker.getDate()));
            } else if (dtFor.equals("repeat")) {


                Calendar repeatercalendartemp = Calendar.getInstance(Locale.getDefault());
                repeatLayout.performClick();
                Date startDatedate = dateUtl.FormetDate(selstartDate + " " + selstartTime);
                Common.DisplayLog("startDatedate", startDatedate + "");
                repeatercalendartemp.setTime(startDatedate);
                Common.DisplayLog("dateUtl.getHour()", (Integer.parseInt(dateUtl.getHour()) - 1) + "");
                Common.DisplayLog("Integer.parseInt(dateUtl.getMin())", Integer.parseInt(dateUtl.getMin()) + "");
                Common.DisplayLog("repeatercalendartemp", repeatercalendartemp + "");

//                int hpos = (Integer.parseInt(dateUtl.getHour())>12)?(Integer.parseInt(dateUtl.getHour())-12):Integer.parseInt(dateUtl.getHour());
//                startdate_day_picker.setHoursStep(hpos);
//                startdate_day_picker.setStepMinutes(Integer.parseInt(dateUtl.getMonth()));

                repeatercalendartemp.set(Calendar.HOUR_OF_DAY, Integer.parseInt(dateUtl.getHour()) - 1);
//                repeatercalendartemp.set(Calendar.MINUTE, Integer.parseInt(dateUtl.getMin()));
//                repeatercalendartemp.set(Calendar.SECOND, 0);

                startdate_day_picker.selectDate(repeatercalendartemp);

                JSONObject sdays;
                try {
                    sdays = new JSONObject(rdt.getDays().toString());
                    Common.DisplayLog("sdays", sdays.toString());

                    if (sdays.getString("repeatFor").equals("Everyday")) {
                        everydayradbtn.setChecked(true);
//                        everydayLayout.setVisibility(View.VISIBLE);

                        everyday_time_picker.selectDate(repeatercalendartemp);
//                        everyday_time_picker.setHoursStep((Integer.parseInt(dateUtl.getHour())));
//                        everyday_time_picker.setStepMinutes(Integer.parseInt(dateUtl.getMonth()));

                    } else if (sdays.getString("repeatFor").equals("Every Weekday")) {

                        everywdayradbtn.setChecked(true);

//                        everyweekdayLayout.setVisibility(View.VISIBLE);
                        everywday_time_picker.selectDate(repeatercalendartemp);


                    } else if (sdays.getString("repeatFor").equals("Every Week")) {
                        everyweekradbtn.setChecked(true);

//                        everyweekLayout.setVisibility(View.VISIBLE);
                        everyweek_time_picker.selectDate(repeatercalendartemp);


                    } else if (sdays.getString("repeatFor").equals("Every Month")) {

                        everymonthradbtn.setChecked(true);
//                        everymonth_day_loopview.setInitPosition(Integer.parseInt(sdays.getString("seldays")));
                        everymonth_day_val = Integer.parseInt(sdays.getString("seldays"));
                        everymonth_day_loopview.setSelectedDay(everymonth_day_val);
                        everymonth_time_picker.selectDate(repeatercalendartemp);

//                        everymonthLayout.setVisibility(View.VISIBLE);
                        everymonth_day_picker.selectDate(repeatercalendartemp);


                    } else if (sdays.getString("repeatFor").equals("Custom")) {
                        customradbtn.setChecked(true);
//                        customLayout.setVisibility(View.VISIBLE);
                        custom_time_picker.selectDate(repeatercalendartemp);

                        CheckBox[] cbIds = {cb1, cb2, cb3, cb4, cb5, cb6, cb7};

                        JSONArray jsonarray = new JSONArray(sdays.getString("seldays"));

                        for (int i = 0; i < jsonarray.length(); i++) {
                            cbIds[i].setChecked(jsonarray.getBoolean(i));
                            weekdayList[i] = jsonarray.getString(i);
                            Common.DisplayLog("" + i, weekdayList[i]);
                        }

                    }
//                    else if (sdays.getString("repeatFor").equals("startend"))
                    {
//                        startDateLayout.performClick();


                        txtStartDate.setText(dateUtl.setforDisplayTitleForRep(startdate_day_picker.getDate()));

                        Date endDatedate = dateUtl.FormetDate(selendDate + " " + selendTime);
                        Common.DisplayLog("startDatedate", endDatedate + "");
                        repeatercalendartemp.setTime(endDatedate);
                        repeatercalendartemp.set(Calendar.HOUR_OF_DAY, Integer.parseInt(dateUtl.getHour()) - 1);
                        enddate_day_picker.selectDate(repeatercalendartemp);

                        txtEndDate.setText(dateUtl.setforDisplayTitleForRep(enddate_day_picker.getDate()));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                selstartDate = dateUtl.getDateForStore(startdate_day_picker.getDate());
                selendDate = dateUtl.getDateForStore(enddate_day_picker.getDate());
                selstartTime = dateUtl.getTimeForStore(repeatercalendartemp.getTime());
                selendTime = dateUtl.getTimeForStore(repeatercalendartemp.getTime());
                seldays = "";
                JSONArray jsonarray = new JSONArray();

                for (int i = 0; i < weekdayList.length; i++) {
                    jsonarray.put(weekdayList[i]);
                }
                System.out.println("Prints the Json Array data :" + jsonarray.toString());
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("repeatFor", repeatFor);
                    jsonObject.put("seldays", jsonarray);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                System.out.println("Prints the Json Object data :" + jsonObject.toString());
                seldays = jsonObject.toString();


                repeatercalendar.setTime(repeatercalendartemp.getTime());

            } else if (dtFor.equals("calender")) {

                calLayout.performClick();
                Date date = dateUtl.FormetDate(selstartDate + " " + selstartTime);
                day = Integer.parseInt(dateUtl.getDay());
                month = Integer.parseInt(dateUtl.getMonth());
                year = Integer.parseInt(dateUtl.getYear());
                setGridCellAdapterToDate(day, month, year);

                selstartDate = dateUtl.getDateForStore(calendarmonth.getTime());
                selendDate = dateUtl.getDateForStore(calendarmonth.getTime());
                selstartTime = dateUtl.getTimeForStore(calendarmonth.getTime());
                selendTime = dateUtl.getTimeForStore(calendarmonth.getTime());
                seldays = "";
            }
        }

    }

    public class GridCellAdapter extends BaseAdapter implements View.OnClickListener {
        private static final String tag = "GridCellAdapter";
        private final Context _context;

        private final List<String> list;
        private static final int DAY_OFFSET = 1;
        private final String[] weekdays = new String[]{"Sun", "Mon", "Tue",
                "Wed", "Thu", "Fri", "Sat"};
        private final String[] months = {"January", "February", "March",
                "April", "May", "June", "July", "August", "September",
                "October", "November", "December"};
        private final int[] daysOfMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30,
                31, 30, 31};
        private int daysInMonth;
        private int currentDayOfMonth;
        private int currentWeekDay;
        private TextView gridcell;
        private final HashMap<String, Integer> eventsPerMonthMap;
        Date d = new Date();
        CharSequence cun = android.text.format.DateFormat.format("dd-MMMM-yyyy ", d.getTime());
        String[] cun_sys_date = cun.toString().split("-");
        private int selectedDay = 0;

        // Days in Current Month
        public GridCellAdapter(Context context, int day,
                               int month, int year) {
            super();
            this._context = context;
            this.list = new ArrayList<String>();
            this.selectedDay = day;

            Common.DisplayLog(tag, "==> Passed in Date FOR Month: " + month + " "
                    + "Year: " + year);

            Calendar calendar = Calendar.getInstance();
            setCurrentDayOfMonth(calendar.get(Calendar.DAY_OF_MONTH));
//            setCurrentDayOfMonth(((day>0)?day:calendar.get(Calendar.DAY_OF_MONTH)));
            setCurrentWeekDay(calendar.get(Calendar.DAY_OF_WEEK));

            Common.DisplayLog(tag, "New Calendar:= " + calendar.getTime().toString());
            Common.DisplayLog(tag, "CurrentDayOfWeek :" + getCurrentWeekDay());
            Common.DisplayLog(tag, "CurrentDayOfMonth :" + getCurrentDayOfMonth());


            // Print Month
            printMonth(month, year);

            // Find Number of Events
            eventsPerMonthMap = findNumberOfEventsPerMonth(year, month);
        }

        private String getMonthAsString(int i) {
            return months[i];
        }

        private String getWeekDayAsString(int i) {
            return weekdays[i];
        }

        private int getNumberOfDaysOfMonth(int i) {
            return daysOfMonth[i];
        }

        public String getItem(int position) {
            return list.get(position);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        private void printMonth(int mm, int yy) {

            Common.DisplayLog(tag, "==> printMonth: mm: " + mm + " " + "yy: " + yy);

            int trailingSpaces = 0;
            int daysInPrevMonth = 0;
            int prevMonth = 0;
            int prevYear = 0;
            int nextMonth = 0;
            int nextYear = 0;

            int currentMonth = mm - 1;
            String currentMonthName = getMonthAsString(currentMonth);
            daysInMonth = getNumberOfDaysOfMonth(currentMonth);


            Common.DisplayLog(tag, "Current Month: " + " " + currentMonthName
                    + " having " + daysInMonth + " days.");


            GregorianCalendar cal = new GregorianCalendar(yy, currentMonth, 1);

            Common.DisplayLog(tag, "Gregorian Calendar:= " + cal.getTime().toString());


            if (currentMonth == 11) {
                prevMonth = currentMonth - 1;
                daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
                nextMonth = 0;
                prevYear = yy;
                nextYear = yy + 1;

                Common.DisplayLog(tag, "*->PrevYear: " + prevYear + " PrevMonth:"
                        + prevMonth + " NextMonth: " + nextMonth
                        + " NextYear: " + nextYear);

            } else if (currentMonth == 0) {
                prevMonth = 11;
                prevYear = yy - 1;
                nextYear = yy;
                daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
                nextMonth = 1;

                Common.DisplayLog(tag, "**--> PrevYear: " + prevYear + " PrevMonth:"
                        + prevMonth + " NextMonth: " + nextMonth
                        + " NextYear: " + nextYear);

            } else {
                prevMonth = currentMonth - 1;
                nextMonth = currentMonth + 1;
                nextYear = yy;
                prevYear = yy;
                daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);

                Common.DisplayLog(tag, "***---> PrevYear: " + prevYear + " PrevMonth:"
                        + prevMonth + " NextMonth: " + nextMonth
                        + " NextYear: " + nextYear);

            }

            int currentWeekDay = cal.get(Calendar.DAY_OF_WEEK) - 1;
            trailingSpaces = currentWeekDay;

            Common.DisplayLog(tag, "Week Day:" + currentWeekDay + " is "
                    + getWeekDayAsString(currentWeekDay));
            Common.DisplayLog(tag, "No. Trailing space to Add: " + trailingSpaces);
            Common.DisplayLog(tag, "No. of Days in Previous Month: " + daysInPrevMonth);


            if (cal.isLeapYear(cal.get(Calendar.YEAR)))
                if (mm == 2)
                    ++daysInMonth;
                else if (mm == 3)
                    ++daysInPrevMonth;

            // Trailing Month days
            for (int i = 0; i < trailingSpaces; i++) {

                Common.DisplayLog(tag,
                        "PREV MONTH:= "
                                + prevMonth
                                + " => "
                                + getMonthAsString(prevMonth)
                                + " "
                                + String.valueOf((daysInPrevMonth
                                - trailingSpaces + DAY_OFFSET)
                                + i));

                list.add(String
                        .valueOf((daysInPrevMonth - trailingSpaces + DAY_OFFSET)
                                + i)
                        + "-GREY"
                        + "-"
                        + getMonthAsString(prevMonth)
                        + "-"
                        + prevYear);
            }

            // Current Month Days
            for (int i = 1; i <= daysInMonth; i++) {

                Common.DisplayLog(currentMonthName + " Abhijit", String.valueOf(i)
                        + " " + getMonthAsString(currentMonth) + " " + yy);

                /*if (i == getCurrentDayOfMonth()
                        && cun_sys_date[1]
                        .toString()
                        .trim()
                        .equalsIgnoreCase(String.valueOf(selectedDay))) {
                    list.add(String.valueOf(i) + "-SELECTBLUE" + "-"
                            + getMonthAsString(currentMonth) + "-" + yy);
                }else*/
                if (i == getCurrentDayOfMonth()
                        && cun_sys_date[1]
                        .toString()
                        .trim()
                        .equalsIgnoreCase(
                                getMonthAsString(currentMonth))) {
                    list.add(String.valueOf(i) + "-BLUE" + "-"
                            + getMonthAsString(currentMonth) + "-" + yy);
                } else if (i < getCurrentDayOfMonth()
                        && cun_sys_date[1]
                        .toString()
                        .trim()
                        .equalsIgnoreCase(
                                getMonthAsString(currentMonth))) {
                    list.add(String.valueOf(i) + "-LIGHTBLACK" + "-"
                            + getMonthAsString(currentMonth) + "-" + yy);
                } else {
                    list.add(String.valueOf(i) + "-WHITE" + "-"
                            + getMonthAsString(currentMonth) + "-" + yy);
                }
            }

            // Leading Month days
            for (int i = 0; i < list.size() % 7; i++) {

                Common.DisplayLog(tag, "NEXT MONTH:= " + getMonthAsString(nextMonth));

                list.add(String.valueOf(i + 1) + "-GREY" + "-"
                        + getMonthAsString(nextMonth) + "-" + nextYear);
            }
        }

        private HashMap<String, Integer> findNumberOfEventsPerMonth(int year,
                                                                    int month) {
            HashMap<String, Integer> map = new HashMap<String, Integer>();

            return map;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                LayoutInflater inflater = (LayoutInflater) _context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.calender_day, parent, false);
            }

            // Get a reference to the Day gridcell
            // gridcell = (Button) row.findViewById(R.id.calendar_day_gridcell);
            gridcell = (TextView) row.findViewById(R.id.day);
            gridcell.setOnClickListener(this);

            // ACCOUNT FOR SPACING


            Common.DisplayLog(tag, "Current Day: " + getCurrentDayOfMonth());

            String[] day_color = list.get(position).split("-");
            String theday = day_color[0];
            String themonth = day_color[2];
            String theyear = day_color[3];
            if ((!eventsPerMonthMap.isEmpty()) && (eventsPerMonthMap != null)) {
                if (eventsPerMonthMap.containsKey(theday)) {
                    // num_events_per_day = (TextView)
                    // row.findViewById(R.id.day);
                    Integer numEvents = (Integer) eventsPerMonthMap.get(theday);
                    gridcell.setText(numEvents.toString());
                }
            }

            // Set the Day GridCell
            gridcell.setText(theday);
            gridcell.setTag(theday + "-" + themonth + "-" + theyear + "-"
                    + day_color[1]);

            Common.DisplayLog(tag, "Setting GridCell " + theday + "-" + themonth + "-"
                    + theyear);


            if (day_color[1].equals("GREY")) {
                gridcell.setTextColor(_context.getResources().getColor(
                        R.color.calselected));
                gridcell.setEnabled(false);
            }
            if (day_color[1].equals("LIGHTBLACK")) {
                gridcell.setTextColor(_context.getResources()
                        .getColor(R.color.calcurpre));
                gridcell.setEnabled(false);
            }
            if (day_color[1].equals("WHITE")) {
                // gridcell.setTextColor(_context.getResources().getColor(R.color.white));
                gridcell.setTextColor(_context.getResources().getColor(R.color.calender_days_defaulttxt_color));
            }
            if (day_color[1].equals("BLUE")) {
//                gridcell.setTextColor(_context.getResources().getColor(R.color.white));
                gridcell.setBackgroundResource(R.drawable.calecun_bg);
            }
            /*if (day_color[1].equals("SELECTBLUE")) {
//                gridcell.setTextColor(_context.getResources().getColor(R.color.white));
//                row.setBackgroundResource(R.drawable.caleselected_bg);
                gridcell.performClick();
            }*/
            if (selectedDay > 0 && day_color[0].equals(String.valueOf(selectedDay))) {
                gridcell.setTextColor(_context.getResources().getColor(R.color.white));
                gridcell.setBackgroundResource(R.drawable.caleselected_bg);
//                gridcell.performClick();
            }
            return row;
        }

        @Override
        public void onClick(View view) {

            Date d = new Date();
            CharSequence cun = android.text.format.DateFormat.format("dd-MMMM-yyyy ", d.getTime());
            String[] cun_month = cun.toString().split("-");
            String date_month_year = (String) view.getTag();
            String[] sel_date = view.getTag().toString().split("-");
            Common.DisplayLog("date_month_year", date_month_year);
            for (int position = 1; position < getCount(); position++) {
                View vw = gridcalender.getChildAt(position);
                TextView tvday = (TextView) vw.findViewById(R.id.day);
                tvday.setBackgroundResource(R.color.white);
                tvday.setTextColor(_context.getResources().getColor(R.color.calender_days_defaulttxt_color));

                String[] tvday_month = tvday.getTag().toString().split("-");
                if (tvday.getTag().toString().trim()
                        .equalsIgnoreCase(cun.toString().trim())) {
                    tvday.setBackgroundResource(R.drawable.calecun_bg);
//                    tvday.setTextColor(_context.getResources().getColor(R.color.white));
                }

                if (!cun_month[1].toString().trim()
                        .equalsIgnoreCase(tvday_month[1].toString().trim())) {
                    tvday.setTextColor(_context.getResources().getColor(
                            R.color.calselected));
                }

                if (calmonth.getText().toString().trim()
                        .equalsIgnoreCase(tvday_month[1].toString().trim())) {
                    tvday.setTextColor(_context.getResources().getColor(R.color.calender_days_defaulttxt_color));
                }
                // -----------
                if (tvday_month[3].equals("GREY")) {
                    tvday.setTextColor(_context.getResources().getColor(
                            R.color.calselected));
                    tvday.setEnabled(false);
                }
                if (tvday_month[3].equals("LIGHTBLACK")) {
                    tvday.setTextColor(_context.getResources().getColor(
                            R.color.calcurpre));
                    tvday.setEnabled(false);
                }
                if (tvday_month[3].equals("WHITE")) {
                    // gridcell.setTextColor(_context.getResources().getColor(R.color.white));
//                    tvday.setTextColor(_context.getResources().getColor(R.color.calender_days_defaulttxt_color));
                    tvday.setTextColor(_context.getResources().getColor(R.color.calender_days_defaulttxt_color));
                }
                if (tvday_month[3].equals("BLUE")) {
//                    tvday.setTextColor(_context.getResources().getColor(R.color.white));
                    tvday.setBackgroundResource(R.drawable.calecun_bg);
                }

                /*selected*/
                Common.DisplayLog("cun_month[0]==tvday_month[0]", cun_month[0] + "==" + tvday_month[0]);

                if (sel_date[0].toString().trim()
                        .equalsIgnoreCase(tvday_month[0].toString().trim())) {
                    tvday.setTextColor(_context.getResources().getColor(
                            R.color.white));
                }

            }
//            view.setBackgroundResource(R.color.calselect);
            view.setBackgroundResource(R.drawable.caleselected_bg);
//        ConstantData.selmidate = date_month_year;
            String dt = sel_date[2] + sel_date[1] + sel_date[0];
            DateFormat formatter = new SimpleDateFormat("yyyyMMMMMdd");
            Date date = new Date();
            try {
                date = (Date) formatter.parse(dt.toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            calendarmonth = Calendar.getInstance();
            calendarmonth.setTime(date);
            calendarmonth.set(Calendar.HOUR_OF_DAY, 9);
            calendarmonth.set(Calendar.MINUTE, 00);
            calendarmonth.set(Calendar.SECOND, 0);
            singleDateAndTimePicker.selectDate(calendarmonth);
            txtDateTime.setText(dateUtl.setforDisplayTitle(singleDateAndTimePicker.getDate()));
            //Common.ShowToast(_context, date_month_year);

        }

        public int getCurrentDayOfMonth() {
            return currentDayOfMonth;
        }

        private void setCurrentDayOfMonth(int currentDayOfMonth) {
            this.currentDayOfMonth = currentDayOfMonth;
        }

        public void setCurrentWeekDay(int currentWeekDay) {
            this.currentWeekDay = currentWeekDay;
        }

        public int getCurrentWeekDay() {
            return currentWeekDay;
        }
    }


}
