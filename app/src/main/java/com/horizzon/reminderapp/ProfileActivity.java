package com.horizzon.reminderapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.horizzon.reminderapp.adapter.DrawerItemCustomAdapter;
import com.horizzon.reminderapp.app.MyRegButton;
import com.horizzon.reminderapp.app.MyRegTextView;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.helper.PrefManager;
import com.horizzon.reminderapp.model.DeactivUser;
import com.horizzon.reminderapp.model.UpdateUserProfile;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;
import com.horizzon.reminderapp.utility.ObjectDrawerItem;

import java.io.ByteArrayOutputStream;
import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    protected ListView mDrawerList;
    ObjectDrawerItem[] drawerItem;
    ImageView imgBack, imgOption, imgHome;
    CircleImageView imgProfile;
    String fileData = "";
    private PrefManager pref;
    TextView txtEdit, txtSignOut, txtDelAcnt;
    MyRegButton txtUpgrade, txtUpgradedata;
    EditText editName, editMail, editNameLast;
    MyRegTextView editNo;
    String mobile_no, name, email;
    String s1, userName, userLName, UserImage, UId, countryCode;
    SharedPreferences sh;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        imgOption = (ImageView) findViewById(R.id.imgOpt);
        imgBack = (ImageView) findViewById(R.id.backBtn);
        imgHome = (ImageView) findViewById(R.id.homeBtn);
        imgProfile = findViewById(R.id.imgProfile);
        txtEdit = (TextView) findViewById(R.id.txtEdit);
        txtSignOut = (TextView) findViewById(R.id.txtSignOut);
        txtDelAcnt = (TextView) findViewById(R.id.txtDelAcnt);
        txtUpgrade = findViewById(R.id.txtUpgrade);
        txtUpgradedata = findViewById(R.id.txtUpgradedata);

        editName = (EditText) findViewById(R.id.editName);
        editNameLast = (EditText) findViewById(R.id.editNameLast);
        editNo = (MyRegTextView) findViewById(R.id.editNo);
        editMail = (EditText) findViewById(R.id.editMail);

        pref = new PrefManager(this);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // list the drawer items
        drawerItem = new ObjectDrawerItem[1];

        drawerItem[0] = new ObjectDrawerItem(R.drawable.settingicon, "Settings");
        // drawerItem[1] = new ObjectDrawerItem(R.drawable.syncicon, "Sync Now");

        // Pass the folderData to our ListView adapter
        final DrawerItemCustomAdapter adapter1 = new DrawerItemCustomAdapter(this, R.layout.list_item_row, drawerItem);

        // Set the adapter for the list view
        mDrawerList.setAdapter(adapter1);
        mDrawerList.setSelection(0);
        // set the item click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        sh = getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        s1 = sh.getString("mobile", "");
        userName = sh.getString("userName", "");
        userLName = sh.getString("userLName", "");
        UserImage = sh.getString("userImage", "");
        countryCode = sh.getString("countryCode", "");

        UId = sh.getString("UId", "");
        if (!UserImage.equals("")) {
            String userPic = "https://www.hwpl.in/projects/reminderapp" + UserImage.substring(2);
            Glide.with(this).load(userPic).into(imgProfile);
        }
        imgOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imgHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ProfileActivity.this, HomeActivity.class);
                startActivity(in);
            }
        });
        editName.setText(userName);
        editNameLast.setText(userLName);
        editName.setClickable(false);
        editName.setEnabled(false);
        editNameLast.setClickable(false);
        editNameLast.setEnabled(false);
       /* editName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/

        editNo.setText(String.format("%s %s", "+"+countryCode, s1));
        txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // txtEdit.setEnabled(true);
                editName.setEnabled(true);
                editNameLast.setEnabled(true);
            }
        });

        txtSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pref.clearSession();
            }
        });

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });


        txtUpgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             //   txtEdit.setEnabled(true);
                editNameLast.setClickable(true);
                editNameLast.setEnabled(true);
                editName.setClickable(false);
                editName.setEnabled(true);
                //updateUserdata();
                txtUpgradedata.setVisibility(View.VISIBLE);
                txtUpgrade.setVisibility(View.GONE);
            }
        });

        txtUpgradedata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                updateUserdata();
            }
        });
        txtDelAcnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserDeactive();
            }
        });
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            selectItem(position);
            if (position == 0) {
                Intent in = new Intent(ProfileActivity.this, SettingsActivity.class);
                startActivity(in);
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    //  File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                    //.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, 1);
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 22);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                try {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    imgProfile.setImageResource(0);
                    imgProfile.setImageBitmap(photo);
                    // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                    Uri tempUri = getImageUri(getApplicationContext(), photo);
                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                    File finalFile = new File(getRealPathFromURI(tempUri));
                    Log.d("TAG", "onActivityResult: camrea img::" + finalFile);
                    fileData = getRealPathFromURI(tempUri);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 22) {
                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                Log.d("path gallery.", picturePath + "");
                imgProfile.setImageResource(0);
                imgProfile.setImageBitmap(thumbnail);
                Uri tempUri = getImageUri(getApplicationContext(), thumbnail);
                fileData = getRealPathFromURI(tempUri);
            }
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    /*
     * UserDeactive
     * */
    public void UserDeactive() {


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<DeactivUser> call = apiService.UserDeactive(UId);
        call.enqueue(new Callback<DeactivUser>() {
            @Override
            public void onResponse(Call<DeactivUser> call, Response<DeactivUser> response) {
                Log.d("TAG", "onResponse: " + response.body());
                pref.clearSession();
            }

            @Override
            public void onFailure(Call<DeactivUser> call, Throwable t) {
                Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
            }
        });

    }

    /*
     * User data Update
     * */
    public void updateUserdata() {
        MultipartBody.Part body = null;
        String username1 = editName.getText().toString();
        String username2 = editNameLast.getText().toString();

        String username = username1 + " & " + username2;
        if (!fileData.equals("")) {
            File file = new File(fileData);
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), file);
            // MultipartBody.Part is used to send also the actual file name
            body = MultipartBody.Part.createFormData("vUPhoto", file.getName(), requestFile);
        }
//vUHiddenPhoto
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody cCode = RequestBody.create(MediaType.parse("text/plain"), countryCode);
        RequestBody userOldImg = RequestBody.create(MediaType.parse("text/plain"), UserImage);
        RequestBody mobile1 = RequestBody.create(MediaType.parse("text/plain"), s1);
        RequestBody userName1 = RequestBody.create(MediaType.parse("text/plain"), username);
        RequestBody Uid = RequestBody.create(MediaType.parse("text/plain"), UId);
        Call<UpdateUserProfile> call = apiService.postUpdateProfile(cCode, Uid, mobile1, userName1, userOldImg, body);
        call.enqueue(new Callback<UpdateUserProfile>() {
            @Override
            public void onResponse(Call<UpdateUserProfile> call, Response<UpdateUserProfile> response) {
                UpdateUserProfile updateUserProfile = response.body();

                sh = getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
                editor = sh.edit();
                String userName = updateUserProfile.getData().getUserData().getVUName();
                String img = updateUserProfile.getData().getUserData().getVUPhoto();
                Log.d("TAG", "onResponse: img : " + img);
                editor.putString("userName", userName);
                editor.putString("userImage", img);
                editor.apply();
                Toast.makeText(ProfileActivity.this, response.message(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<UpdateUserProfile> call, Throwable t) {
                Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
            }
        });

    }

}