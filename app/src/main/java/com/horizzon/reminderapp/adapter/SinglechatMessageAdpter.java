package com.horizzon.reminderapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.model.AttachmentTypes;
import com.horizzon.reminderapp.model.GroupChat;
import com.horizzon.reminderapp.model.SingleChat;

import java.util.ArrayList;
import java.util.List;

public class SinglechatMessageAdpter extends RecyclerView.Adapter<BaseMessageViewHolder> {
    List<SingleChat> singleChats = new ArrayList<>();
    Context context;
    private View newMessages;
    public static final int MY = 0x00000000;
    public static final int OTHER = 0x0000100;
    private String myId;
    public SinglechatMessageAdpter(Context ctx,  String myId,List<SingleChat> singleList, View textComment) {
        this.singleChats = singleList;
        this.context = ctx;
        this.newMessages = textComment;
        this.myId = myId;
    }

  /*  @NonNull
    @Override
    public BaseMessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_singel_chat, parent, false);
        return new MessageTextViewHolder(LayoutInflater.from(context).inflate(R.layout.item_message_text, parent, false), newMessages, null);
       // viewType &= 0x00000FF;
       // switch (viewType) {
            *//*case AttachmentTypes.NONE_NOTIFICATION:
                return new MessageTextViewHolder(LayoutInflater.from(context).inflate(R.layout.item_message_notification, parent, false), newMessages, null);
            case AttachmentTypes.NONE_TEXT:
            default:*//*

      //  }
        //  return new MessageTextViewHolder(LayoutInflater.from(context).inflate(R.layout.item_message_text, parent, false), newMessages, null);
        // return new SinglechatMessageAdpter.MyViewHolder(itemView);
    }*/

    @NonNull
    @Override
    public BaseMessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        viewType &= 0x00000FF;
        switch (viewType) {
            /*case AttachmentTypes.RECORDING:
                return new MessageAttachmentRecordingViewHolder(LayoutInflater.from(context).inflate(R.layout.item_message_attachment_recording, parent, false), itemClickListener, recordingViewInteractor);
            case AttachmentTypes.AUDIO:
                return new MessageAttachmentAudioViewHolder(LayoutInflater.from(context).inflate(R.layout.item_message_attachment_audio, parent, false), itemClickListener);
            case AttachmentTypes.CONTACT:
                return new MessageAttachmentContactViewHolder(LayoutInflater.from(context).inflate(R.layout.item_message_attachment_contact, parent, false), itemClickListener);
            case AttachmentTypes.DOCUMENT:
                return new MessageAttachmentDocumentViewHolder(LayoutInflater.from(context).inflate(R.layout.item_message_attachment_document, parent, false), itemClickListener);
            case AttachmentTypes.IMAGE:
                return new MessageAttachmentImageViewHolder(LayoutInflater.from(context).inflate(R.layout.item_message_attachment_image, parent, false), itemClickListener);
            case AttachmentTypes.LOCATION:
                return new MessageAttachmentLocationViewHolder(LayoutInflater.from(context).inflate(R.layout.item_message_attachment_location, parent, false), itemClickListener);
            case AttachmentTypes.VIDEO:
                return new MessageAttachmentVideoViewHolder(LayoutInflater.from(context).inflate(R.layout.item_message_attachment_video, parent, false), itemClickListener);
            case AttachmentTypes.NONE_TYPING:
                return new MessageTypingViewHolder(LayoutInflater.from(context).inflate(R.layout.item_message_typing, parent, false));
            */
            case AttachmentTypes.NONE_NOTIFICATION:
                return new MessageTextViewHolder(LayoutInflater.from(context).inflate(R.layout.item_message_text, parent, false), newMessages, null);
            case AttachmentTypes.NONE_TEXT:
            default:
                return new MessageTextViewHolder(LayoutInflater.from(context).inflate(R.layout.item_message_text, parent, false), newMessages, null);
        }
    }
    @Override
    public void onBindViewHolder(@NonNull BaseMessageViewHolder holder, int position) {
      /*  SingleChat singleChat = singleChats.get(position);
        holder.txtChatSingle.setText(singleChat.getBody());*/
        holder.setSingleData(singleChats.get(position), position);
    }
    @Override
    public int getItemViewType(int position) {
        if (getItemCount() == 0) {
            return super.getItemViewType(position);
        } else {
            SingleChat message = singleChats.get(position);
            int userType;
            if (message.getSenderId().equals("+91"+myId))
                userType = MY;
            else
                userType = OTHER;
            return message.getAttachmentType() | userType;

        }
    }
    @Override
    public int getItemCount() {
        return singleChats.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        AppCompatTextView txtChatSingle;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txtChatSingle = itemView.findViewById(R.id.txtChatSingle);
        }
    }
}
