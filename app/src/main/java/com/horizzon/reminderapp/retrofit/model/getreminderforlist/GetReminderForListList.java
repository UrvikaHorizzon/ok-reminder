
package com.horizzon.reminderapp.retrofit.model.getreminderforlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetReminderForListList {

    @SerializedName("iId")
    @Expose
    private String iId;
    @SerializedName("iUserId")
    @Expose
    private String iUserId;
    @SerializedName("vSesssionId")
    @Expose
    private String vSesssionId;
    @SerializedName("vViewed")
    @Expose
    private String vViewed;
    @SerializedName("vCreatedBy")
    @Expose
    private String vCreatedBy;
    @SerializedName("vUpdatedBy")
    @Expose
    private String vUpdatedBy;
    @SerializedName("tUpdateNote")
    @Expose
    private String tUpdateNote;
    @SerializedName("vReminderStatus")
    @Expose
    private String vReminderStatus;
    @SerializedName("dCreatedDateTime")
    @Expose
    private String dCreatedDateTime;

    public String getIId() {
        return iId;
    }

    public void setIId(String iId) {
        this.iId = iId;
    }

    public String getIUserId() {
        return iUserId;
    }

    public void setIUserId(String iUserId) {
        this.iUserId = iUserId;
    }

    public String getVSesssionId() {
        return vSesssionId;
    }

    public void setVSesssionId(String vSesssionId) {
        this.vSesssionId = vSesssionId;
    }

    public String getVViewed() {
        return vViewed;
    }

    public void setVViewed(String vViewed) {
        this.vViewed = vViewed;
    }

    public String getVCreatedBy() {
        return vCreatedBy;
    }

    public void setVCreatedBy(String vCreatedBy) {
        this.vCreatedBy = vCreatedBy;
    }

    public String getVUpdatedBy() {
        return vUpdatedBy;
    }

    public void setVUpdatedBy(String vUpdatedBy) {
        this.vUpdatedBy = vUpdatedBy;
    }

    public String gettUpdateNote() {
        return tUpdateNote;
    }

    public void settUpdateNote(String tUpdateNote) {
        this.tUpdateNote = tUpdateNote;
    }

    public String getvReminderStatus() {
        return vReminderStatus;
    }

    public void setvReminderStatus(String vReminderStatus) {
        this.vReminderStatus = vReminderStatus;
    }

    public String getDCreatedDateTime() {
        return dCreatedDateTime;
    }

    public void setDCreatedDateTime(String dCreatedDateTime) {
        this.dCreatedDateTime = dCreatedDateTime;
    }

}
