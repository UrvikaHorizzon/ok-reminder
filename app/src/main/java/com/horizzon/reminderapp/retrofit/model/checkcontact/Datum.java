
package com.horizzon.reminderapp.retrofit.model.checkcontact;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("vUNumber")
    @Expose
    private String vUNumber;
    @SerializedName("vUContryNumber")
    @Expose
    private String vUContryNumber;
    @SerializedName("vUEmail")
    @Expose
    private String vUEmail;

    public String getVUNumber() {
        return vUNumber;
    }

    public void setVUNumber(String vUNumber) {
        this.vUNumber = vUNumber;
    }

    public String getVUContryNumber() {
        return vUContryNumber;
    }

    public void setVUContryNumber(String vUContryNumber) {
        this.vUContryNumber = vUContryNumber;
    }

    public String getVUEmail() {
        return vUEmail;
    }

    public void setVUEmail(String vUEmail) {
        this.vUEmail = vUEmail;
    }

}
