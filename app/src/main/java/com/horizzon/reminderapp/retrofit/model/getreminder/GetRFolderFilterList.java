
package com.horizzon.reminderapp.retrofit.model.getreminder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetRFolderFilterList {

    @SerializedName("iId")
    @Expose
    private String iId;
    @SerializedName("iUserId")
    @Expose
    private String iUserId;
    @SerializedName("vSesssionId")
    @Expose
    private String vSesssionId;
    @SerializedName("iFolderId")
    @Expose
    private String iFolderId;
    @SerializedName("dCreatedDateTime")
    @Expose
    private String dCreatedDateTime;

    public String getIId() {
        return iId;
    }

    public void setIId(String iId) {
        this.iId = iId;
    }

    public String getIUserId() {
        return iUserId;
    }

    public void setIUserId(String iUserId) {
        this.iUserId = iUserId;
    }

    public String getVSesssionId() {
        return vSesssionId;
    }

    public void setVSesssionId(String vSesssionId) {
        this.vSesssionId = vSesssionId;
    }

    public String getIFolderId() {
        return iFolderId;
    }

    public void setIFolderId(String iFolderId) {
        this.iFolderId = iFolderId;
    }

    public String getDCreatedDateTime() {
        return dCreatedDateTime;
    }

    public void setDCreatedDateTime(String dCreatedDateTime) {
        this.dCreatedDateTime = dCreatedDateTime;
    }

}
