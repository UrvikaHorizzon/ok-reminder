package com.horizzon.reminderapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horizzon.reminderapp.dao.CReminder;
import com.horizzon.reminderapp.dao.ReminderDateTime;
import com.horizzon.reminderapp.dao.ReminderLocation;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.CRDateTimeDbHandler;
import com.horizzon.reminderapp.handler.CRLocationDbHandler;
import com.horizzon.reminderapp.handler.CreateReminderDbHandler;
import com.horizzon.reminderapp.retrofit.model.reminder.Reminder;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;
import com.horizzon.reminderapp.utility.DateUtilitys;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditReminderActivity extends AppCompatActivity {

    String SessionId,action;
    EditText eSubject;
    TextView eDateTime,eLocation;
    ImageView backBtn,homeBtn, imgPriority1, imgPriority2, imgPriority3,imgCancel,imgYes;
    public int prio1_flag = 1,prio2_flag = 1,prio3_flag = 1;
    CreateReminderDbHandler crdbh;
    Button btnTempDone;
    LinearLayout doneContainer,laycontact;

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_reminder);

        SessionId = getIntent().getStringExtra("SessionId");

        if(SessionId==null || SessionId.equals("")){
            onBackPressed();
        }


        action = getIntent().getStringExtra("action");
        if (action == null || action.equals("")) {
            action = "edit";
        }


        crdbh = new CreateReminderDbHandler(getApplicationContext());

        eSubject = (EditText) findViewById(R.id.eSubject);
        eDateTime = (TextView) findViewById(R.id.eDateTime);
        eLocation = (TextView) findViewById(R.id.eLocation);
        imgPriority1 = (ImageView) findViewById(R.id.imgPriority1);
        imgPriority2 = (ImageView) findViewById(R.id.imgPriority2);
        imgPriority3 = (ImageView) findViewById(R.id.imgPriority3);
        imgCancel = (ImageView) findViewById(R.id.imgCancel);
        imgYes = (ImageView) findViewById(R.id.imgYes);

        backBtn = (ImageView) findViewById(R.id.backBtn);
        homeBtn = (ImageView) findViewById(R.id.homeBtn);

        laycontact = (LinearLayout) findViewById(R.id.laycontact);
        doneContainer = (LinearLayout) findViewById(R.id.doneContainer);
        doneContainer.setVisibility(View.GONE);


        btnTempDone = (Button) findViewById(R.id.btnTempDone);

//        imgPriority1.setVisibility(View.GONE);
//        imgPriority2.setVisibility(View.GONE);
//        imgPriority3.setVisibility(View.GONE);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(in);
            }
        });


        eSubject.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Common.DisplayLog("", "beforeTextChanged");
                Common.DisplayLog("", "beforeTextChanged == " + s.toString());
                crdbh.updataSubject(SessionId, s.toString());
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Common.DisplayLog("", "onTextChanged");
                Common.DisplayLog("", "onTextChanged == " + s.toString());
                crdbh.updataSubject(SessionId, s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                Common.DisplayLog("", "afterTextChanged");
                Common.DisplayLog("", "afterTextChanged == " + s.toString());
                crdbh.updataSubject(SessionId, s.toString());
            }
        });

        eSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDoneLayout(eSubject, v);
            }
        });
        eSubject.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
//                subjectLayout.performClick();
                Common.DisplayLog("", "setOnFocusChangeListener");
                crdbh.updataSubject(SessionId, eSubject.getText().toString());
//                if (hasFocus)
                {
                    showDoneLayout(eSubject, v);
                }
            }
        });




        imgPriority1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prio1_flag == 1) {
                    imgPriority1.setImageResource(R.drawable.priorityiconyellow);
                    imgPriority2.setImageResource(R.drawable.priorityicon);
                    imgPriority3.setImageResource(R.drawable.priorityicon);
                    prio1_flag = 0;
                } else {
                    imgPriority1.setImageResource(R.drawable.priorityicon);
                    imgPriority2.setImageResource(R.drawable.priorityicon);
                    imgPriority3.setImageResource(R.drawable.priorityicon);
                    prio1_flag = 1;
                }
                prio2_flag = 1;
                prio3_flag = 1;
                crdbh.updataPriority(SessionId, "1");
            }
        });

        imgPriority2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prio2_flag == 1) {
                    imgPriority1.setImageResource(R.drawable.priorityiconyellow);
                    imgPriority2.setImageResource(R.drawable.priorityiconyellow);
                    imgPriority3.setImageResource(R.drawable.priorityicon);
                    prio2_flag = 0;
                } else {
                    imgPriority1.setImageResource(R.drawable.priorityicon);
                    imgPriority2.setImageResource(R.drawable.priorityicon);
                    imgPriority3.setImageResource(R.drawable.priorityicon);
                    prio2_flag = 1;
                }
                prio1_flag = 1;
                prio3_flag = 1;
                crdbh.updataPriority(SessionId, "2");
            }
        });

        imgPriority3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prio3_flag == 1) {
                    imgPriority1.setImageResource(R.drawable.priorityiconyellow);
                    imgPriority2.setImageResource(R.drawable.priorityiconyellow);
                    imgPriority3.setImageResource(R.drawable.priorityiconyellow);
                    prio3_flag = 0;
                } else {
                    imgPriority1.setImageResource(R.drawable.priorityicon);
                    imgPriority2.setImageResource(R.drawable.priorityicon);
                    imgPriority3.setImageResource(R.drawable.priorityicon);
                    prio3_flag = 1;
                }
                prio1_flag = 1;
                prio2_flag = 1;
                crdbh.updataPriority(SessionId, "3");
            }
        });


        eDateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), DateTimeActivity.class);
                in.putExtra("action","edit");
                startActivity(in);

            }
        });
        eLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), LocationActivity.class);
                in.putExtra("SessionId", SessionId);
                in.putExtra("action","edit");
                startActivity(in);
            }
        });
        laycontact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), ContactActivity.class);
                in.putExtra("SessionId", SessionId);
                in.putExtra("action", "edit");
                in.putExtra("moveto", "1");
                startActivity(in);
            }
        });
        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        imgYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SavedEditDataOnServer(SessionId);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        loadSavedData(SessionId);
    }

    private void showDoneLayout(EditText edt, final View v) {

        Common.DisplayLog("showDoneLayout", "showDoneLayout");
//        edt.setFocusable(true);
        edt.requestFocus();
        edt.setCursorVisible(true);
        edt.setSelection(edt.getText().length());
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.showSoftInput(edt, InputMethodManager.SHOW_IMPLICIT);
        doneContainer.setVisibility(View.VISIBLE);
//        footerContainer.setVisibility(View.GONE);


        btnTempDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String notesub = eSubject.getText().toString().trim();
                Common.DisplayLog("setOnEditorActionListener", notesub + "");
                crdbh.updataSubject(SessionId, notesub.toString());
                eSubject.setCursorVisible(false);
                doneContainer.setVisibility(View.GONE);
//                footerContainer.setVisibility(View.VISIBLE);
                InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(eSubject.getWindowToken(), 0);

            }
        });

    }


    public void loadSavedData(String SessId) {


        Common.DisplayLog("", "loadSavedData " + SessId);
        CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getApplicationContext());
//        RContactDbHandler rcdbh = new RContactDbHandler(getApplicationContext());


        CReminder cReminder = crdbh.getTableSingleDateWihtDao(SessId);


        Common.DisplayLog("cReminder.getSubject()", cReminder.getSubject() + "");
        // set Subject
        eSubject.setText(cReminder.getSubject());

        // set Priority
        if (cReminder.getPriority().equals("1")) {
            imgPriority1.performClick();
        } else if (cReminder.getPriority().equals("2")) {
            imgPriority2.performClick();
        } else if (cReminder.getPriority().equals("3")) {
            imgPriority3.performClick();
        }


        // set Contact
/*
        CRContactDbHandler crcdbh = new CRContactDbHandler(getApplicationContext());
        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllCurrentSessContactData(SessId, "contact");
//        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllContactData();

        Common.DisplayLog("remindercontactuser", remindercontactuser.size() + "");
*/

        // set Attachment
        /*CRAttachmentDbHandler cradbh = new CRAttachmentDbHandler(getApplicationContext());
        Common.DisplayLog("cradbh.getDataCount(SessId)", cradbh.getDataCount(SessId, "done") + "");
        attachcount.setText(cradbh.getDataCount(SessId, "done") + "");*/

        // set Comment

        /*CRCommentDbHandler crcodbh = new CRCommentDbHandler(getApplicationContext());
        Common.DisplayLog("crcodbh.getDataCount(SessionId)", crcodbh.getDataCount(SessId) + "");
        commentcount.setText(crcodbh.getDataCount(SessId) + "");
        if (crcodbh.getDataCount(SessId) > 0) {
            tabcomment.performClick();
        }*/

        // set Date Time
        CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(getApplicationContext());
        ReminderDateTime rdt = crdtdbh.getTableSingleDateWihtDao(SessId);
        Common.DisplayLog("crdtdbh.getDataCount(SessionId)", crdtdbh.getDataCount(SessId) + "");


        DateUtilitys dateUtl = new DateUtilitys();

        Date date;
        if (crdtdbh.getDataCount(SessId) > 0) {
            date = dateUtl.FormetDate(rdt.getStartDate() + " " + rdt.getStartTime());
        } else {
            Common.DisplayLog("cr.getCreateddate()", cReminder.getCreateddate() + "");
            date = dateUtl.FormetDate(cReminder.getCreateddate());

        }
        dateUtl.setforDisplayTitle(date);
        eDateTime.setText(dateUtl.getMonth() + " " + dateUtl.getDay() + " " + dateUtl.getYear() + ", " + dateUtl.getHour() + ":" + dateUtl.getMin() + " " + dateUtl.getAmpm());

        // set Location
        CRLocationDbHandler crldbh = new CRLocationDbHandler(getApplicationContext());
        Common.DisplayLog("crldbh.getDataCount(SessionId)", crldbh.getDataCount(SessionId) + "");
        if (crldbh.getDataCount(SessionId) > 0) {
            ReminderLocation rl = crldbh.getTableSingleDateWihtDao(SessId);
            eLocation.setText(rl.getLocationDetail());
        }

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        Common.DisplayLog("tag", "This'll run 300 milliseconds later");
                        eSubject.setCursorVisible(false);
                        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(eSubject.getWindowToken(), 0);

                        doneContainer.setVisibility(View.GONE);
                    }
                },
                200);
    }

    public void SavedEditDataOnServer(String SessId) {


        /***  Save main reminder ****/
        CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getApplicationContext().getApplicationContext());
        CReminder cReminder = crdbh.getTableSingleDateWihtDao(SessId);

        if (Common.hasInternetConnection(getApplicationContext().getApplicationContext())) {
            crdbh.updataStatus(SessId, "done");
            Common.DisplayLog("", "loadSavedData " + SessId);
            if (cReminder != null && cReminder.getSubject() != null && !cReminder.getSubject().equalsIgnoreCase("") && !eSubject.getText().toString().trim().equals("")) {
                cReminder = crdbh.getTableSingleDateWihtDao(SessId);

                Common.DisplayLog("SessId", SessId + "");
                pd = new ProgressDialog(EditReminderActivity.this);
                pd.setMessage("Loading...");
                pd.setIndeterminate(true);
                pd.setCancelable(false);
                pd.show();
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                Call<Reminder> call = apiService.editReminder(cReminder.getSesssionId(), cReminder.getiUserId(), cReminder.getSubject(), cReminder.getPriority(), cReminder.getReminderFor(), "done", cReminder.getLockstatus(), cReminder.getExtra());
                call.enqueue(new Callback<Reminder>() {
                    @Override
                    public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                        int statusCode = response.code();
                        Common.DisplayLog("response.body()", response.body().toString());
                        boolean status = response.body().getStatus();
                        Common.DisplayLog("status", status + "");
                        Common.DisplayLog("getMessage", response.body().getMessage() + "");

                        if (status) {
//                        Common.ShowToast(getApplicationContext().getApplicationContext(), response.body().getMessage());
                        } else {
                            Common.ShowToast(getApplicationContext().getApplicationContext(), response.body().getMessage());

                        }
                        pd.dismiss();
                        onBackPressed();
                    }

                    @Override
                    public void onFailure(Call<Reminder> call, Throwable t) {
                        Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
                        pd.dismiss();
                        onBackPressed();
                    }


                });

                //clodeloding();
            } else {
                Common.ShowToast(getApplicationContext().getApplicationContext(), "Please Enter some keyword in subject");
            }
//            crdbh.resetTempstatusdata();
        }

    }

    public void clodeloding() {


//        if (sreminder && scontect && sdate && sloction && sattachment && scomment)
        if (true)
        {
            pd.dismiss();
            crdbh.resetTempstatusdata();
            Intent in = new Intent(getApplicationContext(), HomeActivity.class);
            startActivity(in);
        } else {
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            clodeloding();
                        }
                    },
                    2000);
        }
    }
}
