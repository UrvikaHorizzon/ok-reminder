package com.horizzon.reminderapp.utility;

/**
 * Created by horizzon on 12/16/2016.
 */
public class CommentItem {
    public String reminder_name, reminder_comment, reminder_comment_by, reminder_img_name;

    public String getReminder_name() {
        return reminder_name;
    }

    public void setReminder_name(String reminder_name) {
        this.reminder_name = reminder_name;
    }

    public String getReminder_comment() {
        return reminder_comment;
    }

    public void setReminder_comment(String reminder_comment) {
        this.reminder_comment = reminder_comment;
    }

    public String getReminder_comment_by() {
        return reminder_comment_by;
    }

    public void setReminder_comment_by(String reminder_comment_by) {
        this.reminder_comment_by = reminder_comment_by;
    }

    public String getReminder_img_name() {
        return reminder_img_name;
    }

    public void setReminder_img_name(String reminder_img_name) {
        this.reminder_img_name = reminder_img_name;
    }
}
