package com.horizzon.reminderapp.handler;

/**
 * Created by Abhijit Sojitra on 3/4/13.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.horizzon.reminderapp.data.Common;


public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "reminderapp";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        Common.DisplayLog("columnNames", "db create");

        //CREATE NOTIFICATION TABLE
//        db.execSQL(UserDbHandler.CREATE_USER_TABLE);
        db.execSQL(CRAttachmentDbHandler.CREATE_TABLE);
        db.execSQL(CRCommentDbHandler.CREATE_TABLE);
        db.execSQL(CRContactDbHandler.CREATE_TABLE);
        db.execSQL(CRDateTimeDbHandler.CREATE_TABLE);
        db.execSQL(CRLocationDbHandler.CREATE_TABLE);
        db.execSQL(CreateReminderDbHandler.CREATE_TABLE);
        db.execSQL(RContactDbHandler.CREATE_TABLE);
        db.execSQL(RUserDbHandler.CREATE_TABLE);
        db.execSQL(RUserAddressDbHandler.CREATE_TABLE);
        db.execSQL(CReminderForListDbHandler.CREATE_TABLE);
        db.execSQL(CRMakeAdminDbHandler.CREATE_TABLE);
        db.execSQL(RUserFolderDbHandler.CREATE_TABLE);
        db.execSQL(RFolderFilterDbHandler.CREATE_TABLE);
        db.execSQL(CRSessoinIdDbHandler.CREATE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Common.DisplayLog("columnNames", "db update");
        // Drop older table if existed
//		db.execSQL("DROP TABLE IF EXISTS " + UserDbHandler.TABLE_STUDENT);
		db.execSQL("DROP TABLE IF EXISTS " + CRAttachmentDbHandler.TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + CRCommentDbHandler.TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + CRContactDbHandler.TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + CRDateTimeDbHandler.TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + CreateReminderDbHandler.TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + RContactDbHandler.TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + RUserDbHandler.TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + RUserAddressDbHandler.TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + CRMakeAdminDbHandler.TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + RUserFolderDbHandler.TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + RFolderFilterDbHandler.TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + CRSessoinIdDbHandler.TABLE_NAME);
        // Create tables again
        onCreate(db);
    }

}
