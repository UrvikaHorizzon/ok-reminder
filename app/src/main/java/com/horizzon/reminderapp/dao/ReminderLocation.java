package com.horizzon.reminderapp.dao;

/**
 * Created by horizzon on 4/3/2017.
 */

public class ReminderLocation {

    private String Id;
    private String sesssionId;
    private String iUserId;
    private String locationFor;
    private String locationDetail;
    private String lat;
    private String lon;
    private String areaFor;
    private String areaSize;
    private String areaUnit;
    private String status;

    public ReminderLocation() {
    }

    public ReminderLocation(String id, String sesssionId, String iUserId, String locationFor, String locationDetail, String lat, String lon, String areaFor, String areaSize, String areaUnit, String status) {
        Id = id;
        this.sesssionId = sesssionId;
        this.iUserId = iUserId;
        this.locationFor = locationFor;
        this.locationDetail = locationDetail;
        this.lat = lat;
        this.lon = lon;
        this.areaFor = areaFor;
        this.areaSize = areaSize;
        this.areaUnit = areaUnit;
        this.status = status;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getSesssionId() {
        return sesssionId;
    }

    public void setSesssionId(String sesssionId) {
        this.sesssionId = sesssionId;
    }

    public String getiUserId() {
        return iUserId;
    }

    public void setiUserId(String iUserId) {
        this.iUserId = iUserId;
    }

    public String getLocationFor() {
        return locationFor;
    }

    public void setLocationFor(String locationFor) {
        this.locationFor = locationFor;
    }

    public String getLocationDetail() {
        return locationDetail;
    }

    public void setLocationDetail(String locationDetail) {
        this.locationDetail = locationDetail;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getAreaFor() {
        return areaFor;
    }

    public void setAreaFor(String areaFor) {
        this.areaFor = areaFor;
    }

    public String getAreaSize() {
        return areaSize;
    }

    public void setAreaSize(String areaSize) {
        this.areaSize = areaSize;
    }

    public String getAreaUnit() {
        return areaUnit;
    }

    public void setAreaUnit(String areaUnit) {
        this.areaUnit = areaUnit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
