package com.horizzon.reminderapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.retrofit.model.GetFolderNameList;

import java.util.ArrayList;
import java.util.List;

public class NewFolderAdapter extends RecyclerView.Adapter<NewFolderAdapter.MyViewHolder> {
    List<GetFolderNameList.Datum> list = new ArrayList<>();
    Context context;
    OnClick onClick;
    private int lastSelectedPosition = -1;

    public NewFolderAdapter(Context ctx, List<GetFolderNameList.Datum> lists, OnClick onClik) {
        this.context = ctx;
        this.list = lists;
        this.onClick = onClik;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_folder_name, parent, false);
        return new NewFolderAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        GetFolderNameList.Datum str = list.get(position);
        holder.rdGrpNewFolderName.setText(str.getVFoldername());
        //holder.rdGrpNewFolderName.
        holder.rdGrpNewFolderName.setChecked(lastSelectedPosition == position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        RadioButton rdGrpNewFolderName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            rdGrpNewFolderName = itemView.findViewById(R.id.rdGrpNewFolderName);
            rdGrpNewFolderName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String selectedButton = rdGrpNewFolderName.getText().toString();
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                    onClick.ClickData(getAdapterPosition(), selectedButton);
                }
            });

        }
    }

    public interface OnClick {
        void ClickData(int postion, String text);
    }

}
