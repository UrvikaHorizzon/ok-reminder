package com.horizzon.reminderapp.dao;

/**
 * Created by horizzon on 4/3/2017.
 */

public class RFolderFilter {

    private String iId;
    private String iUserId;
    private String vSesssionId;
    private String iFolderId;
    private String dCreatedDateTime;

    public RFolderFilter() {
    }

    public RFolderFilter(String iId, String iUserId, String vSesssionId, String iFolderId, String dCreatedDateTime) {
        this.iId = iId;
        this.iUserId = iUserId;
        this.vSesssionId = vSesssionId;
        this.iFolderId = iFolderId;
        this.dCreatedDateTime = dCreatedDateTime;
    }

    public String getiId() {
        return iId;
    }

    public void setiId(String iId) {
        this.iId = iId;
    }

    public String getiUserId() {
        return iUserId;
    }

    public void setiUserId(String iUserId) {
        this.iUserId = iUserId;
    }

    public String getvSesssionId() {
        return vSesssionId;
    }

    public void setvSesssionId(String vSesssionId) {
        this.vSesssionId = vSesssionId;
    }

    public String getiFolderId() {
        return iFolderId;
    }

    public void setiFolderId(String iFolderId) {
        this.iFolderId = iFolderId;
    }

    public String getdCreatedDateTime() {
        return dCreatedDateTime;
    }

    public void setdCreatedDateTime(String dCreatedDateTime) {
        this.dCreatedDateTime = dCreatedDateTime;
    }
}
