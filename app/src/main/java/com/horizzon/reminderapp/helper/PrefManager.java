package com.horizzon.reminderapp.helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.horizzon.reminderapp.HomeActivity;
import com.horizzon.reminderapp.SmsActivity;
import com.horizzon.reminderapp.dao.ReminderUser;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.RUserDbHandler;

import java.util.HashMap;

/**
 * Created by horizzon on 10/3/2016.
 */
public class PrefManager {
    // Shared Preferences
    SharedPreferences pref;
    // Editor for Shared preferences
    Editor editor;
    // Context
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Shared preferences file name
    private static final String PREF_NAME = "AndroidHive";
    // All Shared Preferences Keys
    private static final String KEY_IS_WAITING_FOR_SMS = "IsWaitingForSms";
    private static final String KEY_MOBILE_NUMBER = "mobile_number";
    private static final String KEY_MOBILE_CODE = "mobile_code";
    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    private static final String KEY_NAME = "name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_MOBILE = "mobile";
    public static final String KEY_USERID = "uid";

    RUserDbHandler rudbh;


    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        rudbh = new RUserDbHandler(context);
    }

    public void setIsWaitingForSms(boolean isWaiting) {
        editor.putBoolean(KEY_IS_WAITING_FOR_SMS, isWaiting);
        editor.commit();
        editor.putBoolean(KEY_IS_LOGGED_IN, true);
    }

    public boolean isWaitingForSms() {
        return pref.getBoolean(KEY_IS_WAITING_FOR_SMS, false);
    }

    public void setMobileNumber(String mobileNumber, String mobileCode) {
        editor.putString(KEY_MOBILE_NUMBER, mobileNumber);
        editor.putString(KEY_MOBILE_CODE, mobileCode);
        editor.putBoolean(KEY_IS_LOGGED_IN, true);
        editor.commit();
    }

    public String getMobileNumber() {
        return pref.getString(KEY_MOBILE_NUMBER, null);
    }

    public String getMobileCode() {
        return pref.getString(KEY_MOBILE_CODE, null);
    }

    public void createLogin(String id) {

        ReminderUser reminderUser = rudbh.getTableSingleDateWihtDao(id);

        if (reminderUser != null) {


            editor.putString(KEY_USERID, reminderUser.getUserId());
            editor.putString(KEY_NAME, reminderUser.getuName());
            editor.putString(KEY_EMAIL, reminderUser.getuEmail());
            editor.putString(KEY_MOBILE, reminderUser.getuNumber());
            editor.putBoolean(KEY_IS_LOGGED_IN, true);
            editor.commit();
        } else {
            editor.putBoolean(KEY_IS_LOGGED_IN, false);
        }
        editor.commit();
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public void checkLogin() {
        ReminderUser reminderUser = rudbh.getTableSingleDateWihtDao(Common.getSharedPreferences(_context, "userId", "0"));
        // Check login status
        boolean st = false;
        if (!this.isLoggedIn()) {
            st = true;
        } else if (reminderUser == null) {
            st = true;
        }
        if (st) {
            editor.putBoolean(KEY_IS_LOGGED_IN, false);
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, SmsActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }
        editor.commit();
        /*else{
            Intent intent = new Intent(_context, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            _context.startActivity(intent);
        }*/

    }

    public void checkIsLogin() {
        // Check login status
        if (this.isLoggedIn()) {
            Intent intent = new Intent(_context, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            _context.startActivity(intent);
        }

    }

    public void clearSession() {
        editor.clear();
        editor.commit();

        Intent i = new Intent(_context, SmsActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> profile = new HashMap<>();
        profile.put("name", pref.getString(KEY_NAME, null));
        profile.put("email", pref.getString(KEY_EMAIL, null));
        profile.put("mobile", pref.getString(KEY_MOBILE, null));
        return profile;
    }
}
