package com.horizzon.reminderapp.helper.customgallery;

import android.content.Context;
import android.graphics.Rect;
import androidx.annotation.DimenRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

/**
 * Created by Abhijit Sojitra on 21/6/16.
 */

public class CustomGalleryItemOffsetDecoration extends RecyclerView.ItemDecoration {

    private int mItemOffset;

    public CustomGalleryItemOffsetDecoration(int itemOffset) {
        mItemOffset = itemOffset;
    }

    public CustomGalleryItemOffsetDecoration(@NonNull Context context, @DimenRes int itemOffsetId) {
        this(context.getResources().getDimensionPixelSize(itemOffsetId));
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.set(mItemOffset, mItemOffset, mItemOffset, mItemOffset);
    }
}
