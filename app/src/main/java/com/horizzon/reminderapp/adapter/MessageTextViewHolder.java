package com.horizzon.reminderapp.adapter;

import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.interfacess.OnMessageItemClick;
import com.horizzon.reminderapp.model.AttachmentTypes;
import com.horizzon.reminderapp.model.GroupChat;
import com.horizzon.reminderapp.model.SingleChat;


public class MessageTextViewHolder extends BaseMessageViewHolder {
    private TextView text;
    CardView cardView;
    // private Message message;
    private GroupChat message;

    public MessageTextViewHolder(View itemView, View newMessageView, OnMessageItemClick itemClickListener) {
        super(itemView, newMessageView, itemClickListener);
        text = itemView.findViewById(R.id.text);
        cardView = itemView.findViewById(R.id.card_view);
       /* text.setTransformationMethod(new LinkTransformationMethod());
        text.setMovementMethod(ClickableMovementMethod.getInstance());*/
        // Reset for TextView.fixFocusableAndClickableSettings(). We don't want View.onTouchEvent()
        // to consume touch events.
        text.setClickable(false);
        text.setLongClickable(false);

      /*  itemView.setOnClickListener(v -> {
            if (message != null && message.getAttachmentType() == AttachmentTypes.NONE_NOTIFICATION)
                return;
            onItemClick(true);
        });
        itemView.setOnLongClickListener(v -> {
            if (message != null && message.getAttachmentType() == AttachmentTypes.NONE_NOTIFICATION)
                return true;
            onItemClick(false);
            return true;
        });*/
    }

    @Override
    public void setData(GroupChat message, int position) {
        if (message.getBody() != null || !message.getBody().matches("")) {
            super.setData(message, position);
            this.message = message;
            LinearLayout.LayoutParams textParams = (LinearLayout.LayoutParams) text.getLayoutParams();
            if (message.getAttachmentType() == AttachmentTypes.NONE_TEXT) {
                text.setGravity(isMine() ? Gravity.END : Gravity.START);
                textParams.gravity = isMine() ? Gravity.END : Gravity.START;
            }
            text.setLayoutParams(textParams);
            text.setTextColor(ContextCompat.getColor(context, message.getAttachmentType() == AttachmentTypes.NONE_NOTIFICATION ? R.color.card_notification_color_text : isMine() ? android.R.color.black : android.R.color.black));
            text.setText(message.getBody());
            time.setVisibility(message.getAttachmentType() == AttachmentTypes.NONE_NOTIFICATION ? View.GONE : View.VISIBLE);
        } else {
            cardView.setVisibility(View.GONE);
        }
    }

    @Override
    public void setSingleData(SingleChat message, int position) {
        if (message.getBody() != null || !message.getBody().matches("")) {
            super.setSingleData(message, position);
            LinearLayout.LayoutParams textParams = (LinearLayout.LayoutParams) text.getLayoutParams();
            if (message.getAttachmentType() == AttachmentTypes.NONE_TEXT) {
                text.setGravity(isMine() ? Gravity.END : Gravity.START);
                textParams.gravity = isMine() ? Gravity.END : Gravity.START;
            }
            text.setLayoutParams(textParams);
            text.setTextColor(ContextCompat.getColor(context, android.R.color.black));
            text.setText(message.getBody());
            time.setVisibility(message.getAttachmentType() == AttachmentTypes.NONE_NOTIFICATION ? View.GONE : View.VISIBLE);
        }else {
            cardView.setVisibility(View.GONE);
        }
    }
}
