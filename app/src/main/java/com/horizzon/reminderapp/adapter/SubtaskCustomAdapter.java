package com.horizzon.reminderapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.toolbox.ImageLoader;
import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.utility.SubtaskItem;

import java.util.ArrayList;

/**
 * Created by Horizzon Admin on 2/28/2017.
 */
public class SubtaskCustomAdapter extends BaseAdapter{
    Activity context;
    private ImageLoader imageLoader;
    ArrayList<SubtaskItem> list;

    public SubtaskCustomAdapter(Activity context, ArrayList<SubtaskItem> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Holder holder;

        if(convertView==null)
        {
            holder = new Holder();

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.customsubtask, null);

            holder.txtSubtask = (EditText) convertView.findViewById(R.id.txtSubtask);
            holder.imgTaskEdit = (ImageView) convertView.findViewById(R.id.imgTaskEdit);

            convertView.setTag(holder);
        }
        else
        {
            holder = (Holder) convertView.getTag();
        }

        SubtaskItem ud = list.get(position);
        holder.txtSubtask.setText(ud.getSubtask_name());
        holder.imgTaskEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                holder.txtSubtask.setEnabled(true);
//                holder.txtSubtask.setCursorVisible(true);
//                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.showSoftInput(holder.txtSubtask, InputMethodManager.SHOW_IMPLICIT);
            }
        });
        return convertView;
    }

    public class Holder
    {
        EditText txtSubtask;
        ImageView imgTaskEdit;
    }
}
