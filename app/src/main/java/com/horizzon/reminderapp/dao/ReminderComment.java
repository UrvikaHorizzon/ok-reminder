package com.horizzon.reminderapp.dao;

/**
 * Created by horizzon on 4/3/2017.
 */

public class ReminderComment {

    private String Id;
    private String sesssionId;
    private String userId;
    private String comment;
    private String tCommentFor;
    private String commentdate;

    public ReminderComment() {
    }

    public ReminderComment(String id, String sesssionId, String userId, String comment, String tCommentFor, String commentdate) {
        Id = id;
        this.sesssionId = sesssionId;
        this.userId = userId;
        this.comment = comment;
        this.tCommentFor = tCommentFor;
        this.commentdate = commentdate;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getSesssionId() {
        return sesssionId;
    }

    public void setSesssionId(String sesssionId) {
        this.sesssionId = sesssionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String gettCommentFor() {
        return tCommentFor;
    }

    public void settCommentFor(String tCommentFor) {
        this.tCommentFor = tCommentFor;
    }

    public String getCommentdate() {
        return commentdate;
    }

    public void setCommentdate(String commentdate) {
        this.commentdate = commentdate;
    }
}
