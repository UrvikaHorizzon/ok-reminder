
package com.horizzon.reminderapp.retrofit.model.getreminderforlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("getReminderForListList")
    @Expose
    private List<GetReminderForListList> getReminderForListList = null;
    @SerializedName("totalRecords")
    @Expose
    private Integer totalRecords;

    public List<GetReminderForListList> getGetReminderForListList() {
        return getReminderForListList;
    }

    public void setGetReminderForListList(List<GetReminderForListList> getReminderForListList) {
        this.getReminderForListList = getReminderForListList;
    }

    public Integer getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }

}
