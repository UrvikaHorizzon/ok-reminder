
package com.horizzon.reminderapp.retrofit.model.getreminder;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetReminderList {

    @SerializedName("getReminder")
    @Expose
    private GetReminder getReminder;
    @SerializedName("getContactList")
    @Expose
    private List<GetContactList> getContactList = null;
    @SerializedName("getDateTimeList")
    @Expose
    private GetDateTimeList getDateTimeList;
    @SerializedName("getLocationList")
    @Expose
    private GetLocationList getLocationList;
    @SerializedName("getCommentList")
    @Expose
    private List<GetCommentList> getCommentList = null;
    @SerializedName("getAttachmentList")
    @Expose
    private List<GetAttachmentList> getAttachmentList = null;
    @SerializedName("getMakeadminList")
    @Expose
    private List<GetMakeadminList> getMakeadminList = null;

    public GetReminder getGetReminder() {
        return getReminder;
    }

    public void setGetReminder(GetReminder getReminder) {
        this.getReminder = getReminder;
    }

    public List<GetContactList> getGetContactList() {
        return getContactList;
    }

    public void setGetContactList(List<GetContactList> getContactList) {
        this.getContactList = getContactList;
    }

    public GetDateTimeList getGetDateTimeList() {
        return getDateTimeList;
    }

    public void setGetDateTimeList(GetDateTimeList getDateTimeList) {
        this.getDateTimeList = getDateTimeList;
    }

    public GetLocationList getGetLocationList() {
        return getLocationList;
    }

    public void setGetLocationList(GetLocationList getLocationList) {
        this.getLocationList = getLocationList;
    }

    public List<GetCommentList> getGetCommentList() {
        return getCommentList;
    }

    public void setGetCommentList(List<GetCommentList> getCommentList) {
        this.getCommentList = getCommentList;
    }

    public List<GetAttachmentList> getGetAttachmentList() {
        return getAttachmentList;
    }

    public void setGetAttachmentList(List<GetAttachmentList> getAttachmentList) {
        this.getAttachmentList = getAttachmentList;
    }

    public List<GetMakeadminList> getGetMakeadminList() {
        return getMakeadminList;
    }

    public void setGetMakeadminList(List<GetMakeadminList> getMakeadminList) {
        this.getMakeadminList = getMakeadminList;
    }

}
