package com.horizzon.reminderapp.receiver;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.horizzon.reminderapp.HomeActivity;
import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.StopMediaPlayer;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import retrofit2.http.Url;


public class NotificationHelper {

    static long[] pattern = {0L, 250L, 200L, 250L, 200L, 250L, 200L, 250L, 200L,
            250L, 200L, 250L, 200L, 250L, 200L};
    public static final int NOTIFICATION_REQUEST_CODE = 100;

    //*********** Used to create Notifications ********//
    public static void showNewNotification(Context context, Intent intent, String title, String msg, Uri ur) {
        NotificationManager mNotificationManager;
        Notification notification = null;
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        //  Uri soundUri = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.tone);
        MediaPlayer player = MediaPlayer.create(context, soundUri);
        player.start();

        AudioManager manager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        manager.setStreamVolume(AudioManager.STREAM_MUSIC, 50, 0);
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(pattern, -1);
        Notification.Builder builder = new Notification.Builder(context);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        Intent notificationIntent;

        if (intent != null) {
            notificationIntent = intent;
        } else {
            notificationIntent = new Intent(context, HomeActivity.class);
        }

        PendingIntent pendingIntent = PendingIntent.getActivity((context), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // Create Notification
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            notification = builder
                    .setContentTitle(title)
                    .setContentText(msg)
                    .setTicker(context.getString(R.string.app_name))
                    .setSmallIcon(R.mipmap.app_icon)
                    .setLights(Color.RED, 3000, 3000)
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true).setSound(soundUri)
                    .setContentIntent(pendingIntent)
                    .setPriority(Notification.PRIORITY_MAX)
                    .build();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .build();
            String channelId = "com.horizzon.reminderapp";
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    "com.horizzon.reminderapp",
                    NotificationManager.IMPORTANCE_HIGH);
            mNotificationManager.createNotificationChannel(channel);
            builder.setChannelId(channelId);
            channel.setSound(soundUri, audioAttributes);
            builder.setDefaults(Notification.DEFAULT_ALL);
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelId, NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setLockscreenVisibility(NotificationCompat.VISIBILITY_PUBLIC);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        notificationManager.notify(NOTIFICATION_REQUEST_CODE, notification);

    }

    public static void ChatNotifaction(Context context, Intent intent, String title, String msg, Uri ur) {
        NotificationManager mNotificationManager;
        Notification notification = null;
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        //  Uri soundUri = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.tone);
        MediaPlayer player = MediaPlayer.create(context, soundUri);
        player.start();

        AudioManager manager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        manager.setStreamVolume(AudioManager.STREAM_MUSIC, 50, 0);
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(pattern, -1);
        Notification.Builder builder = new Notification.Builder(context);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        Intent notificationIntent;

        if (intent != null) {
            notificationIntent = intent;
        } else {
            notificationIntent = new Intent(context, HomeActivity.class);
        }

        PendingIntent pendingIntent = PendingIntent.getActivity((context), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // Create Notification
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            notification = builder
                    .setContentTitle(title)
                    .setContentText(msg)
                    .setTicker(context.getString(R.string.app_name))
                    .setSmallIcon(R.mipmap.app_icon)
                    .setLights(Color.RED, 3000, 3000)
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true).setSound(soundUri)
                    .setContentIntent(pendingIntent)
                    .setPriority(Notification.PRIORITY_MAX)
                    .build();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .build();
            String channelId = "com.horizzon.reminderapp";
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    "com.horizzon.reminderapp",
                    NotificationManager.IMPORTANCE_HIGH);
            mNotificationManager.createNotificationChannel(channel);
            builder.setChannelId(channelId);
            channel.setSound(soundUri, audioAttributes);
            builder.setDefaults(Notification.DEFAULT_ALL);
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelId, NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setLockscreenVisibility(NotificationCompat.VISIBILITY_PUBLIC);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        notificationManager.notify(NOTIFICATION_REQUEST_CODE, notification);

    }
}

