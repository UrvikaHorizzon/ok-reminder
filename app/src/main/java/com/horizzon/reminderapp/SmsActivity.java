package com.horizzon.reminderapp;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.horizzon.reminderapp.app.Iso2Phone;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.firebasedata.Helper;
import com.horizzon.reminderapp.firebasedata.MyFirebaseMessagingService;
import com.horizzon.reminderapp.handler.RUserDbHandler;
import com.horizzon.reminderapp.helper.Country;
import com.horizzon.reminderapp.helper.CountryAdapter;
import com.horizzon.reminderapp.helper.PhoneUtils;
import com.horizzon.reminderapp.helper.PrefManager;
import com.horizzon.reminderapp.model.User;
import com.horizzon.reminderapp.retrofit.model.requestforotp.CheckUserExist;
import com.horizzon.reminderapp.retrofit.model.requestforotp.Data;
import com.horizzon.reminderapp.retrofit.model.requestforotp.RequestForOtp;
import com.horizzon.reminderapp.retrofit.model.requestforotp.UserData;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;
import com.mukesh.OtpView;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SmsActivity extends AppCompatActivity implements View.OnClickListener {

    public SparseArray<ArrayList<Country>> mCountriesMap = new SparseArray<ArrayList<Country>>();
    private DatabaseReference userRef;
    public PhoneNumberUtil mPhoneNumberUtil = PhoneNumberUtil.getInstance();
    public CountryAdapter mAdapter;
    private static String TAG = SmsActivity.class.getSimpleName();
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private Button btnRequestSms, btnVerifyOtp;
    private EditText inputName;
    private EditText inputEmail, inputEmailLastName;
    private EditText inputMobile;
    public OtpView inputOtp;
    private ProgressBar progressBar;
    private PrefManager pref;
    private ImageView btnEditMobile;
    private TextView txtEditMobile, txtCountry;
    private LinearLayout layoutEditMobile, layoutFlag, layoutCountry;
    Iso2Phone isp;
    ListView lv;
    public String mLastEnteredPhone;
    String country_name, country_code, cccCode;
    RUserDbHandler rudbh;
    String otpNumber;
    //UserData userdata = new UserData();
    CheckUserExist.Data userdata;
    private FirebaseAuth mAuth;

    private String mVerificationId;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    String mobile, deviceID, userName, FirstName, LastName;
    TelephonyManager telephonyManager;
    int permissionCheck;
    CircleImageView upload_profile_pic;
    //private static final int REQUEST_WRITE_PERMISSION = 101;
    String fileData = "";
    private static final int PERMISSION_REQUEST_CODE = 1;
    private Helper helper;
    User userMe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);
        helper = new Helper(this);
        rudbh = new RUserDbHandler(getApplicationContext());
        isp = new Iso2Phone();
        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance();
        sharedpreferences = getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        /*
         * Device Id
         * */
        telephonyManager = (TelephonyManager) getBaseContext().
                getSystemService(Context.TELEPHONY_SERVICE);
        String[] PERMISSIONS = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQUEST_CODE);
        }

//        String CountryIso= telephonyManager.getNetworkCountryIso();
        //String NetworkIso = telephonyManager.getSimCountryIso().toUpperCase();
//        String netcode = isp.getPhone(NetworkIso);
//        Toast.makeText(getApplicationContext(),"  "+netcode,Toast.LENGTH_LONG).show();
        upload_profile_pic = findViewById(R.id.upload_profile_pic);
        viewPager = (ViewPager) findViewById(R.id.viewPagerVertical);
        inputName = (EditText) findViewById(R.id.inputName);

        /*
         * First name & last name
         * */
        inputEmail = (EditText) findViewById(R.id.inputEmailFirstname);
        inputEmailLastName = (EditText) findViewById(R.id.inputEmailLastname);

        inputMobile = (EditText) findViewById(R.id.inputMobile);
        inputOtp = (OtpView) findViewById(R.id.inputOtp);
        btnRequestSms = (Button) findViewById(R.id.btn_request_sms);
        btnVerifyOtp = (Button) findViewById(R.id.btn_verify_otp);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        btnEditMobile = (ImageView) findViewById(R.id.btn_edit_mobile);
        txtEditMobile = (TextView) findViewById(R.id.txt_edit_mobile);
        txtCountry = (TextView) findViewById(R.id.countrycode);
        layoutEditMobile = (LinearLayout) findViewById(R.id.layout_edit_mobile);
        layoutFlag = (LinearLayout) findViewById(R.id.layout_flag);
        layoutCountry = (LinearLayout) findViewById(R.id.countryLayout);
//        lv = (ListView) findViewById(R.id.listFlag);
//        mAdapter = new CountryAdapter(getApplicationContext());
//        lv.setAdapter(mAdapter);
//        inputName.setText(netcode);
        // view click listeners
        btnEditMobile.setOnClickListener(this);
        btnRequestSms.setOnClickListener(this);
        btnVerifyOtp.setOnClickListener(this);
        upload_profile_pic.setOnClickListener(this);
        // hiding the edit mobile number
        layoutEditMobile.setVisibility(View.GONE);
        new AsyncPhoneInitTask(getApplicationContext()).execute();

        pref = new PrefManager(this);
        // Checking for user session
        // if user is already logged in, take him to main activity
        pref.checkIsLogin();
        /*if (pref.isLoggedIn()) {
            Intent intent = new Intent(SmsActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }*/

        adapter = new ViewPagerAdapter();
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        layoutCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SmsActivity.this, ChooseCountryActivity.class);
                intent.putExtra("phnno", mLastEnteredPhone);
                startActivityForResult(intent, 2);
            }
        });
        /**
         * Checking if the device is waiting for sms
         * showing the user OTP screen
         */
        if (pref.isWaitingForSms()) {
            viewPager.setCurrentItem(2);
            layoutEditMobile.setVisibility(View.VISIBLE);
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(SmsActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    //  File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                    //.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, 1);
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 22);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        // if (requestCode == 1111) {
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // permission granted!
            // you may now do the action that requires this permission
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                //deviceID = telephonyManager.getImei();
                deviceID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            } else {
                deviceID = telephonyManager.getDeviceId();
            }
        } else {
            Toast.makeText(this, "Please allow permission.", Toast.LENGTH_SHORT).show();
        }
        return;
        // }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2) {
            if (data.getStringExtra("CountryCode") != null && !data.getStringExtra("CountryCode").equals("")) {
                country_code = data.getStringExtra("CountryCode");
            }
            mLastEnteredPhone = data.getStringExtra("MESSAGE");
            txtCountry.setText(mLastEnteredPhone);
        }
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                try {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    upload_profile_pic.setImageBitmap(photo);
                    // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                    Uri tempUri = getImageUri(getApplicationContext(), photo);
                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                    File finalFile = new File(getRealPathFromURI(tempUri));
                    Log.d(TAG, "onActivityResult: camrea img::" + finalFile);
                    fileData = getRealPathFromURI(tempUri);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 22) {
                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                Log.d("path gallery.", picturePath + "");
                upload_profile_pic.setImageBitmap(thumbnail);
                Uri tempUri = getImageUri(getApplicationContext(), thumbnail);
                fileData = getRealPathFromURI(tempUri);
            }
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }


    public class AsyncPhoneInitTask extends AsyncTask<Void, Void, ArrayList<Country>> {
        private int mSpinnerPosition = -1;
        private Context mContext;

        public AsyncPhoneInitTask(Context context) {
            mContext = context;
        }

        @Override
        protected ArrayList<Country> doInBackground(Void... params) {
            ArrayList<Country> data = new ArrayList<Country>(233);
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(mContext.getApplicationContext().getAssets().open("countries.dat"), "UTF-8"));
                // do reading, usually loop until end of file reading
                String line;
                int i = 0;
                while ((line = reader.readLine()) != null) {
                    //process line
                    Country c = new Country(mContext, line, i);
                    data.add(c);
                    ArrayList<Country> list = mCountriesMap.get(c.getCountryCode());
                    if (list == null) {
                        list = new ArrayList<Country>();
                        mCountriesMap.put(c.getCountryCode(), list);
                    }
                    list.add(c);
                    i++;
                }
            } catch (IOException e) {
                //log the exception
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        //log the exception
                    }
                }
            }
//            if (!TextUtils.isEmpty(mPhoneEdit.getText())) {
//                return data;
//            }
            String countryRegion = PhoneUtils.getCountryRegionFromPhone(mContext);
//            Locale locale = Locale.getDefault();
//            countryRegion = locale.getCountry();

            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            countryRegion = telephonyManager.getSimCountryIso();
            int code = mPhoneNumberUtil.getCountryCodeForRegion(countryRegion.toUpperCase(Locale.getDefault()));

            ArrayList<Country> list = mCountriesMap.get(code);
            if (list != null) {
                for (Country c : list) {
                    if (c.getPriority() == 0) {
                        mSpinnerPosition = c.getNum();
                        country_name = c.getName();
                        country_code = c.getCountryCodeStr();
                        mLastEnteredPhone = country_name + " (" + country_code + ")";
                        break;
                    }
                }
            }
            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<Country> data) {
//            mAdapter.addAll(data);
//            mAdapter.notifyDataSetChanged();
            if (mSpinnerPosition > 0) {
//                mSpinner.setSelection(mSpinnerPosition);
                txtCountry.setText(mLastEnteredPhone.toString());
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_request_sms:
                validateForm();
                break;

            case R.id.btn_verify_otp:
                verifyOtp();
                break;

            case R.id.btn_edit_mobile:
                viewPager.setCurrentItem(0);
                layoutEditMobile.setVisibility(View.GONE);
                pref.setIsWaitingForSms(false);
                break;

            case R.id.upload_profile_pic:
                selectImage();
                break;
            default:

        }
    }

    /**
     * Validating user details form
     */
    private void validateForm() {
//        String name = inputName.getText().toString().trim();
//
        mobile = inputMobile.getText().toString().trim();
        FirstName = inputEmail.getText().toString().trim();
        LastName = inputEmailLastName.getText().toString().trim();
        userName = FirstName;
        permissionCheck = ContextCompat.checkSelfPermission(SmsActivity.this, Manifest.permission.READ_PHONE_STATE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SmsActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1111);
            Toast.makeText(this, "Permission not Granted.", Toast.LENGTH_SHORT).show();
        } else {
            MultipartBody.Part body = null;
            if (isValidPhoneNumber(mobile)) {
                /*
                 * Send otp on this number
                 * */
                sendVerificationCode(mobile);
                viewPager.setCurrentItem(1);
                txtEditMobile.setText(pref.getMobileNumber());
                layoutEditMobile.setVisibility(View.VISIBLE);

                // if (fileData != null && fileData.equals("")) {
                /*if (!userName.isEmpty()) {

                    // requesting for sms
                    //requestForSMS(country_name, country_code, mobile);
                    if (Common.hasInternetConnection(getApplicationContext())) {
                        Common.DisplayLog("country_code", country_code.replace("+", "") + "");
                        final ProgressDialog pd = new ProgressDialog(SmsActivity.this);
                        pd.setMessage("Loading...");
                        pd.setIndeterminate(true);
                        pd.setCancelable(false);
                        pd.show();

                        if (!fileData.equals("")) {
                            File file = new File(fileData);
                            RequestBody requestFile =
                                    RequestBody.create(MediaType.parse("multipart/form-data"), file);
                            // MultipartBody.Part is used to send also the actual file name
                            body = MultipartBody.Part.createFormData("vUPhoto", file.getName(), requestFile);
                        }*//*else {
                          //  File file = new File(fileData);
                            RequestBody requestFile =
                                    RequestBody.create(MediaType.parse("multipart/form-data"), "");
                            // MultipartBody.Part is used to send also the actual file name
                            body = MultipartBody.Part.createFormData("vUPhoto","",requestFile);
                        }*//*

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        RequestBody cCode = RequestBody.create(MediaType.parse("text/plain"), country_code.replace("+", ""));
                        RequestBody mobile1 = RequestBody.create(MediaType.parse("text/plain"), mobile);
                        RequestBody userName1 = RequestBody.create(MediaType.parse("text/plain"), userName);
                        RequestBody lastname = RequestBody.create(MediaType.parse("text/plain"), LastName);
                        Call<RequestForOtp> call = apiService.getRequestForOtp(cCode, mobile1, userName1, lastname, body);
                        call.enqueue(new Callback<RequestForOtp>() {
                            @Override
                            public void onResponse(Call<RequestForOtp> call, Response<RequestForOtp> response) {
                                boolean status = response.body().getStatus();
                                RequestForOtp requestForOtp = response.body();
                                String userIsValid = requestForOtp.getData().getUserData().getIUStatus();
                                if (userIsValid.matches("1")) {
                                    if (status) {
                                        //Data
                                        final Data date = response.body().getData();
                                        if (date.getUserData() != null) {

                                            userdata = date.getUserData();
                                            otpNumber = date.getOtpNumber();

                                            editor.putString("mobile", userdata.getVUNumber());
                                            editor.putString("deviceId", deviceID);
                                            editor.putString("userName", userdata.getVUName());
                                            editor.putString("UId", userdata.getIUserId());
                                            editor.putString("userImage", userdata.getVUPhoto());
                                            editor.putString("countryCode", userdata.getVUContryNumber());
                                            editor.apply();
                                            MyFirebaseMessagingService.RegisterDeviceForFCM(SmsActivity.this, "");
                                        } else {
                                            editor.putString("deviceId", deviceID);
                                            editor.apply();
                                            MyFirebaseMessagingService.RegisterDeviceForFCM(SmsActivity.this, "");
                                        }
                                        *//*
                 * Send otp on this number
                 * *//*
                                        sendVerificationCode(mobile);
                                        viewPager.setCurrentItem(1);
                                        txtEditMobile.setText(pref.getMobileNumber());
                                        layoutEditMobile.setVisibility(View.VISIBLE);
                                    } else {
                                        // Common.ShowToast(getApplicationContext(), response.body().getMessage());
                                        viewPager.setCurrentItem(0);
                                        layoutEditMobile.setVisibility(View.GONE);
                                        pref.setIsWaitingForSms(false);
                                    }
                                    pd.dismiss();
                                } else {
                                    pd.dismiss();
                                    Common.ShowToast(getApplicationContext(), response.message());
                                    // Toast.makeText(SmsActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<RequestForOtp> call, Throwable t) {
                                pd.dismiss();
                            }
                        });
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please enter your name.", Toast.LENGTH_SHORT).show();
                }*/
            } else {
                Toast.makeText(getApplicationContext(), "Please enter valid mobile number.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void sendVerificationCode(String mobile) {
        PhoneAuthOptions options = PhoneAuthOptions.newBuilder(mAuth)
                .setPhoneNumber("+91" + mobile)       // Phone number to verify
                .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
                .setActivity(this)                 // Activity (for callback binding)
                .setCallbacks(mCallbacks)          // OnVerificationStateChangedCallbacks
                .build();
        PhoneAuthProvider.verifyPhoneNumber(options);
    }


    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();

            if (code != null) {
                inputOtp.setText(code);
                //verifying the code
                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            // Toast.makeText(SmsActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            // super.onCodeSent(s, forceResendingToken);
            mVerificationId = s;
            Log.e("verification_id", s);
            // Toast.makeText(SmsActivity.this, "Wait for the code", Toast.LENGTH_LONG).show();
        }

    };

    private void verifyVerificationCode(String code) {
        //creating the credential
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
        Log.e("verifi_code", code);

        //signing the user
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                //verification successful we will start the profile activity
                login();
            } else {
                //Toast.makeText(SmsActivity.this, task.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void login() {
        //FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseUser firebaseUser = mAuth.getCurrentUser();
        assert firebaseUser != null;
        String userid = firebaseUser.getUid();
        userRef = FirebaseDatabase.getInstance().getReference("User").child(userid);

        //FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists() && dataSnapshot.getChildrenCount() > 0) {
                    try {
                        User user = dataSnapshot.getValue(User.class);
                        if (User.validate(user)) {
                            helper.setLoggedInUser(user);
                        } else {
                            createUser(new User(mobile, userName, "Hey there! I am using" + " " + getString(R.string.app_name), ""));
                        }
                    } catch (Exception ex) {
                        createUser(new User(mobile, userName, "Hey there! I am using" + " " + getString(R.string.app_name), ""));
                    }
                } else {
                    createUser(new User(mobile, userName, "Hey there! I am using" + " " + getString(R.string.app_name), ""));
                }
                checkUserExist();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }


    private void createUser(final User newUser) {

        userRef.setValue(newUser).addOnSuccessListener(aVoid -> {
            helper.setLoggedInUser(newUser);
        }).addOnFailureListener(e ->
                Toast.makeText(SmsActivity.this, "User is not created.", Toast.LENGTH_LONG).show());
    }


    public void checkUserExist() {
        if (isValidPhoneNumber(mobile)) {

            final ProgressDialog pd = new ProgressDialog(SmsActivity.this);
            pd.setMessage("Loading...");
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            cccCode = country_code.replace("+", "");
            String number = country_code.replace("+", "") + mobile;
            RequestBody cCode = RequestBody.create(MediaType.parse("text/plain"), number);
            Call<CheckUserExist> call = apiService.getUserData(cCode);
            call.enqueue(new Callback<CheckUserExist>() {
                @Override
                public void onResponse(Call<CheckUserExist> call, Response<CheckUserExist> response) {
                    boolean status = response.body().getStatus();
                    CheckUserExist requestForOtp = response.body();

                    if (requestForOtp.getData().getIUserId() != null) {
                        pd.dismiss();
                        //  if (status) {
                        //Data
                        //  final Data date = response.body().getData();
                        userdata = requestForOtp.getData();
                        // otpNumber = date.getOtpNumber();

                        editor.putString("mobile", userdata.getVUNumber());
                        editor.putString("deviceId", deviceID);
                        editor.putString("userName", userdata.getVUName());
                        editor.putString("userLName", userdata.getVLastName());
                        editor.putString("UId", userdata.getIUserId());
                        editor.putString("userImage", userdata.getVUPhoto());
                        editor.putString("countryCode", userdata.getVUContryNumber());
                        editor.apply();
                        MyFirebaseMessagingService.RegisterDeviceForFCM(SmsActivity.this, "");

                        if (rudbh.getDataCount() <= 0) {
                            String[] COLUMN = {
                                    userdata.getIUserId(),
                                    userdata.getVUName(),
                                    userdata.getVUNumber(),
                                    userdata.getVUEmail(),
                                    userdata.getVUPhoto(),
                                    userdata.getVUGender(),
                                    userdata.getIUStatus()
                            };
                            //   lastid = String.valueOf(rudbh.addData(COLUMN));
                            rudbh.addData(COLUMN, userdata.getIUserId());
                        }
                        //create  session
                        pref.createLogin(userdata.getIUserId());

                       /* Intent intent = new Intent(SmsActivity.this, HomeActivity.class);
                        intent.putExtra("countryCodeAndMobileNumber", mobile);
                        intent.putExtra("ccCode", cccCode);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
*/
                        Common.createSharedPreferences(getApplicationContext(), "userId", userdata.getIUserId());
                        Intent intent = new Intent(SmsActivity.this, InstallDataActivity.class);
                        intent.putExtra("countryCodeAndMobileNumber", "+91" + mobile);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(SmsActivity.this, SignFormActivity.class);
                        intent.putExtra("countryCodeAndMobileNumber", mobile);
                        intent.putExtra("ccCode", cccCode);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();

                        Toast.makeText(SmsActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CheckUserExist> call, Throwable t) {
                    pd.dismiss();
                }
            });
        }
    }

    /**
     * sending the OTP to server and activating the user
     */
    private void verifyOtp() {
        String otp = inputOtp.getText().toString().trim();
        if (otp.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter the OTP", Toast.LENGTH_SHORT).show();
        } else {
            verifyVerificationCode(otp);
            /*String lastid = "1";
            if (rudbh.getDataCount() <= 0) {
                String[] COLUMN = {
                        userdata.getIUserId(),
                        userdata.getVUName(),
                        userdata.getVUNumber(),
                        userdata.getVUEmail(),
                        userdata.getVUPhoto(),
                        userdata.getVUGender(),
                        userdata.getIUStatus()
                };
                //   lastid = String.valueOf(rudbh.addData(COLUMN));
                rudbh.addData(COLUMN, userdata.getIUserId());*/
        }
        //create  session
        // pref.createLogin(userdata.getIUserId());
        /*
         * change edit TextView to otp view TextView in xml file.
         * */

            /*Common.createSharedPreferences(getApplicationContext(), "userId", userdata.getIUserId());
            Intent intent = new Intent(SmsActivity.this, InstallDataActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();*/
    }


    /**
     * Regex to validate the mobile number
     * mobile number should be of 10 digits length
     *
     * @param mobile
     * @return
     */
    private static boolean isValidPhoneNumber(String mobile) {
        String regEx = "^[0-9]{10}$";
        return mobile.matches(regEx);
    }

    class ViewPagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        public Object instantiateItem(View collection, int position) {
            int resId = 0;
            switch (position) {
                case 0:
                    resId = R.id.layout_sms;
                    break;
                case 1:
                    resId = R.id.layout_otp;
                    break;
            }
            return findViewById(resId);
        }

        @Override
        public void destroyItem(View container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
