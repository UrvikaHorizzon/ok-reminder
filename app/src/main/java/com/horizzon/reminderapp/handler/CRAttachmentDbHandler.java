package com.horizzon.reminderapp.handler;

/**
 * Created by Abhijit Sojitra on 9/3/17.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.horizzon.reminderapp.dao.AttachmentTable;
import com.horizzon.reminderapp.data.Common;

import java.util.ArrayList;
import java.util.HashMap;

public class CRAttachmentDbHandler {


    // table name
    public static final String TABLE_NAME = "cr_attachment";

    public static final String[] COLUMNS = {
            "Id",
            "sesssionId",
            "iUserId",
            "attachFor",
            "detail",
            "status",
            "createdData"
    };

    DatabaseHandler dbh;
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
            + "(" + COLUMNS[0] + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMNS[1] + " TEXT,"
            + COLUMNS[2] + " TEXT,"
            + COLUMNS[3] + " TEXT,"
            + COLUMNS[4] + " TEXT,"
            + COLUMNS[5] + " TEXT,"
            + COLUMNS[6] + " TEXT"
            + ");";

    public CRAttachmentDbHandler(Context context) {
        dbh = new DatabaseHandler(context);
    }


    public long addData(String[] c) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        for (int j = 1; j < COLUMNS.length; j++) {
           // Common.DisplayLog("columnNames", COLUMNS[j] + " => " + c[j]);
            values.put(COLUMNS[j], c[j]);
        }

        long id = db.insert(TABLE_NAME, null, values);
        db.close();
        return id;
    }
    public long addPreData(String[] c) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        for (int j = 0; j < COLUMNS.length; j++) {
            Common.DisplayLog("columnNames", COLUMNS[j] + " => " + c[j]);
            values.put(COLUMNS[j], c[j]);
        }

        long id = db.insert(TABLE_NAME, null, values);
        db.close();
        return id;
    }

    public long addData(String[] c, byte[] image) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
            for (int j = 1; j < COLUMNS.length; j++) {
                Common.DisplayLog("columnNames", COLUMNS[j]);
                if (j != 4) {
                    values.put(COLUMNS[j], c[j]);
                }
        }
//        values.put(COLUMNS[4], image);

        long id = db.insert(TABLE_NAME, null, values);
        db.close();
        return id;
    }

    public void updataData(String id, String sesid, String attfor, String detail) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[3], attfor);
        values.put(COLUMNS[4], detail);

        db.update(TABLE_NAME, values, COLUMNS[0] + " = ? AND " + COLUMNS[1] + " = ?", new String[]{id, sesid});

    }

    public void updataStatus(String sesid, String status) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[5], status);

        db.update(TABLE_NAME, values, COLUMNS[1] + "=? AND " + COLUMNS[5] + "=? ", new String[]{sesid, "false"});

    }

    public ArrayList<HashMap<String, String>> getAllData(int id) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> hashmap;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " ORDER BY " + COLUMNS[0] + " DESC ";

        Cursor cursor = db.rawQuery(query_select, null);
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                hashmap = new HashMap<String, String>();
                for (int j = 0; j < columnNames.length; j++) {
                    /*if (ConstantData.log_enable) {
                        Log.e("columnNames", columnNames[j]);
						Log.e("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
					}*/
                    hashmap.put(COLUMNS[j], cursor.getString(j));
                }
                list.add(hashmap);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public HashMap<String, String> getTableSingleDate(String sesid) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        HashMap<String, String> hashmap = null;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "=" + sesid + " ORDER BY " + COLUMNS[0] + " DESC";

        Cursor cursor = db.rawQuery(query_select, null);
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                hashmap = new HashMap<String, String>();
                for (int j = 0; j < columnNames.length; j++) {
                    /*if (ConstantData.log_enable) {
                        Log.e("columnNames", columnNames[j]);
						Log.e("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
					}*/
                    hashmap.put(COLUMNS[j], cursor.getString(j));
                }

            } while (cursor.moveToNext());
        }
        cursor.close();

        return hashmap;
    }


    public ArrayList<AttachmentTable> getAllCurrentSessDataWithType(String sesid, String attachFor, String status) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<AttachmentTable> list = new ArrayList<AttachmentTable>();
        AttachmentTable AttachmentTable;
        String statusstr = "";
        if (status != null && !status.equals("")) {
            statusstr = " AND " + COLUMNS[5] + "='" + status + "'";
        }

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "='" + sesid + "' AND " + COLUMNS[3] + "='" + attachFor + "' " + statusstr + " ORDER BY " + COLUMNS[0] + " ASC ";
        Common.DisplayLog("query_select", query_select + "");
        Cursor cursor = db.rawQuery(query_select, null);
        Common.DisplayLog("", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                AttachmentTable = new AttachmentTable(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6));
                for (int j = 0; j < columnNames.length; j++) {
                    if (j != 4) {
                        Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
                    }
                }
                list.add(AttachmentTable);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public ArrayList<AttachmentTable> getAllCurrentSessDataWithoutType(String sesid, String status) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<AttachmentTable> list = new ArrayList<AttachmentTable>();
        AttachmentTable AttachmentTable;
        String statusstr = "";
        if (status != null && !status.equals("")) {
            statusstr = " AND " + COLUMNS[5] + "='" + status + "'";
        }

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "='" + sesid + "' " + statusstr + " ORDER BY " + COLUMNS[0] + " ASC ";
        Common.DisplayLog("query_select", query_select + "");
        Cursor cursor = db.rawQuery(query_select, null);
        Common.DisplayLog("", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                AttachmentTable = new AttachmentTable(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6));
                for (int j = 0; j < columnNames.length; j++) {
                    if (j != 4) {
                        Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
                    }
                }
                list.add(AttachmentTable);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }


    public AttachmentTable getTableSingleDateWihtDao(String sesid, String id) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        AttachmentTable AttachmentTable = null;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "='" + sesid + "' AND " + COLUMNS[0] + "='" + id + "' ORDER BY " + COLUMNS[0] + " DESC";
        Common.DisplayLog("query_select", query_select);
        Cursor cursor = db.rawQuery(query_select, null);

        Common.DisplayLog("cursor.getCount()", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            Common.DisplayLog("cursor", "yes");
            String[] columnNames = cursor.getColumnNames();
            do {
                AttachmentTable = new AttachmentTable(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6));
                for (int j = 0; j < columnNames.length; j++) {
                    /*if (ConstantData.log_enable) {
                        Log.e("columnNames", columnNames[j]);
						Log.e("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
					}*/
                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
                }
            } while (cursor.moveToNext());
        }
        cursor.close();

        return AttachmentTable;
    }

    public void deleteSingleData(String sessid, String id) {
        SQLiteDatabase db = dbh.getWritableDatabase();
        String qu = "delete from " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = '" + sessid + "' AND " + COLUMNS[0] + " = '" + id + "'";
        Common.DisplayLog("qu ", qu);
        db.execSQL(qu);
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }

    public void resetTempdata(String sessid) {
        SQLiteDatabase db = dbh.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.execSQL("delete from " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = '" + sessid + "'");
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }
    public void resetTempStatusdata(String sessid, String status) {

        SQLiteDatabase db = dbh.getWritableDatabase();
        String statusstr = "";
        if (status != null && !status.equals("")) {
            statusstr = " AND " + COLUMNS[5] + "='" + status + "'";
        }
        db.execSQL("delete from " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = '" + sessid + "' " + statusstr);
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }
    public void resetTempIddata(String sessid, String id) {

        SQLiteDatabase db = dbh.getWritableDatabase();
        String idstr = "";
        if (id != null && !id.equals("")) {
            idstr = " AND " + COLUMNS[0] + "='" + id + "'";
        }
        db.execSQL("delete from " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = '" + sessid + "' " + idstr);
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }

    public void resetTempdata(String sessid, String attachFor) {
        SQLiteDatabase db = dbh.getWritableDatabase();
        String sql= "delete from " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = '" + sessid + "' AND " + COLUMNS[3] + "='" + attachFor + "'";
        Common.DisplayLog("sql",sql);
        db.execSQL(sql);
        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = '" + sessid + "' AND " + COLUMNS[3] + "='" + attachFor + "'";

        Cursor cursor = db.rawQuery(query_select, null);
        int totcount = cursor.getCount();
        Common.DisplayLog("totcount",totcount+"");
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }


    public void resetTable() {
        SQLiteDatabase db = dbh.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.execSQL("delete from " + TABLE_NAME);
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }

    public int getDataCount(String sessid, String status) {
        SQLiteDatabase db = dbh.getReadableDatabase();
        String statusstr = "";
        if (status != null && !status.equals("")) {
            statusstr = " AND " + COLUMNS[5] + "='" + status + "'";
        }
        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = '" + sessid + "' " + statusstr;

        Cursor cursor = db.rawQuery(query_select, null);
        int totcount = cursor.getCount();
        cursor.close();

        return totcount;
    }


}
