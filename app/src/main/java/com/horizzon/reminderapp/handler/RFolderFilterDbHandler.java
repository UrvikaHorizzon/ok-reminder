package com.horizzon.reminderapp.handler;

/**
 * Created by Abhijit Sojitra on 9/3/17.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.horizzon.reminderapp.dao.RFolderFilter;
import com.horizzon.reminderapp.data.Common;

import java.util.ArrayList;

public class RFolderFilterDbHandler {


    // table name
    public static final String TABLE_NAME = "r_folder_filter";

    public static final String[] COLUMNS = {
            "iId",
            "iUserId",
            "vSesssionId",
            "iFolderId",
            "dCreatedDateTime"
    };

    DatabaseHandler dbh;
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
            + "(" + COLUMNS[0] + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMNS[1] + " TEXT,"
            + COLUMNS[2] + " TEXT,"
            + COLUMNS[3] + " TEXT,"
            + COLUMNS[4] + " TEXT"
            + ");";

    public RFolderFilterDbHandler(Context context) {
        dbh = new DatabaseHandler(context);
    }


    public long addData(String[] c) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        for (int j = 1; j < COLUMNS.length; j++) {
            Common.DisplayLog("columnNames", COLUMNS[j] + " => " + c[j]);
            values.put(COLUMNS[j], c[j]);
        }

        long id = db.insert(TABLE_NAME, null, values);
        Common.DisplayLog("last created id", id + "");
        db.close();
        return id;
    }

    public void updataData(String iId, String vBoxName) {

        Common.DisplayLog("update vBoxName", vBoxName + "");
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[2], vBoxName);

        db.update(TABLE_NAME, values, COLUMNS[0] + "=? ", new String[]{iId});
        Common.DisplayLog("last update userid", iId + "");

    }

    public ArrayList<RFolderFilter> getAllData(String iUserId) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<RFolderFilter> list = new ArrayList<RFolderFilter>();
        RFolderFilter RFolderFilter;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "='" + iUserId + "' ORDER BY " + COLUMNS[0] + " DESC";
        ;
        Common.DisplayLog("query_select", query_select + "");
        Cursor cursor = db.rawQuery(query_select, null);
        Common.DisplayLog("", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                RFolderFilter = new RFolderFilter(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
                for (int j = 0; j < columnNames.length; j++) {

                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));

                }
                list.add(RFolderFilter);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public RFolderFilter getTableSingleDateWihtDao(String iId) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        RFolderFilter RFolderFilter = null;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[0] + "='" + iId + "' ORDER BY " + COLUMNS[0] + " DESC";
        Common.DisplayLog("query_select", query_select);
        Cursor cursor = db.rawQuery(query_select, null);

        Common.DisplayLog("cursor.getCount()", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            Common.DisplayLog("cursor", "yes");
            String[] columnNames = cursor.getColumnNames();
            do {
                RFolderFilter = new RFolderFilter(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
                for (int j = 0; j < columnNames.length; j++) {
                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
                }
            } while (cursor.moveToNext());
        }
        cursor.close();

        return RFolderFilter;
    }

    public void deleteSingleData(String id) {
        SQLiteDatabase db = dbh.getWritableDatabase();
        String qu = "delete from " + TABLE_NAME + " WHERE " + COLUMNS[0] + " = '" + id + "'";
        Common.DisplayLog("qu ", qu);
        db.execSQL(qu);
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }

    public void resetTable() {
        SQLiteDatabase db = dbh.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.execSQL("delete from " + TABLE_NAME);
        db.execSQL("delete from sqlite_sequence where name='" + TABLE_NAME + "'");
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }

}
