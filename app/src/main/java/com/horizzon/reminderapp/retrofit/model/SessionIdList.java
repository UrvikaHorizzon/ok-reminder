package com.horizzon.reminderapp.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SessionIdList implements Serializable {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Datum implements Serializable {

        @SerializedName("vSesssionId")
        @Expose
        private String vSesssionId;
        @SerializedName("vReminderFor")
        @Expose
        private String vReminderFor;
        @SerializedName("vCreatedBy")
        @Expose
        private String vCreatedBy;
        @SerializedName("vDtFor")
        @Expose
        private Object vDtFor;
        @SerializedName("vStartDate")
        @Expose
        private Object vStartDate;
        @SerializedName("vStartTime")
        @Expose
        private Object vStartTime;
        @SerializedName("vEndDate")
        @Expose
        private Object vEndDate;
        @SerializedName("vEndTime")
        @Expose
        private Object vEndTime;
        @SerializedName("tDays")
        @Expose
        private Object tDays;

        public String getVSesssionId() {
            return vSesssionId;
        }

        public void setVSesssionId(String vSesssionId) {
            this.vSesssionId = vSesssionId;
        }

        public String getVReminderFor() {
            return vReminderFor;
        }

        public void setVReminderFor(String vReminderFor) {
            this.vReminderFor = vReminderFor;
        }

        public String getVCreatedBy() {
            return vCreatedBy;
        }

        public void setVCreatedBy(String vCreatedBy) {
            this.vCreatedBy = vCreatedBy;
        }

        public Object getVDtFor() {
            return vDtFor;
        }

        public void setVDtFor(Object vDtFor) {
            this.vDtFor = vDtFor;
        }

        public Object getVStartDate() {
            return vStartDate;
        }

        public void setVStartDate(Object vStartDate) {
            this.vStartDate = vStartDate;
        }

        public Object getVStartTime() {
            return vStartTime;
        }

        public void setVStartTime(Object vStartTime) {
            this.vStartTime = vStartTime;
        }

        public Object getVEndDate() {
            return vEndDate;
        }

        public void setVEndDate(Object vEndDate) {
            this.vEndDate = vEndDate;
        }

        public Object getVEndTime() {
            return vEndTime;
        }

        public void setVEndTime(Object vEndTime) {
            this.vEndTime = vEndTime;
        }

        public Object getTDays() {
            return tDays;
        }

        public void setTDays(Object tDays) {
            this.tDays = tDays;
        }

    }
}