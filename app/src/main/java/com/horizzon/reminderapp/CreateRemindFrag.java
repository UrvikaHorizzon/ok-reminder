package com.horizzon.reminderapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by horizzon on 12/21/2016.
 */
public class CreateRemindFrag extends Fragment implements Animation.AnimationListener {
    Button btn_send, btn_device;
    GradientDrawable gd;
    float[] arr = new float[8];
    float[] arr1 = new float[8];
    LinearLayout sendLayout, deviceLayout, sendBtnLayout, deviceBtnLayout;
    TextView txtDatetime, dtxtDatetime, txtLocation, dtxtLocation, dtxtTo, txtTo;
    EditText txtSubject, dtxtSubject;
    ImageView imgPriority1, imgPriority2, imgPriority3, dimgPriority1, dimgPriority2, dimgPriority3;
    ImageView attachImg, dattachImg;
    public int prio1_flag = 1;
    public int prio2_flag = 1;
    public int prio3_flag = 1;
    public int dprio1_flag = 1;
    public int dprio2_flag = 1;
    public int dprio3_flag = 1;
    TextView edtComt;
    Animation animFadeIn;
    Animation animSlideDown;
    Animation animSlideUp;
    Animation animMoveUp;
    Animation animMoveDown;
    public int anim_flag = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.create_reminder, container, false);

        btn_send = (Button) v.findViewById(R.id.button1);
        btn_device = (Button) v.findViewById(R.id.button2);

        sendLayout = (LinearLayout) v.findViewById(R.id.sendLayout);
        deviceLayout = (LinearLayout) v.findViewById(R.id.deviceLayout);
        sendBtnLayout = (LinearLayout) v.findViewById(R.id.sendBtnLayout);
        deviceBtnLayout = (LinearLayout) v.findViewById(R.id.deviceBtnLayout);
        txtDatetime = (TextView) v.findViewById(R.id.txtDateTime);
        dtxtDatetime = (TextView) v.findViewById(R.id.dtxtDateTime);
        dtxtTo = (TextView) v.findViewById(R.id.dtxtTo);
        txtTo = (TextView) v.findViewById(R.id.txtTo);

        txtLocation = (TextView) v.findViewById(R.id.txtLocation);
        dtxtLocation = (TextView) v.findViewById(R.id.dtxtLocation);
        txtSubject = (EditText) v.findViewById(R.id.txtSubject);
        dtxtSubject = (EditText) v.findViewById(R.id.dtxtSubject);

        imgPriority1 = (ImageView) v.findViewById(R.id.imgPriority1);
        imgPriority2 = (ImageView) v.findViewById(R.id.imgPriority2);
        imgPriority3 = (ImageView) v.findViewById(R.id.imgPriority3);
        dimgPriority1 = (ImageView) v.findViewById(R.id.dimgPriority1);
        dimgPriority2 = (ImageView) v.findViewById(R.id.dimgPriority2);
        dimgPriority3 = (ImageView) v.findViewById(R.id.dimgPriority3);

        attachImg = (ImageView) v.findViewById(R.id.attchImg);
        dattachImg = (ImageView) v.findViewById(R.id.dattchImg);

        edtComt = (TextView) v.findViewById(R.id.txtComment);

        // load the animation
        animFadeIn = AnimationUtils.loadAnimation(getActivity(),
                R.anim.fade_out);

        // set animation listener
        animFadeIn.setAnimationListener(this);

        // load the animation
        animMoveUp = AnimationUtils.loadAnimation(getActivity(),
                R.anim.move);

        // set animation listener
        animMoveUp.setAnimationListener(this);

        // load the animation
        animMoveDown = AnimationUtils.loadAnimation(getActivity(),
                R.anim.move_down);

        // set animation listener
        animMoveDown.setAnimationListener(this);

        // load the animation
        animSlideDown = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_down);

        // set animation listener
        animSlideDown.setAnimationListener(this);

        // load the animation
        animSlideUp = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_up);

        // set animation listener
        animSlideUp.setAnimationListener(this);

        txtSubject.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_NEXT) || (actionId == EditorInfo.IME_FLAG_NO_ENTER_ACTION)) {
                    String noteName = txtSubject.getText().toString().trim();
                    //txtSubject.setCursorVisible(false);
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(txtSubject.getWindowToken(), 0);
                }
                return false;
            }
        });

        dtxtSubject.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_NEXT) || (actionId == EditorInfo.IME_FLAG_NO_ENTER_ACTION)) {
                    String noteName = dtxtSubject.getText().toString().trim();
                    dtxtSubject.setCursorVisible(false);
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(dtxtSubject.getWindowToken(), 0);
                }
                return false;
            }
        });

        edtComt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                LayoutInflater layoutInflater =
//                        (LayoutInflater)getActivity()
//                                .getSystemService(LAYOUT_INFLATER_SERVICE);
//                View popupView = layoutInflater.inflate(R.layout.popup, null);
//                final PopupWindow popupWindow = new PopupWindow(
//                        popupView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//                popupWindow.showAsDropDown(edtComt, -40, -95);
//                ViewGroup root = (ViewGroup) getActivity().getWindow().getDecorView().getRootView();
//                applyDim(root, 0.5f);
//                EditText yourEditText= (EditText) popupView.findViewById(R.id.edtComment);
//                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.showSoftInput(yourEditText, InputMethodManager.SHOW_IMPLICIT);
                Intent in = new Intent(getActivity(), CommentsActivity.class);
                startActivity(in);
            }
        });

        arr[0] = Float.valueOf(20);
        arr[1] = Float.valueOf(20);
        arr[2] = Float.valueOf(0);
        arr[3] = Float.valueOf(0);
        arr[4] = Float.valueOf(0);
        arr[5] = Float.valueOf(0);
        arr[6] = Float.valueOf(20);
        arr[7] = Float.valueOf(20);

        arr1[0] = Float.valueOf(0);
        arr1[1] = Float.valueOf(0);
        arr1[2] = Float.valueOf(20);
        arr1[3] = Float.valueOf(20);
        arr1[4] = Float.valueOf(20);
        arr1[5] = Float.valueOf(20);
        arr1[6] = Float.valueOf(0);
        arr1[7] = Float.valueOf(0);

        imgPriority1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( prio1_flag == 1) {
                    imgPriority1.setImageResource(R.drawable.priorityiconyellow);
                    imgPriority2.setImageResource(R.drawable.priorityicon);
                    imgPriority3.setImageResource(R.drawable.priorityicon);
                    prio1_flag = 0;
                } else {
                    imgPriority1.setImageResource(R.drawable.priorityicon);
                    imgPriority2.setImageResource(R.drawable.priorityicon);
                    imgPriority3.setImageResource(R.drawable.priorityicon);
                    prio1_flag = 1;
                }
                prio2_flag = 1;
                prio3_flag = 1;
            }
        });

        imgPriority2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( prio2_flag == 1) {
                    imgPriority1.setImageResource(R.drawable.priorityiconyellow);
                    imgPriority2.setImageResource(R.drawable.priorityiconyellow);
                    imgPriority3.setImageResource(R.drawable.priorityicon);
                    prio2_flag = 0;
                } else {
                    imgPriority1.setImageResource(R.drawable.priorityicon);
                    imgPriority2.setImageResource(R.drawable.priorityicon);
                    imgPriority3.setImageResource(R.drawable.priorityicon);
                    prio2_flag = 1;
                }
                prio1_flag = 1;
                prio3_flag = 1;
            }
        });

        imgPriority3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( prio3_flag == 1) {
                    imgPriority1.setImageResource(R.drawable.priorityiconyellow);
                    imgPriority2.setImageResource(R.drawable.priorityiconyellow);
                    imgPriority3.setImageResource(R.drawable.priorityiconyellow);
                    prio3_flag = 0;
                } else {
                    imgPriority1.setImageResource(R.drawable.priorityicon);
                    imgPriority2.setImageResource(R.drawable.priorityicon);
                    imgPriority3.setImageResource(R.drawable.priorityicon);
                    prio3_flag = 1;
                }
                prio1_flag = 1;
                prio2_flag = 1;
            }
        });

        dimgPriority1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dimgPriority1.setImageResource(R.drawable.priorityiconyellow);
                dimgPriority2.setImageResource(R.drawable.priorityicon);
                dimgPriority3.setImageResource(R.drawable.priorityicon);
            }
        });

        dimgPriority2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dimgPriority1.setImageResource(R.drawable.priorityiconyellow);
                dimgPriority2.setImageResource(R.drawable.priorityiconyellow);
                dimgPriority3.setImageResource(R.drawable.priorityicon);
            }
        });

        dimgPriority3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dimgPriority1.setImageResource(R.drawable.priorityiconyellow);
                dimgPriority2.setImageResource(R.drawable.priorityiconyellow);
                dimgPriority3.setImageResource(R.drawable.priorityiconyellow);
            }
        });

        txtDatetime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(),DateTimeActivity.class);
                startActivity(in);
            }
        });

        dtxtDatetime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(),DateTimeActivity.class);
                startActivity(in);
            }
        });

        txtLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(),LocationActivity.class);
                startActivity(in);
            }
        });

        txtTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(),ContactActivity.class);
                startActivity(in);
            }
        });

        dtxtLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(),LocationActivity.class);
                startActivity(in);
            }
        });

        txtSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtSubject.setFocusable(true);
                txtSubject.requestFocus();
                txtSubject.setCursorVisible(true);
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(txtSubject, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        dtxtSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dtxtSubject.setFocusable(true);
                dtxtSubject.requestFocus();
                dtxtSubject.setCursorVisible(true);
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(dtxtSubject, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        attachImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(),AttachmentActivity.class);
                startActivity(in);
            }
        });

        dattachImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(),AttachmentActivity.class);
                startActivity(in);
            }
        });

        if(CreateReminderActivity.btn_flag == 1) {
            gd = new GradientDrawable();
            gd.setStroke(4, Color.parseColor("#1ba1e2"));
            gd.setColor(Color.parseColor("#ffffff"));
            gd.setCornerRadii(arr);
            btn_send.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_send_white));
            btn_send.setTextColor(Color.parseColor("#1ba1e2"));

            gd = new GradientDrawable();
            gd.setStroke(4, Color.parseColor("#1ba1e2"));
            gd.setColor(Color.parseColor("#1ba1e2"));
            gd.setCornerRadii(arr1);
            btn_device.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_device_blue));
            btn_device.setTextColor(Color.parseColor("#ffffff"));

            sendBtnLayout.setVisibility(View.GONE);
//                deviceLayout.setVisibility(View.VISIBLE);
            deviceBtnLayout.setVisibility(View.VISIBLE);
            txtTo.startAnimation(animSlideUp);
            sendLayout.startAnimation(animMoveUp);
        }

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                anim_flag = 1;
                CreateReminderActivity.btn_flag = 0;
                gd = new GradientDrawable();
                gd.setStroke(4, Color.parseColor("#1ba1e2"));
                gd.setColor(Color.parseColor("#1ba1e2"));
                gd.setCornerRadii(arr);
                btn_send.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_send));
                btn_send.setTextColor(Color.parseColor("#ffffff"));

                gd = new GradientDrawable();
                gd.setStroke(4, Color.parseColor("#1ba1e2"));
                gd.setColor(Color.parseColor("#ffffff"));
                gd.setCornerRadii(arr1);
                btn_device.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_device));
                btn_device.setTextColor(Color.parseColor("#1ba1e2"));

                txtTo.startAnimation(animSlideDown);
                sendLayout.startAnimation(animMoveDown);

                deviceBtnLayout.setVisibility(View.GONE);
                sendBtnLayout.setVisibility(View.VISIBLE);
                deviceBtnLayout.startAnimation(animFadeIn);
                sendBtnLayout.startAnimation(animSlideDown);

            }
        });

        btn_device.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                anim_flag = 2;
                CreateReminderActivity.btn_flag = 1;
                gd = new GradientDrawable();
                gd.setStroke(4, Color.parseColor("#1ba1e2"));
                gd.setColor(Color.parseColor("#ffffff"));
                gd.setCornerRadii(arr);
                btn_send.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_send_white));
                btn_send.setTextColor(Color.parseColor("#1ba1e2"));

                gd = new GradientDrawable();
                gd.setStroke(4, Color.parseColor("#1ba1e2"));
                gd.setColor(Color.parseColor("#1ba1e2"));
                gd.setCornerRadii(arr1);
                btn_device.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_device_blue));
                btn_device.setTextColor(Color.parseColor("#ffffff"));

                sendBtnLayout.setVisibility(View.GONE);
                deviceBtnLayout.setVisibility(View.VISIBLE);
                txtTo.startAnimation(animSlideUp);
                sendLayout.startAnimation(animMoveUp);
                sendBtnLayout.startAnimation(animFadeIn);
                deviceBtnLayout.startAnimation(animSlideDown);
            }
        });
        return v;
    }

    public static void applyDim(@NonNull ViewGroup parent, float dimAmount){
        Drawable dim = new ColorDrawable(Color.BLACK);
        dim.setBounds(0, 0, parent.getWidth(), parent.getHeight());
        dim.setAlpha((int) (255 * dimAmount));

        ViewGroupOverlay overlay = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            overlay = parent.getOverlay();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            overlay.add(dim);
        }
    }

    public static void clearDim(@NonNull ViewGroup parent) {
        ViewGroupOverlay overlay = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            overlay = parent.getOverlay();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            overlay.clear();
        }
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation == animMoveUp) {
//            txtTo.setVisibility(View.GONE);
            int left = sendLayout.getLeft();
            int top = sendLayout.getTop();
            int right = sendLayout.getRight();
            int bottom = sendLayout.getBottom();
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}


