package com.horizzon.reminderapp.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.horizzon.reminderapp.AttachmentActivity;
import com.horizzon.reminderapp.ContactActivity;
import com.horizzon.reminderapp.DateTimeActivity;
import com.horizzon.reminderapp.EditReminderActivity;
import com.horizzon.reminderapp.InboxActivity;
import com.horizzon.reminderapp.InboxCommentActivity;
import com.horizzon.reminderapp.InboxDetailActivity;
import com.horizzon.reminderapp.LocationActivity;
import com.horizzon.reminderapp.MoveToFolderActivity;
import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.dao.CReminder;
import com.horizzon.reminderapp.dao.ReminderContactUser;
import com.horizzon.reminderapp.dao.ReminderDateTime;
import com.horizzon.reminderapp.dao.ReminderForList;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.CRAttachmentDbHandler;
import com.horizzon.reminderapp.handler.CRCommentDbHandler;
import com.horizzon.reminderapp.handler.CRContactDbHandler;
import com.horizzon.reminderapp.handler.CRDateTimeDbHandler;
import com.horizzon.reminderapp.handler.CRLocationDbHandler;
import com.horizzon.reminderapp.handler.CReminderForListDbHandler;
import com.horizzon.reminderapp.retrofit.model.reminder.Reminder;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;
import com.horizzon.reminderapp.utility.DateUtilitys;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by horizzon on 4/3/2017.
 */

public class InboxListAdapter extends RecyclerView.Adapter<InboxListAdapter.MyViewHolder> {

    Activity context;
    ArrayList<CReminder> list;
    String currdate = "", inFor;
    int selitem = -1;
    boolean overlay = false;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout ilmain;
        LinearLayout listcontent, controllslayout, tilewoicon;
        ImageView imgProfile, con_image, imgPriority1, imgPriority2, imgPriority3, imgAttachment, imgComment, imgLocation;
        ImageView detailuparrow,inboxlockbtn;
        TextView unameName, name, time, unread,inboxlocktext;
        RelativeLayout btn_ic_Loc, btn_ic_attach, btn_ic_comm, btn_ic_datetime, btn_ic_addperson, btn_ic_folder, btn_ic_lock, btn_ic_edit, btn_ic_detail, btn_ic_share, btn_ic_threedot;
        TextView btn_ic_Loc_count, btn_ic_attach_count, btn_ic_comm_count;
        ImageView icon_loc, icon_attach, icon_comment, icon_datetime, icon_contact, icon_folder, detailreject, detaildone;
        LinearLayout voverlay, adminsidearrow,laymakeadmin;
        ExpandableRelativeLayout locklayoutexpandable;

        public MyViewHolder(View view) {
            super(view);
            ilmain = (RelativeLayout) view.findViewById(R.id.ilmain);
            listcontent = (LinearLayout) view.findViewById(R.id.listcontent);
            controllslayout = (LinearLayout) view.findViewById(R.id.controllslayout);
            tilewoicon = (LinearLayout) view.findViewById(R.id.tilewoicon);

            imgProfile = (ImageView) view.findViewById(R.id.imgProfile);
            con_image = (ImageView) view.findViewById(R.id.con_image);
            imgPriority1 = (ImageView) view.findViewById(R.id.imgPriority1);
            imgPriority2 = (ImageView) view.findViewById(R.id.imgPriority2);
            imgPriority3 = (ImageView) view.findViewById(R.id.imgPriority3);
            imgAttachment = (ImageView) view.findViewById(R.id.imgAttach);
            imgComment = (ImageView) view.findViewById(R.id.imgComt);
            imgLocation = (ImageView) view.findViewById(R.id.imgLocation);

            detailuparrow = (ImageView) view.findViewById(R.id.detailuparrow);
            inboxlockbtn = (ImageView) view.findViewById(R.id.inboxlockbtn);

            unameName = (TextView) view.findViewById(R.id.unameName);
            name = (TextView) view.findViewById(R.id.reminderName);
            time = (TextView) view.findViewById(R.id.reminderTime);
            unread = (TextView) view.findViewById(R.id.reminderUnread);
            inboxlocktext = (TextView) view.findViewById(R.id.inboxlocktext);


            btn_ic_Loc_count = (TextView) view.findViewById(R.id.btn_ic_Loc_count);
            btn_ic_attach_count = (TextView) view.findViewById(R.id.btn_ic_attach_count);
            btn_ic_comm_count = (TextView) view.findViewById(R.id.btn_ic_comm_count);

            btn_ic_Loc = (RelativeLayout) view.findViewById(R.id.btn_ic_Loc);
            btn_ic_attach = (RelativeLayout) view.findViewById(R.id.btn_ic_attach);
            btn_ic_comm = (RelativeLayout) view.findViewById(R.id.btn_ic_comm);
            btn_ic_datetime = (RelativeLayout) view.findViewById(R.id.btn_ic_datetime);
            btn_ic_addperson = (RelativeLayout) view.findViewById(R.id.btn_ic_addperson);
            btn_ic_folder = (RelativeLayout) view.findViewById(R.id.btn_ic_folder);
            btn_ic_lock = (RelativeLayout) view.findViewById(R.id.btn_ic_lock);
            btn_ic_edit = (RelativeLayout) view.findViewById(R.id.btn_ic_edit);
            btn_ic_detail = (RelativeLayout) view.findViewById(R.id.btn_ic_detail);
            btn_ic_share = (RelativeLayout) view.findViewById(R.id.btn_ic_share);
            btn_ic_threedot = (RelativeLayout) view.findViewById(R.id.btn_ic_threedot);

            icon_loc = (ImageView) view.findViewById(R.id.icon_loc);
            icon_attach = (ImageView) view.findViewById(R.id.icon_attach);
            icon_comment = (ImageView) view.findViewById(R.id.icon_comment);
            icon_datetime = (ImageView) view.findViewById(R.id.icon_datetime);
            icon_contact = (ImageView) view.findViewById(R.id.icon_contact);
            icon_folder = (ImageView) view.findViewById(R.id.icon_folder);

            detailreject = (ImageView) view.findViewById(R.id.detailreject);
            detaildone = (ImageView) view.findViewById(R.id.detaildone);
            voverlay = (LinearLayout) view.findViewById(R.id.voverlay);
            adminsidearrow = (LinearLayout) view.findViewById(R.id.adminsidearrow);
            locklayoutexpandable = (ExpandableRelativeLayout) view.findViewById(R.id.locklayoutexpandable);
            locklayoutexpandable = (ExpandableRelativeLayout) view.findViewById(R.id.locklayoutexpandable);
            laymakeadmin = (LinearLayout) view.findViewById(R.id.laymakeadmin);

        }
    }


    public InboxListAdapter(Activity context, ArrayList<CReminder> list, String inFor) {
        this.context = context;
        this.list = list;
        this.inFor = inFor;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_inbox_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final CReminder cr = list.get(position);

        final String SessionId = cr.getSesssionId();

        final Animation slideUpAnimation, slideDownAnimation;
        slideUpAnimation = AnimationUtils.loadAnimation(context,
                R.anim.slide_up);

        slideDownAnimation = AnimationUtils.loadAnimation(context,
                R.anim.slide_down);
        slideUpAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                holder.controllslayout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


        holder.btn_ic_lock.setVisibility(View.GONE);
        holder.btn_ic_edit.setVisibility(View.GONE);

        if (cr.getLockstatus().equals("lock")) {
            holder.btn_ic_lock.setVisibility(View.VISIBLE);
        } else {
            holder.btn_ic_edit.setVisibility(View.VISIBLE);
            holder.btn_ic_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Common.createSharedPreferences(context, "SessionId", SessionId);
//                    Intent in = new Intent(context, CreateReminderActivity.class);
                    Intent in = new Intent(context, EditReminderActivity.class);
                    in.putExtra("SessionId", SessionId);
                    in.putExtra("action", "edit");
                    in.putExtra("newrem", "no");
                    context.startActivity(in);
                }
            });
        }

        holder.name.setText(cr.getSubject());

        CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(context);
        ReminderDateTime rdt = crdtdbh.getTableSingleDateWihtDao(SessionId);
        Common.DisplayLog("crdtdbh.getDataCount(SessionId)", crdtdbh.getDataCount(SessionId) + "");


        final DateUtilitys dateUtl = new DateUtilitys();

        Date date;
        if (crdtdbh.getDataCount(SessionId) > 0) {
            date = dateUtl.FormetDate(rdt.getStartDate() + " " + rdt.getStartTime());

            holder.icon_datetime.setImageResource(R.drawable.datetimeiconblue);
            holder.btn_ic_datetime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Common.createSharedPreferences(context, "SessionId", SessionId);
                    Intent in = new Intent(context, DateTimeActivity.class);
                    in.putExtra("SessionId", SessionId);
                    in.putExtra("action", "edit");
                    in.putExtra("newrem", "no");
                    context.startActivity(in);
                }
            });

        }
        else {
            holder.icon_datetime.setImageResource(R.drawable.datetimeicon);
            Common.DisplayLog("cr.getCreateddate()", cr.getCreateddate() + "");
            date = dateUtl.FormetDate(cr.getCreateddate());

        }
        dateUtl.setforDisplayTitle(date);
        holder.time.setText(dateUtl.getDay() + " " + dateUtl.getMonth());


        holder.imgPriority1.setImageResource(R.drawable.prioritywhite);
        holder.imgPriority2.setImageResource(R.drawable.prioritywhite);
        holder.imgPriority3.setImageResource(R.drawable.prioritywhite);
        if (cr.getPriority().equals("1")) {
            holder.imgPriority1.setImageResource(R.drawable.priorityiconyellow);
        } else if (cr.getPriority().equals("2")) {
            holder.imgPriority1.setImageResource(R.drawable.priorityiconyellow);
            holder.imgPriority2.setImageResource(R.drawable.priorityiconyellow);
        } else if (cr.getPriority().equals("3")) {
            holder.imgPriority1.setImageResource(R.drawable.priorityiconyellow);
            holder.imgPriority2.setImageResource(R.drawable.priorityiconyellow);
            holder.imgPriority3.setImageResource(R.drawable.priorityiconyellow);
        }


        // set Contact
        CRContactDbHandler crcdbh = new CRContactDbHandler(context);
        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllCurrentSessContactData(SessionId, "contact");
//        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllContactData();

        Common.DisplayLog("remindercontactuser", remindercontactuser.size() + "");
        String tempToText = "";
        int i = remindercontactuser.size();
        if (i > 0) {
            if (1 <= i) {
                tempToText += remindercontactuser.get(0).getName() + ",";
            }
            String imgpath = ((remindercontactuser.get(0).getPhoto().equals("")) ? "defaultimg" : remindercontactuser.get(0).getPhoto()) + "";
            Common.DisplayLog("imgpath", imgpath + "");
            Picasso.with(context)
                    .load(imgpath)
                    .placeholder(R.drawable.defaultusericon)
                    .error(R.drawable.defaultusericon)
                    .into(holder.con_image);

            holder.icon_contact.setImageResource(R.drawable.personiconblue);
        } else {
            holder.icon_contact.setImageResource(R.drawable.personicon);
        }


        tempToText = (tempToText != null && !tempToText.equals("")) ? tempToText.substring(0, tempToText.length() - 1) : "";
        if (i > 2) {
            tempToText += " & " + (i - 1) + " others";
        }

        holder.unameName.setText(tempToText);


        // set Attachment
        CRAttachmentDbHandler cradbh = new CRAttachmentDbHandler(context);
        Common.DisplayLog("cradbh.getDataCount(SessionId)", cradbh.getDataCount(SessionId, "done") + "");
        if (cradbh.getDataCount(SessionId, "done") > 0) {
            holder.imgAttachment.setImageResource(R.drawable.attachiconblue);

            holder.icon_attach.setImageResource(R.drawable.attachiconblue);
            holder.btn_ic_attach_count.setText(cradbh.getDataCount(SessionId, "done") + "");
            holder.btn_ic_attach.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Common.createSharedPreferences(context, "SessionId", SessionId);
                    Intent in = new Intent(context, AttachmentActivity.class);
                    in.putExtra("SessionId", SessionId);
                    in.putExtra("action", "edit");
                    in.putExtra("inFrom", "main");
                    context.startActivity(in);
                }
            });
        } else {
            holder.imgAttachment.setImageResource(R.drawable.attachiconwhite);

            holder.icon_attach.setImageResource(R.drawable.attachicon);
            holder.btn_ic_attach_count.setText("0");
            holder.btn_ic_attach_count.setBackground(null);
        }

        // set Comment

        CRCommentDbHandler crcodbh = new CRCommentDbHandler(context);
        Common.DisplayLog("crcodbh.getDataCount(SessionId)", crcodbh.getDataCount(SessionId) + "");
        if (crcodbh.getDataCount(SessionId) > 0) {
            holder.imgComment.setImageResource(R.drawable.comticonblue);

            holder.icon_comment.setImageResource(R.drawable.comticonblue);
            holder.btn_ic_comm_count.setText(crcodbh.getDataCount(SessionId) + "");
            holder.btn_ic_comm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Common.createSharedPreferences(context, "SessionId", SessionId);
                    Intent in = new Intent(context, InboxCommentActivity.class);
                    in.putExtra("SessionId", SessionId);
                    in.putExtra("action", "edit");
                    in.putExtra("newrem", "no");
                    context.startActivity(in);
                }
            });
        } else {
            holder.imgComment.setImageResource(R.drawable.commnticon);

            holder.btn_ic_comm_count.setText("0");
            holder.btn_ic_comm_count.setBackground(null);
        }

        // set Date Time
        /*CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(context);
        Common.DisplayLog("crdtdbh.getDataCount(SessionId)", crdtdbh.getDataCount(SessionId) + "");
        if (crdtdbh.getDataCount(SessionId) > 0) {
            holder.imgLocation.setImageResource(R.drawable.datetimebluerounded);
        } else {
            holder.imgLocation.setImageResource(R.drawable.datetimerounded);
        }*/

        // set Location
        CRLocationDbHandler crldbh = new CRLocationDbHandler(context);
        Common.DisplayLog("crldbh.getDataCount(SessionId)", crldbh.getDataCount(SessionId) + "");
        if (crldbh.getDataCount(SessionId) > 0) {
            holder.imgLocation.setImageResource(R.drawable.locationiconblue);
            holder.icon_loc.setImageResource(R.drawable.locationiconblue);

            holder.btn_ic_Loc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Common.createSharedPreferences(context, "SessionId", SessionId);
                    Intent in = new Intent(context, LocationActivity.class);
                    in.putExtra("SessionId", SessionId);
                    in.putExtra("action", "edit");
                    in.putExtra("newrem", "no");
                    context.startActivity(in);
                }
            });

        } else {
            holder.imgLocation.setImageResource(R.drawable.locationiconwhite);
        }

        /** ADD Contact **/
        holder.btn_ic_addperson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Common.createSharedPreferences(context, "SessionId", SessionId);
                Intent in = new Intent(context, ContactActivity.class);
                in.putExtra("SessionId", SessionId);
                in.putExtra("action", "edit");
                in.putExtra("newrem", "no");
                in.putExtra("moveto", "1");
                context.startActivity(in);
            }
        });

        holder.btn_ic_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Common.createSharedPreferences(context, "SessionId", SessionId);
//                Intent in = new Intent(context, DetailViewActivity.class);
                Intent in = new Intent(context, InboxDetailActivity.class);
                in.putExtra("SessionId", SessionId);
                in.putExtra("action", "edit");
                context.startActivity(in);
            }
        });

        final DateUtilitys dateUtl1 = dateUtl;
        holder.btn_ic_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, cr.getSubject());
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Your reminder for " + dateUtl1.getforDisplayTitle());
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });

        holder.btn_ic_threedot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.locklayoutexpandable.expand();
                /*PopupMenu popup = new PopupMenu(context, v);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.inboxpopuplist, popup.getMenu());
                popup.show();
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id = item.getItemId();
                        if (id == R.id.action_moredetail) {
//                            Toast.makeText(context,"hai it's Removed",Toast.LENGTH_LONG).show();
                        }
                        return false;
                    }
                });*/
            }
        });
        holder.locklayoutexpandable.collapse();
       /* holder.adminsidearrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/
        if (cr.getLockstatus().equals("lock")) {
            holder.inboxlockbtn.setImageResource(R.drawable.inboxlockblue);
            holder.inboxlocktext.setTextColor(Color.parseColor("#1ba1e2"));
        }else{

            holder.inboxlockbtn.setImageResource(R.drawable.inboxlockgrey);
            holder.inboxlocktext.setTextColor(Color.parseColor("#a0a0a1"));
        }
        holder.inboxlockbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        CReminderForListDbHandler crfldbh = new CReminderForListDbHandler(context);
        ReminderForList rfl = crfldbh.getTableSingleDateWihtDao(SessionId, Common.getSharedPreferences(context, "userId", "0"));

        if (rfl == null || rfl.getvReminderStatus().equals("reject") || rfl.getvReminderStatus().equals("accept")) {
            holder.detailreject.setVisibility(View.GONE);
            holder.detaildone.setVisibility(View.GONE);
        }

        if (rfl != null && rfl.getvReminderStatus().equals("")) {
            Common.DisplayLog("rfl.getvReminderStatus()", SessionId + " >> " + Common.getSharedPreferences(context, "userId", "0") + " >> " + rfl.getvReminderStatus() + "");
            holder.detailreject.setVisibility(View.VISIBLE);
            holder.detaildone.setVisibility(View.VISIBLE);
        }

        holder.detailreject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateReminderStatus(context, SessionId, "reject");
            }
        });
        holder.detaildone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateReminderStatus(context, SessionId, "accept");
            }
        });


        /*** open close***/
        holder.controllslayout.setVisibility(View.GONE);
        holder.voverlay.setVisibility(View.GONE);
        Common.DisplayLog("overlay ", "" + overlay + " && " + selitem + " != " + position + "");

        if (overlay && selitem != position) {
            Common.DisplayLog("overlay first con id is  :: ", "" + overlay + " && " + selitem + " != " + position + "");
            holder.voverlay.setVisibility(View.VISIBLE);
            holder.voverlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    overlay = false;
                    selitem = -1;
                    holder.controllslayout.startAnimation(slideUpAnimation);
//                holder.controllslayout.setVisibility(View.GONE);
                    holder.tilewoicon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            holder.controllslayout.setVisibility(View.VISIBLE);
                            holder.controllslayout.startAnimation(slideDownAnimation);
                            holder.tilewoicon.setOnClickListener(null);
                            overlay = true;
                            selitem = position;

                            notifyDataSetChanged();
                        }
                    });
                    notifyDataSetChanged();
                }
            });
        } else {

            holder.tilewoicon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (selitem == position) {
                        holder.detailuparrow.performClick();
                    } else {

                        holder.controllslayout.setVisibility(View.VISIBLE);
                        holder.controllslayout.startAnimation(slideDownAnimation);
                        holder.tilewoicon.setOnClickListener(null);
                        overlay = true;
                        selitem = position;
                        notifyDataSetChanged();
                    }
//                    holder.detailuparrow.performClick();
                }
            });
            /*holder.listcontent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.detailuparrow.performClick();
                }
            });*/
        }
        if (selitem == position) {
            Common.DisplayLog("overlay second con id is  :: ", selitem + " == " + position + "");
            holder.voverlay.setVisibility(View.GONE);
            holder.controllslayout.setVisibility(View.VISIBLE);
            holder.controllslayout.startAnimation(slideDownAnimation);


            holder.imgComment.setImageResource(R.drawable.commnticon);
            holder.imgLocation.setImageResource(R.drawable.locationiconwhite);
            holder.imgAttachment.setImageResource(R.drawable.attachiconwhite);
        }

        holder.detailuparrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overlay = false;
                selitem = -1;
                holder.controllslayout.startAnimation(slideUpAnimation);
//                holder.controllslayout.setVisibility(View.GONE);
                holder.tilewoicon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.controllslayout.setVisibility(View.VISIBLE);
                        holder.controllslayout.startAnimation(slideDownAnimation);
                        holder.tilewoicon.setOnClickListener(null);
                        overlay = true;
                        selitem = position;

                        notifyDataSetChanged();
                    }
                });
                notifyDataSetChanged();
            }
        });
        holder.voverlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overlay = false;
                selitem = -1;
                holder.controllslayout.startAnimation(slideUpAnimation);
//                holder.controllslayout.setVisibility(View.GONE);
                holder.tilewoicon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.controllslayout.setVisibility(View.VISIBLE);
                        holder.controllslayout.startAnimation(slideDownAnimation);
                        holder.tilewoicon.setOnClickListener(null);
                        overlay = true;
                        selitem = position;

                        notifyDataSetChanged();
                    }
                });
                notifyDataSetChanged();
            }
        });


        holder.ilmain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent in = new Intent(context, ReminderDetailActivity.class);
//                in.putExtra("SessionId", SessionId);
//                context.startActivity(in);
            }
        });

        holder.btn_ic_folder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.createSharedPreferences(context, "SessionId", SessionId);
//                    Intent in = new Intent(context, CreateReminderActivity.class);
                Intent in = new Intent(context, MoveToFolderActivity.class);
                in.putExtra("SessionId", SessionId);
                in.putExtra("action", "edit");
                in.putExtra("newrem", "no");
                context.startActivity(in);
            }
        });

        if(cr.getiUserId().equals(Common.getSharedPreferences(context, "userId", "0"))) {
            holder.laymakeadmin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                /*Intent i = new Intent(context, InboxContactActivity.class);
                i.putExtra("moveto", "2");
                i.putExtra("SessionId", SessionId);
                context.startActivityForResult(i, 21);*/
                    Common.createSharedPreferences(context, "SessionId", SessionId);
                    InboxActivity _iA = (InboxActivity) context;
                    _iA.clickmakeadmin(SessionId);
                }
            });
        }else{
            holder.laymakeadmin.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public void updateReminderStatus(final Activity _ac, final String SessId, String status) {


        /*** update Reminder Status ****/
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        CReminderForListDbHandler crfldbh = new CReminderForListDbHandler(_ac);

        ReminderForList rfl = crfldbh.getTableSingleDateWihtDao(SessId, Common.getSharedPreferences(_ac, "userId", "0"));
        if (Common.hasInternetConnection(_ac)) {
            if (rfl != null) {

                crfldbh.updataReminderStatus(rfl.getiUserId(), rfl.getvSesssionId(), status);
                final ProgressDialog pd = new ProgressDialog(_ac);
                pd.setMessage("Loading...");
                pd.setIndeterminate(true);
                pd.setCancelable(false);
                pd.show();
                Call<Reminder> callDateTime = apiService.updateReminderStatus(rfl.getiUserId(), rfl.getvSesssionId(), status);
                callDateTime.enqueue(new Callback<Reminder>() {
                    @Override
                    public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                        int statusCode = response.code();
                        Common.DisplayLog("response.body()", response.body().toString());
                        boolean status = response.body().getStatus();
                        Common.DisplayLog("status", status + "");
                        Common.DisplayLog("getMessage", response.body().getMessage() + "");

                        if (status) {
                            //setBrodCastForReminder(SessId);
//                        Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                        } else {
                            Common.ShowToast(_ac, response.body().getMessage());

                        }
                        pd.dismiss();
//                        imgBack.performClick();

                    }

                    @Override
                    public void onFailure(Call<Reminder> call, Throwable t) {

                        Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
                        pd.dismiss();
//                        imgBack.performClick();
                    }


                });
            }
        }
    }


}
