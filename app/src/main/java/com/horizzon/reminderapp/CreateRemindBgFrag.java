package com.horizzon.reminderapp;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.OneTimeWorkRequest;

import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.horizzon.reminderapp.adapter.BaseMessageViewHolder;
import com.horizzon.reminderapp.adapter.NewFolderAdapter;
import com.horizzon.reminderapp.dao.AttachmentTable;
import com.horizzon.reminderapp.dao.CReminder;
import com.horizzon.reminderapp.dao.ReminderComment;
import com.horizzon.reminderapp.dao.ReminderContactUser;
import com.horizzon.reminderapp.dao.ReminderDateTime;
import com.horizzon.reminderapp.dao.ReminderLocation;
import com.horizzon.reminderapp.dao.ReminderUser;
import com.horizzon.reminderapp.dao.SessionIDModel;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.firebasedata.Helper;
import com.horizzon.reminderapp.firebasedata.MyFirebaseMessagingService;
import com.horizzon.reminderapp.handler.CRAttachmentDbHandler;
import com.horizzon.reminderapp.handler.CRCommentDbHandler;
import com.horizzon.reminderapp.handler.CRContactDbHandler;
import com.horizzon.reminderapp.handler.CRDateTimeDbHandler;
import com.horizzon.reminderapp.handler.CRLocationDbHandler;
import com.horizzon.reminderapp.handler.CreateReminderDbHandler;
import com.horizzon.reminderapp.handler.RUserDbHandler;
import com.horizzon.reminderapp.model.Attachment;
import com.horizzon.reminderapp.model.AttachmentTypes;
import com.horizzon.reminderapp.model.Group;
import com.horizzon.reminderapp.model.GroupChat;
import com.horizzon.reminderapp.model.Message;
import com.horizzon.reminderapp.receiver.AlarmBroadCustReciver;
import com.horizzon.reminderapp.retrofit.model.GetFolderNameList;
import com.horizzon.reminderapp.retrofit.model.ReminderCount;
import com.horizzon.reminderapp.retrofit.model.reminder.Reminder;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;
import com.horizzon.reminderapp.service.MyWorker;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.ALARM_SERVICE;
import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by horizzon on 12/21/2016.
 */
public class CreateRemindBgFrag extends Fragment implements Animation.AnimationListener {
    Button btn_send, btn_device, btnTempDone, btnSave, btnSend, btnShare;
    GradientDrawable gd;
    float[] arr = new float[8];
    float[] arr1 = new float[8];
    EditText dtxtSubject;
    AppCompatEditText txtSubject;
    LinearLayout sendLayout, deviceLayout, sendBtnLayout, deviceBtnLayout, dsubjectLayout;
    RelativeLayout subjectLayout;
    ImageView imgBack, imgDateTime, dimgDateTime, imgLocation, dimgLocation, imgComt, imglock, dimgComt, imgProfile;
    LinearLayout toLayout, doneContainer, footerContainer;
    ImageView imgPriority1, imgPriority2, imgPriority3, dimgPriority1, dimgPriority2, dimgPriority3;
    public int prio1_flag = 1;
    public int prio2_flag = 1;
    public int prio3_flag = 1;
    public int dprio1_flag = 1;
    public int dprio2_flag = 1;
    public int dprio3_flag = 1;
    Animation animFadeIn;
    Animation animSlideDown;
    Animation animSlideUp;
    Animation animMoveUp;
    Animation animMoveDown;
    ImageView attachImg, dattachImg;
    TextView txtSub, dtxtSub, txtTo;
    LinearLayout contactViewLayout;
    String SessionId, action;
    CreateReminderDbHandler crdbh;
    String inFrom = "";
    RUserDbHandler rudbh;
    ProgressDialog pd;
    String commnent;
    SharedPreferences sh;
    String s1, userName;
    boolean sreminder = false, scontect = false, sdate = false, sloction = false, sattachment = false, scomment = false;
    protected DatabaseReference usersRef, groupRef, chatRef, inboxRef;
    AppCompatButton btn_no, btn_yes;
    RecyclerView rvNewFolder;
    String buttondata;
    boolean selectData;
    NewFolderAdapter newFolderAdapter;
    String UId;
    List<GetFolderNameList.Datum> folderName = new ArrayList<>();
    String dateS, Times, part1, part2, part3, time1, time2, time3;
    List<SessionIDModel> sessionIDList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.create_reminder_bg, container, false);
        final View v2 = v;
//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        crdbh = new CreateReminderDbHandler(getActivity().getApplicationContext());
        rudbh = new RUserDbHandler(getActivity().getApplicationContext());

        SessionId = Common.getSharedPreferences(getActivity(), "SessionId", "");
        Common.DisplayLog("SessionId", SessionId);

        if (getActivity().getIntent().getStringExtra("inFrom") != null) {
            inFrom = (getActivity().getIntent().getStringExtra("inFrom"));
        }
        Common.DisplayLog("inFrom", inFrom + "");

        action = getActivity().getIntent().getStringExtra("action");
        if (action == null || action.equals("")) {
            action = "add";
        }
        sh = getActivity().getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        s1 = sh.getString("mobile", "");
        userName = sh.getString("userName", "");
        UId = sh.getString("UId", "");
        btnSave = (Button) v.findViewById(R.id.btnSave);
        btnSend = (Button) v.findViewById(R.id.btnSend);
        btnShare = (Button) v.findViewById(R.id.btnShare);
        btn_send = (Button) v.findViewById(R.id.btn_send);
        btn_device = (Button) v.findViewById(R.id.btn_device);
        btnTempDone = (Button) v.findViewById(R.id.btnTempDone);

        contactViewLayout = (LinearLayout) v.findViewById(R.id.contactViewLayout);
        doneContainer = (LinearLayout) v.findViewById(R.id.doneContainer);
        footerContainer = (LinearLayout) v.findViewById(R.id.footerContainer);
        doneContainer.setVisibility(View.GONE);
        footerContainer.setVisibility(View.VISIBLE);

        sendLayout = (LinearLayout) v.findViewById(R.id.sendLayout);
        deviceLayout = (LinearLayout) v.findViewById(R.id.deviceLayout);
        sendBtnLayout = (LinearLayout) v.findViewById(R.id.sendBtnLayout);
        deviceBtnLayout = (LinearLayout) v.findViewById(R.id.deviceBtnLayout);
        subjectLayout = (RelativeLayout) v.findViewById(R.id.subjectLayout);
        dsubjectLayout = (LinearLayout) v.findViewById(R.id.dsubjectLayout);
        toLayout = (LinearLayout) v.findViewById(R.id.toLayout);

        imgBack = (ImageView) v.findViewById(R.id.backBtn);

        imgProfile = (ImageView) v.findViewById(R.id.imgProfile);
        imgDateTime = (ImageView) v.findViewById(R.id.imgDateTime);
        dimgDateTime = (ImageView) v.findViewById(R.id.dimgDateTime);
        imgLocation = (ImageView) v.findViewById(R.id.imgLocation);
        dimgLocation = (ImageView) v.findViewById(R.id.dimgLocation);
        imgComt = (ImageView) v.findViewById(R.id.imgComt);
        imglock = (ImageView) v.findViewById(R.id.imglock);
        dimgComt = (ImageView) v.findViewById(R.id.dimgComt);

        imgPriority1 = (ImageView) v.findViewById(R.id.imgPriority1);
        imgPriority2 = (ImageView) v.findViewById(R.id.imgPriority2);
        imgPriority3 = (ImageView) v.findViewById(R.id.imgPriority3);
        dimgPriority1 = (ImageView) v.findViewById(R.id.dimgPriority1);
        dimgPriority2 = (ImageView) v.findViewById(R.id.dimgPriority2);
        dimgPriority3 = (ImageView) v.findViewById(R.id.dimgPriority3);

        txtTo = (TextView) v.findViewById(R.id.txtTo);
        txtSub = (TextView) v.findViewById(R.id.txtSub);
        dtxtSub = (TextView) v.findViewById(R.id.dtxtSub);

        attachImg = (ImageView) v.findViewById(R.id.attchImg);
        dattachImg = (ImageView) v.findViewById(R.id.dattchImg);

        txtSubject = v.findViewById(R.id.txtSubject);
        dtxtSubject = (EditText) v.findViewById(R.id.dtxtSubject);


        // load the animation
        animFadeIn = AnimationUtils.loadAnimation(getActivity(),
                R.anim.fade_out);

        // set animation listener
        animFadeIn.setAnimationListener(this);

        // load the animation
        animSlideDown = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_down);

        // set animation listener
        animSlideDown.setAnimationListener(this);

        // load the animation
        animMoveUp = AnimationUtils.loadAnimation(getActivity(),
                R.anim.move);

        // set animation listener
        animMoveUp.setAnimationListener(this);

        // load the animation
        animMoveDown = AnimationUtils.loadAnimation(getActivity(),
                R.anim.move_down);

        // set animation listener
        animMoveDown.setAnimationListener(this);

        // load the animation
        animSlideDown = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_down);

        // set animation listener
        animSlideDown.setAnimationListener(this);

        // load the animation
        animSlideUp = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_up);

        // set animation listener
        animSlideUp.setAnimationListener(this);

        arr[0] = Float.valueOf(20);
        arr[1] = Float.valueOf(20);
        arr[2] = Float.valueOf(0);
        arr[3] = Float.valueOf(0);
        arr[4] = Float.valueOf(0);
        arr[5] = Float.valueOf(0);
        arr[6] = Float.valueOf(20);
        arr[7] = Float.valueOf(20);

        arr1[0] = Float.valueOf(0);
        arr1[1] = Float.valueOf(0);
        arr1[2] = Float.valueOf(20);
        arr1[3] = Float.valueOf(20);
        arr1[4] = Float.valueOf(20);
        arr1[5] = Float.valueOf(20);
        arr1[6] = Float.valueOf(0);
        arr1[7] = Float.valueOf(0);

        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();//get firebase instance
        usersRef = firebaseDatabase.getReference(Helper.REF_USERS);//instantiate user's firebase reference
        groupRef = firebaseDatabase.getReference(Helper.REF_GROUP);//instantiate group's firebase reference
        chatRef = firebaseDatabase.getReference(Helper.REF_CHAT);//instantiate chat's firebase reference
        inboxRef = firebaseDatabase.getReference(Helper.REF_INBOX);//instantiate inbox's firebase reference

        /** save subject **/
        txtSubject.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Common.DisplayLog("", "beforeTextChanged");
                Common.DisplayLog("", "beforeTextChanged == " + s.toString());
                crdbh.updataSubject(SessionId, s.toString());
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Common.DisplayLog("", "onTextChanged");
                Common.DisplayLog("", "onTextChanged == " + s.toString());
                crdbh.updataSubject(SessionId, s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                Common.DisplayLog("", "afterTextChanged");
                Common.DisplayLog("", "afterTextChanged == " + s.toString());
                crdbh.updataSubject(SessionId, s.toString());
            }
        });

        txtSubject.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View arg0, boolean arg1) {
                // subjectLayout.performClick();
                Common.DisplayLog("", "setOnFocusChangeListener");
                crdbh.updataSubject(SessionId, txtSubject.getText().toString());
            }
        });

        /*if (CreateReminderActivity.btn_flag == 1) {
            //subjectLayout.performClick();
            imgComt.setVisibility(View.GONE);
            imglock.setVisibility(View.GONE);
            gd = new GradientDrawable();
            gd.setStroke(4, Color.parseColor("#1ba1e2"));
            gd.setColor(Color.parseColor("#dee0e0"));
            gd.setCornerRadii(arr);
            btn_send.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_send_bg));
            btn_send.setTextColor(Color.parseColor("#1ba1e2"));

            gd = new GradientDrawable();
            gd.setStroke(4, Color.parseColor("#1ba1e2"));
            gd.setColor(Color.parseColor("#1ba1e2"));
            gd.setCornerRadii(arr1);
            btn_device.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_device_blue));
            btn_device.setTextColor(Color.parseColor("#dee0e0"));
            sendBtnLayout.setVisibility(View.GONE);
//                deviceLayout.setVisibility(View.VISIBLE);
            deviceBtnLayout.setVisibility(View.VISIBLE);
            // toLayout.startAnimation(animSlideUp);
            sendLayout.startAnimation(animMoveUp);
            toLayout.setVisibility(View.GONE);
            txtTo.setVisibility(View.GONE);
        } */
       /* else {
            imgComt.setVisibility(View.VISIBLE);
            imglock.setVisibility(View.VISIBLE);
            gd = new GradientDrawable();
            gd.setStroke(4, Color.parseColor("#1ba1e2"));
            gd.setColor(Color.parseColor("#dee0e0"));
            gd.setCornerRadii(arr);
            //  btn_send.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_send_bg));
            btn_send.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_device_blue));
            btn_send.setTextColor(Color.parseColor("#1ba1e2"));

            gd = new GradientDrawable();
            gd.setStroke(4, Color.parseColor("#1ba1e2"));
            gd.setColor(Color.parseColor("#1ba1e2"));
            gd.setCornerRadii(arr1);
            btn_device.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_send_bg));
            //    btn_device.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_device_blue));
            btn_device.setTextColor(Color.parseColor("#dee0e0"));
            sendBtnLayout.setVisibility(View.GONE);
//                deviceLayout.setVisibility(View.VISIBLE);
            deviceBtnLayout.setVisibility(View.VISIBLE);
            // toLayout.startAnimation(animSlideUp);
            sendLayout.startAnimation(animMoveUp);
            toLayout.setVisibility(View.GONE);
            txtTo.setVisibility(View.GONE);
        }*/
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* imgComt.setVisibility(View.VISIBLE);
                imglock.setVisibility(View.VISIBLE);*/
                if (CreateReminderActivity.btn_flag == 1) {
                    CreateReminderActivity.btn_flag = 0;
                    //subjectLayout.performClick();
                    gd = new GradientDrawable();
                    gd.setStroke(4, Color.parseColor("#1ba1e2"));
                    gd.setColor(Color.parseColor("#dee0e0"));
                    gd.setCornerRadii(arr);
                    btn_send.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_send_bg));
                    btn_send.setTextColor(Color.parseColor("#1ba1e2"));

                    gd = new GradientDrawable();
                    gd.setStroke(4, Color.parseColor("#1ba1e2"));
                    gd.setColor(Color.parseColor("#1ba1e2"));
                    gd.setCornerRadii(arr1);
                    btn_device.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_device_blue));
                    btn_device.setTextColor(Color.parseColor("#dee0e0"));
                    sendBtnLayout.setVisibility(View.GONE);
//                deviceLayout.setVisibility(View.VISIBLE);
                    deviceBtnLayout.setVisibility(View.VISIBLE);
                    // toLayout.startAnimation(animSlideUp);
                    sendLayout.startAnimation(animMoveUp);
                    toLayout.setVisibility(View.GONE);
                    txtTo.setVisibility(View.GONE);
                } else {
                    CreateReminderActivity.btn_flag = 0;
                    gd = new GradientDrawable();
                    gd.setStroke(4, Color.parseColor("#1ba1e2"));
                    gd.setColor(Color.parseColor("#1ba1e2"));
                    gd.setCornerRadii(arr);
                    btn_send.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_send));
                    btn_send.setTextColor(Color.parseColor("#dee0e0"));

                    gd = new GradientDrawable();
                    gd.setStroke(4, Color.parseColor("#1ba1e2"));
                    gd.setColor(Color.parseColor("#dee0e0"));
                    gd.setCornerRadii(arr1);
                    btn_device.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_device_bg));
                    btn_device.setTextColor(Color.parseColor("#1ba1e2"));
                    toLayout.setVisibility(View.VISIBLE);
                    txtTo.setVisibility(View.VISIBLE);
                    //  toLayout.startAnimation(animSlideDown);
                    sendLayout.startAnimation(animMoveDown);
                    deviceBtnLayout.setVisibility(View.GONE);
                    sendBtnLayout.setVisibility(View.VISIBLE);
                    deviceBtnLayout.startAnimation(animFadeIn);
                    sendBtnLayout.startAnimation(animSlideDown);
                }
//                deviceLayout.setVisibility(View.GONE);
//                deviceBtnLayout.setVisibility(View.GONE);
//                sendLayout.setVisibility(View.VISIBLE);
//                sendBtnLayout.setVisibility(View.VISIBLE);
//                deviceLayout.startAnimation(animFadeIn);
//                deviceBtnLayout.startAnimation(animFadeIn);
//
//                sendLayout.startAnimation(animSlideDown);
//                sendBtnLayout.startAnimation(animSlideDown);
                //   crdbh.updataReminderFor(SessionId, "send");
            }
        });

        btn_device.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  imgComt.setVisibility(View.GONE);
                imglock.setVisibility(View.GONE);*/
                if (CreateReminderActivity.btn_flag == 1) {
                    CreateReminderActivity.btn_flag = 1;
                    //subjectLayout.performClick();
                    gd = new GradientDrawable();
                    gd.setStroke(4, Color.parseColor("#1ba1e2"));
                    gd.setColor(Color.parseColor("#dee0e0"));
                    gd.setCornerRadii(arr);
                    btn_send.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_send_bg));
                    btn_send.setTextColor(Color.parseColor("#1ba1e2"));

                    gd = new GradientDrawable();
                    gd.setStroke(4, Color.parseColor("#1ba1e2"));
                    gd.setColor(Color.parseColor("#1ba1e2"));
                    gd.setCornerRadii(arr1);
                    btn_device.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_device_blue));
                    btn_device.setTextColor(Color.parseColor("#dee0e0"));
                    sendBtnLayout.setVisibility(View.GONE);
//                deviceLayout.setVisibility(View.VISIBLE);
                    deviceBtnLayout.setVisibility(View.VISIBLE);
                    // toLayout.startAnimation(animSlideUp);
                    sendLayout.startAnimation(animMoveUp);
                    toLayout.setVisibility(View.GONE);
                    txtTo.setVisibility(View.GONE);
                } else {
                    CreateReminderActivity.btn_flag = 1;
                    gd = new GradientDrawable();
                    gd.setStroke(4, Color.parseColor("#1ba1e2"));
                    gd.setColor(Color.parseColor("#dee0e0"));
                    gd.setCornerRadii(arr);
                    btn_send.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_send_bg));
                    btn_send.setTextColor(Color.parseColor("#1ba1e2"));

                    gd = new GradientDrawable();
                    gd.setStroke(4, Color.parseColor("#1ba1e2"));
                    gd.setColor(Color.parseColor("#1ba1e2"));
                    gd.setCornerRadii(arr1);
                    btn_device.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_device_blue));
                    btn_device.setTextColor(Color.parseColor("#dee0e0"));

                    sendBtnLayout.setVisibility(View.GONE);
                    deviceBtnLayout.setVisibility(View.VISIBLE);
                    toLayout.setVisibility(View.GONE);
                    txtTo.setVisibility(View.GONE);
                    //toLayout.startAnimation(animSlideUp);
                    sendLayout.startAnimation(animMoveUp);
                    sendBtnLayout.startAnimation(animFadeIn);
                    deviceBtnLayout.startAnimation(animSlideDown);

                    CRContactDbHandler crcdbh = new CRContactDbHandler(getActivity().getApplicationContext());
                    crcdbh.deleteDataForType(SessionId, "contact");
                    contactViewLayout.removeAllViews();
                    txtTo.setText("");
                }
                //  crdbh.updataReminderFor(SessionId, "device");
            }
        });

        imgPriority1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prio1_flag == 1) {
                    imgPriority1.setImageResource(R.drawable.priorityiconyellow);
                    imgPriority2.setImageResource(R.drawable.prioritywhite);
                    imgPriority3.setImageResource(R.drawable.prioritywhite);
                    prio1_flag = 0;
                } else {
                    imgPriority1.setImageResource(R.drawable.prioritywhite);
                    imgPriority2.setImageResource(R.drawable.prioritywhite);
                    imgPriority3.setImageResource(R.drawable.prioritywhite);
                    prio1_flag = 1;
                }
                prio2_flag = 1;
                prio3_flag = 1;
                crdbh.updataPriority(SessionId, "1");
            }
        });

        imgPriority2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prio2_flag == 1) {
                    imgPriority1.setImageResource(R.drawable.priorityiconyellow);
                    imgPriority2.setImageResource(R.drawable.priorityiconyellow);
                    imgPriority3.setImageResource(R.drawable.prioritywhite);
                    prio2_flag = 0;
                } else {
                    imgPriority1.setImageResource(R.drawable.prioritywhite);
                    imgPriority2.setImageResource(R.drawable.prioritywhite);
                    imgPriority3.setImageResource(R.drawable.prioritywhite);
                    prio2_flag = 1;
                }
                prio1_flag = 1;
                prio3_flag = 1;
                crdbh.updataPriority(SessionId, "2");
            }
        });

        imgPriority3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prio3_flag == 1) {
                    imgPriority1.setImageResource(R.drawable.priorityiconyellow);
                    imgPriority2.setImageResource(R.drawable.priorityiconyellow);
                    imgPriority3.setImageResource(R.drawable.priorityiconyellow);
                    prio3_flag = 0;
                } else {
                    imgPriority1.setImageResource(R.drawable.prioritywhite);
                    imgPriority2.setImageResource(R.drawable.prioritywhite);
                    imgPriority3.setImageResource(R.drawable.prioritywhite);
                    prio3_flag = 1;
                }
                prio1_flag = 1;
                prio2_flag = 1;
                crdbh.updataPriority(SessionId, "3");
            }
        });

        dimgPriority1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dimgPriority1.setImageResource(R.drawable.priorityiconyellow);
                dimgPriority2.setImageResource(R.drawable.prioritywhite);
                dimgPriority3.setImageResource(R.drawable.prioritywhite);
                crdbh.updataPriority(SessionId, "1");
            }
        });

        dimgPriority2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dimgPriority1.setImageResource(R.drawable.priorityiconyellow);
                dimgPriority2.setImageResource(R.drawable.priorityiconyellow);
                dimgPriority3.setImageResource(R.drawable.prioritywhite);
                crdbh.updataPriority(SessionId, "2");
            }
        });

        dimgPriority3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dimgPriority1.setImageResource(R.drawable.priorityiconyellow);
                dimgPriority2.setImageResource(R.drawable.priorityiconyellow);
                dimgPriority3.setImageResource(R.drawable.priorityiconyellow);
                crdbh.updataPriority(SessionId, "3");
            }
        });

        imgDateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), DateTimeActivity.class);
                in.putExtra("action", action);
                startActivity(in);
            }
        });

        dimgDateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), DateTimeActivity.class);
                in.putExtra("action", action);
                startActivity(in);
            }
        });

        imgLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), LocationActivity.class);
                in.putExtra("action", action);
                startActivity(in);
            }
        });

        dimgLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), LocationActivity.class);
                in.putExtra("action", action);
                startActivity(in);
            }
        });

        imgComt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), ReminderCommentsActivity.class);
                in.putExtra("action", action);
                startActivity(in);
            }
        });
        imglock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CReminder cReminder = crdbh.getTableSingleDateWihtDao(SessionId);
                if (cReminder != null && cReminder.getLockstatus().equals("unlock")) {
                    crdbh.updataLockStatus(SessionId, "lock");
                    imglock.setImageResource(R.drawable.lockbluerounded);
                } else {
                    crdbh.updataLockStatus(SessionId, "unlock");
                    imglock.setImageResource(R.drawable.lockrounded);
                }
            }
        });

        dimgComt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), ReminderCommentsActivity.class);
                startActivity(in);
            }
        });

        toLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(getActivity(), ContactActivity.class);
                in.putExtra("action", action);
                in.putExtra("moveto", "1");
                startActivity(in);
            }
        });

//        subjectLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent in = new Intent(getActivity(),AttachmentActivity.class);
//                startActivity(in);
//            }
//        });
//
//        dsubjectLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent in = new Intent(getActivity(),AttachmentActivity.class);
//                startActivity(in);
//            }
//        });

        txtSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDoneLayout(txtSubject, v);
            }
        });

        dtxtSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dtxtSubject.setFocusable(true);
                dtxtSubject.requestFocus();
                dtxtSubject.setCursorVisible(true);
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.showSoftInput(dtxtSubject, InputMethodManager.SHOW_IMPLICIT);
            }
        });


        txtSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDoneLayout(txtSubject, v);
            }
        });

        txtSubject.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    showDoneLayout(txtSubject, v);
                }
            }
        });

        dtxtSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dtxtSubject.setFocusable(true);
                dtxtSubject.requestFocus();
                dtxtSubject.setCursorVisible(true);
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.showSoftInput(dtxtSubject, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        attachImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), AttachmentActivity.class);
                in.putExtra("action", action);
                in.putExtra("inFrom", "main");
                startActivity(in);
            }
        });

        dattachImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), AttachmentActivity.class);
                in.putExtra("action", action);
                in.putExtra("inFrom", "main");
                startActivity(in);
            }
        });

        txtSubject.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_NEXT) || (actionId == EditorInfo.IME_FLAG_NO_ENTER_ACTION)) {
                    String notesub = txtSubject.getText().toString().trim();
                    Common.DisplayLog("setOnEditorActionListener", notesub + "");
                    crdbh.updataSubject(SessionId, notesub.toString());
                    //txtSubject.setCursorVisible(false);
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(txtSubject.getWindowToken(), 0);
                }
                return false;
            }
        });

        dtxtSubject.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_NEXT) || (actionId == EditorInfo.IME_FLAG_NO_ENTER_ACTION)) {
                    String notesub = dtxtSubject.getText().toString().trim();
                    Common.DisplayLog("setOnEditorActionListener", notesub + "");
                    crdbh.updataSubject(SessionId, notesub.toString());
                    dtxtSubject.setCursorVisible(false);
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(dtxtSubject.getWindowToken(), 0);
                }
                return false;
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*selectYourChoiseFloder("send");
                crdbh.updataReminderFor(SessionId, reminderType,"");
                SavedDataOnServer(SessionId, reminderType, "");*/
                crdbh.updataReminderFor(SessionId, "send", "");
                SavedDataOnServer(SessionId, "send", "");

            }
        });
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // selectYourChoiseFloder("share");
               /* crdbh.updataReminderFor(SessionId, "share","");
                SavedDataOnServer(SessionId, "share","");*/
                crdbh.updataReminderFor(SessionId, "share", "");
                SavedDataOnServer(SessionId, "share", "");
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // selectYourChoiseFloder("device");

                crdbh.updataReminderFor(SessionId, "device", "");
                SavedDataOnServer(SessionId, "device", "");
            }
        });

        Common.DisplayLog("", "onCreateView");
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        Common.DisplayLog("", "onResume");
        loadSavedData(SessionId);
        loadProfile();
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    private void loadProfile() {
        ReminderUser reminderUser = rudbh.getTableSingleDateWihtDao(Common.getSharedPreferences(getActivity().getApplicationContext(), "userId", "0"));
        String imgpath = ((reminderUser.getuPhoto().equals("")) ? "defaultimg" : reminderUser.getuPhoto()) + "";
        String userPic = "https://www.hwpl.in/projects/reminderapp" + imgpath.substring(2);
        Log.d("TAG", "loadSavedData: img :" +imgpath );
        Picasso.with(getActivity())
                .load(userPic)
                .placeholder(R.drawable.profilepic)
                .error(R.drawable.profilepic)
                .into(imgProfile);
    }

    private void showDoneLayout(EditText edt, final View v) {

        Common.DisplayLog("showDoneLayout", "showDoneLayout");
//        edt.setFocusable(true);
        edt.requestFocus();
        edt.setCursorVisible(true);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        imm.showSoftInput(edt, InputMethodManager.SHOW_IMPLICIT);
        doneContainer.setVisibility(View.VISIBLE);
        footerContainer.setVisibility(View.GONE);

        /*v.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                int heightDiff = v.getRootView().getHeight() - v.getHeight();
                Common.DisplayLog("heightDiff ", heightDiff + "");
                if (heightDiff > 150) {
                }
                doneContainer.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins(0, 0, 0, heightDiff);
//                doneContainer.setLayoutParams(params);
//                doneContainer.setPadding(0,0,0,heightDiff);
                // IF height diff is more then 150, consider keyboard as visible.
            }
        });*/
        btnTempDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
                String notesub = txtSubject.getText().toString().trim();
                Common.DisplayLog("setOnEditorActionListener", notesub + "");
                crdbh.updataSubject(SessionId, notesub.toString());
                //txtSubject.setCursorVisible(false);
                doneContainer.setVisibility(View.GONE);
                footerContainer.setVisibility(View.VISIBLE);
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(txtSubject.getWindowToken(), 0);
            }
        });

    }


    public void SavedDataOnServer(String SessId, String saveFor, String folderName) {
        /*
         * Check how many reminder is create  :: UId
         * */
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ReminderCount> call = apiService.checkRemider(UId);
        call.enqueue(new Callback<ReminderCount>() {
            @Override
            public void onResponse(Call<ReminderCount> call, Response<ReminderCount> response) {
                if (!response.body().getStatus()) {

                    /*
                     * Select folder name
                     * */
                    getFolderList(saveFor);

                } else {
                    Toast.makeText(getActivity(), "Your reminder is grater then 10.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ReminderCount> call, Throwable t) {
                sreminder = true;
                Common.DisplayLog("fail", t.getMessage().toString());
            }


        });
        // FolderViseData(SessId, saveFor, "");

        /*if (selectData) {
         *//*
         * Add data with selected folder
         * *//*
            FolderViseData(SessId, saveFor, buttondata);
        } else {
            *//*
         * Add data with out selected folder
         * *//*
            FolderViseData(SessId, saveFor, buttondata);
        }*/


    }

    public void dataonNotifaction(List<SessionIDModel> sessionIDModels) {

        for (int i = 0; i < sessionIDModels.size(); i++) {
            Log.d("TAG", "dataonNotifaction: session : " + sessionIDModels.get(i).getSesssionId());
            Log.d("TAG", "dataonNotifaction: session id:: " + sessionIDModels.get(i).getId());

            CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getContext());
            CReminder cReminder = crdbh.getTableSingleDateWihtDao(sessionIDModels.get(i).getSesssionId());

            CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(getContext());
            ReminderDateTime rdt = crdtdbh.getTableSingleDateWihtDao(sessionIDModels.get(i).getSesssionId());

            //if (crdtdbh.getDataCount(sessionIDModels.get(i).getSesssionId()) > 0) {
            // date = dateUtl.FormetDate(rdt.getStartDate() + " " + rdt.getStartTime());
            dateS = rdt.getStartDate();
            Times = rdt.getStartTime();

            String[] parts = dateS.split("-");
            part1 = parts[0];
            part2 = parts[1];
            part3 = parts[2];

            String[] time = Times.split(":");
            time1 = time[0];
            time2 = time[1];
            time3 = time[2];
            //}
            assert part1 != null;
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.MONTH, Integer.parseInt(part2));
            cal.set(Calendar.YEAR, Integer.parseInt(part1));
            cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(part3));
            cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time1));
            cal.set(Calendar.MINUTE, Integer.parseInt(time2));
            cal.set(Calendar.SECOND, 0);
           /* Calendar c = Calendar.getInstance();
            if (cal.compareTo(c) <= 0) {
                // Today Set time passed, count to tomorrow
                cal.add(Calendar.DATE, 1);
            }*/


            // new AlarmBroadCustReciver().setAlarm(getContext(), cal);
        }
    }

    public void dataonNotifaction(String sessionIDModels) {

        //   for (int i = 0; i < sessionIDModels.size(); i++) {
        //   Log.d("TAG", "dataonNotifaction: session : " + sessionIDModels.get(i).getSesssionId());
        //   Log.d("TAG", "dataonNotifaction: session id:: " + sessionIDModels.get(i).getId());

        CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getContext());
        CReminder cReminder = crdbh.getTableSingleDateWihtDao(sessionIDModels);

        CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(getContext());
        ReminderDateTime rdt = crdtdbh.getTableSingleDateWihtDao(sessionIDModels);

        //if (crdtdbh.getDataCount(sessionIDModels.get(i).getSesssionId()) > 0) {
        // date = dateUtl.FormetDate(rdt.getStartDate() + " " + rdt.getStartTime());

        /*
         * If time is less then current time that time not consider that data
         * */
        dateS = rdt.getStartDate();
        Times = rdt.getStartTime();
        Log.d("TAG", "dataonNotifaction:  date :" + dateS);
        Log.d("TAG", "dataonNotifaction:  Times :" + Times);
       /* Date currentTime = Calendar.getInstance().getTime();
        String currentDateTimeString = java.text.DateFormat.getDateTimeInstance().format(new Date());
        Log.d("TAG", "dataonNotifaction: currrent date nad time : " + currentTime);
        Log.d("TAG", "dataonNotifaction: currentDateTimeString : " + currentDateTimeString);*/
        String[] parts = dateS.split("-");
        part1 = parts[0];
        part2 = parts[1];
        part3 = parts[2];

        String[] time = Times.split(":");
        time1 = time[0];
        time2 = time[1];
        time3 = time[2];
        //}
        assert part1 != null;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, Integer.parseInt(part2) - 1);
        cal.set(Calendar.YEAR, Integer.parseInt(part1));
        cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(part3));
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time1));
        cal.set(Calendar.MINUTE, Integer.parseInt(time2));
        cal.set(Calendar.SECOND, 0);
        Intent intent = new Intent(getContext(), AlarmBroadCustReciver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                getContext(), 9876, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(ALARM_SERVICE);
        // alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);

        long timesss = System.currentTimeMillis() - cal.getTimeInMillis();
        Log.d("TAG", "dataonNotifaction: timesss :: " + timesss);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
        }

        // Set multiple alarm in set data
      /*  AlarmManager mgrAlarm = (AlarmManager) getContext().getSystemService(ALARM_SERVICE);
        ArrayList<PendingIntent> intentArray = new ArrayList<PendingIntent>();

        for( int i = 0; i < 10; ++i)
        {
            Intent intent = new Intent(getContext(), AlarmBroadCustReciver.class);
            // Loop counter `i` is used as a `requestCode`
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), i, intent, 0);
            // Single alarms in 1, 2, ..., 10 minutes (in `i` minutes)
            mgrAlarm.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime() + 60000 * i,
                    pendingIntent);

            intentArray.add(pendingIntent);
        }*/

           /* Calendar c = Calendar.getInstance();
            if (cal.compareTo(c) <= 0) {
                // Today Set time passed, count to tomorrow
                cal.add(Calendar.DATE, 1);
            }*/
        //  new AlarmBroadCustReciver().setAlarm(getContext(), cal);
        // setAlarm(getContext(), cal);
        //}
    }

    public void setAlarm(Context context, Calendar calendar) {
        Calendar c = Calendar.getInstance();
        long currentTime = c.getTimeInMillis();
        long diffTime = calendar.getTimeInMillis() - currentTime;
        Intent intent = new Intent(getContext(), AlarmBroadCustReciver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                getContext(), 9876, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }

    public void FolderViseData(String SessId, String saveFor, String buttondata) {
        // dataonNotifaction(SessId);


        if (action.equals("edit")) {
            SavedEditDataOnServer(SessId, saveFor);
            getActivity().onBackPressed();
            return;
        }

        /***  Save main reminder ****/
        CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getActivity().getApplicationContext());
        CReminder cReminder = crdbh.getTableSingleDateWihtDao(SessId);

        if (Common.hasInternetConnection(getActivity().getApplicationContext())) {
            crdbh.updataStatus(SessId, "done");
            Common.DisplayLog("", "loadSavedData " + SessId);

            if (cReminder != null && cReminder.getSubject() != null && !cReminder.getSubject().equalsIgnoreCase("") && !txtSubject.getText().toString().trim().equals("")) {
                cReminder = crdbh.getTableSingleDateWihtDao(SessId);

                Common.DisplayLog("SessId", SessId + "");
                pd = new ProgressDialog(getActivity());
                pd.setMessage("Loading...");
                pd.setIndeterminate(true);
                pd.setCancelable(false);
                pd.show();

                MyFirebaseMessagingService.RegisterDeviceForFCM(getContext(), SessId);
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                String id = cReminder.getiUserId();
                /*
                 * Add param as folder name on server
                 * */

                Call<Reminder> call = apiService.addReminder(cReminder.getSesssionId(),
                        cReminder.getiUserId(), cReminder.getSubject(),
                        cReminder.getPriority(), saveFor, "done",
                        cReminder.getLockstatus(), cReminder.getFolderName(), cReminder.getExtra());
                call.enqueue(new Callback<Reminder>() {
                    @Override
                    public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                        int statusCode = response.code();
                        Common.DisplayLog("response.body()", response.body().toString());
                        boolean status = response.body().getStatus();
                        Common.DisplayLog("status", status + "");
                        Common.DisplayLog("getMessage", response.body().getMessage() + "");

                        sreminder = true;
                        if (status) {

//                        Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                        } else {
                            Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());

                        }
//                    pd.dismiss();

                    }

                    @Override
                    public void onFailure(Call<Reminder> call, Throwable t) {
                        sreminder = true;
                        Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
//                    pd.dismiss();
                    }
                });


                /**** Contact ***/
                CRContactDbHandler crcdbh = new CRContactDbHandler(getActivity().getApplicationContext());
                if (cReminder.getReminderFor().equals("send")) {

                    ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllCurrentSessContactData(SessId, "contact");

                    Common.DisplayLog("remindercontactuser", remindercontactuser.size() + "");
                    if (remindercontactuser.size() <= 0) {
                        scontect = true;
                    }
                    String tempToText = "";
                    int i = 0;
                    for (i = 0; i < remindercontactuser.size(); i++) {

                        Call<Reminder> callContact = apiService.addContact(SessId, remindercontactuser.get(i).getiUserId(), remindercontactuser.get(i).getName(), remindercontactuser.get(i).getNumber(), remindercontactuser.get(i).getPhoto(), remindercontactuser.get(i).getForaction(), remindercontactuser.get(i).getConsel());
                        callContact.enqueue(new Callback<Reminder>() {
                            @Override
                            public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                                int statusCode = response.code();
                                Common.DisplayLog("response.body()", response.body().toString());
                                boolean status = response.body().getStatus();
                                Common.DisplayLog("status", status + "");
                                Common.DisplayLog("getMessage", response.body().getMessage() + "");
                                scontect = true;
                                if (status) {

//                                Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                                } else {
                                    Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());

                                }
//                            pd.dismiss();

                            }

                            @Override
                            public void onFailure(Call<Reminder> call, Throwable t) {
                                scontect = true;
                                Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
//                            pd.dismiss();
                            }


                        });


                    }


                } else {
                    scontect = true;
                }

                /*** Date & Time ****/
                CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(getActivity().getApplicationContext());

                ReminderDateTime rdt = crdtdbh.getTableSingleDateWihtDao(SessId);

                if (rdt != null) {
                    Call<Reminder> callDateTime = apiService.addDatetime(rdt.getSesssionId(), rdt.getiUserId(), rdt.getDtFor(), rdt.getStartDate(), rdt.getStartTime(), rdt.getEndDate(), rdt.getEndTime(), rdt.getDays(), rdt.getStatus());
                    callDateTime.enqueue(new Callback<Reminder>() {
                        @Override
                        public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                            int statusCode = response.code();
                            Common.DisplayLog("response.body()", response.body().toString());
                            boolean status = response.body().getStatus();
                            Common.DisplayLog("status", status + "");
                            Common.DisplayLog("getMessage", response.body().getMessage() + "");
                            sdate = true;
                            if (status) {

//                        Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                            } else {
                                Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());

                            }
//                    pd.dismiss();

                        }

                        @Override
                        public void onFailure(Call<Reminder> call, Throwable t) {
                            sdate = true;
                            Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
//                    pd.dismiss();
                        }


                    });
                } else {
                    sdate = true;
                }

                /*** Location ****/
                CRLocationDbHandler crldbh = new CRLocationDbHandler(getActivity().getApplicationContext());
                Common.DisplayLog("crldbh.getDataCount(SessionId)", crldbh.getDataCount(SessionId) + "");
                ReminderLocation rl = crldbh.getTableSingleDateWihtDao(SessId);

                if (rl != null) {
                    Call<Reminder> callLocation = apiService.addLocation(rl.getSesssionId(), rl.getiUserId(), rl.getLocationFor(), rl.getLocationDetail(), rl.getLat(), rl.getLon(), rl.getAreaFor(), rl.getAreaSize(), rl.getAreaUnit(), rl.getStatus());
                    callLocation.enqueue(new Callback<Reminder>() {
                        @Override
                        public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                            int statusCode = response.code();
                            Common.DisplayLog("response.body()", response.body().toString());
                            boolean status = response.body().getStatus();
                            Common.DisplayLog("status", status + "");
                            Common.DisplayLog("getMessage", response.body().getMessage() + "");
                            sloction = true;
                            if (status) {

//                        Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                            } else {
                                Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());

                            }
//                    pd.dismiss();

                        }

                        @Override
                        public void onFailure(Call<Reminder> call, Throwable t) {
                            sloction = true;
                            Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
//                    pd.dismiss();
                        }


                    });
                } else {
                    sloction = true;
                }

                /*** Comment ****/
                CRCommentDbHandler crcodbh = new CRCommentDbHandler(getActivity().getApplicationContext());
                Common.DisplayLog("crcodbh.getDataCount(SessionId)", crcodbh.getDataCount(SessionId) + "");
                ArrayList<ReminderComment> commentArrayList = crcodbh.getAllCurrentSessDataWithType(SessId);


                Common.DisplayLog("commentArrayList", commentArrayList.size() + "");
                if (commentArrayList.size() <= 0) {
                    scomment = true;
                }
                String tempToText = "";
                int co = 0;
                for (co = 0; co < commentArrayList.size(); co++) {

                    ReminderComment rc = commentArrayList.get(co);
                    commnent = rc.getComment();

                    Call<Reminder> callComment = apiService.addComment(rc.getSesssionId(), rc.getUserId(), rc.getComment(), "1", rc.gettCommentFor());
                    callComment.enqueue(new Callback<Reminder>() {
                        @Override
                        public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                            int statusCode = response.code();
                            Common.DisplayLog("response.body()", response.body().toString());
                            boolean status = response.body().getStatus();
                            Common.DisplayLog("status", status + "");
                            Common.DisplayLog("getMessage", response.body().getMessage() + "");
                            scomment = true;
                            if (status) {

//                        Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                            } else {
                                Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());

                            }
//                    pd.dismiss();

                        }

                        @Override
                        public void onFailure(Call<Reminder> call, Throwable t) {
                            scomment = true;
                            Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
//                    pd.dismiss();
                        }
                    });
                }


                /*** Attachment ****/
                CRAttachmentDbHandler cradbh = new CRAttachmentDbHandler(getActivity().getApplicationContext());
                Common.DisplayLog("cradbh.getDataCount(SessionId)", cradbh.getDataCount(SessionId, "done") + "");


                ArrayList<AttachmentTable> lodsubt = cradbh.getAllCurrentSessDataWithoutType(SessId, "done");
                if (lodsubt.size() <= 0) {
                    sattachment = true;
                }
                for (int j = 0; j < lodsubt.size(); j++) {
                    if (lodsubt.get(j).getAttachFor().equals("image")) {

                        File file = new File(lodsubt.get(j).getDetail());

                        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
                        MultipartBody.Part Detail = MultipartBody.Part.createFormData("tDetail", file.getName(), reqFile);

                        RequestBody SesssionId = RequestBody.create(MediaType.parse("text/plain"), lodsubt.get(j).getSesssionId());
                        RequestBody iUserId = RequestBody.create(MediaType.parse("text/plain"), lodsubt.get(j).getSesssionId());
                        RequestBody AttachFor = RequestBody.create(MediaType.parse("text/plain"), lodsubt.get(j).getAttachFor());
                        RequestBody atStatus = RequestBody.create(MediaType.parse("text/plain"), lodsubt.get(j).getStatus());
                        Call<Reminder> callAttachment = apiService.addImageAttachment(SesssionId, iUserId, AttachFor, Detail, atStatus);
                        callAttachment.enqueue(new Callback<Reminder>() {
                            @Override
                            public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                                int statusCode = response.code();
                                Common.DisplayLog("response.body()", response.body().toString());
                                boolean status = response.body().getStatus();
                                Common.DisplayLog("status", status + "");
                                Common.DisplayLog("getMessage", response.body().getMessage() + "");
                                sattachment = true;
                                if (status) {

//                        Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                                } else {
                                    Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());

                                }
//                    pd.dismiss();

                            }

                            @Override
                            public void onFailure(Call<Reminder> call, Throwable t) {
                                sattachment = true;
                                Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
//                    pd.dismiss();
                            }


                        });
                    } else {
                        Call<Reminder> callAttachment = apiService.addAttachment(lodsubt.get(j).getSesssionId(), lodsubt.get(j).getiUserId(), lodsubt.get(j).getAttachFor(), lodsubt.get(j).getDetail(), lodsubt.get(j).getStatus());
                        callAttachment.enqueue(new Callback<Reminder>() {
                            @Override
                            public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                                int statusCode = response.code();
                                Common.DisplayLog("response.body()", response.body().toString());
                                boolean status = response.body().getStatus();
                                Common.DisplayLog("status", status + "");
                                Common.DisplayLog("getMessage", response.body().getMessage() + "");
                                sattachment = true;
                                if (status) {

//                        Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                                } else {
                                    Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());

                                }
//                    pd.dismiss();

                            }

                            @Override
                            public void onFailure(Call<Reminder> call, Throwable t) {
                                sattachment = true;
                                Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
//                    pd.dismiss();
                            }


                        });
                    }
                }

                clodeloding();
                createGroup(SessId, saveFor);
            } else {
                Common.ShowToast(getActivity().getApplicationContext(), "Please Enter some keyword in subject");
            }
//            crdbh.resetTempstatusdata();
        }


        /*
         * API create for check how many reminder is create  / If 10 or less then 10 then useer can create either not create
         * */

    }

    public void getFolderList(String saveFor) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<GetFolderNameList> call = apiService.getFolderNameList(UId);
        call.enqueue(new Callback<GetFolderNameList>() {
            @Override
            public void onResponse(Call<GetFolderNameList> call, Response<GetFolderNameList> response) {
                GetFolderNameList getFolderNameList = response.body();
                if (getFolderNameList != null) {
                    folderName = getFolderNameList.getData();
                    if (folderName.size() > 0) {
                        selectYourChoiseFloder(saveFor);
                    } else {
                        FolderViseData(SessionId, saveFor, "");
                    }
                } else {
                    selectYourChoiseFloder(saveFor);
                }
            }

            @Override
            public void onFailure(Call<GetFolderNameList> call, Throwable t) {
                Common.DisplayLog("fail", t.getMessage().toString());
            }


        });
    }

    public void selectYourChoiseFloder(String reminderType) {

        Dialog dlg = new Dialog(getActivity());
        dlg.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dlg.setContentView(R.layout.new_floder_rv);
        dlg.show();

        btn_yes = dlg.findViewById(R.id.btn_yes);
        btn_no = dlg.findViewById(R.id.btn_no);
        rvNewFolder = dlg.findViewById(R.id.rvNewFolder);
        rvNewFolder.setLayoutManager(new LinearLayoutManager(getContext()));

        /*
         * List size is not >0 that time set data in adapter and show a Dialog
         * */
        newFolderAdapter = new NewFolderAdapter(getActivity(), folderName, new NewFolderAdapter.OnClick() {
            @Override
            public void ClickData(int postion, String text) {
                buttondata = text;

                Log.d("TAG", "ClickData: " + text);
            }
        });
        rvNewFolder.setAdapter(newFolderAdapter);

        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                crdbh.updataReminderFor(SessionId, reminderType, "");
                //SavedDataOnServer(SessionId, reminderType, "");
                FolderViseData(SessionId, reminderType, "");
                dlg.dismiss();
            }
        });

        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!buttondata.equals("")) {
                    Toast.makeText(getContext(), buttondata, Toast.LENGTH_SHORT).show();
                    crdbh.updataReminderFor(SessionId, reminderType, buttondata);
                    //  crdbh.updataReminderForFoldername(SessionId, buttondata);
                    // SavedDataOnServer(SessionId, reminderType, buttondata);
                    FolderViseData(SessionId, reminderType, buttondata);
                    /*crdbh.updataReminderFor(SessionId, reminderType,buttondata);
                    SavedDataOnServer(SessionId, reminderType,buttondata);*/
                    dlg.dismiss();
                    selectData = true;
                } else {
                    selectData = false;
                    Toast.makeText(getContext(), "Please select data.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void createGroup(String SessId, String saveFor) {

        /*
         * One condition : when send reminder set that time not send login use details on firebase.
         * */

        CRContactDbHandler crcdbh = new CRContactDbHandler(getActivity().getApplicationContext());
        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllCurrentSessContactData(SessId, "contact");

        final Group group = new Group();
        group.setId(SessId);
        group.setName("Group_" + SessId);
        group.setStatus("Reminder application group");
        // group.setImage(groupImageUrl);
        ArrayList<String> userIds = new ArrayList<>();

        if (!saveFor.matches("send")) {
            userIds.add("+91" + s1);
        }
        for (int i = 0; i < remindercontactuser.size(); i++) {
            String contactnuber = "+91" + remindercontactuser.get(i).getNumber();
            userIds.add(contactnuber);
        }
        group.setUserIds(userIds);

        /*
         * Chat data also add
         * */
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference groupRef = firebaseDatabase.getReference(Helper.REF_GROUP).child(SessId);
        groupRef.setValue(group).addOnSuccessListener(aVoid -> {
            if (getActivity() != null) {
                Toast.makeText(getContext(), "Group created.", Toast.LENGTH_SHORT).show();
                // chatItemClickListener.onChatItemClick(new Chat(group), -1, null);
                sendMessage(group, SessId, commnent, AttachmentTypes.NONE_TEXT, null);
            }
        }).addOnFailureListener(e -> {
            if (getActivity() != null) {
                Toast.makeText(getContext(), "Group not created.", Toast.LENGTH_SHORT).show();
                //done.setClickable(true);
                //done.setFocusable(true);
            }
        });
    }


    private void sendMessage(Group group, String sessionId, String messageBody, @AttachmentTypes.AttachmentType int attachmentType, Attachment attachment) {
        //Create message object
        Message message = new Message();
        GroupChat grpMessage = new GroupChat();

        grpMessage.setAttachmentType(attachmentType);
        if (attachmentType != AttachmentTypes.NONE_TEXT)
            grpMessage.setAttachment(attachment);
        else
            BaseMessageViewHolder.animate = true;
        grpMessage.setChatId(sessionId);
        grpMessage.setId(sessionId);
        grpMessage.setBody(messageBody);
        grpMessage.setDateTimeStamp(String.valueOf(System.currentTimeMillis()));
        grpMessage.setSenderId("+91" + s1);
        grpMessage.setSenderName(userName);
        /*message.setSenderName(userMe.getName());
        message.setSenderStatus(userMe.getStatus());
        message.setSenderImage(userMe.getImage());*/
        grpMessage.setSent(true);
        grpMessage.setChatType("Group");
        grpMessage.setDelivered(false);


        grpMessage.setRecipientId(group.getUserIds());
        message.setGroupChatArrayList(grpMessage);
        //message.setRecipientId(user != null ? user.getId() : chat.getUserId());
        //message.setRecipientGroupIds(group != null ? new ArrayList<MyString>(group.getUserIds()) : null);
       /* message.setRecipientName(user != null ? user.getName() : chat.getChatName());
        message.setRecipientImage(user != null ? user.getImage() : chat.getChatImage());
        message.setRecipientStatus(user != null ? user.getStatus() : chat.getChatStatus());*/
        message.setId(chatRef.child(sessionId).push().getKey());

        //Add message in chat child
        chatRef.child(sessionId).child(message.getId()).setValue(message);
        //Add message for inbox updates
        //  if (chat.isGroup()) {
        if (group != null && group.getUserIds() != null) {
            for (String memberId : group.getUserIds()) {
                inboxRef.child(memberId).child(group.getId()).setValue(message);
            }
        }
       /* } else {
            inboxRef.child(message.getSenderId()).child(message.getRecipientId()).setValue(message.getBody());
            inboxRef.child(message.getRecipientId()).child(message.getSenderId()).setValue(message.getBody());
        }*/

        // notifyMessage(message);
    }

    public void SavedEditDataOnServer(String SessId, String saveFor) {


        /***  Save main reminder ****/
        CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getActivity().getApplicationContext());
        CReminder cReminder = crdbh.getTableSingleDateWihtDao(SessId);

        if (Common.hasInternetConnection(getActivity().getApplicationContext())) {
            crdbh.updataStatus(SessId, "done");
            Common.DisplayLog("", "loadSavedData " + SessId);
            if (cReminder != null && cReminder.getSubject() != null && !cReminder.getSubject().equalsIgnoreCase("") && !txtSubject.getText().toString().trim().equals("")) {
                cReminder = crdbh.getTableSingleDateWihtDao(SessId);

                Common.DisplayLog("SessId", SessId + "");
                pd = new ProgressDialog(getActivity());
                pd.setMessage("Loading...");
                pd.setIndeterminate(true);
                pd.setCancelable(false);
                pd.show();
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                Call<Reminder> call = apiService.editReminder(cReminder.getSesssionId(), cReminder.getiUserId(), cReminder.getSubject(), cReminder.getPriority(), saveFor, "done", cReminder.getLockstatus(), cReminder.getExtra());
                call.enqueue(new Callback<Reminder>() {
                    @Override
                    public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                        int statusCode = response.code();
                        Common.DisplayLog("response.body()", response.body().toString());
                        boolean status = response.body().getStatus();
                        Common.DisplayLog("status", status + "");
                        Common.DisplayLog("getMessage", response.body().getMessage() + "");

                        if (status) {
//                        Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                        } else {
                            Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());

                        }
                        pd.dismiss();

                    }

                    @Override
                    public void onFailure(Call<Reminder> call, Throwable t) {
                        Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
                        pd.dismiss();
                    }


                });

                clodeloding();
            } else {
                Common.ShowToast(getActivity().getApplicationContext(), "Please Enter some keyword in subject");
            }
//            crdbh.resetTempstatusdata();
        }

    }

    public void clodeloding() {


        if (sreminder && scontect && sdate && sloction && sattachment && scomment) {
            pd.dismiss();
            crdbh.resetTempstatusdata();
            Intent in = new Intent(getActivity(), HomeActivity.class);
            startActivity(in);
        } else {
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            clodeloding();
                        }
                    },
                    2000);
        }
    }

    public void loadSavedData(String SessId) {


        Common.DisplayLog("", "loadSavedData " + SessId);
        CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getActivity().getApplicationContext());
//        RContactDbHandler rcdbh = new RContactDbHandler(getActivity().getApplicationContext());
        CRContactDbHandler crcdbh = new CRContactDbHandler(getActivity().getApplicationContext());


        CReminder cReminder = crdbh.getTableSingleDateWihtDao(SessId);

        if (cReminder != null) {
            if (cReminder.getReminderFor() != null) {
                // set ReminderFor
                if (cReminder.getReminderFor().equals("send") || cReminder.getReminderFor().equals("share")) {
                    btn_send.performClick();
                } else if (cReminder.getReminderFor().equals("device")) {
                    btn_device.performClick();
                } else {
                    crdbh.updataReminderFor(SessionId, "send", buttondata);
                }
            }
            txtSubject.setText(cReminder.getSubject());
            dtxtSubject.setText(cReminder.getSubject());


            //  Common.DisplayLog("cReminder.getSubject()", cReminder.getSubject() + "");
            // set Subject


            // set Priority
            if (cReminder.getPriority().equals("1")) {
                imgPriority1.performClick();
                dimgPriority1.performClick();
            } else if (cReminder.getPriority().equals("2")) {
                imgPriority2.performClick();
                dimgPriority2.performClick();
            } else if (cReminder.getPriority().equals("3")) {
                imgPriority3.performClick();
                dimgPriority3.performClick();
            } else {
                crdbh.updataPriority(SessionId, "0");
            }

            if (cReminder.getLockstatus().equals("lock")) {
                crdbh.updataLockStatus(SessionId, "lock");
                imglock.setImageResource(R.drawable.lockbluerounded);
            } else {
                crdbh.updataLockStatus(SessionId, "unlock");
                imglock.setImageResource(R.drawable.lockrounded);
            }

        }
        contactViewLayout.removeAllViews();
        // set Contact
        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllCurrentSessContactData(SessId, "contact");
//        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllContactData();

        Common.DisplayLog("remindercontactuser", remindercontactuser.size() + "");
        String tempToText = "";
        int i = 0;
        for (i = 0; i < remindercontactuser.size(); i++) {

            LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = vi.inflate(R.layout.fgcontact_view, null);

            if (i < 2) {
                tempToText += remindercontactuser.get(i).getName() + ",";
            }

            TextView textView = (TextView) v.findViewById(R.id.conName);
            textView.setText(remindercontactuser.get(i).getName());

            ImageView imageView = (ImageView) v.findViewById(R.id.con_image);

            String imgpath = ((remindercontactuser.get(i).getPhoto().equals("")) ? "defaultimg" : remindercontactuser.get(i).getPhoto()) + "";
            Log.d("TAG", "loadSavedData: img :" +imgpath );
            String userPic = "https://www.hwpl.in/projects/reminderapp" + imgpath.substring(2);
            Picasso.with(getActivity())
                    .load(userPic)
                    .placeholder(R.drawable.defaultusericon)
                    .error(R.drawable.defaultusericon)
                    .into(imageView);
            contactViewLayout.addView(v);
        }

        tempToText = (tempToText != null && !tempToText.equals("")) ? tempToText.substring(0, tempToText.length() - 1) : "";
        if (i > 2) {
            tempToText += " & " + (i - 2) + " others";
        }

        txtTo.setText(tempToText);

        // set Attachment
        CRAttachmentDbHandler cradbh = new CRAttachmentDbHandler(getActivity().getApplicationContext());
        Common.DisplayLog("cradbh.getDataCount(SessionId)", cradbh.getDataCount(SessionId, "done") + "");
        if (cradbh.getDataCount(SessionId, "done") > 0) {
            attachImg.setImageResource(R.drawable.attachbluerounded);
        } else {
            attachImg.setImageResource(R.drawable.attachrounded);
        }

        // set Comment

        CRCommentDbHandler crcodbh = new CRCommentDbHandler(getActivity().getApplicationContext());
        Common.DisplayLog("crcodbh.getDataCount(SessionId)", crcodbh.getDataCount(SessionId) + "");
        if (crcodbh.getDataCount(SessionId) > 0) {
            imgComt.setImageResource(R.drawable.commentbluerounded);
        } else {
            imgComt.setImageResource(R.drawable.commentrounded);
        }

        // set Date Time
        CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(getActivity().getApplicationContext());
        Common.DisplayLog("crdtdbh.getDataCount(SessionId)", crdtdbh.getDataCount(SessionId) + "");
        if (crdtdbh.getDataCount(SessionId) > 0) {
            imgDateTime.setImageResource(R.drawable.datetimebluerounded);
        } else {
            imgDateTime.setImageResource(R.drawable.datetimerounded);
        }
        // set Location
        CRLocationDbHandler crldbh = new CRLocationDbHandler(getActivity().getApplicationContext());
        Common.DisplayLog("crldbh.getDataCount(SessionId)", crldbh.getDataCount(SessionId) + "");
        if (crldbh.getDataCount(SessionId) > 0) {
            imgLocation.setImageResource(R.drawable.locationbluerounded);
        } else {
            imgLocation.setImageResource(R.drawable.locationrounded);
        }


        if (inFrom.equals("cont")) {
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            txtSubject.setFocusable(true);
                            txtSubject.requestFocus();
                            txtSubject.requestFocusFromTouch();
                            // txtSubject.setCursorVisible(false);
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                            imm.showSoftInput(txtSubject, InputMethodManager.SHOW_IMPLICIT);
                        }
                    },
                    200);
        } else {
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            Common.DisplayLog("tag", "This'll run 300 milliseconds later");
                            txtSubject.setCursorVisible(false);
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(txtSubject.getWindowToken(), 0);

                            doneContainer.setVisibility(View.GONE);
                            footerContainer.setVisibility(View.VISIBLE);
                        }
                    },
                    200);
        }

    }
}
