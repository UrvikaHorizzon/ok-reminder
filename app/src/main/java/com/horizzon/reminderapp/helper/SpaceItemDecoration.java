package com.horizzon.reminderapp.helper;

import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

/**
 * Created by horizzon on 6/19/2017.
 */

public class SpaceItemDecoration extends RecyclerView.ItemDecoration {

    private final int top, right, bottom, left;

    public SpaceItemDecoration(int top, int right, int bottom, int left) {

        this.top = top;
        this.right = right;
        this.bottom = bottom;
        this.left = left;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        outRect.top = top;
        outRect.right = right;
        outRect.bottom = bottom;
        outRect.left = left;
    }
}
