package com.horizzon.reminderapp.adapter;

import android.app.Activity;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.horizzon.reminderapp.InboxDetailActivity;
import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.dao.ReminderContactUser;
import com.horizzon.reminderapp.data.Common;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by horizzon on 4/3/2017.
 */

public class InboxDetailContactBoxListAdapter extends RecyclerView.Adapter<InboxDetailContactBoxListAdapter.MyViewHolder> {

    Activity context;
    ArrayList<ReminderContactUser> list;
    String currdate = "", inFor;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout maincont;
        public ImageView con_image;
        public TextView conName;
        public ExpandableRelativeLayout conttoolsexpandable;

        public MyViewHolder(View view) {
            super(view);
            maincont = (LinearLayout) view.findViewById(R.id.maincont);

            con_image = (ImageView) view.findViewById(R.id.con_image);

            conName = (TextView) view.findViewById(R.id.conName);
            conttoolsexpandable = (ExpandableRelativeLayout) view.findViewById(R.id.conttoolsexpandable);
        }
    }

    public InboxDetailContactBoxListAdapter(Activity context, ArrayList<ReminderContactUser> list, String inFor) {
        this.context = context;
        this.list = list;
        this.inFor = inFor;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.inboxdetailcommentcontact_view, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ReminderContactUser cr = list.get(position);

        final String SessionId = cr.getSesssionId();

        holder.conName.setText(cr.getName());
        if (inFor.equals("InboxDetailActivity")) {
            holder.conName.setTextColor(Color.parseColor("#504c4c"));

        }

        holder.conName.setTextColor(Color.parseColor("#504c4c"));
        holder.maincont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final InboxDetailActivity inboxDetailActivity = (InboxDetailActivity) context;
                int[] location = new int[2];
                holder.maincont.getLocationOnScreen(location);
                int wid = holder.maincont.getWidth()*2+20;
                int hei = holder.maincont.getHeight()-20;


                inboxDetailActivity.conttoolsexpandable.setX(location[0] - wid);
                inboxDetailActivity.conttoolsexpandable.setY(location[1] + hei);
                Toast.makeText(context, "location[0] :: " + location[0] + "   location[1] :: " + location[1], Toast.LENGTH_LONG).show();
//                    Toast.makeText(context, "clicked :: " + position, Toast.LENGTH_LONG).show();
                inboxDetailActivity.conttoolsexpandable.toggle();
//                holder.conttoolsexpandable.toggle();
            }
        });


        String imgpath = ((cr.getPhoto().equals("")) ? "defaultimg" : cr.getPhoto()) + "";
        Common.DisplayLog("imgpath", imgpath + "");
        Picasso.with(context)
                .load(imgpath)
                .placeholder(R.drawable.defaultusericon)
                .error(R.drawable.defaultusericon)
                .into(holder.con_image);


    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
