package com.horizzon.reminderapp.firebasedata;

import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;


/**
 * FirebaseInstanceIdService Gets FCM instance ID token from Firebase Cloud Messaging Server
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    SharedPreferences sharedPreferences;
    String refreshedToken;

    //*********** Called whenever the Token is Generated ********//

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.d("TAG", "Fetching FCM registration token failed", task.getException());
                            return;
                        }
                        // Get new FCM registration token
                        refreshedToken = task.getResult();

                    }
                });
        //String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.i("notification", "refreshedToken: " + refreshedToken);


        // Save FCM Token to sharedPreferences
        SharedPreferences  sharedPreferences = getSharedPreferences("UserInfo", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("FCM_Token", refreshedToken);
        editor.apply();

        // Register Device


    }


    //*********** Register Device with FCM_Token and some other Device Info ********//



}
