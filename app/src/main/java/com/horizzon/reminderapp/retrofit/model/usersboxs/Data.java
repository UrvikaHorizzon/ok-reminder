
package com.horizzon.reminderapp.retrofit.model.usersboxs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("getUsersBoxsList")
    @Expose
    private List<GetUsersBoxsList> getUsersBoxsList = null;

    public List<GetUsersBoxsList> getGetUsersBoxsList() {
        return getUsersBoxsList;
    }

    public void setGetUsersBoxsList(List<GetUsersBoxsList> getUsersBoxsList) {
        this.getUsersBoxsList = getUsersBoxsList;
    }

}
