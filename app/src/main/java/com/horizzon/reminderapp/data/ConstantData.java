package com.horizzon.reminderapp.data;

public class ConstantData {

	public static boolean log_enable = true;

	public static boolean allow_user = true;

	public static String Appfolder = "/HealthoFit";
	public static String sdcardtmpimage = Appfolder + "/temp";
	public static String sdcardhiddenFolder = Appfolder + "/.nomedia";
	public static int maxscreenwidth = 1204;
	public static int maxscreenheight = 1204;
	public static String DATABASE_NAME = "healthofit";

	/** message retrieved from NIJSOL server to run app */
	public static String CREDENTIAL_MESSAGE = "Internet connection error. Please try after sometime.";

	public static String schoolid = "1";
	public static String userid = "0";
	public static String studentuserid = "0";
	public static int imagepreview = 0;




}
