package com.horizzon.reminderapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.retrofit.model.ChatNotifactionList;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatNotifactionListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<ChatNotifactionList.Datum> chatNotifactionList = new ArrayList<>();
    OnItemClick onItemClick;
    int VIEW_TYPE_SINGLE_CHAT = 0;
    int VIEW_TYPE_GROUP_CHAT = 1;

    public ChatNotifactionListAdapter(Context ctx, List<ChatNotifactionList.Datum> chatNotifaction, OnItemClick onClick) {
        this.context = ctx;
        this.chatNotifactionList = chatNotifaction;
        this.onItemClick = onClick;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_comment_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ChatNotifactionList.Datum chatData = chatNotifactionList.get(position);
        if (chatData.getUserdetail() != null) {
            List<ChatNotifactionList.Userdetail> userdetail = chatData.getUserdetail();
            for (int i = 0; i < userdetail.size(); i++) {
                String userImag = userdetail.get(i).getUserPicture();
                if (!userImag.isEmpty()) {
                    String userPic = "https://www.hwpl.in/projects/reminderapp" + userImag.substring(2);
                    if (holder instanceof MyViewHolder) {
                        if (userdetail.get(i).getChat().size() > 0) {
                            ((MyViewHolder) holder).setSingleChatData(userdetail.get(i).getChat(), chatData.getSubject(), chatData.getSesssionId(), userdetail.get(i).getUsername(), userPic);
                        }
                    }
                }
            }
        }
    }


   /* @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.comment.setText(chatData.getSubject());
        for (int i = 0; i < userdetail.size(); i++) {
            holder.comment_by.setText(userdetail.get(i).getUsername());

            String userImag = userdetail.get(i).getUserPicture();
            String userPic = "https://www.hwpl.in/projects/reminderapp" + userImag.substring(2);
            Glide.with(context).load(userPic).into(holder.imgProfile);
            if (userdetail.get(i).getChat() != null) {
                List<ChatNotifactionList.Chat> chatList = userdetail.get(i).getChat();
                if (chatList.size() > 0) {
                    for (int j = 0; j < chatList.size(); j++) {
                        holder.name.setText(chatList.get(j).getMessage());
                    }
                }
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick.onClick(position, chatData.getSesssionId());
            }
        });
    }*/

    @Override
    public int getItemCount() {
        return chatNotifactionList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView comment_by;
        TextView comment;
        TextView imgName;
        CircleImageView imgProfile;

        public MyViewHolder(@NonNull View convertView) {
            super(convertView);
            name = (TextView) convertView.findViewById(R.id.reminderName);
            comment_by = (TextView) convertView.findViewById(R.id.reminderComtBy);
            comment = (TextView) convertView.findViewById(R.id.reminderComt);
            imgName = (TextView) convertView.findViewById(R.id.txtName);
            imgProfile = (CircleImageView) convertView.findViewById(R.id.imgProfile);
        }

        private void setSingleChatData(List<ChatNotifactionList.Chat> chatList, String subject, String sessionId, String userName, String Imge) {
            comment.setText(subject);
            comment_by.setText(userName);
            Glide.with(context).load(Imge).into(imgProfile);
            if (chatList.size() > 0) {
                for (int j = 0; j < chatList.size(); j++) {
                    name.setText(chatList.get(j).getMessage());
                }
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClick.onClick(sessionId);
                }
            });
        }
    }

  /*  public class MyViewHolderSingleChat extends RecyclerView.ViewHolder {
        TextView name;
        TextView comment_by;
        TextView comment;
        TextView imgName;
        CircleImageView imgProfile;

        public MyViewHolderSingleChat(@NonNull View convertView) {
            super(convertView);
            name = (TextView) convertView.findViewById(R.id.reminderName);
            comment_by = (TextView) convertView.findViewById(R.id.reminderComtBy);
            comment = (TextView) convertView.findViewById(R.id.reminderComt);
            imgName = (TextView) convertView.findViewById(R.id.txtName);
            imgProfile = (CircleImageView) convertView.findViewById(R.id.imgProfile);
        }

        private void setGroupChatData(List<ChatNotifactionList.Groupchat> groupchatList, String subject, String sessionId, String userName, String Imge) {
            comment.setText(subject);
            comment_by.setText(userName);
            Glide.with(context).load(Imge).into(imgProfile);
            if (groupchatList.size() > 0) {
                for (int j = 0; j < groupchatList.size(); j++) {
                    name.setText(groupchatList.get(j).getMessage());
                }
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClick.onClick(sessionId);
                }
            });
        }
    }*/

    public interface OnItemClick {
        void onClick(String SessionID);
    }
}
