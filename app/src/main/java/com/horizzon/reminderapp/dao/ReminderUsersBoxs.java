package com.horizzon.reminderapp.dao;

/**
 * Created by horizzon on 4/3/2017.
 */

public class ReminderUsersBoxs {

    private String iId;
    private String iUserId;
    private String vBoxName;
    private String vBoxType;
    private String dCreatedDateTime;

    public ReminderUsersBoxs() {
    }

    public ReminderUsersBoxs(String iId, String iUserId, String vBoxName, String vBoxType, String dCreatedDateTime) {
        this.iId = iId;
        this.iUserId = iUserId;
        this.vBoxName = vBoxName;
        this.vBoxType = vBoxType;
        this.dCreatedDateTime = dCreatedDateTime;
    }

    public String getiId() {
        return iId;
    }

    public void setiId(String iId) {
        this.iId = iId;
    }

    public String getiUserId() {
        return iUserId;
    }

    public void setiUserId(String iUserId) {
        this.iUserId = iUserId;
    }

    public String getvBoxName() {
        return vBoxName;
    }

    public void setvBoxName(String vBoxName) {
        this.vBoxName = vBoxName;
    }

    public String getvBoxType() {
        return vBoxType;
    }

    public void setvBoxType(String vBoxType) {
        this.vBoxType = vBoxType;
    }

    public String getdCreatedDateTime() {
        return dCreatedDateTime;
    }

    public void setdCreatedDateTime(String dCreatedDateTime) {
        this.dCreatedDateTime = dCreatedDateTime;
    }
}
