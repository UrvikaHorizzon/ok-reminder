
package com.horizzon.reminderapp.retrofit.model.getreminder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetReminder {

    @SerializedName("iId")
    @Expose
    private String iId;
    @SerializedName("vSesssionId")
    @Expose
    private String vSesssionId;
    @SerializedName("iUserId")
    @Expose
    private String iUserId;
    @SerializedName("tSubject")
    @Expose
    private String tSubject;
    @SerializedName("iPriority")
    @Expose
    private String iPriority;
    @SerializedName("vReminderFor")
    @Expose
    private String vReminderFor;

    @SerializedName("vUserFolder")
    @Expose
    private String vUserFolder;
    @SerializedName("iStatus")
    @Expose
    private String iStatus;
    @SerializedName("vLockStatus")
    @Expose
    private String vLockStatus;
    @SerializedName("tExtra")
    @Expose
    private String tExtra;
    @SerializedName("dCreatedDateTime")
    @Expose
    private String dCreatedDateTime;

    public String getIId() {
        return iId;
    }

    public void setIId(String iId) {
        this.iId = iId;
    }

    public String getVSesssionId() {
        return vSesssionId;
    }

    public void setVSesssionId(String vSesssionId) {
        this.vSesssionId = vSesssionId;
    }

    public String getIUserId() {
        return iUserId;
    }

    public void setIUserId(String iUserId) {
        this.iUserId = iUserId;
    }

    public String getTSubject() {
        return tSubject;
    }

    public void setTSubject(String tSubject) {
        this.tSubject = tSubject;
    }

    public String getIPriority() {
        return iPriority;
    }

    public void setIPriority(String iPriority) {
        this.iPriority = iPriority;
    }

    public String getVReminderFor() {
        return vReminderFor;
    }

    public void setVReminderFor(String vReminderFor) {
        this.vReminderFor = vReminderFor;
    }

    public String getIStatus() {
        return iStatus;
    }

    public void setIStatus(String iStatus) {
        this.iStatus = iStatus;
    }

    public String getVLockStatus() {
        return vLockStatus;
    }

    public void setVLockStatus(String vLockStatus) {
        this.vLockStatus = vLockStatus;
    }

    public String getTExtra() {
        return tExtra;
    }

    public void setTExtra(String tExtra) {
        this.tExtra = tExtra;
    }

    public String getDCreatedDateTime() {
        return dCreatedDateTime;
    }

    public void setDCreatedDateTime(String dCreatedDateTime) {
        this.dCreatedDateTime = dCreatedDateTime;
    }

    public String getvUserFolder() {
        return vUserFolder;
    }

    public void setvUserFolder(String vUserFolder) {
        this.vUserFolder = vUserFolder;
    }
}
