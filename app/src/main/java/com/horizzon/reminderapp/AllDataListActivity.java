package com.horizzon.reminderapp;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horizzon.reminderapp.adapter.AllReminderListAdapter;
import com.horizzon.reminderapp.dao.CReminder;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.CreateReminderDbHandler;

import java.util.ArrayList;

public class AllDataListActivity extends AppCompatActivity {

    String comeForm = "";
    TextView pageTitle;
    ImageView backBtn, homeBtn;
    RecyclerView listView;
    ArrayList<CReminder> list;
    AllReminderListAdapter cAdapter;
    LinearLayout noData;
    Button btncreateReminder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_data_list);


        pageTitle = (TextView) findViewById(R.id.pageTitle);
        if (getIntent().getStringExtra("comeForm") != null) {
            comeForm = getIntent().getStringExtra("comeForm");
            pageTitle.setText(comeForm);
        }

        noData = (LinearLayout) findViewById(R.id.noData);
        noData.setVisibility(View.GONE);
        btncreateReminder = (Button) findViewById(R.id.btncreateReminder);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        homeBtn = (ImageView) findViewById(R.id.homeBtn);
        listView = (RecyclerView) findViewById(R.id.list);
        list = new ArrayList<CReminder>();

        SetData(comeForm);
        if(list.size()<=0){
            noData.setVisibility(View.VISIBLE);
        }else {
            cAdapter = new AllReminderListAdapter(AllDataListActivity.this, list, comeForm);
            LinearLayoutManager lLayout = new LinearLayoutManager(getApplicationContext());
            listView.setLayoutManager(lLayout);
            listView.setAdapter(cAdapter);
        }
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), AllActivity.class);
                startActivity(in);
            }
        });

        homeBtn.setVisibility(View.GONE);
        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(in);
            }
        });
        btncreateReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(AllDataListActivity.this, CreateReminderActivity.class);
                in.putExtra("newrem", "yes");
                startActivity(in);
            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = null;
        in = new Intent(getApplicationContext(), AllActivity.class);
        startActivity(in);
    }

    private void SetData(String comeForm) {


        CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getApplicationContext());


        if (comeForm.equals("pending")) {

            list = crdbh.getAllDataForPending(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));

        } else if (comeForm.equals("today")) {

            list = crdbh.getAllDataForToday(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));
            Log.d("TAG", "SetData: list data :" + list);
        } else if (comeForm.equals("tomorrow")) {

            list = crdbh.getAllDataForTomorrow(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));

        } else if (comeForm.equals("noduedate")) {

            list = crdbh.getAllDataForN0DueDate(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));

        } else if (comeForm.equals("location")) {

            list = crdbh.getAllDataForLocation(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));

        } else if (comeForm.equals("alltask")) {

            list = crdbh.getAllCurrentSessDataWithType(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));

        } else if (comeForm.equals("rejected")) {

            list = crdbh.getAllDataForRejected(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));

        } else if (comeForm.equals("completed")) {

            list = crdbh.getAllDataForCompleted(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));

        }
    }
}
