package com.horizzon.reminderapp.helper;

/**
 * 
 * @author guolin
 */
public class Glossary {


	private String name;

	private String sortKey;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSortKey() {
		return sortKey;
	}

	public void setSortKey(String sortKey) {
		this.sortKey = sortKey;
	}

}
