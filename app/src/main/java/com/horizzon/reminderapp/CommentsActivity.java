package com.horizzon.reminderapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.horizzon.reminderapp.adapter.ChatNotifactionListAdapter;
import com.horizzon.reminderapp.adapter.CommentCustomAdapter;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.model.FolderName;
import com.horizzon.reminderapp.retrofit.model.ChatNotifactionList;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;
import com.horizzon.reminderapp.utility.CommentItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentsActivity extends AppCompatActivity {
    ImageView imgBack, imgHome;
    RecyclerView listView;
    ArrayList<CommentItem> list;
    CommentCustomAdapter cAdapter;
    ChatNotifactionListAdapter chatNotifactionListAdapter;
    SharedPreferences sharedPreferences;
    String UId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        sharedPreferences = getSharedPreferences("UserInfo", MODE_PRIVATE);
        imgBack = (ImageView) findViewById(R.id.backBtn);
        imgHome = (ImageView) findViewById(R.id.homeBtn);
        listView = (RecyclerView) findViewById(R.id.list);
        list = new ArrayList<CommentItem>();
        UId = sharedPreferences.getString("mobile", "");
        /*//SetData();
        cAdapter = new CommentCustomAdapter(this, list);
        listView.setAdapter(cAdapter);*/
        getChatNotifactionList();
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imgHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(CommentsActivity.this, HomeActivity.class);
                startActivity(in);
            }
        });
    }


    public void getChatNotifactionList() {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ChatNotifactionList> call = apiService.getChatNotifactionList("91" + UId);
        call.enqueue(new Callback<ChatNotifactionList>() {
            @Override
            public void onResponse(Call<ChatNotifactionList> call, Response<ChatNotifactionList> response) {
                ChatNotifactionList chatNotifactionList = response.body();
                List<ChatNotifactionList.Datum> dataList = new ArrayList<>();
                dataList.addAll(chatNotifactionList.getData());
                Collections.reverse(dataList);
                chatNotifactionListAdapter = new ChatNotifactionListAdapter(getApplicationContext(), dataList, new ChatNotifactionListAdapter.OnItemClick() {
                    @Override
                    public void onClick(String SessionID) {
                        /*
                         * Open Reminder details activity  and pass data as per flow
                         * */
                        Log.d("TAG", "onClick: SessionID : " + SessionID);
                        Intent in = new Intent(getApplicationContext(), DetailViewActivity.class);
                        in.putExtra("SessionId", SessionID);
                        startActivity(in);
                    }
                });
                listView.setHasFixedSize(true);
                listView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                listView.setAdapter(chatNotifactionListAdapter);

            }

            @Override
            public void onFailure(Call<ChatNotifactionList> call, Throwable t) {
                Common.DisplayLog("fail", t.getMessage().toString());
            }
        });
    }

    private void SetData() {
        String[] name = new String[]{"Sneha", "Rannvir", "Nirav"};

        for (int i = 0; i < name.length; i++) {
            CommentItem ud = new CommentItem();
            ud.setReminder_comment_by(name[i]);
            list.add(ud);
        }
    }

}
