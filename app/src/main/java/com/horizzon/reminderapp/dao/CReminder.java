package com.horizzon.reminderapp.dao;

/**
 * Created by Abhijit on 3/15/2017.
 */

public class CReminder {
    private String id;
    private String sesssionId;
    private String iUserId;
    private String subject;
    private String priority;
    private String reminderFor;
    private String status;
    private String lockstatus;
    private String extra;
    private String createddate;
    private String flodername;

    public CReminder() {
    }

    public CReminder(String id, String sesssionId, String iUserId, String subject, String priority, String reminderFor, String status, String lockstatus,
                     String extra, String createddate,String flodername) {
        this.id = id;
        this.sesssionId = sesssionId;
        this.iUserId = iUserId;
        this.subject = subject;
        this.priority = priority;
        this.reminderFor = reminderFor;
        this.status = status;
        this.lockstatus = lockstatus;
        this.extra = extra;
        this.createddate = createddate;
        this.flodername=flodername;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSesssionId() {
        return sesssionId;
    }

    public void setSesssionId(String sesssionId) {
        this.sesssionId = sesssionId;
    }

    public String getiUserId() {
        return iUserId;
    }

    public void setiUserId(String iUserId) {
        this.iUserId = iUserId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getReminderFor() {
        return reminderFor;
    }

    public void setReminderFor(String reminderFor) {
        this.reminderFor = reminderFor;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLockstatus() {
        return lockstatus;
    }

    public void setLockstatus(String lockstatus) {
        this.lockstatus = lockstatus;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }

    public String getFolderName() {
        return flodername;
    }

    public void setFolderName(String folderName) {
        flodername = folderName;
    }
}
