package com.horizzon.reminderapp.app;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by Horizzon-Android on 8/24/2016.
 */
public class MyCustomTextView extends androidx.appcompat.widget.AppCompatTextView{

    public MyCustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(),"fonts/arialbd.ttf"));
    }
}
