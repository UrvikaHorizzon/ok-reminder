package com.horizzon.reminderapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OtherUserList {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Datum {

        @SerializedName("iUserId")
        @Expose
        private String iUserId;
        @SerializedName("vUName")
        @Expose
        private String vUName;
        @SerializedName("vUContryNumber")
        @Expose
        private String vUContryNumber;
        @SerializedName("vUNumber")
        @Expose
        private String vUNumber;
        @SerializedName("vUEmail")
        @Expose
        private String vUEmail;
        @SerializedName("vUPhoto")
        @Expose
        private String vUPhoto;
        @SerializedName("vUGender")
        @Expose
        private String vUGender;
        @SerializedName("iUStatus")
        @Expose
        private String iUStatus;
        @SerializedName("tFcmId")
        @Expose
        private String tFcmId;
        @SerializedName("dCreatedDateTime")
        @Expose
        private String dCreatedDateTime;
        Boolean foraction = false;
        public Boolean getForaction() {
            return foraction;
        }

        public void setForaction(Boolean foraction) {
            this.foraction = foraction;
        }
        public String getIUserId() {
            return iUserId;
        }

        public void setIUserId(String iUserId) {
            this.iUserId = iUserId;
        }

        public String getVUName() {
            return vUName;
        }

        public void setVUName(String vUName) {
            this.vUName = vUName;
        }

        public String getVUContryNumber() {
            return vUContryNumber;
        }

        public void setVUContryNumber(String vUContryNumber) {
            this.vUContryNumber = vUContryNumber;
        }

        public String getVUNumber() {
            return vUNumber;
        }

        public void setVUNumber(String vUNumber) {
            this.vUNumber = vUNumber;
        }

        public String getVUEmail() {
            return vUEmail;
        }

        public void setVUEmail(String vUEmail) {
            this.vUEmail = vUEmail;
        }

        public String getVUPhoto() {
            return vUPhoto;
        }

        public void setVUPhoto(String vUPhoto) {
            this.vUPhoto = vUPhoto;
        }

        public String getVUGender() {
            return vUGender;
        }

        public void setVUGender(String vUGender) {
            this.vUGender = vUGender;
        }

        public String getIUStatus() {
            return iUStatus;
        }

        public void setIUStatus(String iUStatus) {
            this.iUStatus = iUStatus;
        }

        public String getTFcmId() {
            return tFcmId;
        }

        public void setTFcmId(String tFcmId) {
            this.tFcmId = tFcmId;
        }

        public String getDCreatedDateTime() {
            return dCreatedDateTime;
        }

        public void setDCreatedDateTime(String dCreatedDateTime) {
            this.dCreatedDateTime = dCreatedDateTime;
        }

    }
}
