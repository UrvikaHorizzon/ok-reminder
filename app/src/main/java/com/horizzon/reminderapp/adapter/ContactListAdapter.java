package com.horizzon.reminderapp.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AlphabetIndexer;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horizzon.reminderapp.ContactActivity;
import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.dao.ReminderContactUser;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.CRContactDbHandler;
import com.horizzon.reminderapp.model.OtherUserList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class ContactListAdapter extends BaseAdapter {

    public List<OtherUserList.Datum> _data;
    private ArrayList<OtherUserList.Datum> arraylist;
    Context _c;
    ViewHolder vh;
    private AlphabetIndexer mIndexer;
    private String cuncar = "0";
    private String _forWhich = "";
    Integer selected_position = -1;
    ContactActivity _ac;
//    RoundImage roundedImage;

    public ContactListAdapter(List<OtherUserList.Datum> selectUsers, Context context, ContactActivity ac, String forWhich) {
        _ac = ac;
        _data = selectUsers;
        _c = context;
        _forWhich = forWhich;
        this.arraylist = new ArrayList<OtherUserList.Datum>();
        this.arraylist.addAll(_data);
    }

    @Override
    public int getCount() {
        return _data.size();
    }

    @Override
    public Object getItem(int i) {
        return _data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        ViewHolder v = null;
        if (view == null) {
            LayoutInflater li = (LayoutInflater) _c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = li.inflate(R.layout.contact_item, null);

            v = new ViewHolder();

            v.sortKeyLayout = (LinearLayout) view.findViewById(R.id.sort_key_layout);
            v.sortKey = (TextView) view.findViewById(R.id.sort_key);

            v.title = (TextView) view.findViewById(R.id.contact_name);
            v.check = (CheckBox) view.findViewById(R.id.conselchk);
            v.invitcon = (Button) view.findViewById(R.id.invitcon);

            view.setTag(v);

//            Common.DisplayLog("Inside", "here--------------------------- In view1");
        } else {
//            view = convertView;
            v = (ViewHolder) view.getTag();
//            Common.DisplayLog("Inside", "here--------------------------- In view2");
        }


//        v.phone = (TextView) view.findViewById(R.id.no);
//        v.imageView = (ImageView) view.findViewById(R.id.pic);

        final OtherUserList.Datum data = (OtherUserList.Datum) _data.get(position);
        CRContactDbHandler crcdbh_m = new CRContactDbHandler(_c);
        String SessionId_m = Common.getSharedPreferences(_c, "SessionId", "");
        ReminderContactUser tempdata = crcdbh_m.getTableSingleDateWihtDao(SessionId_m, data.getVUNumber(), _forWhich);
        boolean chk = false;
        if (tempdata != null) {
            chk = true;
        }

        v.title.setText(data.getVUName());
//        v.check.setChecked(data.getForaction());
        final int cunpos = position;
//        v.check.setChecked(true);
        v.check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.DisplayLog("position ", cunpos + " is Checked");
            }
        });
        v.check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                CRContactDbHandler crcdbh = new CRContactDbHandler(_c);


                String SessionId = Common.getSharedPreferences(_c, "SessionId", "");

                if (isChecked) {
                    selected_position = cunpos;


                    String[] COLUMN = {
                            data.getIUserId(),
                            SessionId,
                            Common.getSharedPreferences(_ac, "userId", "0"),
                            data.getVUName(),
                            data.getVUNumber(),
                            data.getVUPhoto(),
                            String.valueOf(data.getForaction()),
                            "false"
                    };
                    crcdbh.addData(COLUMN);
                    Common.DisplayLog("position ", cunpos + " is " + isChecked);
                } else {
                    selected_position = -1;
                    crcdbh.deleteTempNumber(SessionId, data.getVUNumber());
                    Common.DisplayLog("position ", cunpos + " is " + isChecked);
                }
                notifyDataSetChanged();
                _ac.setselconlay();
            }
        });
//        v.check.setChecked(position == selected_position);
        v.check.setChecked(chk);

//        if (position == 3 || position == 9 || position == 12 || position == 20 || position == 26 || position == 30) {
       /* if (data.getForaction()) {
            v.invitcon.setVisibility(View.VISIBLE);
        } else {
            v.invitcon.setVisibility(View.GONE);
        }*/

        //Common.DisplayLog("", cuncar + " = = " + getSortKey(data.getName()) + "");
        if (!cuncar.equals(getSortKey(data.getVUNumber()))) {
            cuncar = getSortKey(data.getVUNumber());
            v.sortKey.setText(getSortKey(data.getVUNumber()));
            v.sortKeyLayout.setVisibility(View.VISIBLE);
        } else {
            v.sortKeyLayout.setVisibility(View.GONE);
        }
        return view;
    }

   public String getCarPosition(int pos, boolean isscroll) {


        String cuncar = "0";
        String alphabet = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        HashMap<String, Integer> keyShortpos = new HashMap<>();
        List<String> alpha = new ArrayList<>();

//        Common.DisplayLog("isscroll = ", isscroll + "");
        if (_data.size() > 0) {
            if (isscroll) {
                for (int i = 0; i < _data.size(); i++) {

//                    Common.DisplayLog("getSortKey(_data.get(i).getName())", getSortKey(_data.get(i).getName()) + " => " + i);
                    if (!cuncar.equals(getSortKey(_data.get(i).getVUNumber()))) {
                        cuncar = getSortKey(_data.get(i).getVUNumber());
//                        Common.DisplayLog("selected ", cuncar + " => " + i);
                        keyShortpos.put(cuncar, i);
                        alpha.add(cuncar);
                    }
                }


                for (Map.Entry m : keyShortpos.entrySet()) {

//                    Common.DisplayLog("", "m.getKey() == " + m.getKey() + " == alphabet.charAt(pos) == " + alphabet.charAt(pos));
                    if (m.getKey().toString().equals(String.valueOf(alphabet.charAt(pos)))) {
                        Common.DisplayLog("", " in " + m.getKey() + " == " + alphabet.charAt(pos));
                        return m.getValue().toString();
                    }
                }
//                Common.DisplayLog("alphabet.charAt(pos) ", alphabet.charAt(pos) + "");
//                Common.DisplayLog("keyShortpos.get(alphabet.charAt(pos)) ", keyShortpos.get(alphabet.charAt(pos)) + "");

            } else {

//                Common.DisplayLog("pos ", " => " + pos);
//                Common.DisplayLog("selected ", " => " + getSortKey(_data.get(pos).getName()));
                return getSortKey(_data.get(pos).getVUNumber());
            }
        } else if (isscroll) {
            return "0";
        } else {
            return "#";
        }
        return "0";
    }

    // Filter Class
    public void filter(String charText, boolean status) {
        charText = charText.toLowerCase(Locale.getDefault());
        _data.clear();
        if (charText.length() == 0) {
            _data.addAll(arraylist);
        } else {
            boolean statuss = false;
            for (OtherUserList.Datum wp : arraylist) {
                if (status) {
                    statuss = (wp.getForaction().equals("true")) ? true : false;
                } else {
                    statuss = true;
                }
                if (statuss && wp.getVUNumber().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    _data.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    // Filter Class
    public void removeselnumber(String number) {
        CRContactDbHandler crcdbh = new CRContactDbHandler(_c);
        String SessionId = Common.getSharedPreferences(_c, "SessionId", "");
        crcdbh.deleteTempNumber(SessionId, number);
        notifyDataSetChanged();
    }

    // Filter Contact & user
    public void filterContactUser(boolean forwhat) {

        _data.clear();
        if (!forwhat) {
            _data.addAll(arraylist);
        } else {
            for (OtherUserList.Datum wp : arraylist) {
                if (wp.getForaction().equals(forwhat)) {
                    _data.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    public void setIndexer(AlphabetIndexer indexer) {
        mIndexer = indexer;
    }

    private String getSortKey(String sortKeyString) {
        String key = sortKeyString.toUpperCase().substring(0, 1);
        if (key.matches("[A-Z]")) {
            return key;
        }
        return "#";
    }

    static class ViewHolder {
        LinearLayout sortKeyLayout;
        TextView sortKey;
        ImageView imageView;
        TextView title, phone;
        CheckBox check;
        Button invitcon;
    }
}
