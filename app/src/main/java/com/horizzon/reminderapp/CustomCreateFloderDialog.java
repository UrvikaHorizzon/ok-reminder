package com.horizzon.reminderapp;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

public class CustomCreateFloderDialog extends Dialog implements
        android.view.View.OnClickListener {
    AppCompatButton btn_no, btn_yes;
    AppCompatEditText txtAddFolderName;

    public CustomCreateFloderDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_add_floder);
        btn_yes = findViewById(R.id.btn_yes);
        btn_no = findViewById(R.id.btn_no);
        txtAddFolderName = findViewById(R.id.txtAddFolderName);
        btn_yes.setOnClickListener(this);
        btn_no.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_no:
                dismiss();
                break;
            case R.id.btn_yes:
                addData();
                break;

            default:
        }
    }

    public void addData() {
        String folderName = txtAddFolderName.getText().toString();
        if (!folderName.isEmpty() || !folderName.equals("")) {

        } else {
            Toast.makeText(getContext(), "Please enter folder name.", Toast.LENGTH_SHORT).show();
        }
    }
}
