package com.horizzon.reminderapp.dao;

/**
 * Created by horizzon on 4/3/2017.
 */

public class ReminderDateTime {

    private String Id;
    private String sesssionId;
    private String iUserId;
    private String dtFor;
    private String startDate;
    private String startTime;
    private String endDate;
    private String endTime;
    private String days;
    private String status;

    public ReminderDateTime() {
    }

    public ReminderDateTime(String id, String sesssionId, String iUserId, String dtFor, String startDate, String startTime, String endDate, String endTime, String days, String status) {
        Id = id;
        this.sesssionId = sesssionId;
        this.iUserId = iUserId;
        this.dtFor = dtFor;
        this.startDate = startDate;
        this.startTime = startTime;
        this.endDate = endDate;
        this.endTime = endTime;
        this.days = days;
        this.status = status;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getSesssionId() {
        return sesssionId;
    }

    public void setSesssionId(String sesssionId) {
        this.sesssionId = sesssionId;
    }

    public String getiUserId() {
        return iUserId;
    }

    public void setiUserId(String iUserId) {
        this.iUserId = iUserId;
    }

    public String getDtFor() {
        return dtFor;
    }

    public void setDtFor(String dtFor) {
        this.dtFor = dtFor;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
