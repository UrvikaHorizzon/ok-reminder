package com.horizzon.reminderapp.handler;

/**
 * Created by Abhijit Sojitra on 3/4/13.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.horizzon.reminderapp.data.ConstantData;

import java.util.HashMap;

public class UserDbHandler {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;


	// table name
	public static final String TABLE_USER = "user";

    public static final String[] STDCOLUMN = {
            "stdId",
            "stdStudentName",
            "stdGender",
            "stdEmail",
            "stdPass",
            "stdFit"
    };

	DatabaseHandler dbh;
    public static final String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER
			+ "(" + STDCOLUMN[0] + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + STDCOLUMN[1] + " TEXT,"
            + STDCOLUMN[2] + " TEXT,"
            + STDCOLUMN[3] + " TEXT,"
            + STDCOLUMN[4] + " TEXT,"
            + STDCOLUMN[5] + " TEXT"
			 + ")";
	Context con;
	public UserDbHandler(Context context) {
		this.con=context;
		dbh = new DatabaseHandler(context);
	}

	/**
	 * Storing user details in database
	 * */
    public void addStudent(String[] c) {
		SQLiteDatabase db = dbh.getWritableDatabase();

		ContentValues values = new ContentValues();
//        for (int j = 0; j < STDCOLUMN.length; j++) {
        for (int j = 1; j < c.length; j++) {
			if (ConstantData.log_enable) {
				Log.e("columnNames", STDCOLUMN[j]);
			}
            values.put(STDCOLUMN[j], c[j]);
        }

		long id = db.insert(TABLE_USER, null, values);
		if (ConstantData.log_enable) {
			Log.e("id", id+"");
		}
		db.close();
	}

	public void updateStudent(String stdId, String name) {
		SQLiteDatabase db = dbh.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(STDCOLUMN[3], name);

		db.update(TABLE_USER, values, STDCOLUMN[0], new String[] { stdId });

	}

	public void updateFit(String stdId, String fit) {
		SQLiteDatabase db = dbh.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(STDCOLUMN[5], fit);

		db.update(TABLE_USER, values, STDCOLUMN[0], new String[] { stdId });

	}

	public boolean checkuserLogin(String uEmail, String uPass) {
		getStudentById(1);
//		String countQuery = "SELECT * FROM " + TABLE_USER+" WHERE '"+ STDCOLUMN[3] +"'='"+ uEmail +"' AND '"+ STDCOLUMN[4] +"'='"+ uPass +"' ";
		String countQuery = "SELECT * FROM " + TABLE_USER+" WHERE "+ STDCOLUMN[3] +"=? AND "+ STDCOLUMN[4] +"=? ";
		if (ConstantData.log_enable) {
			Log.e("countQuery", countQuery);
		}
		SQLiteDatabase db = dbh.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, new String[]{uEmail,uPass});
//		cursor.moveToFirst();
		int rowCount = cursor.getCount();
		if (ConstantData.log_enable) {
			Log.e("rowCount", "login rowCount: " + rowCount);
		}

		if(rowCount>0) {
			cursor.moveToFirst();
//			SharedPreferences GS_preferences = con.getSharedPreferences(con.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
//			SharedPreferences.Editor editor = GS_preferences.edit();
//			editor.putString("userid", cursor.getString(0));
//			editor.commit();
//			Common.DisplayLog("cursor.getString(0)",cursor.getString(0));
//			SharedPreferences sharedPref = con.getSharedPreferences(con.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
//			Common.DisplayLog("sharedPref",sharedPref.getString("userid", "0"));
		}
		db.close();
		cursor.close();

		if(rowCount>0){return false;}else {return true;}
	}
	public HashMap<String, String> getStudentById(int stdId) {
		HashMap<String, String> Student = new HashMap<String, String>();
		String selectQuery = "SELECT  * FROM " + TABLE_USER
				+ " WHERE "+STDCOLUMN[0]+"=" + stdId;

		SQLiteDatabase db = dbh.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		// Move to first row
		// Log.e("query", cursor.toString());
		cursor.moveToFirst();
		String[] columnNames = cursor.getColumnNames();

		if (ConstantData.log_enable) {
			Log.e("query ", "SELECT  * FROM " + TABLE_USER
					+ " WHERE "+STDCOLUMN[0]+"=" + stdId);
			Log.e("cursor.getCount()", cursor.getCount()+"");
		}
		if (cursor.getCount() > 0) {
            for (int j = 0; j < columnNames.length; j++) {
				if (ConstantData.log_enable) {
					Log.e("columnNames", columnNames[j]);
					Log.e("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
				}
                Student.put(STDCOLUMN[j], cursor.getString(j));
            }
		}
		cursor.close();
		db.close();
		// return user
		return Student;
	}

	public int getRowCount() {
		String countQuery = "SELECT  * FROM " + TABLE_USER;
		SQLiteDatabase db = dbh.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int rowCount = cursor.getCount();
        cursor.moveToFirst();
		String[] columnNames = cursor.getColumnNames();
		db.close();
		cursor.close();

		// return row count
		return rowCount;
	}

	public void resetStudent() {
		SQLiteDatabase db = dbh.getWritableDatabase();
		db.delete(TABLE_USER, null, null);
		db.execSQL("delete from "+ TABLE_USER);
		db.execSQL("delete from sqlite_sequence where name='" + TABLE_USER + "'");
		// db.execSQL("TRUNCATE table " + TABLE_USER);
		db.close();
	}

    public boolean checkStudentLogin() {
        String countQuery = "SELECT  * FROM " + TABLE_USER+" limit 0,1";
        SQLiteDatabase db = dbh.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int rowCount = cursor.getCount();
		if (ConstantData.log_enable) {
			Log.e("rowCount", "login rowCount: " + rowCount);
		}
        db.close();
        cursor.close();

        if(rowCount>0){return false;}else {return true;}
    }

}
