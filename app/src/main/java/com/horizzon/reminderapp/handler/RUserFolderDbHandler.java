package com.horizzon.reminderapp.handler;

/**
 * Created by Abhijit Sojitra on 9/3/17.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.horizzon.reminderapp.dao.ReminderUsersBoxs;
import com.horizzon.reminderapp.data.Common;

import java.util.ArrayList;

public class RUserFolderDbHandler {


    // table name
    public static final String TABLE_NAME = "r_users_folders";

    public static final String[] COLUMNS = {
            "iId",
            "iUserId",
            "vBoxName",
            "vBoxType",
            "dCreatedDateTime"
    };

    DatabaseHandler dbh;
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
            + "(" + COLUMNS[0] + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMNS[1] + " TEXT,"
            + COLUMNS[2] + " TEXT,"
            + COLUMNS[3] + " TEXT,"
            + COLUMNS[4] + " TEXT"
            + ");";

    public RUserFolderDbHandler(Context context) {
        dbh = new DatabaseHandler(context);
    }


    public long addData(String[] c) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        for (int j = 1; j < COLUMNS.length; j++) {
            Common.DisplayLog("columnNames", COLUMNS[j] + " => " + c[j]);
            values.put(COLUMNS[j], c[j]);
        }

        long id = db.insert(TABLE_NAME, null, values);
        Common.DisplayLog("last created id", id + "");
        db.close();
        return id;
    }

    public void updataData(String iId, String vBoxName) {

        Common.DisplayLog("update vBoxName", vBoxName + "");
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[2], vBoxName);

        db.update(TABLE_NAME, values, COLUMNS[0] + "=? ", new String[]{iId});
        Common.DisplayLog("last update userid", iId + "");

    }

    public void updataWithType(String userid, String address, String addressFor) {

        Common.DisplayLog("update address", address + "");
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[3], address);

        db.update(TABLE_NAME, values, COLUMNS[1] + "=? AND " + COLUMNS[2] + "=? ", new String[]{userid, addressFor});
        Common.DisplayLog("last update userid", userid + "");

    }

    public ArrayList<ReminderUsersBoxs> getAllData(String iUserId) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<ReminderUsersBoxs> list = new ArrayList<ReminderUsersBoxs>();
        ReminderUsersBoxs ReminderUsersBoxs;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "='" + iUserId + "' ORDER BY " + COLUMNS[0] + " DESC";
        ;
        Common.DisplayLog("query_select", query_select + "");
        Cursor cursor = db.rawQuery(query_select, null);
        Common.DisplayLog("", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                ReminderUsersBoxs = new ReminderUsersBoxs(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
                for (int j = 0; j < columnNames.length; j++) {

                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));

                }
                list.add(ReminderUsersBoxs);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public ReminderUsersBoxs getTableSingleDateWihtDao(String iId) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ReminderUsersBoxs ReminderUsersBoxs = null;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[0] + "='" + iId + "' ORDER BY " + COLUMNS[0] + " DESC";
        Common.DisplayLog("query_select", query_select);
        Cursor cursor = db.rawQuery(query_select, null);

        Common.DisplayLog("cursor.getCount()", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            Common.DisplayLog("cursor", "yes");
            String[] columnNames = cursor.getColumnNames();
            do {
                ReminderUsersBoxs = new ReminderUsersBoxs(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
                for (int j = 0; j < columnNames.length; j++) {
                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
                }
            } while (cursor.moveToNext());
        }
        cursor.close();

        return ReminderUsersBoxs;
    }

    public void deleteSingleData(String id) {
        SQLiteDatabase db = dbh.getWritableDatabase();
        String qu = "delete from " + TABLE_NAME + " WHERE " + COLUMNS[0] + " = '" + id + "'";
        Common.DisplayLog("qu ", qu);
        db.execSQL(qu);
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }

    public void deleteDataWithType(String userId, String addressFor) {
        SQLiteDatabase db = dbh.getWritableDatabase();
        String qu = "delete from " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = '" + userId + "' AND " + COLUMNS[2] + " = '" + addressFor + "'";
        Common.DisplayLog("qu ", qu);
        db.execSQL(qu);
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }

    public void resetTable() {
        SQLiteDatabase db = dbh.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.execSQL("delete from " + TABLE_NAME);
        db.execSQL("delete from sqlite_sequence where name='" + TABLE_NAME + "'");
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }

}
