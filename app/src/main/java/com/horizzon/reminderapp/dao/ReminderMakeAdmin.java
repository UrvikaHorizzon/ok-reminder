package com.horizzon.reminderapp.dao;

/**
 * Created by horizzon on 4/3/2017.
 */

public class ReminderMakeAdmin {

    private String Id;
    private String sesssionId;
    private String userId;
    private String extra;

    public ReminderMakeAdmin() {
    }

    public ReminderMakeAdmin(String id, String sesssionId, String userId, String extra) {
        Id = id;
        this.sesssionId = sesssionId;
        this.userId = userId;
        this.extra = extra;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getSesssionId() {
        return sesssionId;
    }

    public void setSesssionId(String sesssionId) {
        this.sesssionId = sesssionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }
}
