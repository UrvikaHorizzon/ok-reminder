package com.horizzon.reminderapp.adapter;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.dao.ReminderContactUser;

import java.util.ArrayList;

public class GroupMemberAdapter extends RecyclerView.Adapter<GroupMemberAdapter.MyViewHolder> {

    Context context;
    //ArrayList<String> user;
    ArrayList<ReminderContactUser> user;
    onClick onClick;
    int row_index = -1;

    public GroupMemberAdapter(Context cnt, ArrayList<ReminderContactUser> userIds, onClick onUserclick) {
        this.context = cnt;
        this.user = userIds;
        this.onClick = onUserclick;
    }

    @NonNull
    @Override
    public GroupMemberAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grpcommentcontact_view, parent, false);
        return new GroupMemberAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull GroupMemberAdapter.MyViewHolder holder, int position) {
        ReminderContactUser s = user.get(position);
        holder.conName.setText(s.getName());
        if (!s.getPhoto().equals("")) {
            String userPic = "https://www.hwpl.in/projects/reminderapp" + s.getPhoto().substring(2);
            Glide.with(context).load(userPic).into(holder.con_image);
        }

        // holder.conName.setText(s);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onClick.onItemClick(s.getNumber());
                // onClick.onItemClick(s);
                int position = holder.getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    row_index = position;
                    notifyDataSetChanged();
                }
            }
        });


        if (row_index == holder.getAdapterPosition()) {
            holder.con_image.setPadding(5, 5, 5, 5);
            holder.con_image.setAlpha(1.0f);
        } else {
            holder.con_image.setPadding(20, 20, 20, 20);
            holder.con_image.setAlpha(0.5f);
        }
    }

    @Override
    public int getItemCount() {
        return user.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout maincont;
        public ImageView con_image;
        public TextView conName;
        public FrameLayout frameTransfer;
        public View viewtype;

        public MyViewHolder(View view) {
            super(view);
            maincont = (LinearLayout) view.findViewById(R.id.maincont);
            viewtype = (View) view.findViewById(R.id.viewtype);
            frameTransfer = (FrameLayout) view.findViewById(R.id.frameTransfer);
            con_image = (ImageView) view.findViewById(R.id.con_image);
            conName = (TextView) view.findViewById(R.id.conName);
        }
    }

    public interface onClick {
        public void onItemClick(String UserNumber);
    }
}
