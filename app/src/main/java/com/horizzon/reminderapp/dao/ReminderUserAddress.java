package com.horizzon.reminderapp.dao;

/**
 * Created by horizzon on 4/3/2017.
 */

public class ReminderUserAddress {

    private String Id;
    private String userId;
    private String addressFor;
    private String address;
    private String hiddenId;

    public ReminderUserAddress() {
    }

    public ReminderUserAddress(String id, String userId, String addressFor, String address,String hiddenId) {
        Id = id;
        this.userId = userId;
        this.addressFor = addressFor;
        this.address = address;
        this.hiddenId=hiddenId;
    }

    public String getHiddenId() {
        return hiddenId;
    }

    public void setHiddenId(String hiddenId) {
        this.hiddenId = hiddenId;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAddressFor() {
        return addressFor;
    }

    public void setAddressFor(String addressFor) {
        this.addressFor = addressFor;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
