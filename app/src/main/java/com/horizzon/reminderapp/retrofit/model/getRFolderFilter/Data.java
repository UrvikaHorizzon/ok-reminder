
package com.horizzon.reminderapp.retrofit.model.getRFolderFilter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("getRFolderFilter")
    @Expose
    private List<GetRFolderFilter> getRFolderFilter = null;

    public List<GetRFolderFilter> getGetRFolderFilter() {
        return getRFolderFilter;
    }

    public void setGetRFolderFilter(List<GetRFolderFilter> getRFolderFilter) {
        this.getRFolderFilter = getRFolderFilter;
    }

}
