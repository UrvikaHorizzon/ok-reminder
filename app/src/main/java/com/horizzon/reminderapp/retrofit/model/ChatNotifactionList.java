package com.horizzon.reminderapp.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ChatNotifactionList {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }public class Userdetail {

        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("user_picture")
        @Expose
        private String userPicture;
        @SerializedName("chat")
        @Expose
        private List<Chat> chat = null;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUserPicture() {
            return userPicture;
        }

        public void setUserPicture(String userPicture) {
            this.userPicture = userPicture;
        }

        public List<Chat> getChat() {
            return chat;
        }

        public void setChat(List<Chat> chat) {
            this.chat = chat;
        }

    }
    public class Datum {

        @SerializedName("SesssionId")
        @Expose
        private String sesssionId;
        @SerializedName("Subject")
        @Expose
        private String subject;
        @SerializedName("userdetail")
        @Expose
        private List<Userdetail> userdetail = null;

        public String getSesssionId() {
            return sesssionId;
        }

        public void setSesssionId(String sesssionId) {
            this.sesssionId = sesssionId;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public List<Userdetail> getUserdetail() {
            return userdetail;
        }

        public void setUserdetail(List<Userdetail> userdetail) {
            this.userdetail = userdetail;
        }

    }

    public class Chat {

        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("entrydate")
        @Expose
        private String entrydate;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getEntrydate() {
            return entrydate;
        }

        public void setEntrydate(String entrydate) {
            this.entrydate = entrydate;
        }

    }
}




   /* @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public class Userdetail {

        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("user_picture")
        @Expose
        private String userPicture;
        @SerializedName("chat")
        @Expose
        private List<Chat> chat = null;
        @SerializedName("groupchat")
        @Expose
        private List<Groupchat> groupchat = null;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUserPicture() {
            return userPicture;
        }

        public void setUserPicture(String userPicture) {
            this.userPicture = userPicture;
        }

        public List<Chat> getChat() {
            return chat;
        }

        public void setChat(List<Chat> chat) {
            this.chat = chat;
        }

        public List<Groupchat> getGroupchat() {
            return groupchat;
        }

        public void setGroupchat(List<Groupchat> groupchat) {
            this.groupchat = groupchat;
        }

    }

    public class Groupchat {

        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("entrydate")
        @Expose
        private String entrydate;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getEntrydate() {
            return entrydate;
        }

        public void setEntrydate(String entrydate) {
            this.entrydate = entrydate;
        }

    }

    public class Datum {

        @SerializedName("SesssionId")
        @Expose
        private String sesssionId;
        @SerializedName("Subject")
        @Expose
        private String subject;
        @SerializedName("userdetail")
        @Expose
        private List<Userdetail> userdetail = null;

        public String getSesssionId() {
            return sesssionId;
        }

        public void setSesssionId(String sesssionId) {
            this.sesssionId = sesssionId;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public List<Userdetail> getUserdetail() {
            return userdetail;
        }

        public void setUserdetail(List<Userdetail> userdetail) {
            this.userdetail = userdetail;
        }

    }

    public class Chat {

        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("entrydate")
        @Expose
        private String entrydate;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getEntrydate() {
            return entrydate;
        }

        public void setEntrydate(String entrydate) {
            this.entrydate = entrydate;
        }

    }

}*/
  /*  @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public class Userdetail {

        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("user_picture")
        @Expose
        private String userPicture;
        @SerializedName("chat")
        @Expose
        private List<Chat> chat = null;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUserPicture() {
            return userPicture;
        }

        public void setUserPicture(String userPicture) {
            this.userPicture = userPicture;
        }

        public List<Chat> getChat() {
            return chat;
        }

        public void setChat(List<Chat> chat) {
            this.chat = chat;
        }

    }

    public class Datum {

        @SerializedName("SesssionId")
        @Expose
        private String sesssionId;
        @SerializedName("Subject")
        @Expose
        private String subject;
        @SerializedName("userdetail")
        @Expose
        private List<Userdetail> userdetail = null;

        public String getSesssionId() {
            return sesssionId;
        }

        public void setSesssionId(String sesssionId) {
            this.sesssionId = sesssionId;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public List<Userdetail> getUserdetail() {
            return userdetail;
        }

        public void setUserdetail(List<Userdetail> userdetail) {
            this.userdetail = userdetail;
        }

    }public class Chat {

        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("entrydate")
        @Expose
        private String entrydate;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getEntrydate() {
            return entrydate;
        }

        public void setEntrydate(String entrydate) {
            this.entrydate = entrydate;
        }

    }
}*/
   /* @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public class Datum  implements Serializable {
        @SerializedName("SesssionId")
        @Expose
        private String sesssionId;
        @SerializedName("Subject")
        @Expose
        private String subject;
        @SerializedName("userdetail")
        @Expose
        private List<Userdetail> userdetail = null;
        public String getSesssionId() {
            return sesssionId;
        }

        public void setSesssionId(String sesssionId) {
            this.sesssionId = sesssionId;
        }
        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public List<Userdetail> getUserdetail() {
            return userdetail;
        }

        public void setUserdetail(List<Userdetail> userdetail) {
            this.userdetail = userdetail;
        }

    }

    public  class Chat implements Serializable{

        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("entrydate")
        @Expose
        private String entrydate;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getEntrydate() {
            return entrydate;
        }

        public void setEntrydate(String entrydate) {
            this.entrydate = entrydate;
        }

    }
    public class Userdetail implements Serializable {

        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("user_picture")
        @Expose
        private String userPicture;
        @SerializedName("chat")
        @Expose
        private List<Chat> chat = null;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUserPicture() {
            return userPicture;
        }

        public void setUserPicture(String userPicture) {
            this.userPicture = userPicture;
        }

        public List<Chat> getChat() {
            return chat;
        }

        public void setChat(List<Chat> chat) {
            this.chat = chat;
        }

    }

}*/
  /*  @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public class Datum implements Serializable {

        @SerializedName("cId")
        @Expose
        private String cId;
        @SerializedName("vSesssionId")
        @Expose
        private String vSesssionId;
        @SerializedName("sentby_userid")
        @Expose
        private String sentbyUserid;
        @SerializedName("sentto_userid")
        @Expose
        private String senttoUserid;
        @SerializedName("sentby_mobile")
        @Expose
        private String sentbyMobile;
        @SerializedName("sentto_mobile")
        @Expose
        private String senttoMobile;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("entrydate")
        @Expose
        private String entrydate;
        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("user_picture")
        @Expose
        private String userPicture;
        @SerializedName("subject")
        @Expose
        private String subject;

        public String getCId() {
            return cId;
        }

        public void setCId(String cId) {
            this.cId = cId;
        }

        public String getVSesssionId() {
            return vSesssionId;
        }

        public void setVSesssionId(String vSesssionId) {
            this.vSesssionId = vSesssionId;
        }

        public String getSentbyUserid() {
            return sentbyUserid;
        }

        public void setSentbyUserid(String sentbyUserid) {
            this.sentbyUserid = sentbyUserid;
        }

        public String getSenttoUserid() {
            return senttoUserid;
        }

        public void setSenttoUserid(String senttoUserid) {
            this.senttoUserid = senttoUserid;
        }

        public String getSentbyMobile() {
            return sentbyMobile;
        }

        public void setSentbyMobile(String sentbyMobile) {
            this.sentbyMobile = sentbyMobile;
        }

        public String getSenttoMobile() {
            return senttoMobile;
        }

        public void setSenttoMobile(String senttoMobile) {
            this.senttoMobile = senttoMobile;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getEntrydate() {
            return entrydate;
        }

        public void setEntrydate(String entrydate) {
            this.entrydate = entrydate;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUserPicture() {
            return userPicture;
        }

        public void setUserPicture(String userPicture) {
            this.userPicture = userPicture;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

    }
*/

//}
