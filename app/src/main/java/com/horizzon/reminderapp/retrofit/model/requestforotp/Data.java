
package com.horizzon.reminderapp.retrofit.model.requestforotp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("UserData")
    @Expose
    private UserData userData;
    @SerializedName("OtpNumber")
    @Expose
    private String otpNumber;

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public String getOtpNumber() {
        return otpNumber;
    }

    public void setOtpNumber(String otpNumber) {
        this.otpNumber = otpNumber;
    }

}
