package com.horizzon.reminderapp.helper;

import android.app.Activity;
import androidx.viewpager.widget.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.data.Common;
import com.squareup.picasso.Picasso;

/**
 * Created by horizzon on 6/8/2017.
 */

public class TouchImageAdapter extends PagerAdapter {
    int lastposition = 0;
    String[] imgList;
    Activity _ac;

    public TouchImageAdapter(Activity ac, String[] imgList, int lastposition) {
        this.lastposition = lastposition;
        this.imgList = imgList;
        this._ac = ac;

    }

    @Override
    public int getCount() {
        return imgList.length;
    }

    @Override
    public View instantiateItem(ViewGroup container, int position) {
        Common.DisplayLog("imgList.length", imgList.length + "");
        if (imgList[position].toString().equalsIgnoreCase("")) {
            position = lastposition;
        }
        TouchImageView img = new TouchImageView(container.getContext());
        img.setScaleType(ImageView.ScaleType.CENTER_CROP);
//            img.setImageResource(images[position]);
        Common.DisplayLog("imgList[position]", imgList[position] + "");
        Picasso.with(_ac)
                .load(imgList[position])
                .placeholder(R.drawable.defaultimg)
                .error(R.drawable.defaultimg)
                .into(img);
        container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        lastposition = position;
        return img;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

}

