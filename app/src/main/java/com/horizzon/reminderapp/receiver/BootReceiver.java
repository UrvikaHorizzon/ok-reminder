package com.horizzon.reminderapp.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.horizzon.reminderapp.dao.ReminderDateTime;
import com.horizzon.reminderapp.dao.SessionIDModel;
import com.horizzon.reminderapp.handler.CRDateTimeDbHandler;
import com.horizzon.reminderapp.utility.DateUtilitys;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class BootReceiver extends BroadcastReceiver {

    AlarmManager mAlarmManager;
    SharedPreferences mSharedPreferences;
    private String TAG = BootReceiver.class.getSimpleName();
    int mMonth;
    Context ctx;
    List<SessionIDModel> sessionIDModels = new ArrayList<>();
    String dateS, Times, part1, part2, part3, time1, time2, time3;
    @Override
    public void onReceive(Context context, Intent intent) {
        this.ctx = context;
      /* SharedPreferences sh = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        String SessId = sh.getString("SessId", "");
        Log.d(TAG, "onReceive:SessId :  " + SessId);
        Log.d(TAG, "onReceive: " + "---------" + intent.getAction());*/
       /* sessionIDModels.addAll(getList());
        setNotifactionoffLine(intent);*/
       /* CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(context);
        ReminderDateTime rdt = crdtdbh.getTableSingleDateWihtDao(SessId);
        dateS = rdt.getStartDate();
        Times = rdt.getStartTime();

        String[] parts = dateS.split("-");
        part1 = parts[0];
        part2 = parts[1];
        part3 = parts[2];

        String[] time = Times.split(":");
        time1 = time[0];
        time2 = time[1];
        time3 = time[2];
        assert part1 != null;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, Integer.parseInt(part2));
        cal.set(Calendar.YEAR, Integer.parseInt(part1));
        cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(part3));
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time1));
        cal.set(Calendar.MINUTE, Integer.parseInt(time2));
        cal.set(Calendar.SECOND, 0);
        Calendar c = Calendar.getInstance();
        long currentTime = c.getTimeInMillis();
        long diffTime = cal.getTimeInMillis() - currentTime;*/
      /*if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            mSharedPreferences = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
            mAlarmManager = MyAlarmManager.getAlarmManager(context);
            long time = mSharedPreferences.getLong("time", 0);
            if (time > 0) {
                mAlarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                        SystemClock.elapsedRealtime() + time, time, MyAlarmManager.getPendingIntent(ctx,
                                PendingIntent.FLAG_UPDATE_CURRENT));
            }
        }*/
    }


    public List<SessionIDModel> getList() {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        // List<SessionIDModel> arrayItems = null;
        String serializedObject = sharedPreferences.getString("SessionId", null);
        if (serializedObject != null) {
            Gson gson = new Gson();
            Type type = new TypeToken<List<SessionIDModel>>() {
            }.getType();
            sessionIDModels = gson.fromJson(serializedObject, type);
        }
        Log.d(TAG, "getList:sessionIDModels :: " + sessionIDModels);
        return sessionIDModels;
    }

    public void setNotifactionoffLine(Intent intent) {
        CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(ctx);
        //CRSessoinIdDbHandler crSessoinIdDbHandler = new CRSessoinIdDbHandler(ctx);
        //Log.d(TAG, "onReceive: reminder list : " + crSessoinIdDbHandler.getAllElements());
        // sessionIDModels.addAll(crSessoinIdDbHandler.getAllElements());
        //Log.d(TAG, "onReceive:sessionIDModels ::  " + sessionIDModels);
        DateUtilitys dateUtl = new DateUtilitys();
        String dateS, Times, part1= null, part2 = null, part3= null, time1= null, time2= null, time3= null;
        if (sessionIDModels.size() > 0) {
            for (int i = 0; i < sessionIDModels.size(); i++) {
                ReminderDateTime rdt = crdtdbh.getTableSingleDateWihtDao(sessionIDModels.get(i).getSesssionId());
                Log.d(TAG, "onReceive: one :" + sessionIDModels.get(i).getId());
                Log.d(TAG, "onReceive: two :" + sessionIDModels.get(i).getSesssionId());
                    dateS = rdt.getStartDate();
                    Times = rdt.getStartTime();

                    String[] parts = dateS.split("-");
                    part1 = parts[0];
                    part2 = parts[1];
                    part3 = parts[2];

                    String[] time = Times.split(":");
                    time1 = time[0];
                    time2 = time[1];
                    time3 = time[2];
                assert part1 != null;
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.MONTH, Integer.parseInt(part2));
                cal.set(Calendar.YEAR, Integer.parseInt(part1));
                cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(part3));
                cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time1));
                cal.set(Calendar.MINUTE, Integer.parseInt(time2));
                cal.set(Calendar.SECOND, 0);
                Calendar c = Calendar.getInstance();
                long currentTime = c.getTimeInMillis();

                long diffTime = cal.getTimeInMillis() - currentTime;
              //  if(currentTime >=diffTime) {
                    if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
                        mAlarmManager = MyAlarmManager.getAlarmManager(ctx);
                        if (diffTime > 0) {
                            mAlarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                                    SystemClock.elapsedRealtime() + diffTime, diffTime, MyAlarmManager.getPendingIntent(ctx,
                                            PendingIntent.FLAG_UPDATE_CURRENT));
                        }

                    }
               // }
            }
        }
    }

}
