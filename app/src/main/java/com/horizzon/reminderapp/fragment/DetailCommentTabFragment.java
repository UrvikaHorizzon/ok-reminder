package com.horizzon.reminderapp.fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.adapter.BaseMessageViewHolder;
import com.horizzon.reminderapp.adapter.DetailsCommentAdapter;
import com.horizzon.reminderapp.adapter.GroupMemberAdapter;
import com.horizzon.reminderapp.adapter.MessageAdapter;
import com.horizzon.reminderapp.adapter.SinglechatMessageAdpter;
import com.horizzon.reminderapp.app.MyRegButton;
import com.horizzon.reminderapp.app.MyRegEditText;
import com.horizzon.reminderapp.dao.DetailsCommnetsModel;
import com.horizzon.reminderapp.dao.ReminderComment;
import com.horizzon.reminderapp.dao.ReminderContactUser;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.firebasedata.Helper;
import com.horizzon.reminderapp.handler.CRCommentDbHandler;
import com.horizzon.reminderapp.handler.CRContactDbHandler;
import com.horizzon.reminderapp.interfacess.OnMessageItemClick;
import com.horizzon.reminderapp.model.Attachment;
import com.horizzon.reminderapp.model.AttachmentTypes;
import com.horizzon.reminderapp.model.Chat;
import com.horizzon.reminderapp.model.GroupChat;
import com.horizzon.reminderapp.model.Message;
import com.horizzon.reminderapp.model.SingleChat;
import com.horizzon.reminderapp.model.User;
import com.horizzon.reminderapp.retrofit.model.GetFolderNameList;
import com.horizzon.reminderapp.retrofit.model.PostChatNotifactionList;
import com.horizzon.reminderapp.retrofit.model.reminder.Reminder;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailCommentTabFragment extends Fragment implements View.OnClickListener, OnMessageItemClick {
    SharedPreferences sh;
    String s1, userName;
    String SessionId;
    LinearLayout llOne, doneContainer;
    MyRegButton btn_grp_chat, btn_private_chat;
    RecyclerView commentList, userList, SingleChatList;
    String SingleUserNumber;
    SinglechatMessageAdpter singlechatMessageAdpter;
    CRCommentDbHandler crcdbh;
    //  CommentAdapter commentAdapter;
    DetailsCommentAdapter commentAdapter;
    MyRegEditText textComment;
    ImageView btnsend;
    GradientDrawable gd;
    float[] arr = new float[8];
    float[] arr1 = new float[8];
    //DatabaseReference reference;
    //DatabaseReference adduserToInbox;
    TextView txtSMS;
    Firebase reference1, reference2;
    ArrayList<Message> messageList = new ArrayList<>();
    Chat chats;
    User user1;
    private static String EXTRA_DATA_CHAT = "extradatachat";
    private static String EXTRA_DATA_LIST = "extradatalist";
    private ArrayList<String> userPlayerIds = new ArrayList<>();
    protected DatabaseReference usersRef, groupRef, chatRef, inboxRef;
    private ArrayList<String> deletedMessages = new ArrayList<>();
    private ArrayList<String> adapterMediaMessagePositions = new ArrayList<>();
    // private ArrayList<Message> dataList = new ArrayList<>();
    private ArrayList<GroupChat> dataList = new ArrayList<>();
    private ArrayList<SingleChat> singleChatArrayList = new ArrayList<>();
    ArrayList<String> userIds = new ArrayList<>();
    ArrayList<String> SingleUSer = new ArrayList<>();
    private MessageAdapter messageAdapter;
    private GroupMemberAdapter groupMemberAdapter;
    private static final int FIREBASE_MESSAGE_QUERY_LIMIT = 50;
    public User userMe;
    public Helper helper;
    String chatType = "group";
    ArrayList<ReminderContactUser> remindercontactuser = new ArrayList<>();
    ArrayList<ReminderContactUser> Newcontactuser = new ArrayList<>();

    //private boolean listeningChats = false;
    public DetailCommentTabFragment() {
        // Required empty public constructor
    }

    public DetailCommentTabFragment(ArrayList<Message> messageForwardList, Chat chat) {
        this.messageList = messageForwardList;
        this.chats = chat;
    }

    private ValueEventListener singleValueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            String playerId = dataSnapshot.getValue(String.class);
            if (playerId != null) userPlayerIds.add(playerId);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };
    private ValueEventListener userValueChangeListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (getActivity() != null) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    user1 = user;
                    user.setName(chats.getChatName());
                    chats.setChatImage(user.getImage());
                    chats.setChatStatus(user.getStatus());
                    //setUserInfo();
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };


   /* public boolean isMine(Message message) {
        return message.getSenderId().equals(user1.getId());
    }*/

    private ChildEventListener messagesChildEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            if (getActivity() != null) {
                //&& listeningChats
                Message newMessage = dataSnapshot.getValue(Message.class);
                GroupChat groupListData = newMessage.getGroupChatArrayList();
                SingleChat singleChat = newMessage.getSingleChatArrayList();
                Log.d("TAG", "onChildAdded:groupListData : " + groupListData);
                Log.d("TAG", "onChildAdded:singleChat : " + singleChat);
                if (singleChat != null) {
                    singleChatArrayList.add(singleChat);
                    Singlechat(SingleUserNumber);
                }

                if (groupListData != null && groupListData.getId() != null && groupListData.getChatId() != null) {
                    /*if (groupListData.getAttachmentType() == AttachmentTypes.NONE_NOTIFICATION) {// setNotificationMessageNames(newMessage);}*/
                    if (groupListData.getBody() != null) {
                        dataList.add(groupListData);
                        groupchat();
                    }
                }
            }
        }


        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            if (getActivity() != null) {
                //&& listeningChats
                Message updateMessage = dataSnapshot.getValue(Message.class);
               /* if (updateMessage != null && updateMessage.getId() != null && updateMessage.getChatId() != null) {
                    if (updateMessage.getAttachmentType() == AttachmentTypes.NONE_NOTIFICATION) {
                        //setNotificationMessageNames(updateMessage);
                    }
                    if (updateMessage.getChatId().startsWith(Helper.GROUP_PREFIX))
                        updateMessage.setSenderName(isMine(updateMessage) ? "You" : updateMessage.getSenderId());
                    addMessage(updateMessage);
                }*/
            }
        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    /*private boolean addMessage(Message msg) {
        if (deletedMessages.contains(msg.getId()))
            return false;
        int existingPos = -1;
        for (int i = dataList.size() - 1; i >= 0; i--) {
            if (dataList.get(i).getId() != null && dataList.get(i).getChatId() != null && dataList.get(i).getId().equals(msg.getId())) {
                existingPos = i;
                break;
            } else if (dataList.size() > FIREBASE_MESSAGE_QUERY_LIMIT && (dataList.size() - (FIREBASE_MESSAGE_QUERY_LIMIT + 1)) == i) {
                break;
            }
        }
        if (existingPos == -1) {
            if (!isMine(msg) && msg.getAttachment() != null && msg.getAttachment().getUrl().equals("loading"))
                return false;
            //showTyping(false);
            dataList.add(msg);
            messageAdapter.notifyItemInserted(dataList.size() - 1);
            //  recyclerView.scrollToPosition(messageAdapter.getItemCount() - 1);
        } else {
            dataList.set(existingPos, msg);
            messageAdapter.notifyItemChanged(existingPos);
        }
        //if (Helper.CHAT_CAB) undoSelectionPrepared();
        return existingPos == -1;
    }*/


    private void registerChatUpdates() {
        chatRef.child(SessionId).limitToLast(FIREBASE_MESSAGE_QUERY_LIMIT).addChildEventListener(messagesChildEventListener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_detail_comment_tab, container, false);
        SessionId = Common.getSharedPreferences(getActivity(), "SessionId", "");
        crcdbh = new CRCommentDbHandler(getActivity());
        sh = getActivity().getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        s1 = sh.getString("mobile", "");
        userName = sh.getString("userName", "");
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();//get firebase instance
        usersRef = firebaseDatabase.getReference(Helper.REF_USERS);//instantiate user's firebase reference
        groupRef = firebaseDatabase.getReference(Helper.REF_GROUP);//instantiate group's firebase reference
        chatRef = firebaseDatabase.getReference(Helper.REF_CHAT);//instantiate chat's firebase reference
        inboxRef = firebaseDatabase.getReference(Helper.REF_INBOX);//instantiate inbox's firebase reference
        helper = new Helper(getContext());
        userMe = helper.getLoggedInUser();
        usersRef.child(SessionId).addValueEventListener(userValueChangeListener);
        usersRef.child(SessionId).child("userPlayerId").addListenerForSingleValueEvent(singleValueEventListener);

        //   reference = FirebaseDatabase.getInstance().getReference("Chat");
        //FirebaseDatabase.getInstance().getReferenceFromUrl(url);
        //   adduserToInbox = FirebaseDatabase.getInstance().getReference();
        Intent intent = getActivity().getIntent();
        if (intent.hasExtra(EXTRA_DATA_CHAT)) {
            chats = intent.getParcelableExtra(EXTRA_DATA_CHAT);
            Helper.CURRENT_CHAT_ID = chats.getUserId();
        }

        llOne = v.findViewById(R.id.llOne);
        doneContainer = v.findViewById(R.id.doneContainer);
        txtSMS = v.findViewById(R.id.txtSMS);
        txtSMS.setText("Group Chat.");
        btn_grp_chat = v.findViewById(R.id.btn_grp_chat);
        btn_private_chat = v.findViewById(R.id.btn_private_chat);
        btnsend = v.findViewById(R.id.btnsend);
        textComment = v.findViewById(R.id.textComment);
        btnsend.setOnClickListener(this);

     /*   reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.getValue().toString().isEmpty()) {
                    txtSMS.setText(dataSnapshot.getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*/

        commentList = (RecyclerView) v.findViewById(R.id.commentList);
        SingleChatList = (RecyclerView) v.findViewById(R.id.SingleChatList);
        userList = (RecyclerView) v.findViewById(R.id.userList);
        registerChatUpdates();
        // groupchat();
        arr[0] = Float.valueOf(20);
        arr[1] = Float.valueOf(20);
        arr[2] = Float.valueOf(0);
        arr[3] = Float.valueOf(0);
        arr[4] = Float.valueOf(0);
        arr[5] = Float.valueOf(0);
        arr[6] = Float.valueOf(20);
        arr[7] = Float.valueOf(20);

        arr1[0] = Float.valueOf(0);
        arr1[1] = Float.valueOf(0);
        arr1[2] = Float.valueOf(20);
        arr1[3] = Float.valueOf(20);
        arr1[4] = Float.valueOf(20);
        arr1[5] = Float.valueOf(20);
        arr1[6] = Float.valueOf(0);
        arr1[7] = Float.valueOf(0);
        groupMemberList();

        if (userIds.size() == 0) {
            llOne.setVisibility(View.GONE);
            doneContainer.setVisibility(View.GONE);
        } else if (userIds.size() == 1) {
            llOne.setVisibility(View.GONE);
        } else {
            llOne.setVisibility(View.VISIBLE);
        }

        btn_grp_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chatType = "group";
                SingleChatList.setVisibility(View.GONE);
                userList.setVisibility(View.GONE);
                commentList.setVisibility(View.VISIBLE);
                //CreateReminderActivity.btn_flag = 0;
                gd = new GradientDrawable();
                gd.setStroke(4, Color.parseColor("#1ba1e2"));
                gd.setColor(Color.parseColor("#1ba1e2"));
                gd.setCornerRadii(arr);
                btn_grp_chat.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_send));
                btn_grp_chat.setTextColor(Color.parseColor("#dee0e0"));

                gd = new GradientDrawable();
                gd.setStroke(4, Color.parseColor("#1ba1e2"));
                gd.setColor(Color.parseColor("#dee0e0"));
                gd.setCornerRadii(arr1);
                btn_private_chat.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_device_bg));
                btn_private_chat.setTextColor(Color.parseColor("#1ba1e2"));
                groupchat();

            }
        });
        btn_private_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chatType = "single";

                userList.setVisibility(View.VISIBLE);
                commentList.setVisibility(View.GONE);
                // CreateReminderActivity.btn_flag = 0;
                gd = new GradientDrawable();
                gd.setStroke(4, Color.parseColor("#1ba1e2"));
                gd.setColor(Color.parseColor("#1ba1e2"));
                gd.setCornerRadii(arr);
                btn_private_chat.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_device_blue));
                btn_private_chat.setTextColor(Color.parseColor("#dee0e0"));

                gd = new GradientDrawable();
                gd.setStroke(4, Color.parseColor("#1ba1e2"));
                gd.setColor(Color.parseColor("#dee0e0"));
                gd.setCornerRadii(arr1);
                btn_grp_chat.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_send_bg));
                btn_grp_chat.setTextColor(Color.parseColor("#1ba1e2"));
            }
        });
        registerMyTypingUpdates();

        return v;
    }

    public void groupMemberList() {

        CRContactDbHandler crcdbh = new CRContactDbHandler(getActivity().getApplicationContext());
        remindercontactuser = crcdbh.getAllCurrentSessContactData(SessionId, "contact");
        userIds.clear();
        // String userNumber= "+91"+;
        for (int i = 0; i < remindercontactuser.size(); i++) {
            String contactnuber = remindercontactuser.get(i).getNumber();
            if (!contactnuber.matches(s1)) {
                Newcontactuser.add(remindercontactuser.get(i));
                userIds.add("+91" + contactnuber);
            }
        }

        groupMemberAdapter = new GroupMemberAdapter(getContext(), Newcontactuser, new GroupMemberAdapter.onClick() {
            @Override
            public void onItemClick(String UserNumber) {
                SingleChatList.setVisibility(View.VISIBLE);
                Singlechat("+91" + UserNumber);
                SingleUserNumber = "+91" + UserNumber;
            }
        });
        userList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        userList.setAdapter(groupMemberAdapter);

    }

    public void Singlechat(String userId) {
        if (userId != null) {
            String chatId = "+91" + s1 + "_" + userId;
            String ReceiverchatId = userId + "_" + "+91" + s1;
            List<SingleChat> singleChats = new ArrayList<>();
            singleChats.clear();
            /*
             * Create one recyclerview and adapter and set list as per contact id
             * */
            //singleChatArrayList

            if (singleChatArrayList.size() > 0) {

                for (int i = 0; i < singleChatArrayList.size(); i++) {
                    Log.d("TAG", "Singlechat: getchatId: " + singleChatArrayList.get(i).getChatId());
                    if (chatId.equals(singleChatArrayList.get(i).getChatId()) || ReceiverchatId.equals(singleChatArrayList.get(i).getChatId())) {
                        singleChats.add(singleChatArrayList.get(i));
                    }
                }
            }
            singlechatMessageAdpter = new SinglechatMessageAdpter(getContext(), userMe.getId(),singleChats, textComment);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            SingleChatList.setLayoutManager(linearLayoutManager);
            SingleChatList.setAdapter(singlechatMessageAdpter);
        }
    }

    public void singleChatNotifactionListSendToSercer(String userId, String message, String chattype, ArrayList<String> userIds) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<PostChatNotifactionList> call = apiService.chatNotifactionListPost(SessionId, "",
                "", "+91" + s1, userIds, message, chattype);
        call.enqueue(new Callback<PostChatNotifactionList>() {
            @Override
            public void onResponse(Call<PostChatNotifactionList> call, Response<PostChatNotifactionList> response) {
                PostChatNotifactionList getFolderNameList = response.body();
                if (getFolderNameList != null) {
                    Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PostChatNotifactionList> call, Throwable t) {
                Common.DisplayLog("fail", t.getMessage().toString());
            }
        });
    }


    public void groupchat() {
        if (userMe != null) {
            if (userMe.getId() != null || !userMe.getId().matches("")) {
                messageAdapter = new MessageAdapter(getContext(), dataList, userMe.getId(), textComment, new OnMessageItemClick() {
                    @Override
                    public void OnMessageClick(Message message, int position) {

                    }

                    @Override
                    public void OnMessageLongClick(Message message, int position) {

                    }
                });
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
                commentList.setLayoutManager(linearLayoutManager);
                commentList.setAdapter(messageAdapter);
            }
        }
    }

    private void registerMyTypingUpdates() {
        //get chat user
        //Publish logged in user's typing status
        textComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //sendMessage.setImageDrawable(ContextCompat.getDrawable(getContext(), s.length() == 0 ? R.drawable.ic_keyboard_voice_24dp : R.drawable.ic_send));
                if (userMe != null) {
                    if (userMe.getId() != null || !userMe.getId().matches("")) {
                        usersRef.child(userMe.getId()).child("typing").setValue(true);
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        //getComment();
    }

    private void getComment() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<DetailsCommnetsModel> call = apiService.getCommentList(SessionId);
        call.enqueue(new Callback<DetailsCommnetsModel>() {
            @Override
            public void onResponse(Call<DetailsCommnetsModel> call, Response<DetailsCommnetsModel> response) {
                int statusCode = response.code();
                DetailsCommnetsModel detailsCommnetsModel = response.body();
                ArrayList<DetailsCommnetsModel.Datum> commentArrayList = new ArrayList<>();
                commentArrayList.addAll(detailsCommnetsModel.getData());
                commentList.setAdapter(null);
                commentAdapter = new DetailsCommentAdapter(getActivity(), commentArrayList, "add");
                LinearLayoutManager lLayout = new LinearLayoutManager(getActivity());
                commentList.setLayoutManager(lLayout);
                commentList.setAdapter(commentAdapter);
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                commentList.scrollToPosition(commentArrayList.size() - 1);

                            }
                        },
                        200);
            }

            @Override
            public void onFailure(Call<DetailsCommnetsModel> call, Throwable t) {
                Common.DisplayLog("fail", t.getMessage().toString());
            }
        });
        //commentArrayList = crcdbh.getAllCurrentSessDataWithType(SessionId);
       /* Log.d("TAG", "getComment: list : " + commentArrayList);
        Common.DisplayLog("commentArrayList count", commentArrayList.size() + "");*/


//                        commentAdapter = new ImageGridCustomAdapter(AttachmentActivity.this, commentArrayList, AttachmentActivity.this);
        //  commentAdapter = new CommentAdapter(getActivity(), commentArrayList, "add");

//                        lLayout.setStackFromEnd(true);
//                        lLayout.setReverseLayout(true);


//        commentAdapter.notifyDataSetChanged();


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnsend:
                commnetSendClick();
                break;
            default:
        }
    }

    private void commnetSendClick() {
        if (chatType.matches("single")) {
            SingleUSer.add(SingleUserNumber);
            singleChatNotifactionListSendToSercer(SingleUserNumber, textComment.getText().toString(), "single", SingleUSer);
        } else {
            singleChatNotifactionListSendToSercer(SingleUserNumber, textComment.getText().toString(), "group", userIds);
        }

        String comment = textComment.getText().toString();
        if (!comment.isEmpty()) {
            sendMessage(comment, AttachmentTypes.NONE_TEXT, null);
            textComment.setText("");
        }
        //reference.push().setValue(comment);
           /* ReminderComment rc = new ReminderComment("", SessionId, Common.getSharedPreferences(getContext(), "userId", "0"), textComment.getText().toString(), "group", Common.getSQLDateTime());
            SavedEditDataOnServer(rc);
            SendMessage("Send from urvika.");*/
    }


    private void sendMessage(String messageBody, @AttachmentTypes.AttachmentType int attachmentType, Attachment attachment) {
        //Create message object

        if (chatType.matches("group")) {
            CRContactDbHandler crcdbh = new CRContactDbHandler(getActivity().getApplicationContext());
            ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllCurrentSessContactData(SessionId, "contact");

            Message message = new Message();
            GroupChat grpMessage = new GroupChat();
            grpMessage.setAttachmentType(attachmentType);
            if (attachmentType != AttachmentTypes.NONE_TEXT)
                grpMessage.setAttachment(attachment);
            else
                BaseMessageViewHolder.animate = true;
            grpMessage.setChatId(SessionId);
            grpMessage.setBody(messageBody);
            grpMessage.setDateTimeStamp(String.valueOf(System.currentTimeMillis()));
            grpMessage.setSenderId("+91" + s1);
            grpMessage.setSenderName(userName);
            grpMessage.setSent(true);
            grpMessage.setChatType(chatType);
            grpMessage.setDelivered(false);
            grpMessage.setId(SessionId);

            ArrayList<String> userIds = new ArrayList<>();
            userIds.add("+91" + s1);
            for (int i = 0; i < remindercontactuser.size(); i++) {
                String contactnuber = "+91" + remindercontactuser.get(i).getNumber();
                userIds.add(contactnuber);
            }
            grpMessage.setRecipientId(userIds);
            message.setGroupChatArrayList(grpMessage);
            message.setId(chatRef.child(SessionId).push().getKey());
            //Add message in chat child
            chatRef.child(SessionId).child(message.getId()).setValue(message);
        } else {
            Message message = new Message();
            SingleChat singleChat = new SingleChat();
            singleChat.setAttachmentType(attachmentType);
            if (attachmentType != AttachmentTypes.NONE_TEXT)
                singleChat.setAttachment(attachment);
            else
                BaseMessageViewHolder.animate = true;
            singleChat.setChatId("+91" + s1 + "_" + SingleUserNumber);
            singleChat.setBody(messageBody);
            singleChat.setDateTimeStamp(String.valueOf(System.currentTimeMillis()));
            singleChat.setSenderId("+91" + s1);
            singleChat.setSenderName(userName);
            singleChat.setSent(true);
            singleChat.setChatType(chatType);
            singleChat.setDelivered(false);
            singleChat.setId(SessionId);

           /* ArrayList<String> userIds = new ArrayList<>();
            userIds.add("+91" + s1);
            for (int i = 0; i < remindercontactuser.size(); i++) {
                String contactnuber = "+91" + remindercontactuser.get(i).getNumber();
                userIds.add(contactnuber);
            }*/
            singleChat.setRecipientId(SingleUserNumber);
            message.setSingleChatArrayList(singleChat);
            message.setId(chatRef.child(SessionId).push().getKey());
            //Add message in chat child
            chatRef.child(SessionId).child(message.getId()).setValue(message);
        }
       /* if (group != null && group.getUserIds() != null) {
            for (String memberId : group.getUserIds()) {
                inboxRef.child(memberId).child(group.getId()).setValue(message);
            }
        }*/
        //Add message for inbox updates
        //inboxRef.child(message.getSenderId()).child(message.getRecipientId()).setValue(message.getBody());
        //inboxRef.child(message.getRecipientId()).child(message.getSenderId()).setValue(message.getBody());

        //notifyMessage(message);
    }

    public void SavedEditDataOnServer(ReminderComment rc) {

        if (Common.hasInternetConnection(getContext())) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<Reminder> callComment = apiService.addComment(rc.getSesssionId(),
                    rc.getUserId(), rc.getComment(), "1",
                    rc.gettCommentFor());
            callComment.enqueue(new Callback<Reminder>() {
                @Override
                public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                    int statusCode = response.code();
                    Common.DisplayLog("response.body()", response.body().toString());
                    boolean status = response.body().getStatus();
                    Common.DisplayLog("status", status + "");
                    Common.DisplayLog("getMessage", response.body().getMessage() + "");
                    if (status) {
                        textComment.setText("");
                        //  getComment();
//                        Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                    } else {
                        Common.ShowToast(getContext(), response.body().getMessage());

                    }
//                    pd.dismiss();

                }

                @Override
                public void onFailure(Call<Reminder> call, Throwable t) {
                    Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
//                    pd.dismiss();
                }


            });
        }
    }


    public void SendMessage(final String message) {
        SimpleDateFormat df =
                new SimpleDateFormat("dd-MM-yyyy HH:mm:ssZZ", Locale.ENGLISH);

        Date c = Calendar.getInstance().getTime();
        final String formattedDate = df.format(c);


// final String current_user_ref = "chat" + "/" + "9723328289" + "-" + "8155014996";
        final String chat_user_ref = "chat" + "/" + "8155014996" + "-" + "9723328289";

//DatabaseReference reference = this.reference.child("chat").child("9723328289" + "-" + "8155014996").push();
// final String pushid = reference.getKey();
        final HashMap message_user_map = new HashMap<>();
        message_user_map.put("receiver_id", "8155014996");
        message_user_map.put("sender_id", "9723328289");
        // message_user_map.put("chat_id",pushid);
        message_user_map.put("text", message);
        message_user_map.put("type", "text");
        message_user_map.put("pic_url", "");
        message_user_map.put("status", "0");
        message_user_map.put("time", "");
        message_user_map.put("sender_name", "urvika");
        message_user_map.put("timestamp", formattedDate);

        final HashMap user_map = new HashMap<>();
        //  user_map.put(current_user_ref + "/" + pushid, message_user_map);
        user_map.put(chat_user_ref + "/" + "", message_user_map);

    }

    @Override
    public void OnMessageClick(Message message, int position) {

    }

    @Override
    public void OnMessageLongClick(Message message, int position) {

    }
}
