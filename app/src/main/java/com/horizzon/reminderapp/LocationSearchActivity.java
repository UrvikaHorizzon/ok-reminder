package com.horizzon.reminderapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.horizzon.reminderapp.adapter.LocationSearchAdapter;
import com.horizzon.reminderapp.dao.LocationSearch;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.helper.location.TrackGPS;
import com.horizzon.reminderapp.retrofit.model.location.Location;
import com.horizzon.reminderapp.retrofit.model.location.Nearbysearch;
import com.horizzon.reminderapp.retrofit.model.location.Result;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationSearchActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    //    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String TYPE_AUTOCOMPLETE = "/nearbysearch";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyDpf4sjXeIPUPLM9o1SXCLlDe_MfG6SQmw";
    ImageView imgBack;
    ListView searchlocationList;
    String search_flag, search_address;
    TextView txtLayout;
    EditText autoCompleteTextView;
    private TrackGPS gps;
    double longitude, latitude, sellongitude, sellatitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_search);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        txtLayout = (TextView) findViewById(R.id.txtLayout);
        txtLayout.setVisibility(View.VISIBLE);

        Intent in = getIntent();
        search_flag = in.getStringExtra("search_flag");
        search_address = in.getStringExtra("search_address");

        autoCompleteTextView = (EditText) findViewById(R.id.autoCompleteTextView);
        autoCompleteTextView.setText(search_address);
        autoCompleteTextView.setSelection(autoCompleteTextView.getText().length());

//        autoCompView.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.list_item));
//        autoCompView.setOnItemClickListener(this);
        searchlocationList = (ListView) findViewById(R.id.searchlocationList);

        imgBack = (ImageView) findViewById(R.id.imgBack);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gobacktocr();
            }
        });


        autoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                autocompleteadd2(autoCompleteTextView, s.toString());
            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();
        gps = new TrackGPS(LocationSearchActivity.this);

        if (gps.canGetLocation()) {

            longitude = gps.getLongitude();
            latitude = gps.getLatitude();
            if (longitude <= 0 && latitude <= 0) {
                onResume();
            }

            Common.DisplayLog("Longitude-Latitude", "Longitude:" + Double.toString(longitude) + "\nLatitude:" + Double.toString(latitude));
        } else {

            gps.showSettingsAlert();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        gobacktocr();
    }

    private void gobacktocr() {

        Common.DisplayLog("autoCompleteTextView.getText()", autoCompleteTextView.getText().toString().trim() + "");
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(autoCompleteTextView.getWindowToken(), 0);
        Intent returnIntent = new Intent();
        returnIntent.putExtra("goto", 1);
        returnIntent.putExtra("address", autoCompleteTextView.getText().toString().trim());
        returnIntent.putExtra("sellongitude", sellongitude);
        returnIntent.putExtra("sellatitude", sellatitude);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();

    }


    public void autocompleteadd(final EditText autocomp, String input) {
        List<LocationSearch> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
//            sb.append("&sensor=false&types=geocode");
            sb.append("&location=" + latitude + "," + longitude);
            sb.append("&rankby=distance&keyword=" + URLEncoder.encode(input, "utf8"));
            Common.DisplayLog("url", sb + "");

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
//            Log.e(LOG_TAG, "Error processing Places API URL", e);
        } catch (IOException e) {
//            Log.e(LOG_TAG, "Error connecting to Places API", e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        resultList = new ArrayList();
        try {
            Common.DisplayLog("GoogleObject", jsonResults.toString());
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
//            resultList = new ArrayList(predsJsonArray.length());

            for (int i = 0; i < predsJsonArray.length(); i++) {
                Common.DisplayLog("", predsJsonArray.getJSONObject(i).getString("description"));
                Common.DisplayLog("", "============================================================");
//                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));

                JSONObject structured_formatting = predsJsonArray.getJSONObject(i).getJSONObject("structured_formatting");
                resultList.add(new LocationSearch(predsJsonArray.getJSONObject(i).getString("description"), structured_formatting.getString("main_text"), structured_formatting.getString("secondary_text")));
            }
        } catch (JSONException e) {
            Common.DisplayLog("ExceptionJSON", "Cannot process JSON results " + e);
        }

        /*LocationSearchAdapter adapter = new LocationSearchAdapter(LocationSearchActivity.this, resultList);

        // Assign adapter to ListView
        searchlocationList.setAdapter(adapter);

        final List<LocationSearch> finalResultList = resultList;
        searchlocationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                String str = (String) parent.getItemAtPosition(position);
                String str = finalResultList.get(position).getDescription().toString();
                autocomp.setText(str);
                autocomp.setSelection(autocomp.getText().length());
                gobacktocr();
            }
        });*/

    }

    public void autocompleteadd2(final EditText autocomp, String input) {
        List<LocationSearch> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();

        StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
        sb.append("?key=" + API_KEY);
//            sb.append("&sensor=false&types=geocode");
        sb.append("&location=" + latitude + "," + longitude);
//        sb.append("&rankby=distance&keyword=" + URLEncoder.encode(input, "utf8"));
        Common.DisplayLog("url", sb + "");


        ApiInterface apiService =
                ApiClient.getCustomClient(PLACES_API_BASE + "/").create(ApiInterface.class);

        Call<Nearbysearch> call = apiService.getNearbysearchDetails(API_KEY, latitude + "," + longitude, "distance", input);
        call.enqueue(new Callback<Nearbysearch>() {
            @Override
            public void onResponse(Call<Nearbysearch> call, Response<Nearbysearch> response) {
                int statusCode = response.code();
//                    List<Nearbysearch> movies = response.body().getResults();
                final List<Result> results = response.body().getResults();
                Common.DisplayLog("results.size()", results.size() + "");


                LocationSearchAdapter adapter = new LocationSearchAdapter(LocationSearchActivity.this, results);

                // Assign adapter to ListView
                searchlocationList.setAdapter(adapter);

                searchlocationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        Result rs = results.get(position);
                        Location loc = rs.getGeometry().getLocation();
                        sellongitude = loc.getLng();
                        sellatitude = loc.getLat();
                        String str = rs.getVicinity().toString();
                        autocomp.setText(str);
                        autocomp.setSelection(autocomp.getText().length());
                        gobacktocr();
                    }
                });

            }

            @Override
            public void onFailure(Call<Nearbysearch> call, Throwable t) {

            }

        });
        /*

        LocationSearchAdapter adapter = new LocationSearchAdapter(LocationSearchActivity.this, resultList);

        // Assign adapter to ListView
        searchlocationList.setAdapter(adapter);

        final List<LocationSearch> finalResultList = resultList;
        searchlocationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                String str = (String) parent.getItemAtPosition(position);
                String str = finalResultList.get(position).getDescription().toString();
                autocomp.setText(str);
                autocomp.setSelection(autocomp.getText().length());
                gobacktocr();
            }
        });*/

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String str = (String) parent.getItemAtPosition(position);
        Intent intentMessage = new Intent();
        // put the message in Intent
        intentMessage.putExtra("checkin", str);
        // Set The Result in Intent
        setResult(5, intentMessage);
        // finish The activity
        finish();
    }

}
