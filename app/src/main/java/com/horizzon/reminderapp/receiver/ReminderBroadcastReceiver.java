package com.horizzon.reminderapp.receiver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.os.Vibrator;

public class ReminderBroadcastReceiver  extends BroadcastReceiver {
    public Vibrator vibrator;
    long[] pattern = { 0L, 250L, 200L, 250L, 200L, 250L, 200L, 250L, 200L,
            250L, 200L, 250L, 200L, 250L, 200L };
    @Override
    public void onReceive(Context context, Intent intent) {
     vibrator = (Vibrator) context
                .getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(pattern, -1);// No repetition
        Intent intentToFire = new Intent(context, Activity.class);
        /*NotificationHelper.showNewNotification
                (context, intentToFire,"Reminder App.",
                        "Reminder task set.");*/

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();

        wl.release();
    }



}
