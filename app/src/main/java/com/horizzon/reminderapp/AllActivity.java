package com.horizzon.reminderapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.horizzon.reminderapp.adapter.DrawerItemCustomAdapter;
import com.horizzon.reminderapp.dao.CReminder;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.CreateReminderDbHandler;
import com.horizzon.reminderapp.utility.ObjectDrawerItem;

import java.util.ArrayList;

public class AllActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imgBack, imgBtnOpt;
    private DrawerLayout mDrawerLayout;
    protected ListView mDrawerList;
    ObjectDrawerItem[] drawerItem;
    FloatingActionButton fab;
    LinearLayout list_pending, list_today, list_tomorrow, list_noduedate, list_location, list_alltask, list_rejected, list_completed;
    TextView count_pending, count_today, count_tomorrow, count_noduedate, count_location, count_alltask, count_rejected, count_completed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all);

        imgBack = (ImageView) findViewById(R.id.backBtn);
        imgBtnOpt = (ImageView) findViewById(R.id.imgOpt);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);


        count_pending = (TextView) findViewById(R.id.count_pending);
        count_today = (TextView) findViewById(R.id.count_today);
        count_tomorrow = (TextView) findViewById(R.id.count_tomorrow);
        count_noduedate = (TextView) findViewById(R.id.count_noduedate);
        count_location = (TextView) findViewById(R.id.count_location);
        count_alltask = (TextView) findViewById(R.id.count_alltask);
        count_rejected = (TextView) findViewById(R.id.count_rejected);
        count_completed = (TextView) findViewById(R.id.count_completed);

        list_pending = (LinearLayout) findViewById(R.id.list_pending);
        list_today = (LinearLayout) findViewById(R.id.list_today);
        list_tomorrow = (LinearLayout) findViewById(R.id.list_tomorrow);
        list_noduedate = (LinearLayout) findViewById(R.id.list_noduedate);
        list_location = (LinearLayout) findViewById(R.id.list_location);
        list_alltask = (LinearLayout) findViewById(R.id.list_alltask);
        list_rejected = (LinearLayout) findViewById(R.id.list_rejected);
        list_completed = (LinearLayout) findViewById(R.id.list_completed);

        list_pending.setOnClickListener(this);
        list_today.setOnClickListener(this);
        list_tomorrow.setOnClickListener(this);
        list_noduedate.setOnClickListener(this);
        list_location.setOnClickListener(this);
        list_alltask.setOnClickListener(this);
        list_rejected.setOnClickListener(this);
        list_completed.setOnClickListener(this);


        // list the drawer items
        drawerItem = new ObjectDrawerItem[4];

        drawerItem[0] = new ObjectDrawerItem(R.drawable.sentlisticon, "Sent List");
        drawerItem[1] = new ObjectDrawerItem(R.drawable.printlisticon, "Print List");
        drawerItem[2] = new ObjectDrawerItem(R.drawable.clearcompletedicon, "Clear Completed");
        drawerItem[3] = new ObjectDrawerItem(R.drawable.settingicon, "Settings");
        //drawerItem[4] = new ObjectDrawerItem(R.drawable.syncicon, "Sync Now");

        // Pass the folderData to our ListView adapter
        final DrawerItemCustomAdapter adapter1 = new DrawerItemCustomAdapter(this, R.layout.list_item_row, drawerItem);

        // Set the adapter for the list view
        mDrawerList.setAdapter(adapter1);
        // set the item click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // for app icon control for nav drawer
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        final Animation zoomin = AnimationUtils.loadAnimation(this, R.anim.zzomin);
        final Animation zoomout = AnimationUtils.loadAnimation(this, R.anim.zoomout);

        fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setAnimation(zoomin);
        fab.setAnimation(zoomout);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                Intent in = new Intent(AllActivity.this, CreateReminderActivity.class);
                in.putExtra("newrem", "yes");
                startActivity(in);
            }
        });

        zoomin.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                fab.startAnimation(zoomout);
            }
        });

        zoomout.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                fab.startAnimation(zoomin);
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imgBtnOpt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadData();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = null;
        in = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(in);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        Intent in = null;
        in = new Intent(AllActivity.this, AllDataListActivity.class);
        if (id == R.id.list_pending) {
            in.putExtra("comeForm", "pending");
        } else if (id == R.id.list_today) {
            in.putExtra("comeForm", "today");
        } else if (id == R.id.list_tomorrow) {
            in.putExtra("comeForm", "tomorrow");
        } else if (id == R.id.list_noduedate) {
            in.putExtra("comeForm", "noduedate");
        } else if (id == R.id.list_location) {
            in.putExtra("comeForm", "location");
        } else if (id == R.id.list_alltask) {
            in.putExtra("comeForm", "alltask");
        } else if (id == R.id.list_rejected) {
            in.putExtra("comeForm", "rejected");
        } else if (id == R.id.list_completed) {
            in.putExtra("comeForm", "completed");
        }
        startActivity(in);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            selectItem(position);
        }
    }

    private void loadData() {


        CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getApplicationContext());

        //pending
        ArrayList<CReminder> pending = crdbh.getAllDataForPending(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));

        count_pending.setText(String.valueOf(pending.size()));

        //today
        ArrayList<CReminder> _today = crdbh.getAllDataForToday(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));
        Log.d("TAG", "loadData:  _today : " +  _today);
        count_today.setText(String.valueOf(_today.size()));

        //tomorrow
        ArrayList<CReminder> _tomorrow = crdbh.getAllDataForTomorrow(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));

        count_tomorrow.setText(String.valueOf(_tomorrow.size()));

        //Location
        ArrayList<CReminder> _location = crdbh.getAllDataForLocation(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));

        count_location.setText(String.valueOf(_location.size()));

        //NoDueDate
        ArrayList<CReminder> _noduedate = crdbh.getAllDataForN0DueDate(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));

        count_noduedate.setText(String.valueOf(_noduedate.size()));

        //Alltask
       // ArrayList<CReminder> _alltask = crdbh.getAllDataWithSearch(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), "", new HashMap<String, String>(), new HashMap<String, String>());
        ArrayList<CReminder> _alltask = crdbh.getAllCurrentSessDataWithType(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));

        count_alltask.setText(String.valueOf(_alltask.size()));



        //Rejected
        ArrayList<CReminder> _rejected = crdbh.getAllDataForRejected(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));

        count_rejected.setText(String.valueOf(_rejected.size()));

        //Completed
        ArrayList<CReminder> _completed = crdbh.getAllDataForCompleted(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));

        count_completed.setText(String.valueOf(_completed.size()));
    }

}
