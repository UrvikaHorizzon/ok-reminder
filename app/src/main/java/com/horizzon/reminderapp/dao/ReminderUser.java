package com.horizzon.reminderapp.dao;

/**
 * Created by horizzon on 4/3/2017.
 */

public class ReminderUser {

    private String userId;
    private String uName;
    private String uNumber;
    private String uEmail;
    private String uPhoto;
    private String uGender;
    private String uStatus;

    public ReminderUser() {
    }

    public ReminderUser(String userId, String uName, String uNumber, String uEmail, String uPhoto, String uGender, String uStatus) {
        this.userId = userId;
        this.uName = uName;
        this.uNumber = uNumber;
        this.uEmail = uEmail;
        this.uPhoto = uPhoto;
        this.uGender = uGender;
        this.uStatus = uStatus;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getuNumber() {
        return uNumber;
    }

    public void setuNumber(String uNumber) {
        this.uNumber = uNumber;
    }

    public String getuEmail() {
        return uEmail;
    }

    public void setuEmail(String uEmail) {
        this.uEmail = uEmail;
    }

    public String getuPhoto() {
        return uPhoto;
    }

    public void setuPhoto(String uPhoto) {
        this.uPhoto = uPhoto;
    }

    public String getuGender() {
        return uGender;
    }

    public void setuGender(String uGender) {
        this.uGender = uGender;
    }

    public String getuStatus() {
        return uStatus;
    }

    public void setuStatus(String uStatus) {
        this.uStatus = uStatus;
    }
}
