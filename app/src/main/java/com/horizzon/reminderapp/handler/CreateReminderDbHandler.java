package com.horizzon.reminderapp.handler;

/**
 * Created by Abhijit Sojitra on 9/3/17.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.horizzon.reminderapp.dao.CReminder;
import com.horizzon.reminderapp.data.Common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CreateReminderDbHandler {


    // table name
    public static final String TABLE_NAME = "reminder";

    public static final String[] COLUMNS = {
            "Id",
            "sesssionId",
            "userId",
            "subject",
            "priority",
            "reminderFor",
            "status",
            "lockstatus",
            "extra",
            "createddate",
            "flodername"
    };

    DatabaseHandler dbh;
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
            + "(" + COLUMNS[0] + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMNS[1] + " TEXT,"
            + COLUMNS[2] + " TEXT,"
            + COLUMNS[3] + " TEXT,"
            + COLUMNS[4] + " TEXT,"
            + COLUMNS[5] + " TEXT,"
            + COLUMNS[6] + " TEXT,"
            + COLUMNS[7] + " TEXT,"
            + COLUMNS[8] + " TEXT,"
            + COLUMNS[9] + " TEXT,"
            + COLUMNS[10] + " TEXT"
            + ");";

    public CreateReminderDbHandler(Context context) {
        dbh = new DatabaseHandler(context);
    }


    public void addData(String[] c) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        for (int j = 1; j < COLUMNS.length; j++) {
            Common.DisplayLog("columnNames session :: ", COLUMNS[j]);
            values.put(COLUMNS[j], c[j]);
        }

        long id = db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public void updataSubject(String sesid, String name) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[3], name);

        db.update(TABLE_NAME, values, COLUMNS[1] + "=?", new String[]{sesid});
        HashMap<String, String> ex = getTableSingleDate(sesid);

    }

    public void updataPriority(String sesid, String name) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[4], name);

        db.update(TABLE_NAME, values, COLUMNS[1] + "=?", new String[]{sesid});

    }

    public void updataReminderFor(String sesid, String name,String folderName) {
        SQLiteDatabase db = dbh.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMNS[4], name);
        values.put(COLUMNS[10], folderName);
        db.update(TABLE_NAME, values, COLUMNS[1] + "=?", new String[]{sesid});

    }
    public void updataReminderForFoldername(String sesid, String name) {
        SQLiteDatabase db = dbh.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMNS[10], name);
        // values.put(COLUMNS[10], folderName);
        db.update(TABLE_NAME, values, COLUMNS[1] + "=?", new String[]{sesid});

    }
    public void updataStatus(String sesid, String status) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[6], status);

        db.update(TABLE_NAME, values, COLUMNS[1] + "=?", new String[]{sesid});

    }

    public void updataLockStatus(String sesid, String status) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[7], status);

        db.update(TABLE_NAME, values, COLUMNS[1] + "=?", new String[]{sesid});

    }

    public HashMap<String, String> getTableSingleDate(String sesid) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        HashMap<String, String> hashmap = null;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "='" + sesid + "' ORDER BY " + COLUMNS[0] + " DESC";

        Cursor cursor = db.rawQuery(query_select, null);
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                hashmap = new HashMap<String, String>();
                for (int j = 0; j < columnNames.length; j++) {
                    /*if (ConstantData.log_enable) {
                        Log.e("columnNames", columnNames[j]);
						Log.e("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
					}*/
                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
                    hashmap.put(COLUMNS[j], cursor.getString(j));
                }

            } while (cursor.moveToNext());
        }
        cursor.close();

        return hashmap;
    }

    public ArrayList<CReminder> getAllCurrentSessDataWithType(String uid) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<CReminder> list = new ArrayList<CReminder>();
        CReminder CReminder;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[2] + "='" + uid + "' ORDER BY " + COLUMNS[0] + " ASC";
        Common.DisplayLog("query_select", query_select + "");
        Cursor cursor = db.rawQuery(query_select, null);
        Common.DisplayLog("cursor.getCount()", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                CReminder = new CReminder(cursor.getString(0),
                        cursor.getString(1), cursor.getString(2),
                        cursor.getString(3), cursor.getString(4),
                        cursor.getString(5), cursor.getString(6),
                        cursor.getString(7), cursor.getString(8),
                        cursor.getString(9), cursor.getString(10));
                for (int j = 0; j < columnNames.length; j++) {
                    Common.DisplayLog("", "columnNames  " + columnNames[j] + " => " + cursor.getString(j));
                }
                list.add(CReminder);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public ArrayList<CReminder> getAllDataWithSearch(String uid, String comeForm, HashMap<String, String> cond,
                                                     HashMap<String, String> odercon) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<CReminder> list = new ArrayList<CReminder>();
        CReminder CReminder;

        String qu = "";

        for (Map.Entry m : cond.entrySet()) {
            qu = " AND " + m.getKey() + "='" + m.getValue() + "'";
        }
        String oderby = COLUMNS[0] + " DESC";
        String groupby = "";


        Common.DisplayLog("odercon.size():: ", odercon.size() + "");
        if (odercon.size() > 0) {
            oderby = "";
        }
        String forfilterperson = "", forfilterpersoncon = "";

        for (int j = 1; j < 4; j++) {
            if (odercon!=null && !odercon.get("filterperson" + j).equals("")) {
                forfilterperson = " LEFT JOIN " + CRContactDbHandler.TABLE_NAME
                        + " ON " + CRContactDbHandler.TABLE_NAME + "." + CRContactDbHandler.COLUMNS[1] + "=" + TABLE_NAME + "." + COLUMNS[1];
                forfilterpersoncon += "'" + odercon.get("filterperson" + j).toString() + "',";

                Common.DisplayLog("filterperson" + j + " ** ", "'" + odercon.get("filterperson" + j).toString() + "',");
            }
        }
        if (forfilterpersoncon.equals("")) {
            oderby = COLUMNS[0] + " DESC";
        }
        if (odercon.size() > 3) {
            oderby = "";

        }

        for (Map.Entry m : odercon.entrySet()) {
            Common.DisplayLog("query :: ", m.getValue() + "");
            if (m.getKey().equals("filterperson1") || m.getKey().equals("filterperson2") || m.getKey().equals("filterperson3")) {
                /*forfilterperson = " LEFT JOIN " + CRContactDbHandler.TABLE_NAME
                        + " ON " + CRContactDbHandler.TABLE_NAME + "." + CRContactDbHandler.COLUMNS[1] + "=" + TABLE_NAME + "." + COLUMNS[1];
                forfilterpersoncon += "'" + m.getValue().toString() + "',";

                Common.DisplayLog(m.getKey().toString() + " ** ", "'" + m.getValue().toString() + "',");*/
            } else {

                oderby += m.getValue() + ",";
            }
        }


        if (!forfilterpersoncon.equals("")) {
//            oderby = "FIELD(" + CRContactDbHandler.TABLE_NAME + "." + CRContactDbHandler.COLUMNS[3] + "," + forfilterpersoncon.replaceAll(",$", "") + ")," + CRContactDbHandler.TABLE_NAME + "." + CRContactDbHandler.COLUMNS[3] + "," + oderby;
            Common.DisplayLog("forfilterpersoncon", forfilterpersoncon.replaceAll(",$", "") + "");
            String[] st = forfilterpersoncon.replaceAll(",$", "").toString().split(",");
            String condst = "";
            int i = 0;
            for (i = 0; i < st.length; i++) {
                condst += " WHEN " + st[i] + " THEN " + i;
            }
            oderby = " (CASE " + CRContactDbHandler.TABLE_NAME + "." + CRContactDbHandler.COLUMNS[3] + condst + " ELSE " + (i++) + " END) ASC," + CRContactDbHandler.TABLE_NAME + "." + CRContactDbHandler.COLUMNS[3] + "," + oderby;
        }
//        oderby += COLUMNS[0] + " DESC";
        oderby = oderby.replaceAll(",$", "");

        Common.DisplayLog("query :: ", qu);


        String query_select = "SELECT  distinct " + TABLE_NAME + "." + "*,"
                + CRDateTimeDbHandler.TABLE_NAME + "." + CRDateTimeDbHandler.COLUMNS[4] + ","
                + CRDateTimeDbHandler.TABLE_NAME + "." + CRDateTimeDbHandler.COLUMNS[5] + ",";
        if (!forfilterpersoncon.equals("")) {
            query_select+= CRContactDbHandler.TABLE_NAME + "." + CRContactDbHandler.COLUMNS[3] + ",";
        }

        query_select+= "(CASE WHEN " + CRDateTimeDbHandler.COLUMNS[4] + "!='' THEN (" + CRDateTimeDbHandler.COLUMNS[4] + " || ' ' || " + CRDateTimeDbHandler.COLUMNS[5] + ") ELSE " + TABLE_NAME + "." + COLUMNS[9] + " END ) as custondate"

                + " FROM " + TABLE_NAME
                + " LEFT JOIN " + CRDateTimeDbHandler.TABLE_NAME
                + " ON " + CRDateTimeDbHandler.TABLE_NAME + "." + CRDateTimeDbHandler.COLUMNS[1] + "=" + TABLE_NAME + "." + COLUMNS[1]
                + forfilterperson
                + " WHERE " + TABLE_NAME + "." + COLUMNS[2] + "='" + uid + "' " + qu + " ORDER BY " + oderby;


        Common.DisplayLog("query_select", query_select + "");
        Cursor cursor = db.rawQuery(query_select, null);
        Common.DisplayLog("cursor.getCount()", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                CReminder = new CReminder(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3),
                        cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7),
                        cursor.getString(8), cursor.getString(9), cursor.getString(10));
                for (int j = 0; j < columnNames.length; j++) {
                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
                }
                list.add(CReminder);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }


    public ArrayList<CReminder> getAllDataWithFolder(String uid, String comeForm, HashMap<String, String> cond,
                                                     HashMap<String, String> odercon) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<CReminder> list = new ArrayList<CReminder>();
        CReminder CReminder;

        String qu = "";

        for (Map.Entry m : cond.entrySet()) {
            qu = " AND " + m.getKey() + "='" + m.getValue() + "'";
        }
        String oderby = COLUMNS[0] + " DESC";
        String groupby = "";


        Common.DisplayLog("odercon.size():: ", odercon.size() + "");
        if (odercon.size() > 0) {
            oderby = "";
        }
        String forfilterperson = "", forfilterpersoncon = "";

        for (int j = 1; j < 4; j++) {
            if (odercon!=null && !odercon.get("filterperson" + j).equals("")) {
                forfilterperson = " LEFT JOIN " + CRContactDbHandler.TABLE_NAME
                        + " ON " + CRContactDbHandler.TABLE_NAME + "." + CRContactDbHandler.COLUMNS[1] + "=" + TABLE_NAME + "." + COLUMNS[1];
                forfilterpersoncon += "'" + odercon.get("filterperson" + j).toString() + "',";

                Common.DisplayLog("filterperson" + j + " ** ", "'" + odercon.get("filterperson" + j).toString() + "',");
            }
        }
        if (forfilterpersoncon.equals("")) {
            oderby = COLUMNS[0] + " DESC";
        }
        if (odercon.size() > 3) {
            oderby = "";

        }

        for (Map.Entry m : odercon.entrySet()) {
            Common.DisplayLog("query :: ", m.getValue() + "");
            if (m.getKey().equals("filterperson1") || m.getKey().equals("filterperson2") || m.getKey().equals("filterperson3")) {
                /*forfilterperson = " LEFT JOIN " + CRContactDbHandler.TABLE_NAME
                        + " ON " + CRContactDbHandler.TABLE_NAME + "." + CRContactDbHandler.COLUMNS[1] + "=" + TABLE_NAME + "." + COLUMNS[1];
                forfilterpersoncon += "'" + m.getValue().toString() + "',";

                Common.DisplayLog(m.getKey().toString() + " ** ", "'" + m.getValue().toString() + "',");*/
            } else {

                oderby += m.getValue() + ",";
            }


        }


        if (!forfilterpersoncon.equals("")) {
//            oderby = "FIELD(" + CRContactDbHandler.TABLE_NAME + "." + CRContactDbHandler.COLUMNS[3] + "," + forfilterpersoncon.replaceAll(",$", "") + ")," + CRContactDbHandler.TABLE_NAME + "." + CRContactDbHandler.COLUMNS[3] + "," + oderby;
            Common.DisplayLog("forfilterpersoncon", forfilterpersoncon.replaceAll(",$", "") + "");
            String[] st = forfilterpersoncon.replaceAll(",$", "").toString().split(",");
            String condst = "";
            int i = 0;
            for (i = 0; i < st.length; i++) {
                condst += " WHEN " + st[i] + " THEN " + i;
            }
            oderby = " (CASE " + CRContactDbHandler.TABLE_NAME + "." + CRContactDbHandler.COLUMNS[3] + condst + " ELSE " + (i++) + " END) ASC," + CRContactDbHandler.TABLE_NAME + "." + CRContactDbHandler.COLUMNS[3] + "," + oderby;
        }
//        oderby += COLUMNS[0] + " DESC";
        oderby = oderby.replaceAll(",$", "");

        Common.DisplayLog("query :: ", qu);


        String query_select = "SELECT  distinct " + TABLE_NAME + "." + "*,"
                + CRDateTimeDbHandler.TABLE_NAME + "." + CRDateTimeDbHandler.COLUMNS[4] + ","
                + CRDateTimeDbHandler.TABLE_NAME + "." + CRDateTimeDbHandler.COLUMNS[5] + ",";
        if (!forfilterpersoncon.equals("")) {
            query_select+= CRContactDbHandler.TABLE_NAME + "." + CRContactDbHandler.COLUMNS[3] + ",";
        }

        query_select+= "(CASE WHEN " + CRDateTimeDbHandler.COLUMNS[4] + "!='' THEN (" + CRDateTimeDbHandler.COLUMNS[4] + " || ' ' || " + CRDateTimeDbHandler.COLUMNS[5] + ") ELSE " + TABLE_NAME + "." + COLUMNS[9] + " END ) as custondate"

                + " FROM " + TABLE_NAME
                + " LEFT JOIN " + CRDateTimeDbHandler.TABLE_NAME
                + " ON " + CRDateTimeDbHandler.TABLE_NAME + "." + CRDateTimeDbHandler.COLUMNS[1] + "=" + TABLE_NAME + "." + COLUMNS[1]
                + forfilterperson
                + " WHERE " + TABLE_NAME + "." + COLUMNS[2] + "='" + uid + "' " + qu + " ORDER BY " + oderby;


        Common.DisplayLog("query_select", query_select + "");
        Cursor cursor = db.rawQuery(query_select, null);
        Common.DisplayLog("cursor.getCount()", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                CReminder = new CReminder(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3),
                        cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7),
                        cursor.getString(8), cursor.getString(9), cursor.getString(10));
                for (int j = 0; j < columnNames.length; j++) {

                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));

                }
                list.add(CReminder);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public ArrayList<CReminder> getAllDataForNotification(String uid, String comeForm, HashMap<String, String> cond) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<CReminder> list = new ArrayList<CReminder>();
        CReminder CReminder;

        String qu = "";

        for (Map.Entry m : cond.entrySet()) {
            qu = " AND " + m.getKey() + "='" + m.getValue() + "'";
        }

        Common.DisplayLog("query :: ", qu);

        String subquery = "SELECT  " + CReminderForListDbHandler.COLUMNS[2] + " FROM " + CReminderForListDbHandler.TABLE_NAME + " WHERE " + CReminderForListDbHandler.COLUMNS[1] + "='" + uid + "' ";
        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + " in (" + subquery + ") " + qu + " ORDER BY " + COLUMNS[0] + " ASC";
        Common.DisplayLog("query_select", query_select + "");
        Cursor cursor = db.rawQuery(query_select, null);
        Common.DisplayLog("cursor.getCount()", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                CReminder = new CReminder(cursor.getString(0), cursor.getString(1),
                        cursor.getString(2), cursor.getString(3), cursor.getString(4),
                        cursor.getString(5), cursor.getString(6), cursor.getString(7),
                        cursor.getString(8), cursor.getString(9), cursor.getString(10));
                for (int j = 0; j < columnNames.length; j++) {

                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));

                }
                list.add(CReminder);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public ArrayList<CReminder> getAllDataForPending(String uid) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<CReminder> list = new ArrayList<CReminder>();
        CReminder CReminder;

        String subqu = "SELECT " + CReminderForListDbHandler.COLUMNS[2] + " FROM " + CReminderForListDbHandler.TABLE_NAME
                + " WHERE " + CReminderForListDbHandler.COLUMNS[1] + " = '" + uid + "' AND " + CReminderForListDbHandler.COLUMNS[7] + " = 'accept'";
        String query_select = "SELECT * " +
                "FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + " in " +
                "(" + subqu + ")" +
                " AND " + COLUMNS[6] + " != 'complete'" +
                " AND DATE(" + COLUMNS[9] + ") >= DATE('now')";


        Common.DisplayLog("query_select", query_select + "");
        Cursor cursor = db.rawQuery(query_select, null);
        Common.DisplayLog("cursor.getCount()", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                CReminder = new CReminder(cursor.getString(0), cursor.getString(1),
                        cursor.getString(2), cursor.getString(3),
                        cursor.getString(4), cursor.getString(5),
                        cursor.getString(6), cursor.getString(7),
                        cursor.getString(8), cursor.getString(9), cursor.getString(10));
                for (int j = 0; j < columnNames.length; j++) {

                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));

                }
                list.add(CReminder);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public ArrayList<CReminder> getAllDataForToday(String uid) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<CReminder> list = new ArrayList<CReminder>();
        CReminder CReminder;

        String query_select = "SELECT DISTINCT  cr.* " +
                "FROM " + TABLE_NAME + " cr," + CRDateTimeDbHandler.TABLE_NAME + " crd " +
                "WHERE (cr." + COLUMNS[1] + " = crd." + CRDateTimeDbHandler.COLUMNS[1] +
                " AND (DATE((crd." + CRDateTimeDbHandler.COLUMNS[3] + "||' '||crd." + CRDateTimeDbHandler.COLUMNS[4] + ")) = DATE('now')))" +
                " OR (DATE(cr." + COLUMNS[9] + ") = DATE('now'))";


        Common.DisplayLog("query_select", query_select + "");
        Cursor cursor = db.rawQuery(query_select, null);
        Common.DisplayLog("cursor.getCount()", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                CReminder = new CReminder(cursor.getString(0), cursor.getString(1),
                        cursor.getString(2), cursor.getString(3), cursor.getString(4),
                        cursor.getString(5), cursor.getString(6), cursor.getString(7),
                        cursor.getString(8), cursor.getString(9), cursor.getString(10));
                for (int j = 0; j < columnNames.length; j++) {

                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));

                }
                list.add(CReminder);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public ArrayList<CReminder> getAllDataForTomorrow(String uid) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<CReminder> list = new ArrayList<CReminder>();
        CReminder CReminder;

        String query_select = "SELECT DISTINCT  cr.* " +
                "FROM " + TABLE_NAME + " cr," + CRDateTimeDbHandler.TABLE_NAME + " crd " +
                "WHERE (cr." + COLUMNS[1] + " = crd." + CRDateTimeDbHandler.COLUMNS[1] +
                " AND (DATE((crd." + CRDateTimeDbHandler.COLUMNS[3] + "||' '||crd." + CRDateTimeDbHandler.COLUMNS[4] + ")) = DATE('now','1 day')))" +
                " OR (DATE(cr." + COLUMNS[9] + ") = DATE('now','1 day'))";


        Common.DisplayLog("query_select", query_select + "");
        Cursor cursor = db.rawQuery(query_select, null);
        Common.DisplayLog("cursor.getCount()", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                CReminder = new CReminder(cursor.getString(0), cursor.getString(1),
                        cursor.getString(2), cursor.getString(3), cursor.getString(4),
                        cursor.getString(5), cursor.getString(6), cursor.getString(7),
                        cursor.getString(8), cursor.getString(9), cursor.getString(10));
                for (int j = 0; j < columnNames.length; j++) {

                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));

                }
                list.add(CReminder);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public ArrayList<CReminder> getAllDataForN0DueDate(String uid) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<CReminder> list = new ArrayList<CReminder>();
        CReminder CReminder;

        String query_select = "SELECT DISTINCT  cr.* " +
                "FROM " + TABLE_NAME + " cr," + CRDateTimeDbHandler.TABLE_NAME + " crd " +
                "WHERE (cr." + COLUMNS[1] + " = crd." + CRDateTimeDbHandler.COLUMNS[1] +
                " AND (DATE((crd." + CRDateTimeDbHandler.COLUMNS[3] + "||' '||crd." + CRDateTimeDbHandler.COLUMNS[4] + ")) < DATE('now')))" +
                " OR (DATE(cr." + COLUMNS[9] + ") < DATE('now'))";


        Common.DisplayLog("query_select", query_select + "");
        Cursor cursor = db.rawQuery(query_select, null);
        Common.DisplayLog("cursor.getCount()", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                CReminder = new CReminder(cursor.getString(0), cursor.getString(1),
                        cursor.getString(2), cursor.getString(3), cursor.getString(4),
                        cursor.getString(5), cursor.getString(6), cursor.getString(7),
                        cursor.getString(8), cursor.getString(9), cursor.getString(10));
                for (int j = 0; j < columnNames.length; j++) {

                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));

                }
                list.add(CReminder);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public ArrayList<CReminder> getAllDataForRejected(String uid) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<CReminder> list = new ArrayList<CReminder>();
        CReminder CReminder;

        String subqu = "SELECT " + CReminderForListDbHandler.COLUMNS[2] + " FROM " + CReminderForListDbHandler.TABLE_NAME
                + " WHERE " + CReminderForListDbHandler.COLUMNS[1] + " = '" + uid + "' AND " + CReminderForListDbHandler.COLUMNS[7] + " = 'reject'";
        String query_select = "SELECT * " +
                "FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + " in " +
                "(" + subqu + ")";


        Common.DisplayLog("query_select", query_select + "");
        Cursor cursor = db.rawQuery(query_select, null);
        Common.DisplayLog("cursor.getCount()", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                CReminder = new CReminder(cursor.getString(0), cursor.getString(1),
                        cursor.getString(2), cursor.getString(3), cursor.getString(4),
                        cursor.getString(5), cursor.getString(6), cursor.getString(7),
                        cursor.getString(8), cursor.getString(9), cursor.getString(10));
                for (int j = 0; j < columnNames.length; j++) {

                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));

                }
                list.add(CReminder);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public ArrayList<CReminder> getAllDataForLocation(String uid) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<CReminder> list = new ArrayList<CReminder>();
        CReminder CReminder;

        String query_select = "SELECT cr.* " +
                "FROM " + TABLE_NAME + " cr," + CRLocationDbHandler.TABLE_NAME + " crl " +
                "WHERE cr." + COLUMNS[1] + " = crl." + CRLocationDbHandler.COLUMNS[1];


        Common.DisplayLog("query_select", query_select + "");
        Cursor cursor = db.rawQuery(query_select, null);
        Common.DisplayLog("cursor.getCount()", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                CReminder = new CReminder(cursor.getString(0), cursor.getString(1),
                        cursor.getString(2), cursor.getString(3),
                        cursor.getString(4), cursor.getString(5),
                        cursor.getString(6), cursor.getString(7),
                        cursor.getString(8), cursor.getString(9), cursor.getString(10));
                for (int j = 0; j < columnNames.length; j++) {

                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));

                }
                list.add(CReminder);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public ArrayList<CReminder> getAllDataForCompleted(String uid) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<CReminder> list = new ArrayList<CReminder>();
        CReminder CReminder;

        /*String subqu = "SELECT " + CReminderForListDbHandler.COLUMNS[2] + " FROM " + CReminderForListDbHandler.TABLE_NAME
                + " WHERE " + CReminderForListDbHandler.COLUMNS[1] + " = '" + uid + "'";
        String query_select = "SELECT * " +
                "FROM " + TABLE_NAME + " WHERE " +
                //+ COLUMNS[1] + " in " +
//                "(" + subqu + ")" +
                COLUMNS[6] + " = 'complete'" +
                " AND " + COLUMNS[3] + " =" + uid;*/
        String subqu = "SELECT " + CReminderForListDbHandler.COLUMNS[2] + " FROM " + CReminderForListDbHandler.TABLE_NAME
                + " WHERE " + CReminderForListDbHandler.COLUMNS[1] + " = '" + uid + "' AND " + CReminderForListDbHandler.COLUMNS[7] + " = 'accept'";
        String query_select = "SELECT * " +
                "FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + " in " +
                "(" + subqu + ")";

        Common.DisplayLog("query_select", query_select + "");
        Cursor cursor = db.rawQuery(query_select, null);
        Common.DisplayLog("cursor.getCount()", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                CReminder = new CReminder(cursor.getString(0), cursor.getString(1),
                        cursor.getString(2), cursor.getString(3), cursor.getString(4),
                        cursor.getString(5), cursor.getString(6), cursor.getString(7),
                        cursor.getString(8), cursor.getString(9), cursor.getString(10));
                for (int j = 0; j < columnNames.length; j++) {

                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));

                }
                list.add(CReminder);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public CReminder getTableSingleDateWihtDao(String sesid) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        CReminder cReminder = null;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "='" + sesid + "' ORDER BY " + COLUMNS[0] + " DESC";

        Cursor cursor = db.rawQuery(query_select, null);

        Common.DisplayLog("cursor.getCount()", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            Common.DisplayLog("cursor", "yes");
            String[] columnNames = cursor.getColumnNames();
            do {
                cReminder = new CReminder(cursor.getString(0), cursor.getString(1),
                        cursor.getString(2), cursor.getString(3), cursor.getString(4),
                        cursor.getString(5), cursor.getString(6), cursor.getString(7),
                        cursor.getString(8), cursor.getString(9), cursor.getString(10));
                for (int j = 0; j < columnNames.length; j++) {
                    /*if (ConstantData.log_enable) {
                        Log.e("columnNames", columnNames[j]);
						Log.e("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
					}*/
                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
                }
            } while (cursor.moveToNext());
        }
        cursor.close();

        return cReminder;
    }

    public void resetTempdata(String sessid) {
        SQLiteDatabase db = dbh.getWritableDatabase();
//        db.delete(TABLE_NAME, null, null);
        db.execSQL("delete from " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = " + sessid);
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }

    public void resetTempstatusdata() {
        SQLiteDatabase db = dbh.getWritableDatabase();
//        db.delete(TABLE_NAME, null, null);
        db.execSQL("delete from " + TABLE_NAME + " WHERE " + COLUMNS[6] + " != 'done'");
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }


    public void resetTable() {
        SQLiteDatabase db = dbh.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.execSQL("delete from " + TABLE_NAME);
        db.execSQL("delete from sqlite_sequence where name='" + TABLE_NAME + "'");
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }

}
