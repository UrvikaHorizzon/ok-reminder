package com.horizzon.reminderapp.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.dao.DetailsCommnetsModel;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.RUserDbHandler;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.util.ArrayList;

public class DetailsCommentAdapter extends RecyclerView.Adapter<DetailsCommentAdapter.MyViewHolder> {

    Activity context;
    ArrayList<DetailsCommnetsModel.Datum> list;
    RUserDbHandler rudbh;
    String currdate = "", inFor;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout u_layout, cu_layout;
        public ImageView u_image, cu_image;
        public TextView commentdate, uName, cuName, u_comment, cu_comment;

        public MyViewHolder(View view) {
            super(view);
            u_layout = (LinearLayout) view.findViewById(R.id.u_layout);
            cu_layout = (LinearLayout) view.findViewById(R.id.cu_layout);

            u_image = (ImageView) view.findViewById(R.id.u_image);
            cu_image = (ImageView) view.findViewById(R.id.cu_image);

            commentdate = (TextView) view.findViewById(R.id.commentdate);
            uName = (TextView) view.findViewById(R.id.uName);
            cuName = (TextView) view.findViewById(R.id.cuName);
            u_comment = (TextView) view.findViewById(R.id.u_comment);
            cu_comment = (TextView) view.findViewById(R.id.cu_comment);
        }
    }


    public DetailsCommentAdapter(Activity context, ArrayList<DetailsCommnetsModel.Datum> list, String inFor) {
        this.context = context;
        this.list = list;
        this.rudbh = new RUserDbHandler(context);
        this.inFor = inFor;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (inFor.equals("view")) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.comment_row_view, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.comment_row, parent, false);
        }
        return new DetailsCommentAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final DetailsCommnetsModel.Datum rc = list.get(position);
        // ReminderUser ru = rudbh.getTableSingleDateWihtDao(rc.getUserId());
        //if (ru != null) {

        holder.u_layout.setVisibility(View.GONE);
        holder.cu_layout.setVisibility(View.GONE);
        holder.uName.setText("");
        holder.cuName.setText("");
        holder.u_comment.setText("");
        holder.cu_comment.setText("");
        holder.commentdate.setVisibility(View.GONE);

        try {
            Common.DisplayLog("Common.formatToYesterdayOrToday(rc.getCommentdate()).toString()", Common.formatToYesterdayOrToday(rc.getTComment()).toString() + "");
            holder.commentdate.setText(Common.formatToYesterdayOrToday(rc.getDCreatedDateTime()).toString());
            holder.commentdate.setVisibility(View.VISIBLE);
            currdate = rc.getDCreatedDateTime().toString();
            if (!rc.getDCreatedDateTime().toString().equals(currdate)) {
            } else {
                holder.commentdate.setText("");
                holder.commentdate.setVisibility(View.GONE);
            }
        } catch (ParseException e) {
            e.printStackTrace();
            holder.commentdate.setVisibility(View.GONE);
        }

         /*   String imgFile;
            if (ru.getuPhoto() != null) {
                imgFile = (ru.getuPhoto().equals("")) ? "default" : ru.getuPhoto();
            } else {
                imgFile = "";

            }*/

        // String uname = (ru.getuName().equals("")) ? ru.getuNumber() : ru.getuName();
        String uPix = rc.getVUPhoto();
        String imgFile = "https://www.hwpl.in/projects/reminderapp" + uPix.substring(2);
        Picasso.with(context)
                .load(imgFile)
                .placeholder(R.drawable.defaultusericon)
                .error(R.drawable.defaultusericon)
                .into(holder.cu_image);
        holder.cuName.setText(rc.getVUName());
        holder.cu_comment.setText(rc.getTComment());
        holder.cu_layout.setVisibility(View.VISIBLE);
    }
       /* if (Common.getSharedPreferences(context, "userId", "0").equals(rc.getUserId().toString())) {

//        Picasso.with(context)
//        Glide.with(context)
                *//*Picasso.with(context)
                        .load(imgFile)
                        .placeholder(R.drawable.defaultusericon)
                        .error(R.drawable.defaultusericon)
                        .into(holder.cu_image);
                holder.cuName.setText(uname);*//*
            holder.cu_comment.setText(rc.getComment());
            holder.cu_layout.setVisibility(View.VISIBLE);

        } else {

                *//*Picasso.with(context)
                        .load(imgFile)
                        .placeholder(R.drawable.defaultusericon)
                        .error(R.drawable.defaultusericon)
                        .into(holder.u_image);
                holder.uName.setText(uname);*//*
            holder.u_comment.setText(rc.getComment());
            holder.u_layout.setVisibility(View.VISIBLE);
        }
    }*//*else {
            Toast.makeText(context, "No data found.", Toast.LENGTH_SHORT).show();
        }
    }*/

    @Override
    public int getItemCount() {
        return list.size();
    }
}
