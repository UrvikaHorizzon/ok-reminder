package com.horizzon.reminderapp.fragment;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horizzon.reminderapp.DetailViewActivity;
import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.adapter.ImageAttachmentAdapter;
import com.horizzon.reminderapp.dao.AttachmentTable;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.CRAttachmentDbHandler;
import com.horizzon.reminderapp.handler.CRContactDbHandler;
import com.horizzon.reminderapp.helper.Clickable;
import com.horizzon.reminderapp.helper.ExtendedViewPager;
import com.horizzon.reminderapp.helper.SpaceItemDecoration;
import com.horizzon.reminderapp.helper.TouchImageAdapter;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailAttachmentTabFragment extends Fragment implements Clickable {

    String SessionId;
    LinearLayout subtaskLayout, maincontentlay, pafeviewerlay, notesLayout, imagesLayout, subtasklistLayout, notlistLayout, contactMainLayout, contactLayout;


    ArrayList<AttachmentTable> imagesList;
    ImageAttachmentAdapter imgAdapter;
    RecyclerView imglistView;
    CRAttachmentDbHandler cradbh;
    ExtendedViewPager miimgpwEViewPager;
    String inFrom = "done";
    int backstate = 0;
    DetailViewActivity _ac;

    public DetailAttachmentTabFragment(DetailViewActivity ac) {
        _ac = ac;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_detail_attachment_tab, container, false);

        SessionId = Common.getSharedPreferences(getActivity(), "SessionId", "");
        cradbh = new CRAttachmentDbHandler(getActivity());

        subtaskLayout = (LinearLayout) v.findViewById(R.id.subtaskLayout);
        notesLayout = (LinearLayout) v.findViewById(R.id.notesLayout);
        imagesLayout = (LinearLayout) v.findViewById(R.id.imagesLayout);

        subtasklistLayout = (LinearLayout) v.findViewById(R.id.subtasklistLayout);
        notlistLayout = (LinearLayout) v.findViewById(R.id.notlistLayout);
        maincontentlay = (LinearLayout) v.findViewById(R.id.maincontentlay);
        pafeviewerlay = (LinearLayout) v.findViewById(R.id.pafeviewerlay);
        contactMainLayout = (LinearLayout) v.findViewById(R.id.contactMainLayout);
        contactLayout = (LinearLayout) v.findViewById(R.id.contactLayout);

        miimgpwEViewPager = (ExtendedViewPager) v.findViewById(R.id.miimgpwEViewPager);
        imglistView = (RecyclerView) v.findViewById(R.id.imglistView);


        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        getAttachData(SessionId);
    }


    public void getAttachData(String SessId) {
        Common.DisplayLog("", "loadSavedData " + SessId);
        //load Subtask

        subtasklistLayout.removeAllViews();

        ArrayList<AttachmentTable> lodsubt = cradbh.getAllCurrentSessDataWithType(SessId, "subtask", inFrom);

        if (lodsubt.size() > 0) {
            subtaskLayout.setVisibility(View.VISIBLE);
            for (int i = 0; i < lodsubt.size(); i++) {
                subtasklistLayout.addView(createEditView(lodsubt.get(i).getAttachFor(), lodsubt.get(i).getId()));
            }
        }

        //load nots
        notlistLayout.removeAllViews();
        ArrayList<AttachmentTable> lodnote = cradbh.getAllCurrentSessDataWithType(SessId, "note", inFrom);
        if (lodnote.size() > 0) {
            notesLayout.setVisibility(View.VISIBLE);

            Common.DisplayLog("", "imgNoteEdit clicked");
            notlistLayout.addView(createEditView(lodnote.get(0).getAttachFor(), lodnote.get(0).getId()));
        }

        //load image layout
        getAttachImages();
        //load Contact
        getAttachContact();
    }

    private View createEditView(final String attfor, String preId) {
//        mContainerView = (LinearLayout)findViewById(R.id.parentView);
        Common.DisplayLog("preId ", preId);

        final String lastid = preId;

        final AttachmentTable at = cradbh.getTableSingleDateWihtDao(SessionId, lastid);
        Common.DisplayLog("lastid ", lastid + "");
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View myView = inflater.inflate(R.layout.attachment_edit_row, null);
        myView.setTag(lastid);
        final CheckBox attselchk = (CheckBox) myView.findViewById(R.id.attselchk);


        attselchk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                attselchk.setChecked(false);
            }
        });
        attselchk.setChecked(false);
        if (attfor.equals("note")) {
            attselchk.setVisibility(View.GONE);
        }

        final LinearLayout editcontanor = (LinearLayout) myView.findViewById(R.id.editcontanor);
        final TextView texv = (TextView) myView.findViewById(R.id.textChange);
        String st = at.getDetail().replace("\\n", "\n").replace("\n", "\r\n");
//        texv.setVisibility(View.GONE);

        texv.setText(st);

//        ed.setText(preId);
        final Button textEdit = (Button) myView.findViewById(R.id.textEdit);
        final Button textDelete = (Button) myView.findViewById(R.id.textDelete);

        textEdit.setVisibility(View.GONE);
        textDelete.setVisibility(View.GONE);

        if (preId.equals("")) {
//            textEdit.performClick();
        } else if (attfor.equals("note")) {

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {

                            final EditText texv1 = new EditText(getActivity()); // Pass it an Activity or Context
                            texv1.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                            texv1.setGravity(Gravity.CENTER | Gravity.LEFT);
                            texv1.setBackgroundResource(0);
                            final float scale = getResources().getDisplayMetrics().density;
                            int lr = (int) (26 * scale + 0.5f);
                            int tb = (int) (10 * scale + 0.5f);
                            texv1.setPadding(lr, tb, lr, tb);
                            texv1.setTextColor(Color.parseColor("#adadad"));
                            texv1.setHintTextColor(Color.parseColor("#adadad"));
                            texv1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                            editcontanor.removeAllViews();
                            editcontanor.addView(texv1);
                            texv1.setText(at.getDetail());
                            Common.DisplayLog("texv1", texv1.getText().toString());
                            /*text text*/

                            String edst = texv1.getText().toString().trim();
                            final TextView texv2 = new TextView(getActivity()); // Pass it an Activity or Context
                            texv2.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                            texv2.setGravity(Gravity.CENTER | Gravity.LEFT);
                            texv2.setBackgroundResource(0);
                            texv2.setPadding(lr, tb, lr, tb);
                            texv2.setTextColor(Color.parseColor("#adadad"));
                            texv2.setHintTextColor(Color.parseColor("#adadad"));
                            texv2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                            editcontanor.removeAllViews();
                            editcontanor.addView(texv2);
                            texv2.setText(edst);
                        }
                    },
                    1000);
        }
        return myView;
    }

    private void getAttachImages() {

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        imagesList = cradbh.getAllCurrentSessDataWithType(SessionId, "image", inFrom);
                        Common.DisplayLog("imagesList count", imagesList.size() + "");
                        if (imagesList.size() > 0) {
                            imagesLayout.setVisibility(View.VISIBLE);
                        } else {
                            imagesLayout.setVisibility(View.GONE);
                        }
                        imglistView.setAdapter(null);
                        imgAdapter = new ImageAttachmentAdapter(getActivity(), imagesList, DetailAttachmentTabFragment.this, "DetailAttachmentTabFragment","detail");
                        LinearLayoutManager lLayout = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                        imglistView.addItemDecoration(new SpaceItemDecoration(0, 5, 0, 0));
                        imglistView.setHasFixedSize(true);
                        imglistView.setLayoutManager(lLayout);
                        imglistView.setAdapter(imgAdapter);

                        String[] imgLists = new String[imagesList.size() + 1];
                        for (int i = 0; i < imagesList.size(); i++) {
                            imgLists[i] = imagesList.get(i).getDetail();
                        }

                        miimgpwEViewPager.setAdapter(new TouchImageAdapter(getActivity(), imgLists, 0));

                    }
                },
                200);

    }

    public void getExtendedViewPager(int pos) {
        ArrayList<AttachmentTable> imagesLists = cradbh.getAllCurrentSessDataWithType(SessionId, "image", inFrom);
        String[] imgLists = new String[imagesLists.size()];
        for (int i = 0; i < imagesLists.size(); i++) {
            imgLists[i] = imagesLists.get(i).getDetail();
        }

        miimgpwEViewPager.setAdapter(new TouchImageAdapter(getActivity(), imgLists, 0));
        miimgpwEViewPager.setCurrentItem(pos);
//        maincontentlay.setVisibility(View.GONE);
        pafeviewerlay.setVisibility(View.VISIBLE);
        _ac.backstate = 1;
    }

    public void closeExtendedViewPager() {
        pafeviewerlay.setVisibility(View.GONE);
    }

    private void getAttachContact() {
        contactMainLayout.setVisibility(View.GONE);

        contactLayout.removeAllViews();
        final CRContactDbHandler crcdbh = new CRContactDbHandler(getActivity());
        ArrayList<AttachmentTable> remindercontactuser = cradbh.getAllCurrentSessDataWithType(SessionId, "contact", inFrom);


        Common.DisplayLog("remindercontactuser", remindercontactuser.size() + "");
        if (remindercontactuser.size() > 0) {
            contactMainLayout.setVisibility(View.VISIBLE);
            int i = 0;
            for (i = 0; i < remindercontactuser.size(); i++) {

                final AttachmentTable rcu = remindercontactuser.get(i);


                LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = vi.inflate(R.layout.attachcontact_view, null);

                TextView conName = (TextView) v.findViewById(R.id.conName);

                ImageView imageView = (ImageView) v.findViewById(R.id.con_image);
                LinearLayout btncall = (LinearLayout) v.findViewById(R.id.btncall);
                LinearLayout btntask = (LinearLayout) v.findViewById(R.id.btntask);
                LinearLayout btnsave = (LinearLayout) v.findViewById(R.id.btnsave);
                Button conDelete = (Button) v.findViewById(R.id.conDelete);
                conDelete.setVisibility(View.GONE);

                try {
                    final JSONObject json = new JSONObject(rcu.getDetail());
                    Common.DisplayLog("json.toString()", json.toString() + "");
                    conName.setText(json.getString("name").toString());
//                    String imgpath = getRealPathFromURI(Uri.parse(((json.getString("photo").toString().equals("")) ? "defaultimg" : json.getString("photo").toString()) + ""));
                    String imgpath = ((json.getString("photo").toString().equals("")) ? "defaultimg" : json.getString("photo").toString()) + "";
                    final String number = json.getString("number").toString();
                    Common.DisplayLog("imgpath", imgpath + "");
                    Common.DisplayLog("json.getString('photo').toString()", json.getString("photo").toString() + "");
                    Picasso.with(getActivity())
                            .load(imgpath)
                            .placeholder(R.drawable.defaultusericon)
                            .error(R.drawable.defaultusericon)
                            .into(imageView);

                    btncall.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            try {
                                intent.setData(Uri.parse("tel:" + json.getString("number").toString()));
                                startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    btntask.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                            try {
                                sendIntent.setData(Uri.parse("sms:" + json.getString("number").toString()));
                                startActivity(sendIntent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    btnsave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                Intent intent = new Intent(Intent.ACTION_INSERT);
                                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                                intent.putExtra(ContactsContract.Intents.Insert.NAME, json.getString("name").toString());
                                intent.putExtra(ContactsContract.Intents.Insert.PHONE, json.getString("number").toString());
                                int PICK_CONTACT = 100;
                                startActivityForResult(intent, PICK_CONTACT);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    contactLayout.addView(v);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }

    }

    @Override
    public void OnClick(int pos) {
        getExtendedViewPager(pos);
    }
}
