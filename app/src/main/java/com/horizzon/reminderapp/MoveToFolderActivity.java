package com.horizzon.reminderapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.horizzon.reminderapp.adapter.DrawerItemCustomAdapter;
import com.horizzon.reminderapp.adapter.InboxListAdapter;
import com.horizzon.reminderapp.adapter.ReminderListAdapter;
import com.horizzon.reminderapp.dao.RFolderFilter;
import com.horizzon.reminderapp.dao.ReminderUsersBoxs;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.RFolderFilterDbHandler;
import com.horizzon.reminderapp.handler.RUserFolderDbHandler;
import com.horizzon.reminderapp.retrofit.model.reminder.Reminder;
import com.horizzon.reminderapp.retrofit.model.usersboxs.Data;
import com.horizzon.reminderapp.retrofit.model.usersboxs.GetUsersBoxsList;
import com.horizzon.reminderapp.retrofit.model.usersboxs.UsersBoxs;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;
import com.horizzon.reminderapp.utility.ObjectDrawerItem;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoveToFolderActivity extends AppCompatActivity {

    String SessionId,comeForm = "";
    ImageView imgBack, imgBtnOpt;
    RecyclerView listView;
    private DrawerLayout mDrawerLayout;
    protected ListView mDrawerList;
    ObjectDrawerItem[] drawerItem;
    ArrayList<ReminderUsersBoxs> list;
    ReminderListAdapter cAdapter;
    InboxListAdapter IndAdapter;
    FloatingActionButton fab;
    TextView pageTitle;
    RUserFolderDbHandler rufdbh;
    boolean taskDone = true;
    boolean addfolder = true;
    public int item_count = 0;
    Button btnTempDone;
    LinearLayout doneContainer, folderlistLayout, folderLayout, addLayout;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movetofolder);

        pageTitle = (TextView) findViewById(R.id.pageTitle);
        if (getIntent().getStringExtra("comeForm") != null) {
            comeForm = getIntent().getStringExtra("comeForm");
            pageTitle.setText(comeForm);
        }
        SessionId = Common.getSharedPreferences(getApplicationContext(), "SessionId", "");
        rufdbh = new RUserFolderDbHandler(getApplicationContext());

        imgBack = (ImageView) findViewById(R.id.backBtn);
        imgBtnOpt = (ImageView) findViewById(R.id.imgOpt);
        listView = (RecyclerView) findViewById(R.id.list);


        btnTempDone = (Button) findViewById(R.id.btnTempDone);

        doneContainer = (LinearLayout) findViewById(R.id.doneContainer);
        folderlistLayout = (LinearLayout) findViewById(R.id.folderlistLayout);
        folderLayout = (LinearLayout) findViewById(R.id.folderLayout);


        addLayout = (LinearLayout) findViewById(R.id.addLayout);

        list = new ArrayList<ReminderUsersBoxs>();

        addLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (taskDone) {
                    addfolder=true;
                    btnTempDone.performClick();
                    folderlistLayout.addView(createEditView("folder", ""));
                    folderLayout.setVisibility(View.VISIBLE);
                    addLayout.setVisibility(View.VISIBLE);
                    taskDone = false;
                    item_count++;
                } else {
                    Common.ShowToast(getApplicationContext(), "Please complete your task");
                }

            }
        });
//        SetData(comeForm);


        /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ReminderUsersBoxs cr = list.get(position);
                Intent in = new Intent(InboxActivity.this, ReminderDetailActivity.class);
                in.putExtra("SessionId", cr.getSesssionId());
                startActivity(in);
            }
        });*/

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // list the drawer items
        drawerItem = new ObjectDrawerItem[4];

        drawerItem[0] = new ObjectDrawerItem(R.drawable.sentlisticon, "Sent List");
        drawerItem[1] = new ObjectDrawerItem(R.drawable.printlisticon, "Print List");
        drawerItem[2] = new ObjectDrawerItem(R.drawable.clearcompletedicon, "Clear Completed");
        drawerItem[3] = new ObjectDrawerItem(R.drawable.settingicon, "Settings");
//        drawerItem[4] = new ObjectDrawerItem(R.drawable.syncicon, "Sync Now");

        // Pass the folderData to our ListView adapter
        final DrawerItemCustomAdapter adapter1 = new DrawerItemCustomAdapter(this, R.layout.list_item_row, drawerItem);

        // Set the adapter for the list view
        mDrawerList.setAdapter(adapter1);
        // set the item click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // for app icon control for nav drawer
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imgBtnOpt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        });

        pageTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }


    @Override
    protected void onStart() {
        super.onStart();

        Common.DisplayLog("onStart", "onStart");
        loadSavedData();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Common.DisplayLog("onRestart", "onRestart");
        loadSavedData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Common.DisplayLog("onResume", "onResume");
//        loadSavedData(SessionId);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            selectItem(position);
        }
    }

    private void SetData() {


        list = rufdbh.getAllData(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));

        LinearLayoutManager lLayout = new LinearLayoutManager(getApplicationContext());
        listView.setLayoutManager(lLayout);
        if (comeForm.equals("INBOX")) {
//            IndAdapter = new InboxListAdapter(MoveToFolderActivity.this, list, "inbox");
//            listView.setAdapter(IndAdapter);
        } else {

//            cAdapter = new ReminderListAdapter(MoveToFolderActivity.this, list, "inbox");
//            listView.setAdapter(cAdapter);
        }
    }

    private View createEditView(final String attfor, String preId) {
//        mContainerView = (LinearLayout)findViewById(R.id.parentView);
        Common.DisplayLog("preId ", preId);
        String selId;
        if (preId.equals("")) {
            String[] COLUMN = {
                    "",
                    Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                    "",
                    "folder",
                    Common.getSQLDateTime()
            };
            selId = String.valueOf(rufdbh.addData(COLUMN));
            Common.DisplayLog("if == selId ", selId);
        } else {

            selId = preId;
            Common.DisplayLog("else == selId ", selId);
        }
        final String lastid = selId;

        final ReminderUsersBoxs rub = rufdbh.getTableSingleDateWihtDao(lastid);
        Common.DisplayLog("lastid ", lastid + "");
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View myView = inflater.inflate(R.layout.folder_edit_row, null);
        myView.setTag(lastid);

        final CheckBox folderselchk = (CheckBox) myView.findViewById(R.id.folderselchk);


        folderselchk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                folderselchk.setChecked(false);
                AddFolderFilterDataOnServer(rub.getiId());
            }
        });

        final LinearLayout editcontanor = (LinearLayout) myView.findViewById(R.id.editcontanor);
        final TextView texv = (TextView) myView.findViewById(R.id.textChange);
        String st = rub.getvBoxName();
//        texv.setVisibility(View.GONE);

        texv.setText(st);

//        ed.setText(preId);
        final Button textEdit = (Button) myView.findViewById(R.id.textEdit);
        final Button textDelete = (Button) myView.findViewById(R.id.textDelete);

        textEdit.setVisibility(View.VISIBLE);
        textDelete.setVisibility(View.GONE);

        tasksEdit(texv, editcontanor, textEdit, textDelete, lastid, attfor, true);
        if (preId.equals("")) {
//            textEdit.performClick();
            textEdit.performClick();
            addfolder=true;
        }
/*
        if (action.equals("edit") && !Common.getSharedPreferences(getApplicationContext(), "userId", "0").equals(at.getiUserId())) {
            textEdit.setOnClickListener(null);
            textEdit.setVisibility(View.GONE);
            textDelete.setOnClickListener(null);
            textDelete.setVisibility(View.GONE);
        }*/

        return myView;
    }

    private void animationForDelete(final Button textEdit, final Button textDelete) {
        final Animation RightSwipe = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.righttoleft);
        final Animation LeftSwipe = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.righttoleft);
        RightSwipe.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                textEdit.setVisibility(View.GONE);
                textDelete.startAnimation(LeftSwipe);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        LeftSwipe.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                textEdit.setVisibility(View.GONE);
                textDelete.setVisibility(View.VISIBLE);
                textDelete.clearAnimation();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
//                textDelete.setVisibility(View.GONE);
//                textEdit.startAnimation(LeftSwipe);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        textEdit.startAnimation(RightSwipe);
    }

    private void animationForEdit(final Button textEdit, final Button textDelete) {
        final Animation RightSwipe = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.righttoleft);
        final Animation LeftSwipe = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.righttoleft);
        RightSwipe.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
//                textDelete.setVisibility(View.GONE);
//                textEdit.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                textDelete.setVisibility(View.GONE);
                textEdit.startAnimation(LeftSwipe);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        LeftSwipe.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                textDelete.setVisibility(View.GONE);
                textEdit.setVisibility(View.VISIBLE);
                textEdit.clearAnimation();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
//                textDelete.setVisibility(View.GONE);
//                textEdit.startAnimation(LeftSwipe);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        textDelete.startAnimation(RightSwipe);
    }

    private void tasksEdit(final TextView texv, final LinearLayout editcontanor, final Button te, final Button td, final String lastid, final String attfor, final boolean preFacus) {


                    /*create edit text*/

        te.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                btnTempDone.performClick();
                if (taskDone) {

                    addfolder=false;
                    String notesub = texv.getText().toString().trim();
                    Common.DisplayLog("edit notesub", notesub + "");
                    EditText ed = new EditText(getApplicationContext()); // Pass it an Activity or Context
                    ed.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    ed.setGravity(Gravity.CENTER | Gravity.LEFT);
                    ed.setBackgroundResource(0);
                    ed.setHint("Type here..");
                    final float scale = getResources().getDisplayMetrics().density;
                    int lr = (int) (26 * scale + 0.5f);
                    int tb = (int) (10 * scale + 0.5f);
                    ed.setPadding(lr, tb, lr, tb);
                    ed.setTextColor(Color.parseColor("#adadad"));
                    ed.setHintTextColor(Color.parseColor("#adadad"));
                    ed.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                    editcontanor.removeAllViews();
                    editcontanor.addView(ed);

                    ed.setText(notesub);
                    ed.setSelection(ed.getText().length());
                    ed.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            Common.DisplayLog("actionId ", actionId + " done " + EditorInfo.IME_ACTION_DONE);
                            if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                                Common.DisplayLog("", actionId + "");
                                btnTempDone.performClick();
                            }
                            return false;
                        }
                    });

                    animationForDelete(te, td);

                    ed.setFocusable(true);
                    ed.requestFocus();
                    ed.setCursorVisible(true);
                    showDoneLayout(ed, editcontanor, te, td, lastid, attfor, true);
                }
            }
        });

                    /*end edit text*/

    }

    private void showDoneLayout(final EditText edt, final LinearLayout editcontanor, final Button te, final Button td, final String lastid, final String attfor, final boolean preFacus) {
        if (preFacus) {
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            Common.DisplayLog("tag", "This'll run 300 milliseconds later");
                            edt.setSelection(edt.getText().length());
                            edt.setFocusable(true);
                            edt.requestFocus();
                            edt.setCursorVisible(true);
                            InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(edt, InputMethodManager.SHOW_IMPLICIT);

                            doneContainer.setVisibility(View.VISIBLE);
                        }
                    },
                    500);
        }


        edt.setSingleLine(true);
        edt.setImeOptions(EditorInfo.IME_ACTION_DONE);

        td.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.DisplayLog("attfor", "textDelete clicked");
                taskDone = true;

                removeFolderDataOnServer(lastid);

                rufdbh.deleteSingleData(lastid);
                /*if (action.equals("edit")) {
                    removeAttachmentDataOnServer(SessionId, lastid);
                }*/
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                LinearLayout linLayout = (LinearLayout) folderlistLayout.findViewWithTag(lastid);

                folderlistLayout.removeView(linLayout);
                item_count--;
                if (item_count <= 0) {
                    folderLayout.setVisibility(View.GONE);
                }
                rufdbh.deleteSingleData(lastid);
                btnTempDone.setOnClickListener(null);
                doneContainer.setVisibility(View.GONE);

            }
        });

        btnTempDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String notesub = edt.getText().toString().trim();
                Common.DisplayLog("Done", notesub + "");
                if (!notesub.equals("")) {


                    rufdbh.updataData(lastid, notesub.toString());



                    edt.setCursorVisible(false);

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
//                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                    final TextView texv = new TextView(getApplicationContext()); // Pass it an Activity or Context
                    texv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)); // Pass two args; must be LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, or an integer pixel value.
                    texv.setGravity(Gravity.CENTER | Gravity.LEFT);
                    texv.setBackgroundResource(0);
                    final float scale = getResources().getDisplayMetrics().density;
                    int lr = (int) (26 * scale + 0.5f);
                    int tb = (int) (10 * scale + 0.5f);
                    texv.setPadding(lr, tb, lr, tb);
                    texv.setTextColor(Color.parseColor("#adadad"));
                    texv.setHintTextColor(Color.parseColor("#adadad"));
                    texv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                    editcontanor.removeAllViews();
                    editcontanor.addView(texv);

                    texv.setText(notesub);


                    /*create edit text*/
                    tasksEdit(texv, editcontanor, te, td, lastid, attfor, true);

                    doneContainer.setVisibility(View.GONE);


                    if(addfolder) {
                        AddFolderDataOnServer(lastid);
                    }else{
                        EditFolderDataOnServer(lastid);
                    }
                    animationForEdit(te, td);
                    taskDone = true;
                    btnTempDone.setOnClickListener(null);
                } else {
                    Common.ShowToast(getApplicationContext(), "Please enter some words.");
                }
            }
        });

    }

    public void loadSavedData() {

        //load Subtask
        folderlistLayout.removeAllViews();
        ArrayList<ReminderUsersBoxs> lodfolder = rufdbh.getAllData(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));

        if (lodfolder.size() > 0) {
            folderLayout.setVisibility(View.VISIBLE);
            addLayout.setVisibility(View.VISIBLE);
            for (int i = 0; i < lodfolder.size(); i++) {
                if (taskDone) {
                    btnTempDone.performClick();
                    folderlistLayout.addView(createEditView(lodfolder.get(i).getvBoxType(), lodfolder.get(i).getiId()));
//                    taskDone = false;
                }
            }

        }


    }


    public void GetFolderDataOnServer() {


        /***  Save main AddFolder ****/

        if (Common.hasInternetConnection(getApplicationContext())) {
            if (pd != null) {
                pd.dismiss();
            }
            pd = new ProgressDialog(MoveToFolderActivity.this);
            pd.setMessage("Loading...");
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);



            {
                Call<UsersBoxs> calladdBox = apiService.getBoxs(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));
                calladdBox.enqueue(new Callback<UsersBoxs>() {
                    @Override
                    public void onResponse(Call<UsersBoxs> call, Response<UsersBoxs> response) {
                        int statusCode = response.code();
                        Common.DisplayLog("response.body()", response.body().toString());
                        boolean status = response.body().getStatus();
                        Common.DisplayLog("status", status + "");
                        Common.DisplayLog("getMessage", response.body().getMessage() + "");
                        if (status) {

                            Data data = response.body().getData();

                            rufdbh.resetTable();
                            /*** GetUsersboxsList  ***/
                            List<GetUsersBoxsList> GetUsersBoxsList = data.getGetUsersBoxsList();
                            for (int c = 0; c < GetUsersBoxsList.size(); c++) {
                                GetUsersBoxsList GetUsersBoxsListData = GetUsersBoxsList.get(c);
                                String[] crcodbhCOLUMN = {
                                        GetUsersBoxsListData.getIId(),
                                        GetUsersBoxsListData.getIUserId(),
                                        GetUsersBoxsListData.getVBoxName(),
                                        GetUsersBoxsListData.getVBoxType(),
                                        GetUsersBoxsListData.getDCreatedDateTime()
                                };
                                rufdbh.addData(crcodbhCOLUMN);
                            }
                            loadSavedData();

//                        Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                        } else {
                            Common.ShowToast(getApplicationContext(), response.body().getMessage());

                        }
                        addfolder=true;
                    pd.dismiss();

                    }

                    @Override
                    public void onFailure(Call<UsersBoxs> call, Throwable t) {
                        Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
                    pd.dismiss();
                    }


                });
            }
        }
    }

    public void AddFolderDataOnServer(String id) {


        /***  Save main AddFolder ****/

        if (Common.hasInternetConnection(getApplicationContext())) {
            if (pd != null) {
                pd.dismiss();
            }
            pd = new ProgressDialog(MoveToFolderActivity.this);
            pd.setMessage("Loading...");
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            ReminderUsersBoxs tsdwd = rufdbh.getTableSingleDateWihtDao(id);

            if (tsdwd != null) {
                Call<Reminder> calladdBox = apiService.addBox(tsdwd.getiUserId(), tsdwd.getvBoxName(), tsdwd.getvBoxType());
                calladdBox.enqueue(new Callback<Reminder>() {
                    @Override
                    public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                        int statusCode = response.code();
                        Common.DisplayLog("response.body()", response.body().toString());
                        boolean status = response.body().getStatus();
                        Common.DisplayLog("status", status + "");
                        Common.DisplayLog("getMessage", response.body().getMessage() + "");
                        if (status) {
                            GetFolderDataOnServer();
//                        Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                        } else {
                            Common.ShowToast(getApplicationContext(), response.body().getMessage());

                        }
                    pd.dismiss();

                    }

                    @Override
                    public void onFailure(Call<Reminder> call, Throwable t) {
                        Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
                    pd.dismiss();
                    }


                });
            }
        }
    }

    public void EditFolderDataOnServer(String id) {


        /***  Save main AddFolder ****/

        if (Common.hasInternetConnection(getApplicationContext())) {
            if (pd != null) {
                pd.dismiss();
            }
            pd = new ProgressDialog(MoveToFolderActivity.this);
            pd.setMessage("Loading...");
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            ReminderUsersBoxs tsdwd = rufdbh.getTableSingleDateWihtDao(id);

            if (tsdwd != null) {
                Call<Reminder> calladdBox = apiService.editBox(tsdwd.getiId(),tsdwd.getiUserId(), tsdwd.getvBoxName(), tsdwd.getvBoxType());
                calladdBox.enqueue(new Callback<Reminder>() {
                    @Override
                    public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                        int statusCode = response.code();
                        Common.DisplayLog("response.body()", response.body().toString());
                        boolean status = response.body().getStatus();
                        Common.DisplayLog("status", status + "");
                        Common.DisplayLog("getMessage", response.body().getMessage() + "");
                        if (status) {
                            GetFolderDataOnServer();
//                        Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                        } else {
                            Common.ShowToast(getApplicationContext(), response.body().getMessage());

                        }
                    pd.dismiss();

                    }

                    @Override
                    public void onFailure(Call<Reminder> call, Throwable t) {
                        Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
                    pd.dismiss();
                    }


                });
            }
        }
    }

    public void removeFolderDataOnServer(String id) {


        /***  Save main AddFolder ****/

        if (Common.hasInternetConnection(getApplicationContext())) {
            if (pd != null) {
                pd.dismiss();
            }
            pd = new ProgressDialog(MoveToFolderActivity.this);
            pd.setMessage("Loading...");
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            ReminderUsersBoxs tsdwd = rufdbh.getTableSingleDateWihtDao(id);

            if (tsdwd != null) {
                Call<Reminder> calladdBox = apiService.removeBox(tsdwd.getiUserId(), tsdwd.getvBoxName());
                calladdBox.enqueue(new Callback<Reminder>() {
                    @Override
                    public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                        int statusCode = response.code();
                        Common.DisplayLog("response.body()", response.body().toString());
                        boolean status = response.body().getStatus();
                        Common.DisplayLog("status", status + "");
                        Common.DisplayLog("getMessage", response.body().getMessage() + "");
                        if (status) {
                            GetFolderDataOnServer();
//                        Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                        } else {
                            Common.ShowToast(getApplicationContext(), response.body().getMessage());

                        }
                    pd.dismiss();

                    }

                    @Override
                    public void onFailure(Call<Reminder> call, Throwable t) {
                        Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
                    pd.dismiss();
                    }


                });
            }
        }
    }

    public void AddFolderFilterDataOnServer(String id) {


        /***  Save main AddFolder ****/

        if (Common.hasInternetConnection(getApplicationContext())) {
            if (pd != null) {
                pd.dismiss();
            }
            pd = new ProgressDialog(MoveToFolderActivity.this);
            pd.setMessage("Loading...");
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


            RFolderFilterDbHandler rffdbh = new RFolderFilterDbHandler(getApplicationContext());

            String[] COLUMN = {
                    "",
                    Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                    SessionId,
                    id,
                    Common.getSQLDateTime()
            };
            String selId = String.valueOf(rffdbh.addData(COLUMN));

            RFolderFilter tsdwd = rffdbh.getTableSingleDateWihtDao(selId);

//            if (tsdwd != null)
            {
                Call<Reminder> calladdBox = apiService.addRFolderFilter(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), SessionId, id);
                calladdBox.enqueue(new Callback<Reminder>() {
                    @Override
                    public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                        int statusCode = response.code();
                        Common.DisplayLog("response.body()", response.body().toString());
                        boolean status = response.body().getStatus();
                        Common.DisplayLog("status", status + "");
                        Common.DisplayLog("getMessage", response.body().getMessage() + "");
                        if (status) {
                            onBackPressed();
//                        Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                        } else {
                            Common.ShowToast(getApplicationContext(), response.body().getMessage());

                        }
                        pd.dismiss();

                    }

                    @Override
                    public void onFailure(Call<Reminder> call, Throwable t) {
                        Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
                        pd.dismiss();
                    }


                });
            }
        }
    }

}
