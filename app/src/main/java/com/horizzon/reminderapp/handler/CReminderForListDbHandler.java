package com.horizzon.reminderapp.handler;

/**
 * Created by Abhijit Sojitra on 9/3/17.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.horizzon.reminderapp.dao.ReminderForList;
import com.horizzon.reminderapp.data.Common;

public class CReminderForListDbHandler {


    // table name
    public static final String TABLE_NAME = "cr_reminderforlist";

    public static final String[] COLUMNS = {
            "iId",
            "iUserId",
            "vSesssionId",
            "vViewed",
            "vCreatedBy",
            "vUpdatedBy",
            "tUpdateNote",
            "vReminderStatus"
    };

    DatabaseHandler dbh;
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
            + "("
            + COLUMNS[0] + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMNS[1] + " TEXT,"
            + COLUMNS[2] + " TEXT,"
            + COLUMNS[3] + " TEXT,"
            + COLUMNS[4] + " TEXT,"
            + COLUMNS[5] + " TEXT,"
            + COLUMNS[6] + " TEXT,"
            + COLUMNS[7] + " TEXT"
            + ");";

    public CReminderForListDbHandler(Context context) {
        dbh = new DatabaseHandler(context);
    }


    public long addData(String[] c) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        for (int j = 1; j < COLUMNS.length; j++) {
            Common.DisplayLog("columnNames", COLUMNS[j] + " => " + c[j]);
            values.put(COLUMNS[j], c[j]);
        }

        long id = db.insert(TABLE_NAME, null, values);
        db.close();
        return id;
    }


    public void updataReminderStatus(String userid,String sesid, String status) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[7], status);

        db.update(TABLE_NAME, values, COLUMNS[1] + "=? AND " + COLUMNS[2] + "=?", new String[]{userid,sesid});

    }

    public ReminderForList getTableSingleDateWihtDao(String sesid, String userid) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ReminderForList ReminderForList = null;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "='" + userid + "' AND " + COLUMNS[2] + "='" + sesid + "' ORDER BY " + COLUMNS[0] + " ASC";
        Common.DisplayLog("query_select", query_select);
        Cursor cursor = db.rawQuery(query_select, null);

        Common.DisplayLog("cursor.getCount()", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            Common.DisplayLog("cursor", "yes");
            String[] columnNames = cursor.getColumnNames();
            do {
                ReminderForList = new ReminderForList(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7));
                for (int j = 0; j < columnNames.length; j++) {
                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
                }
            } while (cursor.moveToNext());
        }
        cursor.close();

        return ReminderForList;
    }

    public void resetTempdata(String sessid) {
        SQLiteDatabase db = dbh.getWritableDatabase();
        db.execSQL("delete from " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = '" + sessid + "'");
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }


    public void resetTable() {
        SQLiteDatabase db = dbh.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.execSQL("delete from " + TABLE_NAME);
        db.execSQL("delete from sqlite_sequence where name='" + TABLE_NAME + "'");
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }

    public int getDataCount(String sessid) {
        SQLiteDatabase db = dbh.getReadableDatabase();
        String statusstr = "";

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = '" + sessid + "' " + statusstr;

        Cursor cursor = db.rawQuery(query_select, null);
        int totcount = cursor.getCount();
        cursor.close();

        return totcount;
    }
}
