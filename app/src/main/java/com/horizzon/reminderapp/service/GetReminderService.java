package com.horizzon.reminderapp.service;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.ReminderDetailActivity;
import com.horizzon.reminderapp.dao.CReminder;
import com.horizzon.reminderapp.dao.ReminderContactUser;
import com.horizzon.reminderapp.dao.ReminderDateTime;
import com.horizzon.reminderapp.dao.ReminderForList;
import com.horizzon.reminderapp.dao.SessionIDModel;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.CRAttachmentDbHandler;
import com.horizzon.reminderapp.handler.CRCommentDbHandler;
import com.horizzon.reminderapp.handler.CRContactDbHandler;
import com.horizzon.reminderapp.handler.CRDateTimeDbHandler;
import com.horizzon.reminderapp.handler.CRLocationDbHandler;
import com.horizzon.reminderapp.handler.CRMakeAdminDbHandler;
import com.horizzon.reminderapp.handler.CRSessoinIdDbHandler;
import com.horizzon.reminderapp.handler.CReminderForListDbHandler;
import com.horizzon.reminderapp.handler.CreateReminderDbHandler;
import com.horizzon.reminderapp.handler.RUserAddressDbHandler;
import com.horizzon.reminderapp.handler.RUserFolderDbHandler;
import com.horizzon.reminderapp.receiver.AlarmBroadCustReciver;
import com.horizzon.reminderapp.retrofit.model.GetAllAddress;
import com.horizzon.reminderapp.retrofit.model.SessionIdList;
import com.horizzon.reminderapp.retrofit.model.getreminder.Data;
import com.horizzon.reminderapp.retrofit.model.getreminder.GetAttachmentList;
import com.horizzon.reminderapp.retrofit.model.getreminder.GetCommentList;
import com.horizzon.reminderapp.retrofit.model.getreminder.GetContactList;
import com.horizzon.reminderapp.retrofit.model.getreminder.GetDateTimeList;
import com.horizzon.reminderapp.retrofit.model.getreminder.GetLocationList;
import com.horizzon.reminderapp.retrofit.model.getreminder.GetMakeadminList;
import com.horizzon.reminderapp.retrofit.model.getreminder.GetReminder;
import com.horizzon.reminderapp.retrofit.model.getreminder.GetReminderList;
import com.horizzon.reminderapp.retrofit.model.getreminder.ReminderList;
import com.horizzon.reminderapp.retrofit.model.getreminderforlist.GetReminderForList;
import com.horizzon.reminderapp.retrofit.model.getreminderforlist.GetReminderForListList;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;
import com.horizzon.reminderapp.utility.DateUtilitys;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetReminderService extends Service {
    String createdDate;
    int mMonth;
    String Id, sessionId;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    String dateS, Times, part1, part2, part3, time1, time2, time3;
    List<SessionIDModel> sessionIDList = new ArrayList<>();
    List<SessionIdList.Datum> dataList = new ArrayList<>();

    public GetReminderService() {
    }

    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Common.DisplayLog("", "onStartCommand of Services GetReminderService");
        sharedpreferences = getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        getData();
        return super.onStartCommand(intent, flags, startId);
    }


    private void getData() {

        if (Common.hasInternetConnection(getApplicationContext(), false)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<ReminderList> call = apiService.getListOfReminder(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));
            call.enqueue(new Callback<ReminderList>() {
                @Override
                public void onResponse(Call<ReminderList> call, Response<ReminderList> response) {
                    boolean status = response.body().getStatus();
                    if (status) {
                        Data data = response.body().getData();
                        int totalRecords = data.getTotalRecords();
                        Common.DisplayLog("totalRecords", totalRecords + "");

                        if (totalRecords > 0) {

                            /*** Remove Data ***/
                            CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getApplicationContext());
                            crdbh.resetTable();

                            CRContactDbHandler crcdbh = new CRContactDbHandler(getApplicationContext());
                            crcdbh.resetTable();

                            CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(getApplicationContext());
                            crdtdbh.resetTable();

                            CRLocationDbHandler crldbh = new CRLocationDbHandler(getApplicationContext());
                            crldbh.resetTable();

                            CRCommentDbHandler crcodbh = new CRCommentDbHandler(getApplicationContext());
                            crcodbh.resetTable();

                            CRAttachmentDbHandler cradbh = new CRAttachmentDbHandler(getApplicationContext());
                            cradbh.resetTable();

                            CRMakeAdminDbHandler crmadbh = new CRMakeAdminDbHandler(getApplicationContext());
                            crmadbh.resetTable();

                            CRSessoinIdDbHandler crSID = new CRSessoinIdDbHandler(getApplicationContext());
                            crSID.resetTable();

                            final List<GetReminderList> getReminderList = data.getGetReminderList();
                            for (int i = 0; i < getReminderList.size(); i++) {
                                GetReminderList getReminderListDate = getReminderList.get(i);
//                                rcdbh.updataForAction(getReminderList.get(i).getVUNumber());


                                /*** GetContact  ***/
                                List<GetContactList> getContactList = getReminderListDate.getGetContactList();
                                for (int c = 0; c < getContactList.size(); c++) {
                                    GetContactList getContactListData = getContactList.get(c);
                                    String[] crcdbhCOLUMN = {
                                            getContactListData.getIId(),
                                            getContactListData.getVSesssionId(),
                                            getContactListData.getIUserId(),
                                            getContactListData.getVName(),
                                            getContactListData.getVNumber(),
                                            getContactListData.getVPhoto(),
                                            getContactListData.getVForaction(),
                                            getContactListData.getVConsel()
                                    };
                                    crcdbh.addData(crcdbhCOLUMN);
                                }

                                /*** GetDateTime ***/
                                if (getReminderListDate.getGetDateTimeList() != null && !getReminderListDate.getGetDateTimeList().equals("")) {
                                    GetDateTimeList getDateTimeList = getReminderListDate.getGetDateTimeList();
                                    createdDate = getDateTimeList.getVStartDate();
                                    String[] crdtdbhCOLUMN = {
                                            getDateTimeList.getIId(),
                                            getDateTimeList.getVSesssionId(),
                                            getDateTimeList.getIUserId(),
                                            getDateTimeList.getVDtFor(),
                                            getDateTimeList.getVStartDate(),
                                            getDateTimeList.getVStartTime(),
                                            getDateTimeList.getVEndDate(),
                                            getDateTimeList.getVEndTime(),
                                            getDateTimeList.getTDays(),
                                            getDateTimeList.getIStatus()
                                    };
                                    crdtdbh.addData(crdtdbhCOLUMN);
                                    SessionIDModel sessionIDModel = new SessionIDModel(getDateTimeList.getIId(), getDateTimeList.getVSesssionId());
                                    sessionIDModel.setId(getDateTimeList.getIId());
                                    sessionIDModel.setSesssionId(getDateTimeList.getVSesssionId());
                                    sessionIDList.add(sessionIDModel);
                                    //dataonNotifaction(sessionIDList);
                                    // dataonNotifaction(getDateTimeList.getVSesssionId());
                                    //setList(sessionIDList);
                                }

                                /*** GetReminder  ***/
                                GetReminder getReminder = getReminderListDate.getGetReminder();
                                String satus = getReminder.getIStatus();
                                sessionId = getReminder.getVSesssionId();
                                setBrodCastForReminder(getReminder.getVSesssionId());

                                Id = getReminder.getIId();
                                String[] COLUMN = {
                                        getReminder.getIId(),
                                        getReminder.getVSesssionId(),
                                        getReminder.getIUserId(),
                                        getReminder.getTSubject(),
                                        getReminder.getIPriority(),
                                        getReminder.getVReminderFor(),
                                        getReminder.getIStatus(),
                                        getReminder.getVLockStatus(),
                                        getReminder.getTExtra(),
                                        createdDate,
                                        getReminder.getvUserFolder()
                                };
                                /*
                                 * When folder is not created that time data show either not show
                                 * */
                                crdbh.addData(COLUMN);


                                /*** GetLocation ***/
                                if (getReminderListDate.getGetLocationList() != null && !getReminderListDate.getGetLocationList().equals("")) {
                                    GetLocationList getLocationList = getReminderListDate.getGetLocationList();
                                    String[] crldbhCOLUMN = {
                                            getLocationList.getIId(),
                                            getLocationList.getVSesssionId(),
                                            getLocationList.getIUserId(),
                                            getLocationList.getVLocationFor(),
                                            getLocationList.getTLocationDetail(),
                                            getLocationList.getVLat(),
                                            getLocationList.getVLon(),
                                            getLocationList.getVAreaFor(),
                                            getLocationList.getVAreaSize(),
                                            getLocationList.getVAreaUnit(),
                                            getLocationList.getIStatus()
                                    };
                                    crldbh.addData(crldbhCOLUMN);
                                }

                                /*** GetComment  ***/
                                List<GetCommentList> getCommentList = getReminderListDate.getGetCommentList();
                                for (int c = 0; c < getCommentList.size(); c++) {
                                    GetCommentList getCommentListData = getCommentList.get(c);
                                    String[] crcodbhCOLUMN = {
                                            getCommentListData.getIId(),
                                            getCommentListData.getVSesssionId(),
                                            getCommentListData.getIUserId(),
                                            getCommentListData.getTComment(),
                                            getCommentListData.gettCommentFor(),
                                            getCommentListData.getDCreatedDateTime()
                                    };
                                    crcodbh.addData(crcodbhCOLUMN);
                                }

                                /*** GetAttachment  ***/
                                List<GetAttachmentList> getAttachmentList = getReminderListDate.getGetAttachmentList();
                                for (int c = 0; c < getAttachmentList.size(); c++) {
                                    GetAttachmentList getAttachmentListData = getAttachmentList.get(c);

                                    String s = getAttachmentListData.getTDetail();
                                    if (getAttachmentListData.getVAttachFor().equals("image")) {
                                        s = getAttachmentListData.getVImage();
                                    }

                                    String[] cradbhCOLUMN = {
                                            getAttachmentListData.getIId(),
                                            getAttachmentListData.getVSesssionId(),
                                            getAttachmentListData.getIUserId(),
                                            getAttachmentListData.getVAttachFor(),
                                            s,
                                            getAttachmentListData.getIStatus(),
                                            getAttachmentListData.getDCreatedDateTime()
                                    };
                                    cradbh.addPreData(cradbhCOLUMN);
                                }

                                /*** GetMakeadminList  ***/
                                List<GetMakeadminList> getMakeadminList = getReminderListDate.getGetMakeadminList();
                                for (int c = 0; c < getMakeadminList.size(); c++) {
                                    GetMakeadminList getMakeadminListData = getMakeadminList.get(c);
                                    String[] crmadbhCOLUMN = {
                                            getMakeadminListData.getIId(),
                                            getMakeadminListData.getVSesssionId(),
                                            getMakeadminListData.getVUserId(),
                                            getMakeadminListData.getTExtra()
                                    };
                                    crmadbh.addData(crmadbhCOLUMN);
                                }
                            }
                            Log.d("TAG", "onResponse: complete get reminder :::");

                        } else {
                            Log.d("TAG", "onResponse: NO data found.");
                            // Toast.makeText(GetReminderService.this, "NO data found.", Toast.LENGTH_SHORT).show();
                        }
                        RUserFolderDbHandler rufdbh = new RUserFolderDbHandler(getApplicationContext());
                        rufdbh.resetTable();

                        /*** GetUsersboxsList  ***/
                      /*  List<GetUsersboxsList> getUsersboxsList = data.getGetUsersboxsList();
                        for (int c = 0; c < getUsersboxsList.size(); c++) {
                            GetUsersboxsList getUsersboxsListData = getUsersboxsList.get(c);
                            String[] crcodbhCOLUMN = {
                                    getUsersboxsListData.getIId(),
                                    getUsersboxsListData.getIUserId(),
                                    getUsersboxsListData.getVBoxName(),
                                    getUsersboxsListData.getVBoxType(),
                                    getUsersboxsListData.getDCreatedDateTime()
                            };
                            rufdbh.addData(crcodbhCOLUMN);
                        }*/


                        Intent intent = new Intent("custom-event-name");
                        // You can also include some extra data.
                        intent.putExtra("message", "This is my message!");
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                    } else {
                        Common.DisplayLog("response.body().getMessage()", response.body().getMessage() + "");
                    }

                }

                @Override
                public void onFailure(Call<ReminderList> call, Throwable t) {
                    Common.DisplayLog("fail", t.getMessage() + "");
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
                }
            });

            Call<GetReminderForList> call1 = apiService.getReminderForList(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));
            call1.enqueue(new Callback<GetReminderForList>() {
                @Override
                public void onResponse(Call<GetReminderForList> call, Response<GetReminderForList> response) {
                    boolean status = response.body().getStatus();

                    if (status) {
                        com.horizzon.reminderapp.retrofit.model.getreminderforlist.Data data = response.body().getData();
                        int totalRecords = data.getTotalRecords();

                        if (totalRecords > 0) {
                            /*** Remove Data ***/

                            CReminderForListDbHandler crfldbh = new CReminderForListDbHandler(getApplicationContext());
                            crfldbh.resetTable();


                            final List<GetReminderForListList> getReminderForListList = data.getGetReminderForListList();
                            for (int i = 0; i < getReminderForListList.size(); i++) {

                                /*** GetReminderForListList  ***/
                                GetReminderForListList GetReminderForListListDate = getReminderForListList.get(i);

                                String[] COLUMN = {
                                        GetReminderForListListDate.getIId(),
                                        GetReminderForListListDate.getIUserId(),
                                        GetReminderForListListDate.getVSesssionId(),
                                        GetReminderForListListDate.getVViewed(),
                                        GetReminderForListListDate.getVCreatedBy(),
                                        GetReminderForListListDate.getVUpdatedBy(),
                                        GetReminderForListListDate.gettUpdateNote(),
                                        GetReminderForListListDate.getvReminderStatus()
                                };
                                crfldbh.addData(COLUMN);

                            }
                        } else {

                            Log.d("TAG", "onResponse: ");
                        }

                    } else {
                        Common.DisplayLog("response.body().getMessage()", response.body().getMessage() + "");
                    }

                }

                @Override
                public void onFailure(Call<GetReminderForList> call, Throwable t) {
                    Common.DisplayLog("fail", t.getMessage() + "");
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
                }
            });


            Call<GetAllAddress> call2 = apiService.getForListAddress(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));
            call2.enqueue(new Callback<GetAllAddress>() {
                @Override
                public void onResponse(Call<GetAllAddress> call, Response<GetAllAddress> response) {
                    RUserAddressDbHandler ruadbh = new RUserAddressDbHandler(getApplicationContext());
                    ruadbh.deleteDataWithType(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), "home");
                    ruadbh.deleteDataWithType(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), "office");
                    ruadbh.deleteDataWithType(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), "favourite");
                    ruadbh.resetTable();

                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            GetAllAddress getAllAddress = response.body();

                            if (getAllAddress.getData() != null) {

                                if (getAllAddress.getData().getFavourite() != null) {
                                    List<GetAllAddress.Favourite> favourite = getAllAddress.getData().getFavourite();
                                    if (favourite.size() > 0) {
                                        for (int i = 0; i < getAllAddress.getData().getFavourite().size(); i++) {
                                            String[] COLUMN = new String[]{
                                                    "",
                                                    Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                                                    getAllAddress.getData().getFavourite().get(i).getVAddressFor(),
                                                    getAllAddress.getData().getFavourite().get(i).getTAddress(),
                                                    getAllAddress.getData().getFavourite().get(i).getId()
                                            };
                                            ruadbh.addData(COLUMN);
                                        }
                                    }
                                }


                                if (getAllAddress.getData().getHome() != null) {

                                    List<GetAllAddress.Home> homes = getAllAddress.getData().getHome();
                                    if (homes.size() > 0) {
                                        for (int i = 0; i < getAllAddress.getData().getHome().size(); i++) {
                                            String[] COLUMN = new String[]{
                                                    "",
                                                    Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                                                    getAllAddress.getData().getHome().get(i).getVAddressFor(),
                                                    getAllAddress.getData().getHome().get(i).getTAddress(),
                                                    getAllAddress.getData().getHome().get(i).getId()
                                            };
                                            ruadbh.addData(COLUMN);
                                        }
                                    }

                                }
                                if (getAllAddress.getData().getOffice() != null) {
                                    List<GetAllAddress.Office> offices = getAllAddress.getData().getOffice();
                                    if (offices.size() > 0) {
                                        for (int i = 0; i < getAllAddress.getData().getOffice().size(); i++) {
                                            String[] COLUMN = new String[]{
                                                    "",
                                                    Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                                                    getAllAddress.getData().getOffice().get(i).getVAddressFor(),
                                                    getAllAddress.getData().getOffice().get(i).getTAddress(),
                                                    getAllAddress.getData().getOffice().get(i).getId()
                                            };
                                            ruadbh.addData(COLUMN);
                                        }
                                    }

                                }
                            } else {

                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<GetAllAddress> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.getMessage());
                }
            });


            /*
             * Set data in alarm manager as per sessionId wise and save in  sqlite database
             * */
            Call<SessionIdList> call3 = apiService.getSessionIdList(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));
            call3.enqueue(new Callback<SessionIdList>() {
                @Override
                public void onResponse(Call<SessionIdList> call, Response<SessionIdList> response) {

                    if (response.body() != null) {
                        SessionIdList sessionIdList = response.body();
                        if (sessionIdList.getData().size() > 0) {
                            dataList.addAll(sessionIdList.getData());
                            dataonNotifaction(dataList);
                        }else {
                            Log.d("TAG", "onResponse: no data ");
                        }
                    }
                }

                @Override
                public void onFailure(Call<SessionIdList> call, Throwable t) {
                    Log.d("TAG", "onFailure: msg :: " + t.getMessage());
                }
            });
        }
    }

   /* public void dataonNotifaction(String sessionIDModels) {

        //   for (int i = 0; i < sessionIDModels.size(); i++) {
        //   Log.d("TAG", "dataonNotifaction: session : " + sessionIDModels.get(i).getSesssionId());
        //   Log.d("TAG", "dataonNotifaction: session id:: " + sessionIDModels.get(i).getId());

        *//*CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getContext());
        CReminder cReminder = crdbh.getTableSingleDateWihtDao(sessionIDModels);*//*

        CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(getApplicationContext());
        ReminderDateTime rdt = crdtdbh.getTableSingleDateWihtDao(sessionIDModels);

        //if (crdtdbh.getDataCount(sessionIDModels.get(i).getSesssionId()) > 0) {
        // date = dateUtl.FormetDate(rdt.getStartDate() + " " + rdt.getStartTime());

        *//*
     * If time is less then current time that time not consider that data
     * *//*
        dateS = rdt.getStartDate();
        Times = rdt.getStartTime();
        Log.d("TAG", "dataonNotifaction:  date :" + dateS);
        Log.d("TAG", "dataonNotifaction:  Times :" + Times);
       *//* Date currentTime = Calendar.getInstance().getTime();
        String currentDateTimeString = java.text.DateFormat.getDateTimeInstance().format(new Date());
        Log.d("TAG", "dataonNotifaction: currrent date nad time : " + currentTime);
        Log.d("TAG", "dataonNotifaction: currentDateTimeString : " + currentDateTimeString);*//*
        String[] parts = dateS.split("-");
        part1 = parts[0];
        part2 = parts[1];
        part3 = parts[2];

        String[] time = Times.split(":");
        time1 = time[0];
        time2 = time[1];
        time3 = time[2];
        //}
        assert part1 != null;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, Integer.parseInt(part2)-1);
        cal.set(Calendar.YEAR, Integer.parseInt(part1));
        cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(part3));
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time1));
        cal.set(Calendar.MINUTE, Integer.parseInt(time2));
        cal.set(Calendar.SECOND, 0);
        Intent intent = new Intent(getContext(), AlarmBroadCustReciver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                getContext(), 9876, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(ALARM_SERVICE);
        // alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);

        long timesss = System.currentTimeMillis() -cal.getTimeInMillis();
        Log.d("TAG", "dataonNotifaction: timesss :: "+ timesss);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
        }

        // Set multiple alarm in set data
      *//*  AlarmManager mgrAlarm = (AlarmManager) getContext().getSystemService(ALARM_SERVICE);
        ArrayList<PendingIntent> intentArray = new ArrayList<PendingIntent>();

        for( int i = 0; i < 10; ++i)
        {
            Intent intent = new Intent(getContext(), AlarmBroadCustReciver.class);
            // Loop counter `i` is used as a `requestCode`
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), i, intent, 0);
            // Single alarms in 1, 2, ..., 10 minutes (in `i` minutes)
            mgrAlarm.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime() + 60000 * i,
                    pendingIntent);

            intentArray.add(pendingIntent);
        }*//*

     *//* Calendar c = Calendar.getInstance();
            if (cal.compareTo(c) <= 0) {
                // Today Set time passed, count to tomorrow
                cal.add(Calendar.DATE, 1);
            }*//*
        //  new AlarmBroadCustReciver().setAlarm(getContext(), cal);
        // setAlarm(getContext(), cal);
        //}
    }*/

    /* public <T> void setList(List<T> list) {
         Gson gson = new Gson();
         String json = gson.toJson(list);
         dataonNotifaction(sessionIDList);
         Log.d("TAG", "setList: sessionIDList ::" + sessionIDList);
         Log.d("TAG", "setList: list ::" + list);
         set("SessionId", json);
     }

     public void set(String key, String value) {
         editor.putString(key, value);
         editor.commit();
     }

     */
    public void setBrodCastForReminder(String SessId) {


        Common.DisplayLog("", "loadSavedData " + SessId);
        CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getApplicationContext());
        //RContactDbHandler rcdbh = new RContactDbHandler(getApplicationContext());
        //CReminder cReminder = crdbh.getTableSingleDateWihtDao(SessId);
        //  Common.DisplayLog("cReminder.getSubject()", cReminder.getSubject() + "");
        // set Subject
        // set Contact
        CRContactDbHandler crcdbh = new CRContactDbHandler(getApplicationContext());
        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllCurrentSessContactData(SessId, "contact");
        //ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllContactData();

        // set Attachment
        CRAttachmentDbHandler cradbh = new CRAttachmentDbHandler(getApplicationContext());

        // set Comment
        CRCommentDbHandler crcodbh = new CRCommentDbHandler(getApplicationContext());

        // set Date Time
        CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(getApplicationContext());
        ReminderDateTime rdt = crdtdbh.getTableSingleDateWihtDao(SessId);

        DateUtilitys dateUtl = new DateUtilitys();
        Date date;
        if (crdtdbh.getDataCount(SessId) > 0) {
            date = dateUtl.FormetDate(rdt.getStartDate() + " " + rdt.getStartTime());

            dateUtl.setforDisplayTitle(date);
            CReminderForListDbHandler crfldbh = new CReminderForListDbHandler(getApplicationContext());
            ReminderForList rfl = crfldbh.getTableSingleDateWihtDao(SessId, Common.getSharedPreferences(getApplicationContext(), "userId", "0"));
            if (rfl != null) {
                if (rfl.getvReminderStatus().equals("accept")) {
                    /***** SET BROADCAST FOR REMINDER ******/
                    Calendar cal = Calendar.getInstance();
                    cal.set(Calendar.MONTH, 12);
                    cal.set(Calendar.YEAR, Integer.parseInt(dateUtl.getYear()));
                    cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateUtl.getDay()));
                    cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(dateUtl.getHour()));
                    cal.set(Calendar.MINUTE, Integer.parseInt(dateUtl.getMin()));

                    Common.DisplayLog("timeStamp", "Month: " + dateUtl.getMonth()
                            + " Year: " + dateUtl.getYear()
                            + " Day: " + dateUtl.getDay()
                            + " Hour: " + dateUtl.getHour()
                            + " Year: " + dateUtl.getYear());

                    String timeStamp = new SimpleDateFormat("ddMMHHmmss").format(new Date());
                    Common.DisplayLog("timeStamp", ": " + timeStamp);
                    //int requestCode = Integer.parseInt(timeStamp);

                    Intent myIntent1 = new Intent(getApplicationContext(), AlarmBroadCustReciver.class);
                    myIntent1.putExtra("SesssionId", SessId);
                    // myIntent1.putExtra("Subject", cReminder.getSubject());
                    PendingIntent pendingIntent1 = PendingIntent.getBroadcast(getApplicationContext(),
                            9876, myIntent1,
                            PendingIntent.FLAG_UPDATE_CURRENT);

                    AlarmManager alarmManager1 = (AlarmManager) getSystemService(ALARM_SERVICE);
                    // alarmManager1.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent1);
                    if (Build.VERSION.SDK_INT < 19) {
                        alarmManager1.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent1);
                    } else {
                        alarmManager1.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent1);
                    }
                }
            }
        }

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        setBrodCastForReminder(sessionId);
        super.onTaskRemoved(rootIntent);
    }

    public void dataonNotifaction(List<SessionIdList.Datum> sessionIDModels) {
        //String sessionIDModels

        for (int i = 0; i < sessionIDModels.size(); i++) {
           /* CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(getApplicationContext());
            ReminderDateTime rdt = crdtdbh.getTableSingleDateWihtDao(sessionIDModels.get(i).getVSesssionId());*/

            if (sessionIDModels.get(i).getVStartDate() != null && sessionIDModels.get(i).getVStartTime() != null) {
                dateS = String.valueOf(sessionIDModels.get(i).getVStartDate());
                Times = String.valueOf(sessionIDModels.get(i).getVStartTime());
                String[] parts = dateS.split("-");
                part1 = parts[0];
                part2 = parts[1];
                part3 = parts[2];

                String[] time = Times.split(":");
                time1 = time[0];
                time2 = time[1];
                time3 = time[2];
                //}
                assert part1 != null;
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.MONTH, Integer.parseInt(part2) - 1);
                cal.set(Calendar.YEAR, Integer.parseInt(part1));
                cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(part3));
                cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time1));
                cal.set(Calendar.MINUTE, Integer.parseInt(time2));
                cal.set(Calendar.SECOND, 0);

                Intent intent = new Intent(getApplicationContext(), AlarmBroadCustReciver.class);
                intent.putExtra("SessionId", sessionIDModels.get(i).getVSesssionId());
                //intent.putExtra(AlarmBroadCustReciver.NOTIFICATION_ID, 1);
                //intent.putExtra(AlarmBroadCustReciver.NOTIFICATION, getNotification("30 second delay"));
                PendingIntent pendingIntent = PendingIntent.getBroadcast(
                        getApplicationContext(), 9876, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
                // alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);

                long timesss = System.currentTimeMillis() - cal.getTimeInMillis();
                Calendar cal1 = Calendar.getInstance();
                if (cal1.getTimeInMillis() < cal.getTimeInMillis()) {
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
                    } else {
                        alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
                    }
                } else {
                    Log.d("TAG", "dataonNotifaction: time is less than to current time.");
                }
            }
        }
        //   for (int i = 0; i < sessionIDModels.size(); i++) {
        //   Log.d("TAG", "dataonNotifaction: session : " + sessionIDModels.get(i).getSesssionId());
        //   Log.d("TAG", "dataonNotifaction: session id:: " + sessionIDModels.get(i).getId());

     /*   CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getApplicationContext());
        CReminder cReminder = crdbh.getTableSingleDateWihtDao(sessionIDModels);

        CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(getApplicationContext());
        ReminderDateTime rdt = crdtdbh.getTableSingleDateWihtDao(sessionIDModels);
        Log.d("TAG", "dataonNotifaction: Start date  : " + rdt.getStartDate());
        Log.d("TAG", "dataonNotifaction: time : " + rdt.getStartTime());
        //if (crdtdbh.getDataCount(sessionIDModels.get(i).getSesssionId()) > 0) {
        // date = dateUtl.FormetDate(rdt.getStartDate() + " " + rdt.getStartTime());

        *//*
         * If time is less then current time that time not consider that data
         * *//*
        dateS = rdt.getStartDate();
        Times = rdt.getStartTime();
       *//* Date currentTime = Calendar.getInstance().getTime();
        String currentDateTimeString = java.text.DateFormat.getDateTimeInstance().format(new Date());
        Log.d("TAG", "dataonNotifaction: currrent date nad time : " + currentTime);
        Log.d("TAG", "dataonNotifaction: currentDateTimeString : " + currentDateTimeString);*//*
        String[] parts = dateS.split("-");
        part1 = parts[0];
        part2 = parts[1];
        part3 = parts[2];

        String[] time = Times.split(":");
        time1 = time[0];
        time2 = time[1];
        time3 = time[2];
        //}
        assert part1 != null;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, Integer.parseInt(part2) - 1);
        cal.set(Calendar.YEAR, Integer.parseInt(part1));
        cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(part3));
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time1));
        cal.set(Calendar.MINUTE, Integer.parseInt(time2));
        cal.set(Calendar.SECOND, 0);
           *//* Calendar c = Calendar.getInstance();
            if (cal.compareTo(c) <= 0) {
                // Today Set time passed, count to tomorrow
                cal.add(Calendar.DATE, 1);
            }*//*
        // new AlarmBroadCustReciver().setAlarm(getApplicationContext(), cal);
        Intent intent = new Intent(getApplicationContext(), AlarmBroadCustReciver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                getApplicationContext(), 9876, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
        }*/
    }

    private Notification getNotification(String content) {
        Notification.Builder builder = new Notification.Builder(this);
        builder.setContentTitle("Scheduled Notification");
        builder.setContentText(content);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        return builder.build();
    }
}
