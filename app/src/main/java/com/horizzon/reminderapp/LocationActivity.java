package com.horizzon.reminderapp;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.horizzon.reminderapp.dao.ReminderLocation;
import com.horizzon.reminderapp.dao.ReminderUserAddress;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.CRLocationDbHandler;
import com.horizzon.reminderapp.handler.RUserAddressDbHandler;
import com.horizzon.reminderapp.handler.RUserDbHandler;
import com.horizzon.reminderapp.helper.LoopScrollListener;
import com.horizzon.reminderapp.helper.LoopView;
import com.horizzon.reminderapp.retrofit.model.reminder.Reminder;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationActivity extends AppCompatActivity implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<LocationSettingsResult>, OnMapReadyCallback {
    ImageView imgBack, imgHome, imgCancel, imgYes;
    Button onArrivbtn, onLeavingbtn, onNearbybtn;
    GradientDrawable gd;
    float[] arr = new float[8];
    float[] arr1 = new float[8];
    GoogleMap mMap;
    Marker current_marker;
    LinearLayout mapLayout, distLayout;
    public LoopView distLoopView1;
    public LoopView unitLoopView1;
    private int distPos = 0;
    private int unitPos = 0;
    public int back_flag = 1;
    TextView edtSearch;
    List<String> distList1 = new ArrayList();
    List<String> unitList1 = new ArrayList();
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest locationRequest;
    int REQUEST_CHECK_SETTINGS = 100;
    LocationManager mLocationManager;
    String provider, txtloc;
    Geocoder geocoder;
    List<Address> addresses;
    TextView txtLocation;
    Location mLastLocation;
    LinearLayout curLayout, homeLayout, offcLayout, favLayout;
    double latitude = 0;
    double longitude = 0;
    private String[] arrlocationFor = {"current", "home", "office", "favourite"};
    private String[] arrareaFor = {"onArriving", "onLeaving", "nearBy"};
    private String[] arrareaUnit = {"miles", "km"};
    String locationFor = arrlocationFor[0], locationDetail = "", areaFor = arrareaFor[0], areaSize = "0.5", areaUnit = arrareaUnit[0];

    private String SessionId, action;
    private CRLocationDbHandler crldbh;
    private RUserDbHandler rudbh;
    private RUserAddressDbHandler ruadbh;
    private ReminderUserAddress reminderUserAddress;
    int homedubclick = 1, officedubclick = 1;
    float zoomTo = 15;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }


        action = getIntent().getStringExtra("action");
        if (action == null || action.equals("")) {
            action = "";
        }


        SessionId = Common.getSharedPreferences(getApplicationContext(), "SessionId", "");
        crldbh = new CRLocationDbHandler(getApplicationContext());
        rudbh = new RUserDbHandler(getApplicationContext());
        ruadbh = new RUserAddressDbHandler(getApplicationContext());


        txtLocation = (TextView) findViewById(R.id.txtLocation);
        edtSearch = (TextView) findViewById(R.id.edtSearch);
        imgBack = (ImageView) findViewById(R.id.backBtn);
        imgHome = (ImageView) findViewById(R.id.homeBtn);
        imgYes = (ImageView) findViewById(R.id.imgYes);
        imgCancel = (ImageView) findViewById(R.id.imgCancel);
        onArrivbtn = (Button) findViewById(R.id.onArrivbtn);
        onLeavingbtn = (Button) findViewById(R.id.onLeavingbtn);
        onNearbybtn = (Button) findViewById(R.id.onNearbybtn);
        mapLayout = (LinearLayout) findViewById(R.id.mapLayout);
        distLayout = (LinearLayout) findViewById(R.id.distLayout);
        curLayout = (LinearLayout) findViewById(R.id.curLayout);
        homeLayout = (LinearLayout) findViewById(R.id.homeLayout);
        offcLayout = (LinearLayout) findViewById(R.id.offcLayout);
        favLayout = (LinearLayout) findViewById(R.id.favLayout);

        distLoopView1 = (LoopView) findViewById(R.id.picker_dist);
        unitLoopView1 = (LoopView) findViewById(R.id.picker_unit);

        arr[0] = Float.valueOf(20);
        arr[1] = Float.valueOf(20);
        arr[2] = Float.valueOf(0);
        arr[3] = Float.valueOf(0);
        arr[4] = Float.valueOf(0);
        arr[5] = Float.valueOf(0);
        arr[6] = Float.valueOf(20);
        arr[7] = Float.valueOf(20);

        arr1[0] = Float.valueOf(0);
        arr1[1] = Float.valueOf(0);
        arr1[2] = Float.valueOf(20);
        arr1[3] = Float.valueOf(20);
        arr1[4] = Float.valueOf(20);
        arr1[5] = Float.valueOf(20);
        arr1[6] = Float.valueOf(0);
        arr1[7] = Float.valueOf(0);

        initPickerViews1();

        setmap();

        curLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLeavingbtn.performClick();
                locationFor = arrlocationFor[0];
                setmap();
            }
        });

        homeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationFor = arrlocationFor[1];

                /*
                * get data from server and save in db then pass data in reminder Address.
                * */
                reminderUserAddress = ruadbh.getTableSingleTtpeDateWihtDao(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), "home", "0");
                Common.DisplayLog("con", (reminderUserAddress == null) + " && " + (homedubclick == 0));
                Common.DisplayLog("homedubclick", homedubclick + "");

                if (reminderUserAddress == null && homedubclick != 2) {
                    homedubclick = 0;
                }

                if (reminderUserAddress != null && homedubclick == 1) {
                    Common.DisplayLog("if", "if");
                    homedubclick = 0;
                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    homedubclick = 1;
                                }
                            },
                            1000);

                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    getLocationFormAdress(reminderUserAddress.getAddress(), reminderUserAddress.getAddressFor());
                                }
                            },
                            1500);

                } else if (homedubclick == 0) {
                    Common.DisplayLog("else if", "else if");
                    homeLayout.setEnabled(false);
                    Intent in = new Intent(LocationActivity.this, LocationAddressActivity.class);
                    in.putExtra("search_flag", "home");
                    startActivityForResult(in, 21);
                } else if (homedubclick == 2) {
                    Common.DisplayLog("else", "else");
                    curLayout.performClick();
                    homeLayout.setEnabled(true);
                    homedubclick = 1;
                }
            }
        });


        offcLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationFor = arrlocationFor[2];
                /*
                 * get data from server and save in db then pass data in reminder Address.
                 * */
                reminderUserAddress = ruadbh.getTableSingleTtpeDateWihtDao(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), "office", "0");
                Common.DisplayLog("con", (reminderUserAddress == null) + " && " + (officedubclick == 0));
                Common.DisplayLog("officedubclick", officedubclick + "");


                if (reminderUserAddress == null && officedubclick != 2) {
                    officedubclick = 0;
                }

                if (reminderUserAddress != null && officedubclick != 0) {
                    officedubclick = 0;
                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    officedubclick = 1;
                                }
                            },
                            1000);
                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    getLocationFormAdress(reminderUserAddress.getAddress(), reminderUserAddress.getAddressFor());
                                }
                            },
                            1500);

                } else if (officedubclick == 0) {
                    Common.DisplayLog("else if", "else if");
                    offcLayout.setEnabled(false);
                    Intent in = new Intent(LocationActivity.this, LocationAddressActivity.class);
                    in.putExtra("search_flag", "office");
                    startActivityForResult(in, 22);
                } else if (officedubclick == 2) {
                    Common.DisplayLog("else", "else");
                    curLayout.performClick();
                    offcLayout.setEnabled(true);
                    officedubclick = 1;
                }
            }
        });

        favLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationFor = arrlocationFor[3];
                Intent in = new Intent(LocationActivity.this, LocationAddressActivity.class);
                in.putExtra("search_flag", "favourite");
                startActivityForResult(in, 23);
            }
        });

        distLoopView1.setLoopListener(new LoopScrollListener() {
            @Override
            public void onItemSelect(int item) {
                distPos = item;
                areaSize = String.valueOf(item);
                Common.DisplayLog("areaSize", areaSize + "");
            }
        });

        unitLoopView1.setLoopListener(new LoopScrollListener() {
            @Override
            public void onItemSelect(int item) {
                unitPos = item;
                areaUnit = String.valueOf(item);
                Common.DisplayLog("areaUnit", areaUnit + "");
            }
        });

        edtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(LocationActivity.this, LocationSearchActivity.class);
                in.putExtra("search_flag", "edtloc");
                in.putExtra("search_address", edtSearch.getText());
                startActivityForResult(in, 24);
            }
        });

        imgYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (back_flag == 2) {
                    mapLayout.setVisibility(View.VISIBLE);
                    distLayout.setVisibility(View.GONE);
                    back_flag = 1;

                    gd = new GradientDrawable();
                    gd.setStroke(4, Color.parseColor("#1ba1e2"));
                    gd.setColor(Color.parseColor("#ffffff"));
                    gd.setCornerRadii(arr1);
                    onNearbybtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_device));
                    onNearbybtn.setTextColor(Color.parseColor("#1ba1e2"));
                } else*/
                {


                    String[] COLUMN = {
                            "",
                            SessionId,
                            Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                            locationFor,
                            locationDetail,
                            String.valueOf(latitude),
                            String.valueOf(longitude),
                            areaFor,
                            areaSize,
                            areaUnit,
                            "false"
                    };
                    String selId = String.valueOf(crldbh.addData(COLUMN));

                    if (action.equals("edit")) {
                        SavedEditDataOnServer(SessionId);
                        onBackPressed();
                    } else {

                        gobacktocr();
                    }
                }
            }
        });

        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (back_flag == 2) {
                    back_flag = 1;
                    /*mapLayout.setVisibility(View.VISIBLE);
                    distLayout.setVisibility(View.GONE);
                    back_flag = 1;

                    gd = new GradientDrawable();
                    gd.setStroke(4, Color.parseColor("#1ba1e2"));
                    gd.setColor(Color.parseColor("#ffffff"));
                    gd.setCornerRadii(arr1);
                    onNearbybtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_device));
                    onNearbybtn.setTextColor(Color.parseColor("#1ba1e2"));*/
                    onArrivbtn.performClick();
                } else {
                    crldbh.resetTempdata(SessionId);

                    if (action.equals("edit")) {
                        onBackPressed();
                    } else {
                        gobacktocr();
                    }
                }
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (action.equals("edit")) {
                    onBackPressed();
                } else {

                    gobacktocr();
                }
            }
        });

        imgHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(LocationActivity.this, HomeActivity.class);
                startActivity(in);
            }
        });

        onArrivbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gd = new GradientDrawable();
                gd.setStroke(4, Color.parseColor("#1ba1e2"));
                gd.setColor(Color.parseColor("#1ba1e2"));
                gd.setCornerRadii(arr);
                onArrivbtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_send));
                onArrivbtn.setTextColor(Color.parseColor("#ffffff"));

                gd = new GradientDrawable();
                gd.setStroke(4, Color.parseColor("#1ba1e2"));
                gd.setColor(Color.parseColor("#ffffff"));
                gd.setCornerRadii(arr1);
                onNearbybtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_device));
                onNearbybtn.setTextColor(Color.parseColor("#1ba1e2"));

                gd = new GradientDrawable();
                gd.setStroke(4, Color.parseColor("#1ba1e2"));
                gd.setColor(Color.parseColor("#ffffff"));
//                gd.setCornerRadii(arr);
                onLeavingbtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_middle));
                onLeavingbtn.setTextColor(Color.parseColor("#1ba1e2"));

                mapLayout.setVisibility(View.VISIBLE);
                distLayout.setVisibility(View.GONE);

                areaFor = arrareaFor[0];
                areaSize = "0.5";
                areaUnit = arrareaUnit[0];
            }
        });

        onLeavingbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gd = new GradientDrawable();
                gd.setStroke(4, Color.parseColor("#1ba1e2"));
                gd.setColor(Color.parseColor("#1ba1e2"));
//                gd.setCornerRadii(arr);
                onLeavingbtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_middle_blue));
                onLeavingbtn.setTextColor(Color.parseColor("#ffffff"));

                gd = new GradientDrawable();
                gd.setStroke(4, Color.parseColor("#1ba1e2"));
                gd.setColor(Color.parseColor("#ffffff"));
                gd.setCornerRadii(arr1);
                onNearbybtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_device));
                onNearbybtn.setTextColor(Color.parseColor("#1ba1e2"));

                gd = new GradientDrawable();
                gd.setStroke(4, Color.parseColor("#1ba1e2"));
                gd.setColor(Color.parseColor("#ffffff"));
                gd.setCornerRadii(arr);
                onArrivbtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_send_white));
                onArrivbtn.setTextColor(Color.parseColor("#1ba1e2"));

                mapLayout.setVisibility(View.VISIBLE);
                distLayout.setVisibility(View.GONE);


                areaFor = arrareaFor[1];
                areaSize = "0.5";
                areaUnit = arrareaUnit[0];
            }
        });

        onNearbybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back_flag = 2;
                gd = new GradientDrawable();
                gd.setStroke(4, Color.parseColor("#1ba1e2"));
                gd.setColor(Color.parseColor("#ffffff"));
                gd.setCornerRadii(arr);
                onArrivbtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_send_white));
                onArrivbtn.setTextColor(Color.parseColor("#1ba1e2"));

                gd = new GradientDrawable();
                gd.setStroke(4, Color.parseColor("#1ba1e2"));
                gd.setColor(Color.parseColor("#1ba1e2"));
                gd.setCornerRadii(arr1);
                onNearbybtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_device_blue));
                onNearbybtn.setTextColor(Color.parseColor("#ffffff"));

                gd = new GradientDrawable();
                gd.setStroke(4, Color.parseColor("#1ba1e2"));
                gd.setColor(Color.parseColor("#ffffff"));
//                gd.setCornerRadii(arr);
                onLeavingbtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_middle));
                onLeavingbtn.setTextColor(Color.parseColor("#1ba1e2"));

                mapLayout.setVisibility(View.GONE);
                distLayout.setVisibility(View.VISIBLE);

                areaFor = arrareaFor[2];
                areaSize = "0";
                areaUnit = "0";
            }
        });
    }

    private void initPickerViews1() {
        unitList1.add("miles");
        unitList1.add("km");

        unitLoopView1.setDataList((ArrayList) unitList1);
        unitLoopView1.setInitPosition(unitPos);

        distList1.add("0.5");
        distList1.add("1.0");
        distList1.add("1.5");
        distList1.add("2.0");
        distList1.add("3.0");
        distList1.add("4.0");
        distList1.add("5.0");
        distList1.add("6.0");
        distList1.add("7.0");
        distList1.add("8.0");
        distList1.add("9.0");
        distList1.add("10.0");
        distList1.add("15.0");
        distList1.add("20.0");
        distList1.add("25.0");
        distList1.add("30.0");
        distList1.add("35.0");
        distList1.add("40.0");
        distList1.add("45.0");
        distList1.add("50.0");
        distList1.add("60.0");
        distList1.add("70.0");
        distList1.add("80.0");
        distList1.add("90.0");
        distList1.add("100.0");

        distLoopView1.setDataList((ArrayList) distList1);
        distLoopView1.setInitPosition(distPos);
    }

    private void setmap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private List<Address> getLocationFormAdresssearch(Geocoder geocoder, String locationAddress) {
        List<Address> addresses = new ArrayList<>();
        try {
            addresses = geocoder.getFromLocationName(locationAddress, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses == null || addresses.size() <= 0) {
            List<String> elephantList = Arrays.asList(locationAddress.split(","));
            String nwadd = "";
            for (int i = 1; i < elephantList.size(); i++) {
                nwadd += elephantList.get(i) + ",";
            }
            Common.DisplayLog("nwadd", nwadd + "");
            addresses = getLocationFormAdresssearch(geocoder, nwadd);
        }
        return addresses;
    }

    private void getLocationFormAdress(String locationAddress, String locationFor) {
        Geocoder geocoder = new Geocoder(getApplicationContext());
        List<Address> addresses = null;
        //            addresses = geocoder.getFromLocationName(locationAddress, 1);
        addresses = getLocationFormAdresssearch(geocoder, locationAddress);
        if (addresses != null && addresses.size() > 0) {
            latitude = addresses.get(0).getLatitude();
            longitude = addresses.get(0).getLongitude();

            String address = "", city2 = "";
            if (addresses.get(0).getAddressLine(0) != null) {
                address = addresses.get(0).getAddressLine(0);
            }
            if (addresses.get(0).getLocality() != null) {
                city2 = addresses.get(0).getLocality();
            }

            locationDetail = locationAddress;
//            locationDetail = address + ", " + city2;
            setmaponcod(latitude, longitude, locationFor, address + ", " + city2);
        }
//        homedubclick = true;
    }

    private void setmaponcod(double lat, double lon, String locFor, String locaddress) {
        LatLng mLatLng = new LatLng(lat, lon);

        Common.DisplayLog("setmaponcod", locationFor + " => " + lat + " == " + lon + "/nlocaddress = " + locaddress);
        if (current_marker != null) {
            current_marker.remove();
        }

        current_marker = mMap.addMarker(new MarkerOptions()
                .position(mLatLng)
                .title(locFor)
                .snippet(" ")
                .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA)));

//        mMap.moveCamera(CameraUpdateFactory.newLatLng(mLatLng));
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(zoomTo));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, zoomTo));
        txtLocation.setText(locaddress);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (action.equals("edit")) {
            super.onBackPressed();
        } else {
            gobacktocr();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadSavedData(SessionId);
    }

    private void gobacktocr() {

        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        Intent in = null;
        in = new Intent(getApplicationContext(), CreateReminderActivity.class);
        in.putExtra("newrem", "no");
        startActivity(in);

    }


    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        LatLng mLatLng = new LatLng(location.getLatitude(),
                location.getLongitude());


        if (current_marker != null) {
            current_marker.remove();
        }

        current_marker = mMap.addMarker(new MarkerOptions()
                .position(mLatLng)
                .title("Current Location")
                .snippet(" ")
                .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA)));

//            mMap.moveCamera(CameraUpdateFactory.newLatLng(mLatLng));
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(zoomTo));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, zoomTo));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String address = "", city2 = "";
        if (addresses != null && addresses.size() > 0) {
            if (addresses.get(0).getAddressLine(0) != null) {
                address = addresses.get(0).getAddressLine(0);
            }
            if (addresses.get(0).getLocality() != null) {
                city2 = addresses.get(0).getLocality();
            }
        }

        latitude = location.getLatitude();
        longitude = location.getLongitude();
        locationDetail = address + ", " + city2;

        txtLocation.setText(locationDetail);
    }

    @Override
    public void onConnected(Bundle bundle) {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
        }
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        builder.build()
                );
        result.setResultCallback(this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                // NO need to show the dialog;
                if (mGoogleApiClient == null) {
                    buildGoogleApiClient();
                }
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mMap.setMyLocationEnabled(true);
                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                //  GPS turned off, Show the user a dialog
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(LocationActivity.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    //failed to show dialog
                }

                break;

            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are unavailable so not possible to show any dialog now
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Common.DisplayLog("requestCode", requestCode + "");
        homeLayout.setEnabled(true);
        offcLayout.setEnabled(true);

        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == RESULT_OK) {

            } else {
                Toast.makeText(getApplicationContext(), "GPS is not enabled", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == 5) {
            if (null != data) {
//                radioGroup.clearCheck();
                // fetch the message String
                txtloc = data.getStringExtra("checkin");
                txtLocation.setText(txtloc);
            }
        } else if (requestCode == 21 && data != null) {
            homedubclick = data.getIntExtra("goto", 0);


            final String address = data.getStringExtra("address");
            latitude = data.getDoubleExtra("sellatitude", 0);
            longitude = data.getDoubleExtra("sellongitude", 0);
            if (data != null && longitude != 0 && latitude != 0) {
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                Common.DisplayLog("address", address);
                                setmaponcod(latitude, longitude, locationFor, address);
                                txtLocation.setText(address);
//                                getLocationFormAdress(address, "search");
                            }
                        },
                        3000);

            } else {
                homeLayout.performClick();
            }
        } else if (requestCode == 22 && data != null) {

            officedubclick = data.getIntExtra("goto", 0);

            final String address = data.getStringExtra("address");
            latitude = data.getDoubleExtra("sellatitude", 0);
            longitude = data.getDoubleExtra("sellongitude", 0);
            if (data != null && longitude != 0 && latitude != 0) {
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                Common.DisplayLog("address", address);
                                setmaponcod(latitude, longitude, locationFor, address);
                                txtLocation.setText(address);
//                                getLocationFormAdress(address, "search");
                            }
                        },
                        3000);

            } else {
                offcLayout.performClick();
            }
        } else if (requestCode == 23 && data != null) {
            String selectlastid = data.getStringExtra("selectlastid");
            reminderUserAddress = ruadbh.getTableSingleTtpeDateWihtDao(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), "favourite", selectlastid);

            if (reminderUserAddress != null && officedubclick != 0) {
                officedubclick = 0;
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                officedubclick = 1;
                            }
                        },
                        1000);
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {

                                getLocationFormAdress(reminderUserAddress.getAddress(), reminderUserAddress.getAddressFor());
                                txtLocation.setText(reminderUserAddress.getAddressFor());
                            }
                        },
                        3000);

            } else {
                Intent in = new Intent(LocationActivity.this, LocationAddressActivity.class);
                in.putExtra("search_flag", "favourite");
                startActivityForResult(in, 23);
            }
//            favLayout.performClick();
        } else if (requestCode == 24 && data != null) {
            final String address = data.getStringExtra("address");


            latitude = data.getDoubleExtra("sellatitude", 0);
            longitude = data.getDoubleExtra("sellongitude", 0);

            locationDetail = address;


            Common.DisplayLog("address", address);
            if (address != null && !address.equals("")) {
                edtSearch.setText(address);

                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                Common.DisplayLog("address", address);
                                setmaponcod(latitude, longitude, locationFor, address);
                                txtLocation.setText(address);
//                                getLocationFormAdress(address, "search");
                            }
                        },
                        3000);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Enabling MyLocation Layer of Google Map
        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }


    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }


    public void loadSavedData(final String SessId) {


        Common.DisplayLog("", "loadSavedData " + SessId);
        Common.DisplayLog("", "crdtdbh.getDataCount(SessId) " + crldbh.getDataCount(SessId));
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            if (crldbh.getDataCount(SessId) > 0) {
                ReminderLocation rl = crldbh.getTableSingleDateWihtDao(SessId);

                locationFor = rl.getLocationFor();
                locationDetail = rl.getLocationDetail();
                latitude = Double.parseDouble(rl.getLat());
                longitude = Double.parseDouble(rl.getLon());
                areaFor = rl.getAreaFor();
                areaSize = rl.getAreaSize();
                areaUnit = rl.getAreaUnit();

                setmaponcod(latitude, longitude, locationFor, locationDetail);
                if (areaFor.equals(arrareaFor[0])) {
                    onArrivbtn.performClick();
                } else if (areaFor.equals(arrareaFor[1])) {
                    onLeavingbtn.performClick();
                } else if (areaFor.equals(arrareaFor[2])) {


                    onNearbybtn.performClick();
                    distLoopView1.setInitPosition(Integer.parseInt(rl.getAreaSize()));
                    unitLoopView1.setInitPosition(Integer.parseInt(rl.getAreaUnit()));

                }
            }
        } else {
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            loadSavedData(SessId);
                        }
                    },
                    2000);
        }

    }


    public void SavedEditDataOnServer(String SessId) {


        /***  Save main reminder ****/

        if (Common.hasInternetConnection(getApplicationContext())) {



            Common.DisplayLog("", "loadSavedData " + SessId);

            Common.DisplayLog("SessId", SessId + "");
            pd = new ProgressDialog(LocationActivity.this);
            pd.setMessage("Loading...");
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


            /*** Location ****/
            CRLocationDbHandler crldbh = new CRLocationDbHandler(getApplicationContext());
            Common.DisplayLog("crldbh.getDataCount(SessionId)", crldbh.getDataCount(SessionId) + "");
            ReminderLocation rl = crldbh.getTableSingleDateWihtDao(SessId);

            if (rl != null) {
                Call<Reminder> callDateTime = apiService.editLocation(rl.getSesssionId(),rl.getiUserId(), rl.getLocationFor(), rl.getLocationDetail(), rl.getLat(), rl.getLon(), rl.getAreaFor(), rl.getAreaSize(), rl.getAreaUnit(), rl.getStatus());
                callDateTime.enqueue(new Callback<Reminder>() {
                    @Override
                    public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                        int statusCode = response.code();
                        Common.DisplayLog("response.body()", response.body().toString());
                        boolean status = response.body().getStatus();
                        Common.DisplayLog("status", status + "");
                        Common.DisplayLog("getMessage", response.body().getMessage() + "");

                        if (status) {
                            pd.dismiss();
                            onBackPressed();
                        } else {
                            Common.ShowToast(getApplicationContext(), response.body().getMessage());
                        }
                        pd.dismiss();

                    }

                    @Override
                    public void onFailure(Call<Reminder> call, Throwable t) {

                        Common.DisplayLog("fail", t.getMessage().toString());
                    }

                });
            } else {

            }
        }
    }



}