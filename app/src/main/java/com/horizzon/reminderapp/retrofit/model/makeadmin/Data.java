
package com.horizzon.reminderapp.retrofit.model.makeadmin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("getMakeadminList")
    @Expose
    private List<GetMakeadminList> getMakeadminList = null;

    public List<GetMakeadminList> getGetMakeadminList() {
        return getMakeadminList;
    }

    public void setGetMakeadminList(List<GetMakeadminList> getMakeadminList) {
        this.getMakeadminList = getMakeadminList;
    }

}
