package com.horizzon.reminderapp.adapter;

import android.app.Activity;
import android.content.Intent;

import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horizzon.reminderapp.DetailViewActivity;
import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.ReminderDetailActivity;
import com.horizzon.reminderapp.dao.CReminder;
import com.horizzon.reminderapp.dao.ReminderContactUser;
import com.horizzon.reminderapp.dao.ReminderDateTime;
import com.horizzon.reminderapp.dao.ReminderForList;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.CRAttachmentDbHandler;
import com.horizzon.reminderapp.handler.CRCommentDbHandler;
import com.horizzon.reminderapp.handler.CRContactDbHandler;
import com.horizzon.reminderapp.handler.CRDateTimeDbHandler;
import com.horizzon.reminderapp.handler.CRLocationDbHandler;
import com.horizzon.reminderapp.handler.CReminderForListDbHandler;
import com.horizzon.reminderapp.handler.CreateReminderDbHandler;
import com.horizzon.reminderapp.utility.DateUtilitys;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by horizzon on 4/3/2017.
 */

public class AllReminderListAdapter extends RecyclerView.Adapter<AllReminderListAdapter.MyViewHolder> {

    Activity context;
    ArrayList<CReminder> list;
    String currdate = "", inFor;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout ilmain, cu_layout;
        public ImageView imgProfile, con_image, imgPriority1, imgPriority2, imgPriority3, imgAttachment, imgComment, imgLocation;
        public TextView unameName, name, time, unread;

        public MyViewHolder(View view) {
            super(view);
            ilmain = (LinearLayout) view.findViewById(R.id.ilmain);
//            cu_layout = (LinearLayout) view.findViewById(R.id.cu_layout);

            imgProfile = (ImageView) view.findViewById(R.id.imgProfile);
            con_image = (ImageView) view.findViewById(R.id.con_image);
            imgPriority1 = (ImageView) view.findViewById(R.id.imgPriority1);
            imgPriority2 = (ImageView) view.findViewById(R.id.imgPriority2);
            imgPriority3 = (ImageView) view.findViewById(R.id.imgPriority3);
            imgAttachment = (ImageView) view.findViewById(R.id.imgAttach);
            imgComment = (ImageView) view.findViewById(R.id.imgComt);
            imgLocation = (ImageView) view.findViewById(R.id.imgLocation);

            unameName = (TextView) view.findViewById(R.id.unameName);
            name = (TextView) view.findViewById(R.id.reminderName);
            time = (TextView) view.findViewById(R.id.reminderTime);
            unread = (TextView) view.findViewById(R.id.reminderUnread);
        }
    }

    public AllReminderListAdapter(Activity context, ArrayList<CReminder> list, String inFor) {
        this.context = context;
        this.list = list;
        this.inFor = inFor;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_allreminder_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final CReminder cr = list.get(position);

        final String SessionId = cr.getSesssionId();

        holder.name.setText(cr.getSubject());

        CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(context);
        ReminderDateTime rdt = crdtdbh.getTableSingleDateWihtDao(SessionId);
        Common.DisplayLog("crdtdbh.getDataCount(SessionId)", crdtdbh.getDataCount(SessionId) + "");


        DateUtilitys dateUtl = new DateUtilitys();

        Date date;
        if (crdtdbh.getDataCount(SessionId) > 0) {
            date = dateUtl.FormetDate(rdt.getStartDate() + " " + rdt.getStartTime());
        } else {
            Common.DisplayLog("cr.getCreateddate()", cr.getCreateddate() + "");
            date = dateUtl.FormetDate(cr.getCreateddate());

        }
        dateUtl.setforDisplayTitle(date);
        holder.time.setText(dateUtl.getDay() + " " + dateUtl.getMonth());


        holder.imgPriority1.setImageResource(R.drawable.prioritywhite);
        holder.imgPriority2.setImageResource(R.drawable.prioritywhite);
        holder.imgPriority3.setImageResource(R.drawable.prioritywhite);
        if (cr.getPriority().equals("1")) {
            holder.imgPriority1.setImageResource(R.drawable.priorityiconyellow);
        } else if (cr.getPriority().equals("2")) {
            holder.imgPriority1.setImageResource(R.drawable.priorityiconyellow);
            holder.imgPriority2.setImageResource(R.drawable.priorityiconyellow);
        } else if (cr.getPriority().equals("3")) {
            holder.imgPriority1.setImageResource(R.drawable.priorityiconyellow);
            holder.imgPriority2.setImageResource(R.drawable.priorityiconyellow);
            holder.imgPriority3.setImageResource(R.drawable.priorityiconyellow);
        }


        // set Contact
        CRContactDbHandler crcdbh = new CRContactDbHandler(context);
        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllCurrentSessContactData(SessionId, "contact");
//        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllContactData();

        Common.DisplayLog("remindercontactuser", remindercontactuser.size() + "");
        String tempToText = "";
        int i = remindercontactuser.size();
        if (i > 0) {
            if (1 <= i) {
                tempToText += remindercontactuser.get(0).getName() + ",";
            }
            String imgpath = ((remindercontactuser.get(0).getPhoto().equals("")) ? "defaultimg" : remindercontactuser.get(0).getPhoto()) + "";
            Common.DisplayLog("imgpath", imgpath + "");
            Picasso.with(context)
                    .load(imgpath)
                    .placeholder(R.drawable.defaultusericon)
                    .error(R.drawable.defaultusericon)
                    .into(holder.con_image);
        }


        tempToText = (tempToText != null && !tempToText.equals("")) ? tempToText.substring(0, tempToText.length() - 1) : "";
        if (i > 2) {
            tempToText += " & " + (i - 1) + " others";
        }

        holder.unameName.setText(tempToText);


        // set Attachment
        CRAttachmentDbHandler cradbh = new CRAttachmentDbHandler(context);
        Common.DisplayLog("cradbh.getDataCount(SessionId)", cradbh.getDataCount(SessionId, "done") + "");
        if (cradbh.getDataCount(SessionId, "done") > 0) {
            holder.imgAttachment.setImageResource(R.drawable.attachiconblue);
        } else {
            holder.imgAttachment.setImageResource(R.drawable.attachicon);
        }

        // set Comment

        CRCommentDbHandler crcodbh = new CRCommentDbHandler(context);
        Common.DisplayLog("crcodbh.getDataCount(SessionId)", crcodbh.getDataCount(SessionId) + "");
        if (crcodbh.getDataCount(SessionId) > 0) {
            holder.imgComment.setImageResource(R.drawable.comticonblue);
        } else {
            holder.imgComment.setImageResource(R.drawable.comticon);
        }

        // set Date Time
        /*CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(context);
        Common.DisplayLog("crdtdbh.getDataCount(SessionId)", crdtdbh.getDataCount(SessionId) + "");
        if (crdtdbh.getDataCount(SessionId) > 0) {
            holder.imgLocation.setImageResource(R.drawable.datetimebluerounded);
        } else {
            holder.imgLocation.setImageResource(R.drawable.datetimerounded);
        }*/

        // set Location
        CRLocationDbHandler crldbh = new CRLocationDbHandler(context);
        Common.DisplayLog("crldbh.getDataCount(SessionId)", crldbh.getDataCount(SessionId) + "");
        if (crldbh.getDataCount(SessionId) > 0) {
            holder.imgLocation.setImageResource(R.drawable.locationiconblue);
        } else {
            holder.imgLocation.setImageResource(R.drawable.locationicon);
        }

        holder.ilmain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                 * If reminder is  share that time show  accept and reject screen  either show as regular
                 * */
                if (checkReminderIsShare(SessionId)) {
                    Log.d("TAG", "onClick: true :");
                    CReminderForListDbHandler crfldbh = new CReminderForListDbHandler(context);
                    ReminderForList rfl = crfldbh.getTableSingleDateWihtDao(SessionId, Common.getSharedPreferences(context, "userId", "0"));
                    if (rfl != null) {
                        if (rfl.getvCreatedBy().matches(Common.getSharedPreferences(context, "userId", "0"))) {
                            Intent in = new Intent(context, DetailViewActivity.class);
                            in.putExtra("SessionId", SessionId);
                            context.startActivity(in);
                        } else {
                            if (rfl.getvReminderStatus().equals("accept")) {
                                Intent in = new Intent(context, DetailViewActivity.class);
                                in.putExtra("SessionId", SessionId);
                                context.startActivity(in);
                            } else {
                                Intent in = new Intent(context, ReminderDetailActivity.class);
                                in.putExtra("SessionId", SessionId);
                                context.startActivity(in);
                            }
                        }
                    } else {
                        Intent in = new Intent(context, ReminderDetailActivity.class);
                        in.putExtra("SessionId", SessionId);
                        context.startActivity(in);
                    }
                } else {
                    Log.d("TAG", "onClick: false : ");
                    Intent in = new Intent(context, DetailViewActivity.class);
                    in.putExtra("SessionId", SessionId);
                    context.startActivity(in);
                }

            }
        });
    }


    public boolean checkReminderIsShare(String sessionId) {

        CreateReminderDbHandler crdbh = new CreateReminderDbHandler(context);
        CReminder cReminder = crdbh.getTableSingleDateWihtDao(sessionId);
        if (cReminder.getReminderFor().matches("share")) {
            return true;
        }
        Log.d("TAG", "loadSavedData: reminderDetails :" + cReminder);
        return false;

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
