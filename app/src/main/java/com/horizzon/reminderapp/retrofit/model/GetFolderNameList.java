package com.horizzon.reminderapp.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetFolderNameList {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Datum implements Serializable {

        @SerializedName("iId")
        @Expose
        private String iId;
        @SerializedName("iUserId")
        @Expose
        private String iUserId;
        @SerializedName("vFoldername")
        @Expose
        private String vFoldername;
        @SerializedName("dCreatedDateTime")
        @Expose
        private String dCreatedDateTime;

        public String getIId() {
            return iId;
        }

        public void setIId(String iId) {
            this.iId = iId;
        }

        public String getIUserId() {
            return iUserId;
        }

        public void setIUserId(String iUserId) {
            this.iUserId = iUserId;
        }

        public String getVFoldername() {
            return vFoldername;
        }

        public void setVFoldername(String vFoldername) {
            this.vFoldername = vFoldername;
        }

        public String getDCreatedDateTime() {
            return dCreatedDateTime;
        }

        public void setDCreatedDateTime(String dCreatedDateTime) {
            this.dCreatedDateTime = dCreatedDateTime;
        }

    }

}