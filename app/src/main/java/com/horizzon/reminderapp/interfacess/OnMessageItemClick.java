package com.horizzon.reminderapp.interfacess;


import com.horizzon.reminderapp.model.Message;

public interface OnMessageItemClick {
    void OnMessageClick(Message message, int position);

    void OnMessageLongClick(Message message, int position);
}
