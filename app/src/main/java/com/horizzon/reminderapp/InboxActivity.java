package com.horizzon.reminderapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.horizzon.reminderapp.adapter.DrawerItemCustomAdapter;
import com.horizzon.reminderapp.adapter.InboxListAdapter;
import com.horizzon.reminderapp.adapter.ReminderListAdapter;
import com.horizzon.reminderapp.dao.CReminder;
import com.horizzon.reminderapp.dao.ContactUser;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.CreateReminderDbHandler;
import com.horizzon.reminderapp.utility.ObjectDrawerItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

public class InboxActivity extends AppCompatActivity {

    String comeForm = "";
    ImageView imgBack, imgBtnOpt;
    RecyclerView listView;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    protected ListView mDrawerList;
    ObjectDrawerItem[] drawerItem;
    ArrayList<CReminder> list;
    ReminderListAdapter cAdapter;
    InboxListAdapter IndAdapter;
    FloatingActionButton fab;
    TextView pageTitle, filterdatatime, filterpriority;
    LinearLayout filterlayout, filterpersonLayout, filterperson, expandbackground;
    ImageView filterclose, filterdatatimeimg, filterpriorityimg, pagetitleimg;
    Animation slideUpAnimation, slideDownAnimation, slideUpAnimation2, slideDownAnimation2;
    boolean filterdatatimestat = false, filterprioritystat = false;
    ExpandableRelativeLayout filterlayoutexpandable, filterpersonLayoutexpandable;
    TextView pertitletext, filterperson1, filterperson2, filterperson3;
    ImageView pertitleimg, filterperson1edit, filterperson2edit, filterperson3edit, filterperson1del, filterperson2del, filterperson3del, filterperson1img, filterperson2img, filterperson3img;
    HashMap<String, String> odercon = new HashMap<>();
    LinearLayout filterperson1lay, filterperson2lay, filterperson3lay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);


        pageTitle = (TextView) findViewById(R.id.pageTitle);
        if (getIntent().getStringExtra("comeForm") != null) {
            comeForm = getIntent().getStringExtra("comeForm");
            pageTitle.setText(comeForm);
        }

        odercon = new HashMap<>();
        odercon.put("filterperson1", "");
        odercon.put("filterperson2", "");
        odercon.put("filterperson3", "");

        filterdatatime = (TextView) findViewById(R.id.filterdatatime);
        filterdatatimeimg = (ImageView) findViewById(R.id.filterdatatimeimg);
        filterpriority = (TextView) findViewById(R.id.filterpriority);
        filterpriorityimg = (ImageView) findViewById(R.id.filterpriorityimg);
        filterperson = (LinearLayout) findViewById(R.id.filterperson);
        filterlayout = (LinearLayout) findViewById(R.id.filterlayout);
        filterpersonLayout = (LinearLayout) findViewById(R.id.filterpersonLayout);
        expandbackground = (LinearLayout) findViewById(R.id.expandbackground);
        filterlayoutexpandable = (ExpandableRelativeLayout) findViewById(R.id.filterlayoutexpandable);
        filterpersonLayoutexpandable = (ExpandableRelativeLayout) findViewById(R.id.filterpersonLayoutexpandable);

        pertitletext = (TextView) findViewById(R.id.pertitletext);
        filterperson1 = (TextView) findViewById(R.id.filterperson1);
        filterperson2 = (TextView) findViewById(R.id.filterperson2);
        filterperson3 = (TextView) findViewById(R.id.filterperson3);

        filterperson1lay = (LinearLayout) findViewById(R.id.filterperson1lay);
        filterperson2lay = (LinearLayout) findViewById(R.id.filterperson2lay);
        filterperson3lay = (LinearLayout) findViewById(R.id.filterperson3lay);
        filterperson1edit = (ImageView) findViewById(R.id.filterperson1edit);
        filterperson2edit = (ImageView) findViewById(R.id.filterperson2edit);
        filterperson3edit = (ImageView) findViewById(R.id.filterperson3edit);
        filterperson1del = (ImageView) findViewById(R.id.filterperson1del);
        filterperson2del = (ImageView) findViewById(R.id.filterperson2del);
        filterperson3del = (ImageView) findViewById(R.id.filterperson3del);
        filterperson1img = (ImageView) findViewById(R.id.filterperson1img);
        filterperson2img = (ImageView) findViewById(R.id.filterperson2img);
        filterperson3img = (ImageView) findViewById(R.id.filterperson3img);
        pertitleimg = (ImageView) findViewById(R.id.pertitleimg);
        pagetitleimg = (ImageView) findViewById(R.id.pagetitleimg);


        filterclose = (ImageView) findViewById(R.id.filterclose);
        imgBack = (ImageView) findViewById(R.id.backBtn);
        imgBtnOpt = (ImageView) findViewById(R.id.imgOpt);
        listView = (RecyclerView) findViewById(R.id.list);
        list = new ArrayList<CReminder>();

        fab = (FloatingActionButton) findViewById(R.id.fab);
        final Animation zoomin = AnimationUtils.loadAnimation(this, R.anim.zzomin);
        final Animation zoomout = AnimationUtils.loadAnimation(this, R.anim.zoomout);

        fab.setAnimation(zoomin);
        fab.setAnimation(zoomout);

        zoomin.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                fab.startAnimation(zoomout);
            }
        });

        zoomout.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                fab.startAnimation(zoomin);
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                Intent in = new Intent(InboxActivity.this, CreateReminderActivity.class);
                in.putExtra("newrem", "yes");
                startActivity(in);
            }
        });

        SetData(comeForm);


        /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CReminder cr = list.get(position);
                Intent in = new Intent(InboxActivity.this, ReminderDetailActivity.class);
                in.putExtra("SessionId", cr.getSesssionId());
                startActivity(in);
            }
        });*/

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // list the drawer items
        drawerItem = new ObjectDrawerItem[4];

        drawerItem[0] = new ObjectDrawerItem(R.drawable.sentlisticon, "Sent List");
        drawerItem[1] = new ObjectDrawerItem(R.drawable.printlisticon, "Print List");
        drawerItem[2] = new ObjectDrawerItem(R.drawable.clearcompletedicon, "Clear Completed");
        drawerItem[3] = new ObjectDrawerItem(R.drawable.settingicon, "Settings");
      //  drawerItem[4] = new ObjectDrawerItem(R.drawable.syncicon, "Sync Now");

        // Pass the folderData to our ListView adapter
        final DrawerItemCustomAdapter adapter1 = new DrawerItemCustomAdapter(this, R.layout.list_item_row, drawerItem);

        // Set the adapter for the list view
        mDrawerList.setAdapter(adapter1);
        // set the item click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // for app icon control for nav drawer
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // onBackPressed();
                Intent in = new Intent(InboxActivity.this, HomeActivity.class);
                startActivity(in);
            }
        });

        imgBtnOpt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        });


        slideUpAnimation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up);
        slideUpAnimation2 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up);

        slideDownAnimation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);
        slideDownAnimation2 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);

        slideUpAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
//                filterlayout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        slideUpAnimation2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                filterpersonLayout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        slideDownAnimation2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                filterpersonLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        pageTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandbackground.setVisibility(View.VISIBLE);
                pagetitleimg.setImageResource(R.drawable.downaarowup);
                expandbackground.animate().alpha(1.0f).setDuration(500);
                if (filterlayoutexpandable.isExpanded()) {
                    filterclose.performClick();
                } else {
                    filterlayoutexpandable.expand();
                }
            }
        });
        filterclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterlayoutexpandable.collapse();
                if (filterpersonLayoutexpandable.isExpanded()) {

                    filterpersonLayoutexpandable.collapse();
                }
                pagetitleimg.setImageResource(R.drawable.downaarow);
//                filterpersonLayoutexpandable.setExpanded(false);
                expandbackground.animate().alpha(0.0f).setDuration(600);
            }
        });
        expandbackground.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                filterclose.performClick();
                return false;
            }
        });
        filterlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                filterlayoutexpandable.collapse();
                filterclose.performClick();
                listView.setVisibility(View.VISIBLE);

            }
        });
        filterclose.performClick();
        filterdatatime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetfilter(true);
                if (filterdatatimestat) {
                    filterdatatimestat = false;
                    filterdatatimeimg.setImageResource(R.drawable.datetimeiconblack);
                    filterdatatime.setTextColor(Color.parseColor("#666666"));
                } else {
                    filterdatatimestat = true;
                    filterdatatimeimg.setImageResource(R.drawable.datetimeiconblue);
                    filterdatatime.setTextColor(Color.parseColor("#1ba1e2"));
                }
                filterclose.performClick();
                filterpersondropdown();
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {

                                SetData(comeForm);
                            }
                        },
                        1000);

            }
        });
        filterpriority.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetfilter(true);
                if (filterprioritystat) {
                    filterprioritystat = false;
                    filterpriorityimg.setImageResource(R.drawable.stariconblack);
                    filterpriority.setTextColor(Color.parseColor("#666666"));
                } else {
                    filterprioritystat = true;
                    filterpriorityimg.setImageResource(R.drawable.stariconblue);
                    filterpriority.setTextColor(Color.parseColor("#1ba1e2"));
                }
                filterclose.performClick();
                filterpersondropdown();
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {

                                SetData(comeForm);
                            }
                        },
                        1000);
            }
        });

        filterperson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterpersonLayoutexpandable.toggle();
            }
        });
        filterperson1edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.DisplayLog(".get(\"filterperson1\")", odercon.get("filterperson1") + "");
                if (odercon.get("filterperson1").equals("")) {
                    Intent i = new Intent(InboxActivity.this, InboxContactActivity.class);
                    i.putExtra("moveto", "1");
                    startActivityForResult(i, 11);
                } else {
                    filterperson1del.performClick();
                }
            }
        });
        filterperson2edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (odercon.get("filterperson2").equals("")) {
                    Intent i = new Intent(InboxActivity.this, InboxContactActivity.class);
                    i.putExtra("moveto", "1");
                    startActivityForResult(i, 12);
                } else {
                    filterperson2del.performClick();
                }
            }
        });
        filterperson3edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (odercon.get("filterperson3").equals("")) {
                    Intent i = new Intent(InboxActivity.this, InboxContactActivity.class);
                    i.putExtra("moveto", "1");
                    startActivityForResult(i, 13);
                } else {
                    filterperson3del.performClick();
                }
            }
        });
        filterperson1lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterperson1edit.performClick();
            }
        });
        filterperson2lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterperson2edit.performClick();
            }
        });
        filterperson3lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterperson3edit.performClick();
            }
        });


        filterperson1del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterperson1.setText("Add");
                filterperson1img.setImageResource(R.drawable.attachedcontactblack);
                filterperson1edit.setVisibility(View.VISIBLE);
                filterperson1del.setVisibility(View.GONE);
                odercon.put("filterperson1", "");
                resetpersonfilter();
            }
        });
        filterperson2del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterperson2.setText("Add");
                filterperson2img.setImageResource(R.drawable.attachedcontactblack);
                filterperson2edit.setVisibility(View.VISIBLE);
                filterperson2del.setVisibility(View.GONE);
                odercon.put("filterperson2", "");
                resetpersonfilter();
            }
        });
        filterperson3del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterperson3.setText("Add");
                filterperson3img.setImageResource(R.drawable.attachedcontactblack);
                filterperson3edit.setVisibility(View.VISIBLE);
                filterperson3del.setVisibility(View.GONE);
                odercon.put("filterperson3", "");
                resetpersonfilter();
            }
        });
//        filterpersondropdown();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /*Toast.makeText(getApplicationContext(), "requestCode : " + requestCode, Toast.LENGTH_LONG).show();
        Toast.makeText(getApplicationContext(), "resultCode : " + resultCode, Toast.LENGTH_LONG).show();

        Common.DisplayLog("requestCode", requestCode + "");
        Common.DisplayLog("resultCode", resultCode + "");*/


        ContactUser cu = null;
        if (resultCode == 2) {

            pertitleimg.setImageResource(R.drawable.attachedcontactblack);
            pertitletext.setTextColor(Color.parseColor("#666666"));
            pertitletext.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.downaarowblack, 0);

            resetfilter(false);
            Intent i = data;
            cu = (ContactUser) i.getSerializableExtra("ContactUser");
//            Toast.makeText(getApplicationContext(), "cu.getName()" + cu.getName(), Toast.LENGTH_LONG).show();
            Common.DisplayLog("data.getName()", cu.getName() + "");
            Common.DisplayLog("data.getNumber()", cu.getNumber() + "");
        }

        if (requestCode == 11 && resultCode == 2) {

            String imgpath = ((cu.getPhoto().equals("")) ? "defaultimg" : cu.getPhoto()) + "";
            Picasso.with(getApplicationContext())
                    .load(imgpath)
                    .placeholder(R.drawable.defaultusericon)
                    .error(R.drawable.defaultusericon)
                    .into(filterperson1img);
            filterperson1.setText(cu.getName());
            filterperson1edit.setVisibility(View.GONE);
            filterperson1del.setVisibility(View.VISIBLE);

            odercon.put("filterperson1", cu.getName().toString() + "");
            SetData(comeForm);
        }
        if (requestCode == 12 && resultCode == 2) {

            String imgpath = ((cu.getPhoto().equals("")) ? "defaultimg" : cu.getPhoto()) + "";
            Picasso.with(getApplicationContext())
                    .load(imgpath)
                    .placeholder(R.drawable.defaultusericon)
                    .error(R.drawable.defaultusericon)
                    .into(filterperson2img);
            filterperson2.setText(cu.getName());
            filterperson2edit.setVisibility(View.GONE);
            filterperson2del.setVisibility(View.VISIBLE);

            odercon.put("filterperson2", cu.getName().toString() + "");
            SetData(comeForm);
        }
        if (requestCode == 13 && resultCode == 2) {

            String imgpath = ((cu.getPhoto().equals("")) ? "defaultimg" : cu.getPhoto()) + "";
            Picasso.with(getApplicationContext())
                    .load(imgpath)
                    .placeholder(R.drawable.defaultusericon)
                    .error(R.drawable.defaultusericon)
                    .into(filterperson3img);
            filterperson3.setText(cu.getName());
            filterperson3edit.setVisibility(View.GONE);
            filterperson3del.setVisibility(View.VISIBLE);

            odercon.put("filterperson3", cu.getName().toString() + "");
            SetData(comeForm);
        }

        if (resultCode == 2) {

            pertitleimg.setImageResource(R.drawable.attachedcontact);
            pertitletext.setTextColor(Color.parseColor("#1ba1e2"));
            pertitletext.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.downaarow, 0);
        } else {

            filterclose.performClick();
        }
//        filterlayoutexpandable.collapse();
//        filterpersonLayoutexpandable.collapse();
    }

    void resetfilter(boolean forperson) {
        filterdatatimestat = false;
        filterdatatimeimg.setImageResource(R.drawable.datetimeiconblack);
        filterdatatime.setTextColor(Color.parseColor("#666666"));
        filterprioritystat = false;
        filterpriorityimg.setImageResource(R.drawable.stariconblack);
        filterpriority.setTextColor(Color.parseColor("#666666"));
        if (forperson) {

            pertitleimg.setImageResource(R.drawable.attachedcontactblack);
            pertitletext.setTextColor(Color.parseColor("#666666"));
            pertitletext.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.downaarowblack, 0);

            filterperson1.setText("Add");
            filterperson1img.setImageResource(R.drawable.attachedcontactblack);
            filterperson1edit.setVisibility(View.VISIBLE);
            filterperson1del.setVisibility(View.GONE);
            odercon.put("filterperson1", "");

            filterperson2.setText("Add");
            filterperson2img.setImageResource(R.drawable.attachedcontactblack);
            filterperson2edit.setVisibility(View.VISIBLE);
            filterperson2del.setVisibility(View.GONE);
            odercon.put("filterperson2", "");

            filterperson3.setText("Add");
            filterperson3img.setImageResource(R.drawable.attachedcontactblack);
            filterperson3edit.setVisibility(View.VISIBLE);
            filterperson3del.setVisibility(View.GONE);
            odercon.put("filterperson3", "");
        }
    }

    void resetpersonfilter() {
        if (odercon.get("filterperson1").equals("") && odercon.get("filterperson2").equals("") && odercon.get("filterperson3").equals("")) {

            pertitleimg.setImageResource(R.drawable.attachedcontactblack);
            pertitletext.setTextColor(Color.parseColor("#666666"));
            pertitletext.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.downaarowblack, 0);

        }
    }

    void filterpersondropdown() {
//        filterpersonLayout.setVisibility(View.GONE);
        filterperson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterpersonLayoutexpandable.toggle();
            }
        });
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            selectItem(position);
        }
    }

    private void SetData(String comeForm) {


        CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getApplicationContext());
//        odercon.clear();
//        odercon.put("filterperson1", "");
//        odercon.put("filterperson2", "");
//        odercon.put("filterperson3", "");

        HashMap<String, String> cond = new HashMap<>();
        //HashMap<String, String> folderName = new HashMap<>();
        if (comeForm.equals("INBOX")) {
//            cond.put("", "");//priority
            cond.put("reminderFor", "device");
        } else if (comeForm.equals("SHARED")) {
            cond.put("reminderFor", "share");
        } else if (comeForm.equals("SENT")) {
            cond.put("reminderFor", "send");
        } else {
            /*
            * Folder name
            * */
            cond.put("flodername", comeForm);
        }


        if (filterdatatimestat) {
//            odercon.put("filterdatatimestat", "cr_datetime.startDate ASC,cr_datetime.startTime ASC,reminder.createddate ASC");
            odercon.put("filterdatatimestat", "custondate ASC");
        } else if (filterprioritystat) {
            odercon.put("filterprioritystat", "reminder.priority DESC,custondate ASC");
        }
        odercon.put("filterperson1", filterperson1.getText().toString().replace("Add", ""));
        odercon.put("filterperson2", filterperson2.getText().toString().replace("Add", ""));
        odercon.put("filterperson3", filterperson3.getText().toString().replace("Add", ""));
//        ArrayList<CReminder> cReminder = crdbh.getAllDataWithSearch(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), comeForm, cond);

        list = crdbh.getAllDataWithSearch(Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                comeForm, cond, odercon);


      /*  *//*
        * particular folder name : pass in hashMap
        * *//*

        list = crdbh.getAllDataWithFolder(Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                comeForm, folderName, odercon);*/


        LinearLayoutManager lLayout = new LinearLayoutManager(getApplicationContext());
        listView.setLayoutManager(lLayout);
        if (comeForm.equals("INBOX")) {
            IndAdapter = new InboxListAdapter(InboxActivity.this, list, "inbox");
            listView.setAdapter(IndAdapter);
        } else {

            cAdapter = new ReminderListAdapter(InboxActivity.this, list, "inbox");
            listView.setAdapter(cAdapter);
        }
    }

    public void clickmakeadmin(String SessionId) {
        Intent i = new Intent(InboxActivity.this, InboxContactActivity.class);
        i.putExtra("moveto", "2");
        i.putExtra("SessionId", SessionId);
        startActivityForResult(i, 21);
    }
}
