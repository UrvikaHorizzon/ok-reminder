package com.horizzon.reminderapp.retrofit.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.horizzon.reminderapp.data.Common;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by horizzon on 5/5/2017.
 */

public class ApiClient {

    public static final String BASE_DOMIN = "https://www.hwpl.in/projects/";
    //    public static final String BASE_DOMIN = "http://192.168.0.53/";
    public static final String BASE_URL = BASE_DOMIN + "reminderapp/api/v1/";
    private static Retrofit retrofitCustom = null,retrofit = null;


    public static Retrofit getClient() {
        if (retrofit == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor).build();
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            retrofit = new Retrofit.Builder()
                    .client(client)
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getCustomClient(String url) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
//        if (retrofit == null || retrofit.baseUrl().host().toString().equals(BASE_DOMIN)) {
        if (retrofitCustom == null) {
            Common.DisplayLog("retrofit.baseUrl().host()", retrofit.baseUrl().host().toString());
            retrofitCustom = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofitCustom;
    }
}