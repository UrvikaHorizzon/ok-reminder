package com.horizzon.reminderapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.horizzon.reminderapp.app.MyCustomButton;
import com.horizzon.reminderapp.app.MyRegEditText;
import com.horizzon.reminderapp.app.MyRegTextView;
import com.horizzon.reminderapp.handler.RUserDbHandler;
import com.horizzon.reminderapp.helper.PrefManager;
import com.horizzon.reminderapp.retrofit.model.ChatNotifactionList;
import com.horizzon.reminderapp.retrofit.model.requestforotp.RequestForOtp;
import com.horizzon.reminderapp.retrofit.model.requestforotp.UserData;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;

import java.io.ByteArrayOutputStream;
import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignFormActivity extends AppCompatActivity implements View.OnClickListener {
    CircleImageView upload_profile_pic;
    String fileData = "";
    MyRegEditText inputFirstName, inputLastName;
    MyRegTextView inputNumber;
    MyCustomButton btnSubmit;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    String monileNumberWithCode, ccCode;
    UserData userdata;
    RUserDbHandler rudbh;
    private PrefManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_form);

        //countryCodeAndMobileNumber
        sharedpreferences = getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            monileNumberWithCode = null;
        } else {
            monileNumberWithCode = extras.getString("countryCodeAndMobileNumber");
            ccCode = extras.getString("ccCode");
        }
        rudbh = new RUserDbHandler(getApplicationContext());
        pref = new PrefManager(this);
        upload_profile_pic = findViewById(R.id.upload_profile_pic);
        btnSubmit = findViewById(R.id.btnSubmit);
        inputFirstName = findViewById(R.id.inputFirstName);
        inputLastName = findViewById(R.id.inputLastName);
        inputNumber = findViewById(R.id.inputNumber);
        upload_profile_pic.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                try {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    upload_profile_pic.setImageBitmap(photo);
                    // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                    Uri tempUri = getImageUri(getApplicationContext(), photo);
                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                    File finalFile = new File(getRealPathFromURI(tempUri));
                    Log.d("TAG", "onActivityResult: camrea img::" + finalFile);
                    fileData = getRealPathFromURI(tempUri);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 22) {
                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                Log.d("path gallery.", picturePath + "");
                upload_profile_pic.setImageBitmap(thumbnail);
                Uri tempUri = getImageUri(getApplicationContext(), thumbnail);
                fileData = getRealPathFromURI(tempUri);
            }
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.upload_profile_pic:
                selectImage();
                break;

            case R.id.btnSubmit:
                String firstName = inputFirstName.getText().toString();
                String lastName = inputLastName.getText().toString();


                if (firstName.isEmpty()) {
                    Toast.makeText(this, "Please enter your first name.", Toast.LENGTH_SHORT).show();
                } else if (lastName.isEmpty()) {
                    Toast.makeText(this, "Please enter your last name.", Toast.LENGTH_SHORT).show();
                } else {
                    getUserdata(firstName, lastName);
                }

             /*   Intent intent = new Intent(SignFormActivity.this, InstallDataActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();*/
                break;

            default:
        }
    }

    /*
     * API call in this api send data on server example: first name, last name,User  image
     * */
    public void getUserdata(String firstName, String lastName) {
        MultipartBody.Part body = null;
        final ProgressDialog pd = new ProgressDialog(SignFormActivity.this);
        pd.setMessage("Loading...");
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        pd.show();
        if (!fileData.equals("")) {
            File file = new File(fileData);
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), file);
            // MultipartBody.Part is used to send also the actual file name
            body = MultipartBody.Part.createFormData("vUPhoto", file.getName(), requestFile);
        }
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody cCode = RequestBody.create(MediaType.parse("text/plain"), ccCode);
        RequestBody mobile1 = RequestBody.create(MediaType.parse("text/plain"), monileNumberWithCode);
        RequestBody userName1 = RequestBody.create(MediaType.parse("text/plain"), firstName);
        RequestBody userLast = RequestBody.create(MediaType.parse("text/plain"), lastName);
        Call<RequestForOtp> call = apiService.getRequestForOtp(cCode, mobile1, userName1, userLast, body);
        call.enqueue(new Callback<RequestForOtp>() {
            @Override
            public void onResponse(Call<RequestForOtp> call, Response<RequestForOtp> response) {

                userdata = response.body().getData().getUserData();


                if (userdata != null) {

                    editor.putString("mobile", userdata.getVUNumber());
                    editor.putString("userName", userdata.getVUName());
                    editor.putString("userLName", userdata.getVLastName());
                    editor.putString("UId", userdata.getIUserId());
                    editor.putString("userImage", userdata.getVUPhoto());
                    editor.putString("countryCode", userdata.getVUContryNumber());
                    editor.apply();

                    // data save in  Sqlite Database
                    if (rudbh.getDataCount() <= 0) {
                        String[] COLUMN = {
                                userdata.getIUserId(),
                                userdata.getVUName(),
                                userdata.getVUNumber(),
                                userdata.getVUEmail(),
                                userdata.getVUPhoto(),
                                userdata.getVUGender(),
                                userdata.getIUStatus()
                        };
                        //   lastid = String.valueOf(rudbh.addData(COLUMN));
                        rudbh.addData(COLUMN, userdata.getIUserId());
                    }
                    //create  session
                    pref.createLogin(userdata.getIUserId());

                    Intent intent = new Intent(SignFormActivity.this, InstallDataActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(SignFormActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RequestForOtp> call, Throwable t) {
                pd.dismiss();
            }
        });
    }

    /*
     * Select image and set in Circle imageView
     * */
    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(SignFormActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    //  File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                    //.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, 1);
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 22);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
}