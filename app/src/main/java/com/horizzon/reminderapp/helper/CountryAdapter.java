/*
 * Copyright (c) 2014-2015 Amberfog.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.horizzon.reminderapp.helper;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.horizzon.reminderapp.R;

import java.util.ArrayList;
import java.util.Locale;

public class CountryAdapter extends BaseAdapter {

    private LayoutInflater mLayoutInflater;
    ArrayList<Country> list;
    Activity context;
    ArrayList<Country> storelist = new ArrayList<Country>();

    public CountryAdapter(Activity context, ArrayList<Country> list) {
        this.context = context;
        this.list = list;
        this.storelist.addAll(list);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mLayoutInflater.inflate(R.layout.item_country_drop, null);
            holder = new ViewHolder();
            holder.mImageView = (ImageView) convertView.findViewById(R.id.image);
            holder.mNameView = (TextView) convertView.findViewById(R.id.country_name);
            holder.mCodeView = (TextView) convertView.findViewById(R.id.country_code);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Country country = list.get(position);
        if (country != null) {
            holder.mNameView.setText(country.getName());
            holder.mCodeView.setText(country.getCountryCodeStr());
            holder.mImageView.setImageResource(country.getResId());
        }
        return convertView;
    }

    private static class ViewHolder {
        public ImageView mImageView;
        public TextView mNameView;
        public TextView mCodeView;
    }

    public void filter(String filterText)
    {
        filterText = filterText.toLowerCase(Locale.getDefault());
        list.clear();
        if(filterText.length()==0)
        {
            list.addAll(storelist);
        }
        else
        {
            for(int i=0; i<storelist.size(); i++)
            {
                Country ud = storelist.get(i);
                if(ud.getName().toLowerCase(Locale.getDefault()).contains(filterText))
                {
                    list.add(ud);
                }
            }
        }
        notifyDataSetChanged();
    }
}
