package com.horizzon.reminderapp.utility;

/**
 * Created by horizzon on 12/15/2016.
 */
public class InboxItem {
    public String reminder_name, reminder_time, reminder_priority, reminder_location, reminder_comment, reminder_attach, reminder_unread, reminder_img_name;

    public String getReminder_img_name() {
        return reminder_img_name;
    }

    public void setReminder_img_name(String reminder_img_name) {
        this.reminder_img_name = reminder_img_name;
    }

    public String getReminder_name() {
        return reminder_name;
    }

    public void setReminder_name(String reminder_name) {
        this.reminder_name = reminder_name;
    }

    public String getReminder_time() {
        return reminder_time;
    }

    public void setReminder_time(String reminder_time) {
        this.reminder_time = reminder_time;
    }

    public String getReminder_priority() {
        return reminder_priority;
    }

    public void setReminder_priority(String reminder_priority) {
        this.reminder_priority = reminder_priority;
    }

    public String getReminder_location() {
        return reminder_location;
    }

    public void setReminder_location(String reminder_location) {
        this.reminder_location = reminder_location;
    }

    public String getReminder_comment() {
        return reminder_comment;
    }

    public void setReminder_comment(String reminder_comment) {
        this.reminder_comment = reminder_comment;
    }

    public String getReminder_attach() {
        return reminder_attach;
    }

    public void setReminder_attach(String reminder_attach) {
        this.reminder_attach = reminder_attach;
    }

    public String getReminder_unread() {
        return reminder_unread;
    }

    public void setReminder_unread(String reminder_unread) {
        this.reminder_unread = reminder_unread;
    }
}
