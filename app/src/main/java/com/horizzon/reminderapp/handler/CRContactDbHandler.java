package com.horizzon.reminderapp.handler;

/**
 * Created by Abhijit Sojitra on 9/3/17.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.horizzon.reminderapp.dao.ContactUser;
import com.horizzon.reminderapp.dao.ReminderContactUser;
import com.horizzon.reminderapp.data.Common;

import java.util.ArrayList;
import java.util.HashMap;

public class CRContactDbHandler {


    // table name
    public static final String TABLE_NAME = "cr_contact";

    public static final String[] COLUMNS = {
            "Id",
            "sesssionId",
            "iUserId",
            "name",
            "number",
            "photo",
            "foraction",
            "consel"
    };

    DatabaseHandler dbh;
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
            + "(" + COLUMNS[0] + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMNS[1] + " TEXT,"
            + COLUMNS[2] + " TEXT,"
            + COLUMNS[3] + " TEXT,"
            + COLUMNS[4] + " TEXT,"
            + COLUMNS[5] + " TEXT,"
            + COLUMNS[6] + " TEXT,"
            + COLUMNS[7] + " TEXT"
            + ");";

    public CRContactDbHandler(Context context) {
        dbh = new DatabaseHandler(context);
    }


    public void addData(String[] c) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        for (int j = 1; j < COLUMNS.length; j++) {
            Common.DisplayLog("columnNames", COLUMNS[j] + " => " + c[j]);
            values.put(COLUMNS[j], c[j]);
        }

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "='" + c[1] + "' AND " + COLUMNS[4] + "='" + c[4] + "' ORDER BY " + COLUMNS[0] + " DESC";

        Cursor cursor = db.rawQuery(query_select, null);
        if (cursor.getCount() <= 0) {
            long id = db.insert(TABLE_NAME, null, values);
        }
        db.close();
    }

    public void updataData(String id, String sesid, String name, String number, String photo) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[3], name);
        values.put(COLUMNS[4], number);
        values.put(COLUMNS[5], photo);

        db.update(TABLE_NAME, values, COLUMNS[0] + " = ? AND " + COLUMNS[1] + " = ?", new String[]{id, sesid});

    }

    public void updataForAction(String sesid, String name) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[6], name);

        db.update(TABLE_NAME, values, COLUMNS[1], new String[]{sesid});

    }

    public void updataConSel(String sesid, String name) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[7], name);

        db.update(TABLE_NAME, values, COLUMNS[1] + "=? AND " + COLUMNS[7] + "=? ", new String[]{sesid, "false"});

    }

    public ArrayList<HashMap<String, String>> getAllData(int id) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> hashmap;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " ORDER BY " + COLUMNS[0] + " DESC ";

        Cursor cursor = db.rawQuery(query_select, null);
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                hashmap = new HashMap<String, String>();
                for (int j = 0; j < columnNames.length; j++) {
                    /*if (ConstantData.log_enable) {
                        Log.e("columnNames", columnNames[j]);
						Log.e("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
					}*/
                    hashmap.put(COLUMNS[j], cursor.getString(j));
                }
                list.add(hashmap);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public ArrayList<ReminderContactUser> getAllContactData() {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<ReminderContactUser> list = new ArrayList<ReminderContactUser>();
        ReminderContactUser ReminderContactUser;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " ORDER BY " + COLUMNS[0] + " ASC ";

        Cursor cursor = db.rawQuery(query_select, null);
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                ReminderContactUser = new ReminderContactUser(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), String.valueOf(cursor.getString(5)), Boolean.valueOf(cursor.getString(6)), String.valueOf(cursor.getString(7)));
                for (int j = 0; j < columnNames.length; j++) {
                    /*if (ConstantData.log_enable) {
                        Log.e("columnNames", columnNames[j]);
						Log.e("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
					}*/
                }
                list.add(ReminderContactUser);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }


    public ArrayList<ReminderContactUser> getAllCurrentSessContactData(String sesid, String consel) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<ReminderContactUser> list = new ArrayList<ReminderContactUser>();
        ReminderContactUser ReminderContactUser;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "='" + sesid + "' AND " + COLUMNS[7] + "='" + consel + "' ORDER BY " + COLUMNS[0] + " ASC ";

        Common.DisplayLog("query_select", query_select);
        Cursor cursor = db.rawQuery(query_select, null);
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                ReminderContactUser = new ReminderContactUser(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), String.valueOf(cursor.getString(5)), Boolean.valueOf(cursor.getString(6)), String.valueOf(cursor.getString(7)));
                for (int j = 0; j < columnNames.length; j++) {
                    /*if (ConstantData.log_enable) {
                        Log.e("columnNames", columnNames[j]);
						Log.e("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
					}*/
                }
                list.add(ReminderContactUser);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public ArrayList<ReminderContactUser> getAllselContactData(String sesid, String consel) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<ReminderContactUser> list = new ArrayList<ReminderContactUser>();
        ReminderContactUser ReminderContactUser;


        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "='" + sesid + "' AND (" + COLUMNS[7] + "='" + consel + "' OR " + COLUMNS[7] + "='false') ORDER BY " + COLUMNS[0] + " ASC ";

        Common.DisplayLog("query_select", query_select);
        Cursor cursor = db.rawQuery(query_select, null);
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                ReminderContactUser = new ReminderContactUser(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), String.valueOf(cursor.getString(5)), Boolean.valueOf(cursor.getString(6)), String.valueOf(cursor.getString(7)));
                for (int j = 0; j < columnNames.length; j++) {
                    /*if (ConstantData.log_enable) {
                        Log.e("columnNames", columnNames[j]);
						Log.e("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
					}*/
                }
                list.add(ReminderContactUser);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }


    public ReminderContactUser getTableSingleDateWihtDao(String sesid, String num) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ReminderContactUser contactuser = null;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "='" + sesid + "' AND " + COLUMNS[4] + "='" + num + "' ORDER BY " + COLUMNS[0] + " DESC";

        Cursor cursor = db.rawQuery(query_select, null);

        Common.DisplayLog("cursor.getCount()", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            Common.DisplayLog("cursor", "yes");
            String[] columnNames = cursor.getColumnNames();
            do {
                contactuser = new ReminderContactUser(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), String.valueOf(cursor.getString(5)), Boolean.valueOf(cursor.getString(6)), String.valueOf(cursor.getString(7)));
                for (int j = 0; j < columnNames.length; j++) {
                    /*if (ConstantData.log_enable) {
                        Log.e("columnNames", columnNames[j]);
						Log.e("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
					}*/
                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
                }
            } while (cursor.moveToNext());
        }
        cursor.close();

        return contactuser;
    }


    public ReminderContactUser getTableSingleDateWihtDao(String sesid, String num, String forWhich) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ReminderContactUser contactuser = null;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "='" + sesid + "' AND " + COLUMNS[4] + "='" + num + "' AND (" + COLUMNS[7] + "='" + forWhich + "' OR " + COLUMNS[7] + "='false') ORDER BY " + COLUMNS[0] + " DESC";
//        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "='" + sesid + "' AND " + COLUMNS[4] + "='" + num + "'  ORDER BY " + COLUMNS[0] + " DESC";

        Cursor cursor = db.rawQuery(query_select, null);

        Common.DisplayLog("cursor.getCount()", cursor.getCount() + "");
        if (cursor.moveToFirst()) {
            Common.DisplayLog("cursor", "yes");
            String[] columnNames = cursor.getColumnNames();
            do {
                contactuser = new ReminderContactUser(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), String.valueOf(cursor.getString(5)), Boolean.valueOf(cursor.getString(6)), String.valueOf(cursor.getString(7)));
                for (int j = 0; j < columnNames.length; j++) {
                    /*if (ConstantData.log_enable) {
                        Log.e("columnNames", columnNames[j]);
						Log.e("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
					}*/
                    Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
                }
            } while (cursor.moveToNext());
        }
        cursor.close();

        return contactuser;
    }


    public ArrayList<ContactUser> getAllContactDataforcontactuser(String sesid) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<ContactUser> list = new ArrayList<ContactUser>();
        ContactUser ContactUser;
        String qu = "";
        if (!sesid.equals("")) {
            qu = " AND " + COLUMNS[1] + "='" + sesid + "'";
        }

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[7] + "='contact'" + qu + "  ORDER BY " + COLUMNS[3] + " ASC ";

        Common.DisplayLog("getAllContactDataforcontactuser", query_select);
        Cursor cursor = db.rawQuery(query_select, null);
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                ContactUser = new ContactUser(cursor.getString(0), cursor.getString(1), cursor.getString(3), cursor.getString(4), cursor.getString(5), Boolean.valueOf(cursor.getString(6)));
                for (int j = 0; j < columnNames.length; j++) {
                    /*if (ConstantData.log_enable) {
                        Log.e("columnNames", columnNames[j]);
						Log.e("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
					}*/
                }
                list.add(ContactUser);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public ArrayList<ReminderContactUser> getAllContactDataformakeadmin(String sesid) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<ReminderContactUser> list = new ArrayList<ReminderContactUser>();
        ReminderContactUser ReminderContactUser;
        String qu = "";
        if (!sesid.equals("")) {
            qu = " AND " + COLUMNS[1] + "='" + sesid + "'" + " AND " + COLUMNS[4] + " in ( SELECT " + CRMakeAdminDbHandler.COLUMNS[2] + " FROM " + CRMakeAdminDbHandler.TABLE_NAME +" WHERE "+ CRMakeAdminDbHandler.COLUMNS[1] + "='" + sesid + "'" + ")";
        }

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[7] + "='contact'" + qu + "  ORDER BY " + COLUMNS[3] + " ASC ";

        Common.DisplayLog("getAllContactDataforcontactuser", query_select);
        Cursor cursor = db.rawQuery(query_select, null);
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                ReminderContactUser = new ReminderContactUser(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), String.valueOf(cursor.getString(5)), Boolean.valueOf(cursor.getString(6)), String.valueOf(cursor.getString(7)));
                for (int j = 0; j < columnNames.length; j++) {
                    /*if (ConstantData.log_enable) {
                        Log.e("columnNames", columnNames[j]);
						Log.e("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
					}*/
                }
                list.add(ReminderContactUser);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public void deleteDataForType(String sessid, String foraction) {
        SQLiteDatabase db = dbh.getWritableDatabase();
        Common.DisplayLog("", "delete from " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = '" + sessid + "' AND " + COLUMNS[7] + " = '" + foraction + "'");
        db.execSQL("delete from " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = '" + sessid + "' AND " + COLUMNS[7] + " = '" + foraction + "'");

        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }

    public void deleteTempNumber(String sessid, String num) {
        SQLiteDatabase db = dbh.getWritableDatabase();
        Common.DisplayLog("", "delete from " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = '" + sessid + "' AND " + COLUMNS[4] + " = '" + num + "'");
        db.execSQL("delete from " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = '" + sessid + "' AND " + COLUMNS[4] + " = '" + num + "'");

        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }

    public void resetTempContdata(String sessid) {
        SQLiteDatabase db = dbh.getWritableDatabase();
        db.execSQL("delete from " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = '" + sessid + "' AND " + COLUMNS[7] + " = 'false'");
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }

    public void resetTempdata(String sessid) {
        SQLiteDatabase db = dbh.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.execSQL("delete from " + TABLE_NAME + " WHERE " + COLUMNS[1] + " = '" + sessid + "'");
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }


    public void resetTable() {
        SQLiteDatabase db = dbh.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.execSQL("delete from " + TABLE_NAME);
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }

}
