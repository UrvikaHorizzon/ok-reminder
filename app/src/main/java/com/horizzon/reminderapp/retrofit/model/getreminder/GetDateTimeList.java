
package com.horizzon.reminderapp.retrofit.model.getreminder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetDateTimeList {

    @SerializedName("iId")
    @Expose
    private String iId;
    @SerializedName("vSesssionId")
    @Expose
    private String vSesssionId;
    @SerializedName("iUserId")
    @Expose
    private String iUserId;
    @SerializedName("vDtFor")
    @Expose
    private String vDtFor;
    @SerializedName("vStartDate")
    @Expose
    private String vStartDate;
    @SerializedName("vStartTime")
    @Expose
    private String vStartTime;
    @SerializedName("vEndDate")
    @Expose
    private String vEndDate;
    @SerializedName("vEndTime")
    @Expose
    private String vEndTime;
    @SerializedName("tDays")
    @Expose
    private String tDays;
    @SerializedName("iStatus")
    @Expose
    private String iStatus;
    @SerializedName("dCreatedDateTime")
    @Expose
    private String dCreatedDateTime;

    public String getIId() {
        return iId;
    }

    public void setIId(String iId) {
        this.iId = iId;
    }

    public String getVSesssionId() {
        return vSesssionId;
    }

    public void setVSesssionId(String vSesssionId) {
        this.vSesssionId = vSesssionId;
    }

    public String getIUserId() {
        return iUserId;
    }

    public void setIUserId(String iUserId) {
        this.iUserId = iUserId;
    }

    public String getVDtFor() {
        return vDtFor;
    }

    public void setVDtFor(String vDtFor) {
        this.vDtFor = vDtFor;
    }

    public String getVStartDate() {
        return vStartDate;
    }

    public void setVStartDate(String vStartDate) {
        this.vStartDate = vStartDate;
    }

    public String getVStartTime() {
        return vStartTime;
    }

    public void setVStartTime(String vStartTime) {
        this.vStartTime = vStartTime;
    }

    public String getVEndDate() {
        return vEndDate;
    }

    public void setVEndDate(String vEndDate) {
        this.vEndDate = vEndDate;
    }

    public String getVEndTime() {
        return vEndTime;
    }

    public void setVEndTime(String vEndTime) {
        this.vEndTime = vEndTime;
    }

    public String getTDays() {
        return tDays;
    }

    public void setTDays(String tDays) {
        this.tDays = tDays;
    }

    public String getIStatus() {
        return iStatus;
    }

    public void setIStatus(String iStatus) {
        this.iStatus = iStatus;
    }

    public String getDCreatedDateTime() {
        return dCreatedDateTime;
    }

    public void setDCreatedDateTime(String dCreatedDateTime) {
        this.dCreatedDateTime = dCreatedDateTime;
    }

}
