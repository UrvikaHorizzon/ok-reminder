package com.horizzon.reminderapp.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.adapter.ContactBoxListAdapter;
import com.horizzon.reminderapp.dao.CReminder;
import com.horizzon.reminderapp.dao.ReminderContactUser;
import com.horizzon.reminderapp.dao.ReminderDateTime;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.CRContactDbHandler;
import com.horizzon.reminderapp.handler.CRDateTimeDbHandler;
import com.horizzon.reminderapp.handler.CreateReminderDbHandler;
import com.horizzon.reminderapp.utility.DateUtilitys;

import java.util.ArrayList;
import java.util.Date;

public class DetailTabFragment extends Fragment {

    TextView rSub, reminderTime, location_data;
    ImageView imgPriority1, imgPriority2, imgPriority3, icon_lock;
    ContactBoxListAdapter contactboxlistadapter;
    RecyclerView participantslist;
    String SessionId;
    ScrollView detaitabscroll;
    SharedPreferences sharedpreferences;
    String mobile;

    public DetailTabFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_detail_tab, container, false);

        SessionId = Common.getSharedPreferences(getActivity(), "SessionId", "");
        Common.DisplayLog("", "SessionId " + SessionId);
        rSub = (TextView) v.findViewById(R.id.rSub);
        reminderTime = (TextView) v.findViewById(R.id.reminderTime);
        location_data = (TextView) v.findViewById(R.id.location_data);

        imgPriority1 = (ImageView) v.findViewById(R.id.imgPriority1);
        imgPriority2 = (ImageView) v.findViewById(R.id.imgPriority2);
        imgPriority3 = (ImageView) v.findViewById(R.id.imgPriority3);
        icon_lock = (ImageView) v.findViewById(R.id.icon_lock);

        participantslist = (RecyclerView) v.findViewById(R.id.participantslist);
        detaitabscroll = (ScrollView) v.findViewById(R.id.detaitabscroll);
        sharedpreferences = getContext().getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        // Inflate the layout for this fragment
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        loadSavedData(SessionId);
        detaitabscroll.smoothScrollTo(0, 0);
    }


    public void loadSavedData(String SessId) {


        Common.DisplayLog("", "loadSavedData " + SessId);
        CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getActivity().getApplicationContext());
//        RContactDbHandler rcdbh = new RContactDbHandler(getApplicationContext());


        CReminder cReminder = crdbh.getTableSingleDateWihtDao(SessId);


        //Common.DisplayLog("cReminder.getSubject()", cReminder.getSubject() + "");
        // set Subject
        rSub.setText(cReminder.getSubject());

        // set Priority
        if (cReminder.getPriority().equals("1")) {
            imgPriority1.setVisibility(View.VISIBLE);
        } else if (cReminder.getPriority().equals("2")) {
            imgPriority1.setVisibility(View.VISIBLE);
            imgPriority2.setVisibility(View.VISIBLE);
        } else if (cReminder.getPriority().equals("3")) {
            imgPriority1.setVisibility(View.VISIBLE);
            imgPriority2.setVisibility(View.VISIBLE);
            imgPriority3.setVisibility(View.VISIBLE);
        } else if (cReminder.getPriority().equals("0")) {
            imgPriority1.setVisibility(View.GONE);
            imgPriority2.setVisibility(View.GONE);
            imgPriority3.setVisibility(View.GONE);
        }


        // set Date Time
        CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(getActivity().getApplicationContext());
        ReminderDateTime rdt = crdtdbh.getTableSingleDateWihtDao(SessId);
        Common.DisplayLog("crdtdbh.getDataCount(SessionId)", crdtdbh.getDataCount(SessId) + "");


        DateUtilitys dateUtl = new DateUtilitys();

        Date date;
        if (crdtdbh.getDataCount(SessId) > 0) {
            date = dateUtl.FormetDate(rdt.getStartDate() + " " + rdt.getStartTime());
        } else {
            Common.DisplayLog("cr.getCreateddate()", cReminder.getCreateddate() + "");
            date = dateUtl.FormetDate(cReminder.getCreateddate());

        }
        dateUtl.setforDisplayTitle(date);
        reminderTime.setText(dateUtl.getforDisplayTitle());

        icon_lock.setVisibility(View.GONE);

        if (cReminder.getLockstatus().equals("lock")) {
            icon_lock.setVisibility(View.VISIBLE);
        }


        // set Contact
        CRContactDbHandler crcdbh = new CRContactDbHandler(getActivity().getApplicationContext());
        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllCurrentSessContactData(SessId, "contact");
//        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllContactData();
        mobile = sharedpreferences.getString("mobile", "");
        ArrayList<ReminderContactUser> remindercontact = new ArrayList<>();
        for (int i = 0; i < remindercontactuser.size(); i++) {
           // remindercontact.clear();
            if (!remindercontactuser.get(i).getNumber().matches(mobile)) {
                remindercontact.add(remindercontactuser.get(i));
            }
        }

        participantslist.setAdapter(null);
        contactboxlistadapter = new ContactBoxListAdapter(getActivity(), remindercontact, "DetailViewActivity");
        LinearLayoutManager lLayout = new GridLayoutManager(getActivity(), 4);
        participantslist.setLayoutManager(lLayout);
        participantslist.setAdapter(contactboxlistadapter);
        Common.DisplayLog("remindercontactuser", remindercontactuser.size() + "");


    }


}
