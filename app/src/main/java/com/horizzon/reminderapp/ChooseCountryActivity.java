package com.horizzon.reminderapp;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.horizzon.reminderapp.helper.Country;
import com.horizzon.reminderapp.helper.CountryAdapter;
import com.horizzon.reminderapp.helper.PhoneUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Locale;

public class ChooseCountryActivity extends AppCompatActivity {


    public SparseArray<ArrayList<Country>> mCountriesMap = new SparseArray<ArrayList<Country>>();

    public PhoneNumberUtil mPhoneNumberUtil = PhoneNumberUtil.getInstance();
    public CountryAdapter mAdapter;
    ListView lv;
    public String mLastEnteredPhone;
    ArrayList<Country> listCountry;
    EditText editSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_country);
        lv = (ListView) findViewById(R.id.listFlag);
        editSearch = (EditText)findViewById(R.id.editSearch);
        Intent in = getIntent();
        mLastEnteredPhone = in.getStringExtra("phnno");
        listCountry = new ArrayList<Country>(233);

        new AsyncPhoneInitTask(getApplicationContext()).execute();

        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                String name = editSearch.getText().toString().toLowerCase(Locale.getDefault());
                mAdapter.filter(name);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
//                cAdapter.filter(text);
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Country c = listCountry.get(position);
                if (mLastEnteredPhone != null && mLastEnteredPhone.startsWith(c.getCountryCodeStr())) {
                    return;
                }
                mLastEnteredPhone = c.getName() + " (" + c.getCountryCodeStr() + ")";
                Intent intent = new Intent();
                intent.putExtra("MESSAGE",mLastEnteredPhone);
                intent.putExtra("cName",c.getName());
                intent.putExtra("CountryCode",c.getCountryCodeStr());
                setResult(2,intent);
                finish();
//                viewPager.setCurrentItem(0);
            }
        });
    }

    public class AsyncPhoneInitTask extends AsyncTask<Void, Void, ArrayList<Country>> {
        private int mSpinnerPosition = -1;
        private Context mContext;

        public AsyncPhoneInitTask(Context context) {
            mContext = context;
        }

        @Override
        protected ArrayList<Country> doInBackground(Void... params) {
            ArrayList<Country> data = new ArrayList<Country>(233);
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(mContext.getApplicationContext().getAssets().open("countries.dat"), "UTF-8"));
                // do reading, usually loop until end of file reading
                String line;
                int i = 0;
                while ((line = reader.readLine()) != null) {
                    //process line
                    Country c = new Country(mContext, line, i);
                    data.add(c);
                    listCountry.add(c);
                    Log.d("CountryHASH",data.get(i).getCountryCode()+"\n"+data.get(i).getName());
                    ArrayList<Country> list = mCountriesMap.get(c.getCountryCode());
                    if (list == null) {
                        list = new ArrayList<Country>();
                        mCountriesMap.put(c.getCountryCode(), list);
                    }
                    list.add(c);
                    i++;
                }
            } catch (IOException e) {
                //log the exception
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        //log the exception
                    }
                }
            }
//            if (!TextUtils.isEmpty(mPhoneEdit.getText())) {
//                return data;
//            }

            String countryRegion = PhoneUtils.getCountryRegionFromPhone(mContext);
            int code = mPhoneNumberUtil.getCountryCodeForRegion(countryRegion);
            ArrayList<Country> list = mCountriesMap.get(code);
            if (list != null) {
                for (Country c : list) {
                    if (c.getPriority() == 0) {
                        mSpinnerPosition = c.getNum();
                        break;
                    }
                }
            }
            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<Country> data) {
            mAdapter = new CountryAdapter(ChooseCountryActivity.this,listCountry);
            lv.setAdapter(mAdapter);
            if (mSpinnerPosition > 0) {
//                mSpinner.setSelection(mSpinnerPosition);
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("MESSAGE",mLastEnteredPhone);
        setResult(2,intent);
        finish();
    }
}
