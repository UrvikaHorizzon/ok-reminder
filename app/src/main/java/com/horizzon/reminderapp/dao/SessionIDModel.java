package com.horizzon.reminderapp.dao;

import java.io.Serializable;

public class SessionIDModel  implements Serializable {
    String sesssionId;
    String id;
    public SessionIDModel(String id, String sesssionId){
        this.id = id;
        this.sesssionId = sesssionId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSesssionId() {
        return sesssionId;
    }

    public void setSesssionId(String sesssionId) {
        this.sesssionId = sesssionId;
    }
}
