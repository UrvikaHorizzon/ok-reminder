package com.horizzon.reminderapp.app;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by horizzon on 1/19/2017.
 */

public class MyCustomRegAtxtView extends androidx.appcompat.widget.AppCompatAutoCompleteTextView{
    public MyCustomRegAtxtView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(),"fonts/arial.ttf"));
    }
}
