package com.horizzon.reminderapp.app;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by horizzon on 1/18/2017.
 */

public class MyCustomEditText extends androidx.appcompat.widget.AppCompatEditText{
    public MyCustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(),"fonts/arialbd.ttf"));
    }
}
