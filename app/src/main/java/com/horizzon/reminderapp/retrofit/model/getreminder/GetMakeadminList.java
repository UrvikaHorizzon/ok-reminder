
package com.horizzon.reminderapp.retrofit.model.getreminder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetMakeadminList {

    @SerializedName("iId")
    @Expose
    private String iId;
    @SerializedName("vSesssionId")
    @Expose
    private String vSesssionId;
    @SerializedName("vUserId")
    @Expose
    private String vUserId;
    @SerializedName("tExtra")
    @Expose
    private String tExtra;
    @SerializedName("dCreatedDateTime")
    @Expose
    private String dCreatedDateTime;

    public String getIId() {
        return iId;
    }

    public void setIId(String iId) {
        this.iId = iId;
    }

    public String getVSesssionId() {
        return vSesssionId;
    }

    public void setVSesssionId(String vSesssionId) {
        this.vSesssionId = vSesssionId;
    }

    public String getVUserId() {
        return vUserId;
    }

    public void setVUserId(String vUserId) {
        this.vUserId = vUserId;
    }

    public String getTExtra() {
        return tExtra;
    }

    public void setTExtra(String tExtra) {
        this.tExtra = tExtra;
    }

    public String getDCreatedDateTime() {
        return dCreatedDateTime;
    }

    public void setDCreatedDateTime(String dCreatedDateTime) {
        this.dCreatedDateTime = dCreatedDateTime;
    }

}
