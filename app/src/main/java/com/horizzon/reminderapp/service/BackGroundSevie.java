package com.horizzon.reminderapp.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationManagerCompat;

import com.horizzon.reminderapp.R;

public class BackGroundSevie extends Service {

    public int counter = 0;
    public static final int NOTIFICATION_REQUEST_CODE = 100;

    public BackGroundSevie() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        /*if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O)
            startMyOwnForeground();
        else
            startForeground(1, new Notification());*/
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void startMyOwnForeground() {
        Notification notification = null;
        NotificationManager mNotificationManager;
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        //soundUri = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.tone);
        MediaPlayer player = MediaPlayer.create(getApplicationContext(), soundUri);
        player.start();
        AudioManager manager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        manager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
        Notification.Builder builder = new Notification.Builder(getApplicationContext());
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());
        Intent notificationIntent;

     /*   if (intent != null) {
            notificationIntent = intent;
        }
        else {
            notificationIntent = new Intent(context, HomeActivity.class);
        }

        PendingIntent pendingIntent = PendingIntent.getActivity((context), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
*/
        mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        AudioAttributes audioAttributes;


        notification = builder
                .setContentTitle("Reminder App")
                .setContentText("Reminder App")
                .setTicker(getString(R.string.app_name))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setSound(soundUri)
                .setLights(Color.RED, 3000, 3000)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setWhen(System.currentTimeMillis())
                //  .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                // .setContentIntent(pendingIntent)
                .build();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "com.horizzon.reminderapp";
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    "com.horizzon.reminderapp",
                    NotificationManager.IMPORTANCE_HIGH);
            mNotificationManager.createNotificationChannel(channel);
            builder.setChannelId(channelId);
            builder.setDefaults(Notification.DEFAULT_ALL);

            audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .build();

            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelId, NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setSound(soundUri, audioAttributes);
            mNotificationManager.createNotificationChannel(notificationChannel);
           // startForeground(2, notification);
        }
        /* notification = builder
                .setContentTitle("Reminder App")
                .setContentText("Reminder App")
                .setTicker(getString(R.string.app_name))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setSound(soundUri)
                .setLights(Color.RED, 3000, 3000)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setWhen(System.currentTimeMillis())
                //  .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
              // .setContentIntent(pendingIntent)
                .build();*/

        notificationManager.notify(NOTIFICATION_REQUEST_CODE, notification);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

   /* @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        WakefulBroadcastReceiver.completeWakefulIntent(intent);
    }*/


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("ClearFromRecentService", "Service Started");
        Toast.makeText(this, "Background notification.", Toast.LENGTH_SHORT).show();
       /* AlarmBroadCustReciver alarmBroadCustReciver = new AlarmBroadCustReciver();
        alarmBroadCustReciver.onReceive(getApplicationContext(), intent);*/
        /*NotificationHelper.showNewNotification
                (getApplicationContext(), intent, "Reminder App.",
                        "Reminder Application....!!!");*/
        //return START_NOT_STICKY;
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /*Log.d("ClearFromRecentService", "Service Destroyed");
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("restartservice");
        broadcastIntent.setClass(this, AlarmBroadCustReciver.class);
        this.sendBroadcast(broadcastIntent);*/
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e("ClearFromRecentService", "END");
        super.onTaskRemoved(rootIntent);
       // AlarmBroadCustReciver alarmBroadCustReciver = new AlarmBroadCustReciver();
       //alarmBroadCustReciver.onReceive(getApplicationContext(), rootIntent);
        //  stopSelf();
    }
}
