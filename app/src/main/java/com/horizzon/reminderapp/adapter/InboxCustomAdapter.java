package com.horizzon.reminderapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.dao.CReminder;
import com.horizzon.reminderapp.dao.ReminderContactUser;
import com.horizzon.reminderapp.dao.ReminderDateTime;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.CRAttachmentDbHandler;
import com.horizzon.reminderapp.handler.CRCommentDbHandler;
import com.horizzon.reminderapp.handler.CRContactDbHandler;
import com.horizzon.reminderapp.handler.CRDateTimeDbHandler;
import com.horizzon.reminderapp.handler.CRLocationDbHandler;
import com.horizzon.reminderapp.utility.DateUtilitys;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by horizzon on 12/15/2016.
 */
public class InboxCustomAdapter extends BaseAdapter {
    Activity context;
    private ImageLoader imageLoader;
    ArrayList<CReminder> list;

    public InboxCustomAdapter(Activity context, ArrayList<CReminder> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Holder holder;

        if (convertView == null) {
            holder = new Holder();

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.custom_inbox_layout, null);

            holder.unameName = (TextView) convertView.findViewById(R.id.unameName);
            holder.name = (TextView) convertView.findViewById(R.id.reminderName);
            holder.time = (TextView) convertView.findViewById(R.id.reminderTime);
            holder.imgProfile = (ImageView) convertView.findViewById(R.id.imgProfile);
            holder.unread = (TextView) convertView.findViewById(R.id.reminderUnread);
            holder.con_image = (ImageView) convertView.findViewById(R.id.con_image);
            holder.imgPriority1 = (ImageView) convertView.findViewById(R.id.imgPriority1);
            holder.imgPriority2 = (ImageView) convertView.findViewById(R.id.imgPriority2);
            holder.imgPriority3 = (ImageView) convertView.findViewById(R.id.imgPriority3);
            holder.imgAttachment = (ImageView) convertView.findViewById(R.id.imgAttach);
            holder.imgComment = (ImageView) convertView.findViewById(R.id.imgComt);
            holder.imgLocation = (ImageView) convertView.findViewById(R.id.imgLocation);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        CReminder cr = list.get(position);

        String SessionId = cr.getSesssionId();

        holder.name.setText(cr.getSubject());

        CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(context);
        ReminderDateTime rdt = crdtdbh.getTableSingleDateWihtDao(SessionId);
        Common.DisplayLog("crdtdbh.getDataCount(SessionId)", crdtdbh.getDataCount(SessionId) + "");


        DateUtilitys dateUtl = new DateUtilitys();

        Date date;
        if(crdtdbh.getDataCount(SessionId)>0){
            date = dateUtl.FormetDate(rdt.getStartDate() + " " + rdt.getStartTime());
        }else{
            Common.DisplayLog("cr.getCreateddate()",cr.getCreateddate()+"");
            date = dateUtl.FormetDate(cr.getCreateddate());

        }
        dateUtl.setforDisplayTitle(date);
        holder.time.setText(dateUtl.getDay()+" "+dateUtl.getMonth());


        holder.imgPriority1.setImageResource(R.drawable.prioritywhite);
        holder.imgPriority2.setImageResource(R.drawable.prioritywhite);
        holder.imgPriority3.setImageResource(R.drawable.prioritywhite);
        if (cr.getPriority().equals("1")) {
            holder.imgPriority1.setImageResource(R.drawable.priorityiconyellow);
        } else if (cr.getPriority().equals("2")) {
            holder.imgPriority1.setImageResource(R.drawable.priorityiconyellow);
            holder.imgPriority2.setImageResource(R.drawable.priorityiconyellow);
        } else if (cr.getPriority().equals("3")) {
            holder.imgPriority1.setImageResource(R.drawable.priorityiconyellow);
            holder.imgPriority2.setImageResource(R.drawable.priorityiconyellow);
            holder.imgPriority3.setImageResource(R.drawable.priorityiconyellow);
        }


        // set Contact
        CRContactDbHandler crcdbh = new CRContactDbHandler(context);
        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllCurrentSessContactData(SessionId, "contact");
//        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllContactData();

        Common.DisplayLog("remindercontactuser", remindercontactuser.size() + "");
        String tempToText = "";
        int i = remindercontactuser.size();
        if(i>0) {
            if (1 <= i) {
                tempToText += remindercontactuser.get(0).getName() + ",";
            }
            String imgpath = ((remindercontactuser.get(0).getPhoto().equals("")) ? "defaultimg" : remindercontactuser.get(0).getPhoto()) + "";
            Common.DisplayLog("imgpath",imgpath+"");
            Picasso.with(context)
                    .load(imgpath)
                    .placeholder(R.drawable.defaultusericon)
                    .error(R.drawable.defaultusericon)
                    .into(holder.con_image);
        }


        tempToText = (tempToText != null && !tempToText.equals("")) ? tempToText.substring(0, tempToText.length() - 1) : "";
        if (i > 2) {
            tempToText += " & " + (i - 1) + " others";
        }

        holder.unameName.setText(tempToText);


        // set Attachment
        CRAttachmentDbHandler cradbh = new CRAttachmentDbHandler(context);
        Common.DisplayLog("cradbh.getDataCount(SessionId)", cradbh.getDataCount(SessionId, "done") + "");
        if (cradbh.getDataCount(SessionId, "done") > 0) {
            holder.imgAttachment.setImageResource(R.drawable.attachiconblue);
        } else {
            holder.imgAttachment.setImageResource(R.drawable.attachicon);
        }

        // set Comment

        CRCommentDbHandler crcodbh = new CRCommentDbHandler(context);
        Common.DisplayLog("crcodbh.getDataCount(SessionId)", crcodbh.getDataCount(SessionId) + "");
        if (crcodbh.getDataCount(SessionId) > 0) {
            holder.imgComment.setImageResource(R.drawable.comticonblue);
        } else {
            holder.imgComment.setImageResource(R.drawable.comticon);
        }

        // set Date Time
        /*CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(context);
        Common.DisplayLog("crdtdbh.getDataCount(SessionId)", crdtdbh.getDataCount(SessionId) + "");
        if (crdtdbh.getDataCount(SessionId) > 0) {
            holder.imgLocation.setImageResource(R.drawable.datetimebluerounded);
        } else {
            holder.imgLocation.setImageResource(R.drawable.datetimerounded);
        }*/

        // set Location
        CRLocationDbHandler crldbh = new CRLocationDbHandler(context);
        Common.DisplayLog("crldbh.getDataCount(SessionId)", crldbh.getDataCount(SessionId) + "");
        if (crldbh.getDataCount(SessionId) > 0) {
            holder.imgLocation.setImageResource(R.drawable.locationiconblue);
        } else {
            holder.imgLocation.setImageResource(R.drawable.locationicon);
        }


        return convertView;
    }

    public class Holder {
        TextView unameName,name,time,unread;
        ImageView con_image;
        ImageView imgPriority1;
        ImageView imgPriority2;
        ImageView imgPriority3;
        ImageView imgLocation;
        ImageView imgComment;
        ImageView imgAttachment;
        ImageView imgProfile;
    }
}
