package com.horizzon.reminderapp.handler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.horizzon.reminderapp.dao.SessionIDModel;
import com.horizzon.reminderapp.data.Common;

import java.util.ArrayList;
import java.util.List;

public class CRSessoinIdDbHandler {

    // table name
    public static final String TABLE_NAME = "sessionId";
    List<SessionIDModel> list = new ArrayList<>();
    public static final String[] COLUMNS = {
            "Id",
            "sesssionId"
    };
    DatabaseHandler dbh;

    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
            + "(" + COLUMNS[0] + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMNS[1] + " TEXT"
            + ");";

    public CRSessoinIdDbHandler(Context context) {
        dbh = new DatabaseHandler(context);
    }

    public void addData(String[] c) {

        SQLiteDatabase   db = dbh.getWritableDatabase();
        ContentValues values = new ContentValues();
        for (int j = 0; j < COLUMNS.length; j++) {
            Common.DisplayLog("columnNames : session :: ", COLUMNS[j]);
            values.put(COLUMNS[j], c[j]);
        }

        db.insert(TABLE_NAME, null, values);
        db.close();
    }

  /*  public void getAllSessionId() {
        Cursor cursor = db.rawQuery("select * from table", null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String name = cursor.getString(cursor.getColumnIndex("sesssionId"));

               // list.add(name);
                cursor.moveToNext();
            }
        }
    }*/
  public void resetTable() {
      SQLiteDatabase db = dbh.getWritableDatabase();
      db.delete(TABLE_NAME, null, null);
      db.execSQL("delete from " + TABLE_NAME);
      // db.execSQL("TRUNCATE table " + TABLE_NAME);
      db.close();
  }
    public ArrayList<SessionIDModel> getAllElements() {

        ArrayList<SessionIDModel> list = new ArrayList<SessionIDModel>();
        SessionIDModel sessionIDModel;
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME + "ASC";
        SQLiteDatabase  db = dbh.getReadableDatabase();
        try {

            Cursor cursor = db.rawQuery(selectQuery, null);
            try {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    String[] columnNames = cursor.getColumnNames();
                    do {
                        sessionIDModel =new SessionIDModel(cursor.getString(0), cursor.getString(1));

                      /*  SessionIDModel obj = new SessionIDModel();
                        //only one column
                        obj.setSesssionId(cursor.getString(0));*/

                        //you could add additional columns here..
                        for (int j = 0; j < columnNames.length; j++) {

                            Common.DisplayLog("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));

                        }
                        list.add(sessionIDModel);
                        Log.d("TAG", "getAllElements: list :: " + list);
                    } while (cursor.moveToNext());
                }

            } finally {
                try { cursor.close(); } catch (Exception ignore) {}
            }

        } finally {
            try { db.close(); } catch (Exception ignore) {}
        }

        return list;
    }
}
