package com.horizzon.reminderapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.horizzon.reminderapp.adapter.CommentAdapter;
import com.horizzon.reminderapp.dao.ReminderComment;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.CRCommentDbHandler;
import com.horizzon.reminderapp.retrofit.model.reminder.Reminder;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReminderCommentsActivity extends AppCompatActivity {

    ImageView imgBack, imgHome;
    RecyclerView commentList;
    ArrayList<ReminderComment> commentArrayList;
    String SessionId, action;
    EditText textComment;
    ImageView btnsend;
    CRCommentDbHandler crcdbh;
    CommentAdapter commentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_comments);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/


        action = getIntent().getStringExtra("action");
        if (action == null || action.equals("")) {
            action = "";
        }

        SessionId = Common.getSharedPreferences(getApplicationContext(), "SessionId", "");
        crcdbh = new CRCommentDbHandler(getApplicationContext());

        commentList = (RecyclerView) findViewById(R.id.commentList);

        imgBack = (ImageView) findViewById(R.id.backBtn);
        imgHome = (ImageView) findViewById(R.id.homeBtn);

        textComment = (EditText) findViewById(R.id.textComment);
        btnsend = (ImageView) findViewById(R.id.btnsend);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (action.equals("edit")) {
                    onBackPressed();
                } else {
                    gobacktocr();
                }
            }
        });

        imgHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ReminderCommentsActivity.this, HomeActivity.class);
                startActivity(in);
            }
        });

        textComment.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                getComment();

                            }
                        },
                        300);
                return false;
            }
        });
        textComment.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                getComment();

                            }
                        },
                        300);
            }
        });
        btnsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnsend.setEnabled(false);
                Common.DisplayLog("textComment.getText()", textComment.getText() + "");
                if (textComment.getText().toString() != null && !textComment.getText().toString().equals("")) {
                    String[] COLUMN = {
                            "",
                            SessionId,
                            Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                            textComment.getText().toString(),
                            "group",
                            Common.getSQLDateTime()
                    };
                    String lastid = String.valueOf(crcdbh.addData(COLUMN));

                    getComment();

                    if (action.equals("edit")) {
                        ReminderComment rc = new ReminderComment("", SessionId, Common.getSharedPreferences(getApplicationContext(), "userId", "0"), textComment.getText().toString(),"group", Common.getSQLDateTime());
                        SavedEditDataOnServer(rc);
                    }
                    textComment.setText("");
                } else {
                    Common.ShowToast(getApplicationContext(), "Please enter  something...");
                }
                btnsend.setEnabled(true);
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (action.equals("edit")) {
            super.onBackPressed();
        } else {
            gobacktocr();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getComment();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getComment();
    }

    private void gobacktocr() {
        Intent in = null;
        in = new Intent(getApplicationContext(), CreateReminderActivity.class);
        in.putExtra("newrem", "no");
        startActivity(in);

    }

    private void getComment() {


        commentArrayList = crcdbh.getAllCurrentSessDataWithType(SessionId);
        Common.DisplayLog("commentArrayList count", commentArrayList.size() + "");

        commentList.setAdapter(null);
//                        commentAdapter = new ImageGridCustomAdapter(AttachmentActivity.this, commentArrayList, AttachmentActivity.this);
        commentAdapter = new CommentAdapter(ReminderCommentsActivity.this, commentArrayList, "add");
        LinearLayoutManager lLayout = new LinearLayoutManager(getApplicationContext());
//                        lLayout.setStackFromEnd(true);
//                        lLayout.setReverseLayout(true);
        commentList.setLayoutManager(lLayout);
        commentList.setAdapter(commentAdapter);

        commentList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(textComment.getWindowToken(), 0);
                return false;
            }
        });
//        commentAdapter.notifyDataSetChanged();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        commentList.scrollToPosition(commentArrayList.size() - 1);

                    }
                },
                500);

    }

    public void SavedEditDataOnServer(ReminderComment rc) {

        if (Common.hasInternetConnection(getApplicationContext())) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<Reminder> callComment = apiService.addComment(rc.getSesssionId(),
                    rc.getUserId(), rc.getComment(), "1",
                    rc.gettCommentFor());
            callComment.enqueue(new Callback<Reminder>() {
                @Override
                public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                    int statusCode = response.code();
                    Common.DisplayLog("response.body()", response.body().toString());
                    boolean status = response.body().getStatus();
                    Common.DisplayLog("status", status + "");
                    Common.DisplayLog("getMessage", response.body().getMessage() + "");
                    if (status) {

//                        Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                    } else {
                        Common.ShowToast(getApplicationContext(), response.body().getMessage());

                    }
//                    pd.dismiss();

                }

                @Override
                public void onFailure(Call<Reminder> call, Throwable t) {
                    Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
//                    pd.dismiss();
                }


            });
        }
    }

}
