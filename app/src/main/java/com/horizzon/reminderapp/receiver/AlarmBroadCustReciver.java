package com.horizzon.reminderapp.receiver;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.os.SystemClock;
import android.os.Vibrator;
import android.util.Log;

import androidx.work.OneTimeWorkRequest;

import com.horizzon.reminderapp.HomeActivity;
import com.horizzon.reminderapp.service.MyWorker;

import java.util.ArrayList;
import java.util.Calendar;

import static android.content.Context.POWER_SERVICE;

public class AlarmBroadCustReciver extends BroadcastReceiver {
    //WakefulBroadcastReceiver BroadcastReceiver
    AlarmManager mAlarmManager;
    PendingIntent mPendingIntent;
    public Vibrator vibrator;
    MediaPlayer mp;
    long[] pattern = {0L, 250L, 200L, 250L, 200L, 250L, 200L, 250L, 200L,
            250L, 200L, 250L, 200L, 250L, 200L};

    public static String NOTIFICATION_ID = "notification-id";
    public static String NOTIFICATION = "notification";
    OneTimeWorkRequest workRequest = new OneTimeWorkRequest.Builder(MyWorker.class).build();

    public AlarmBroadCustReciver() {

    }


    @SuppressLint("InvalidWakeLockTag")
    @Override
    public void onReceive(Context context, Intent intent) {
        PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();
        if(!isScreenOn)
        {
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP |PowerManager.ON_AFTER_RELEASE,"MyLock");
            wl.acquire(10000);
            PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MyCpuLock");

            wl_cpu.acquire(10000);
        }
        String SessionId = intent.getStringExtra("SessionId");
        Log.d("TAG", "onReceive: " + SessionId);
        //  String Subject = intent.getStringExtra("Subject");
        Intent intentToFire = new Intent(context, HomeActivity.class);
        AudioManager manager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        manager.setStreamVolume(AudioManager.STREAM_MUSIC, 50, 0);
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        //   Uri  soundUri = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.tone);
        MediaPlayer player = MediaPlayer.create(context, soundUri);
        player.start();
        NotificationHelper.showNewNotification
                (context, intentToFire, "Reminder form Application.",
                        "You have one reminder", null);


        /*NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        Notification notification = intent.getParcelableExtra(NOTIFICATION);
        int id = intent.getIntExtra(NOTIFICATION_ID, 0);
        notificationManager.notify(id, notification);*/
        // if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) restartJobScheduler(context);
        // else restartService(context);
    }

    private void restartJobScheduler(Context context) {
       /* //context.startForegroundService(service);
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag("myFCMJob")
                .build();
        dispatcher.mustSchedule(myJob);*/
    }

    public void setAlarm(Context context, Calendar calendar) {
        //, Uri ringtone
        mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        // Put Reminder ID in Intent Extra
        Intent intent = new Intent(context, AlarmBroadCustReciver.class);
        //  intent.putExtra(ReminderEditActivity.EXTRA_REMINDER_ID, Integer.toString(ID));
        //intent.putExtra("ring",ringtone);
        mPendingIntent = PendingIntent.getBroadcast(context, 9876, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Calendar c = Calendar.getInstance();
        long currentTime = c.getTimeInMillis();
        long diffTime = calendar.getTimeInMillis() - currentTime;
        mAlarmManager.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + diffTime, mPendingIntent);
        workRequest = new OneTimeWorkRequest.Builder(MyWorker.class).build();
        ArrayList<PendingIntent> intentArray = new ArrayList<PendingIntent>();

        /*
         * Multiple alarm set.... but not work thi code...
         * */
       /* for (int i = 0; i < 4; ++i) {
            Intent intent1 = new Intent(context, AlarmBroadCustReciver.class);
            // Loop counter `i` is used as a `requestCode`
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, i, intent1, 0);
            // Single alarms in 1, 2, ..., 10 minutes (in `i` minutes)
            mAlarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime() + 60000 * i,
                    pendingIntent);

            intentArray.add(pendingIntent);
        }*/
    }
}