package com.horizzon.reminderapp;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horizzon.reminderapp.adapter.CommentAdapter;
import com.horizzon.reminderapp.adapter.ContactBoxListAdapter;
import com.horizzon.reminderapp.dao.CReminder;
import com.horizzon.reminderapp.dao.ReminderComment;
import com.horizzon.reminderapp.dao.ReminderContactUser;
import com.horizzon.reminderapp.dao.ReminderDateTime;
import com.horizzon.reminderapp.dao.ReminderUser;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.CRAttachmentDbHandler;
import com.horizzon.reminderapp.handler.CRCommentDbHandler;
import com.horizzon.reminderapp.handler.CRContactDbHandler;
import com.horizzon.reminderapp.handler.CRDateTimeDbHandler;
import com.horizzon.reminderapp.handler.CreateReminderDbHandler;
import com.horizzon.reminderapp.handler.RUserDbHandler;
import com.horizzon.reminderapp.retrofit.model.reminder.Reminder;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;
import com.horizzon.reminderapp.utility.DateUtilitys;
import com.horizzon.reminderapp.utility.SoftKeyboard;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class InboxCommentActivity extends AppCompatActivity implements View.OnClickListener {

    String SessionId, inFrom;
    TextView conName, rSub, rDateTime, rday, commentcount, attachcount;
    ImageView imgBack, backBtn, imgHome, con_image1, con_image2, con_image3, imgPriority1, imgPriority2, imgPriority3, tabgroupcomment, tabprivatecomment, btn_reject, btn_edit, btn_accept;
    LinearLayout headerlayout, tabgpcontentlay, tabonlycomment, tabwithback, contactViewLayout, selectcontlay;
    CRAttachmentDbHandler cradbh;
    Button tab_gcomment, tab_pcomment, tab_gwbcomment, tab_pwbcomment;
    EditText textComment;
    ImageView btnsend, imgdownarrow;
    ArrayList<ReminderComment> commentArrayList;
    RecyclerView commentList, participantslist;
    CRCommentDbHandler crcdbh;
    CommentAdapter commentAdapter;
    String commentfor = "group", selectednumber = "";
    boolean tabcommentfor = true;
    ArrayList<ReminderContactUser> remindercontactuser_main;
    ArrayList<ReminderContactUser> remindercontactuser_temp_selection;
    ContactBoxListAdapter contactboxlistadapter;
    String selectprvcont, selectprvcont_temp;
    ReminderContactUser selectprvcont_pojo;

    SoftKeyboard softKeyboard;
    CoordinatorLayout mainLayout;
    InputMethodManager im;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox_comment);

        SessionId = getIntent().getStringExtra("SessionId");
        if (savedInstanceState != null && (SessionId == null || SessionId.equals(""))) {
            SessionId = savedInstanceState.getString("SessionId");
        }

        cradbh = new CRAttachmentDbHandler(getApplicationContext());
        crcdbh = new CRCommentDbHandler(getApplicationContext());

        inFrom = "done";

        mainLayout = (CoordinatorLayout) findViewById(R.id.main_layout); // You must use the layout root
        im = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);


        commentList = (RecyclerView) findViewById(R.id.commentList);
        participantslist = (RecyclerView) findViewById(R.id.participantslist);


        remindercontactuser_main = new ArrayList<>();
        remindercontactuser_temp_selection = new ArrayList<>();
        imgBack = (ImageView) findViewById(R.id.imgBack);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        imgHome = (ImageView) findViewById(R.id.homeBtn);
        imgdownarrow = (ImageView) findViewById(R.id.imgdownarrow);


        conName = (TextView) findViewById(R.id.conName);
        rSub = (TextView) findViewById(R.id.rSub);
        rDateTime = (TextView) findViewById(R.id.rDateTime);

        con_image1 = (ImageView) findViewById(R.id.con_image1);
        con_image2 = (ImageView) findViewById(R.id.con_image2);
        con_image3 = (ImageView) findViewById(R.id.con_image3);
        imgPriority1 = (ImageView) findViewById(R.id.imgPriority1);
        imgPriority2 = (ImageView) findViewById(R.id.imgPriority2);
        imgPriority3 = (ImageView) findViewById(R.id.imgPriority3);


        headerlayout = (LinearLayout) findViewById(R.id.headerlayout);
        tabgpcontentlay = (LinearLayout) findViewById(R.id.tabgpcontentlay);
        tabonlycomment = (LinearLayout) findViewById(R.id.tabonlycomment);
        tabwithback = (LinearLayout) findViewById(R.id.tabwithback);

        contactViewLayout = (LinearLayout) findViewById(R.id.contactViewLayout);
        selectcontlay = (LinearLayout) findViewById(R.id.selectcontlay);

        tabgpcontentlay.setVisibility(View.GONE);
        tabonlycomment.setVisibility(View.GONE);
        tabwithback.setVisibility(View.GONE);

        tab_gcomment = (Button) findViewById(R.id.tab_gcomment);
        tab_pcomment = (Button) findViewById(R.id.tab_pcomment);
        tab_gwbcomment = (Button) findViewById(R.id.tab_gwbcomment);
        tab_pwbcomment = (Button) findViewById(R.id.tab_pwbcomment);

        tab_gcomment.setOnClickListener(this);
        tab_pcomment.setOnClickListener(this);
        tab_gwbcomment.setOnClickListener(this);
        tab_pwbcomment.setOnClickListener(this);

        textComment = (EditText) findViewById(R.id.textComment);
        btnsend = (ImageView) findViewById(R.id.btnsend);


        commentArrayList = new ArrayList<>();
        Common.DisplayLog("commentArrayList count", commentArrayList.size() + "");

        commentList.setAdapter(null);

        commentAdapter = new CommentAdapter(InboxCommentActivity.this, commentArrayList, "groupcomment");
        LinearLayoutManager lLayout = new LinearLayoutManager(getApplicationContext());
        lLayout.setStackFromEnd(true);
//        lLayout.setReverseLayout(true);
        commentList.setLayoutManager(lLayout);
        commentList.setAdapter(commentAdapter);


        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgdownarrow.performClick();
                onBackPressed();

            }
        });
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgdownarrow.performClick();
                onBackPressed();

            }
        });

        imgHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(InboxCommentActivity.this, HomeActivity.class);
                startActivity(in);

            }
        });

        textComment.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
//                textComment.setFocusable(true);
//                textComment.requestFocus();
//                textComment.requestFocusFromTouch();
//                textComment.setCursorVisible(true);
//                softKeyboard.openSoftKeyboard();
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                getComment(SessionId, commentfor);

                            }
                        },
                        300);
                return false;
            }
        });
        textComment.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
//                textComment.setFocusable(true);
//                textComment.requestFocus();
//                textComment.requestFocusFromTouch();
//                textComment.setCursorVisible(true);
//                softKeyboard.openSoftKeyboard();
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                getComment(SessionId, commentfor);

                            }
                        },
                        300);
            }
        });
        btnsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnsend.setEnabled(false);
                Common.DisplayLog("textComment.getText()", textComment.getText() + "");
                if (textComment.getText().toString() != null && !textComment.getText().toString().equals("")) {
                    String[] COLUMN = {
                            "",
                            SessionId,
                            Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                            textComment.getText().toString(),
                            commentfor,
                            Common.getSQLDateTime()
                    };
                    String lastid = String.valueOf(crcdbh.addData(COLUMN));

                    getComment(SessionId, commentfor);

//                    if (action.equals("edit"))
                    {
                        ReminderComment rc = new ReminderComment("", SessionId, Common.getSharedPreferences(getApplicationContext(), "userId", "0"), textComment.getText().toString(), commentfor, Common.getSQLDateTime());
                        SavedAddDataOnServer(rc);
                    }
                    textComment.setText("");
                } else {
                    Common.ShowToast(getApplicationContext(), "Please enter  something...");
                }
                btnsend.setEnabled(true);
            }
        });

        imgdownarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                softKeyboard.closeSoftKeyboard();
                keybordShowHide("hide");
            }
        });

        softKeyboard = new SoftKeyboard(mainLayout, im);
        softKeyboard.setSoftKeyboardCallback(new SoftKeyboard.SoftKeyboardChanged() {

            @Override
            public void onSoftKeyboardHide() {
                // Code here
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
//                        Common.ShowToast(InboxCommentActivity.this, "onSoftKeyboardHide");
                        keybordShowHide("hide");
                    }
                });
            }

            @Override
            public void onSoftKeyboardShow() {
                // Code here
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
//                        Common.ShowToast(InboxCommentActivity.this, "onSoftKeyboardShow");
                        keybordShowHide("show");

                    }
                });

            }

            /*@Override
            public void onSoftKeyboardHide() {
                // Code here
                Common.ShowToast(InboxCommentActivity.this, "onSoftKeyboardHide");
            }

            @Override
            public void onSoftKeyboardShow() {
                // Code here
                Common.ShowToast(InboxCommentActivity.this, "onSoftKeyboardShow");
            }*/
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        loadSavedData(SessionId);
        //getComment(SessionId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        softKeyboard.unRegisterSoftKeyboardCallback();
    }

    public void loadSavedData(String SessId) {


        Common.DisplayLog("", "loadSavedData " + SessId);
        CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getApplicationContext());
//        RContactDbHandler rcdbh = new RContactDbHandler(getApplicationContext());


        CReminder cReminder = crdbh.getTableSingleDateWihtDao(SessId);


        Common.DisplayLog("cReminder.getSubject()", cReminder.getSubject() + "");
        // set Subject
        rSub.setText(cReminder.getSubject());

        // set Priority
        if (cReminder.getPriority().equals("1")) {
            imgPriority1.setVisibility(View.VISIBLE);
        } else if (cReminder.getPriority().equals("2")) {
            imgPriority1.setVisibility(View.VISIBLE);
            imgPriority2.setVisibility(View.VISIBLE);
        } else if (cReminder.getPriority().equals("3")) {
            imgPriority1.setVisibility(View.VISIBLE);
            imgPriority2.setVisibility(View.VISIBLE);
            imgPriority3.setVisibility(View.VISIBLE);
        }


        // set Contact
        CRContactDbHandler crcdbh = new CRContactDbHandler(getApplicationContext());
        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllCurrentSessContactData(SessId, "contact");
        remindercontactuser_main.addAll(remindercontactuser);
//        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllContactData();

        Common.DisplayLog("remindercontactuser", remindercontactuser.size() + "");
        String tempToText = "";
        int i = 0;
        for (i = 0; i < remindercontactuser.size(); i++) {

            LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = vi.inflate(R.layout.fgcontact_view, null);

            if (i < 1) {
                tempToText += remindercontactuser.get(i).getName() + ",";
            }

            TextView textView = (TextView) v.findViewById(R.id.conName);
            textView.setText(remindercontactuser.get(i).getName());

            ImageView imageView = (ImageView) v.findViewById(R.id.con_image);


            String imgpath = ((remindercontactuser.get(i).getPhoto().equals("")) ? "defaultimg" : remindercontactuser.get(i).getPhoto()) + "";
            if (i == 0 && !imgpath.equals("defaultimg")) {
                Picasso.with(getApplicationContext())
                        .load(imgpath)
                        .placeholder(R.drawable.defaultusericon)
                        .error(R.drawable.defaultusericon)
                        .into(con_image1);
            }/*else{
                con_image1.setVisibility(View.GONE);
            }*/
            if (i == 1 && !imgpath.equals("defaultimg")) {
                Picasso.with(getApplicationContext())
                        .load(imgpath)
                        .placeholder(R.drawable.defaultusericon)
                        .error(R.drawable.defaultusericon)
                        .into(con_image2);
            }/*else{
                con_image2.setVisibility(View.GONE);
            }*/
            if (i == 2 && !imgpath.equals("defaultimg")) {
                Picasso.with(getApplicationContext())
                        .load(imgpath)
                        .placeholder(R.drawable.defaultusericon)
                        .error(R.drawable.defaultusericon)
                        .into(con_image3);
            }/*else{
                con_image3.setVisibility(View.GONE);
            }*/
        }

        tempToText = (tempToText != null && !tempToText.equals("")) ? tempToText.substring(0, tempToText.length() - 1) : "";
        if (i > 2) {
            tempToText = tempToText.substring(6) + " & " + (i - 1) + " others";
        }

        conName.setText(tempToText);

        if (remindercontactuser.size() > 1) {
            tabgpcontentlay.setVisibility(View.VISIBLE);
            tab_gcomment.performClick();
        } else {
            tabonlycomment.setVisibility(View.VISIBLE);
        }


        // set participantslist

        participantslist.setAdapter(null);
        contactboxlistadapter = new ContactBoxListAdapter(InboxCommentActivity.this, remindercontactuser, "InboxCommentActivity");
        LinearLayoutManager lLayout = new GridLayoutManager(InboxCommentActivity.this, 4);
//        imglistView.addItemDecoration(new GridSpacingItemDecoration(4, 5, false));
//        imglistView.setHasFixedSize(true);
        participantslist.setLayoutManager(lLayout);
        participantslist.setAdapter(contactboxlistadapter);





        /*// set Attachment
        CRAttachmentDbHandler cradbh = new CRAttachmentDbHandler(getApplicationContext());
        Common.DisplayLog("cradbh.getDataCount(SessId)", cradbh.getDataCount(SessId, "done") + "");
        attachcount.setText(cradbh.getDataCount(SessId, "done") + "");

        // set Comment

        CRCommentDbHandler crcodbh = new CRCommentDbHandler(getApplicationContext());
        Common.DisplayLog("crcodbh.getDataCount(SessionId)", crcodbh.getDataCount(SessId) + "");
        commentcount.setText(crcodbh.getDataCount(SessId) + "");
        if (crcodbh.getDataCount(SessId) > 0) {
            tabcomment.performClick();
        }*/

        // set Date Time
        CRDateTimeDbHandler crdtdbh = new CRDateTimeDbHandler(getApplicationContext());
        ReminderDateTime rdt = crdtdbh.getTableSingleDateWihtDao(SessId);
        Common.DisplayLog("crdtdbh.getDataCount(SessionId)", crdtdbh.getDataCount(SessId) + "");


        DateUtilitys dateUtl = new DateUtilitys();

        Date date;
        if (crdtdbh.getDataCount(SessId) > 0) {
            date = dateUtl.FormetDate(rdt.getStartDate() + " " + rdt.getStartTime());
        } else {
            Common.DisplayLog("cr.getCreateddate()", cReminder.getCreateddate() + "");
            date = dateUtl.FormetDate(cReminder.getCreateddate());

        }
        dateUtl.setforDisplayTitle(date);
        rDateTime.setText(dateUtl.getDay() + " " + dateUtl.getMonth());

        /*CReminderForListDbHandler crfldbh = new CReminderForListDbHandler(getApplicationContext());
        ReminderForList rfl = crfldbh.getTableSingleDateWihtDao(SessId, Common.getSharedPreferences(getApplicationContext(), "userId", "0"));
        if (rfl != null) {
            if (rfl.getvReminderStatus().equals("accept") || rfl.getvReminderStatus().equals("reject")) {
                btn_reject.setVisibility(View.GONE);
                btn_accept.setVisibility(View.GONE);
                btn_edit.setVisibility(View.GONE);
            }
        }*/

    }

    private void getComment(String SessId, String commentFor) {

        CRCommentDbHandler crcdbh = new CRCommentDbHandler(getApplicationContext());
        commentArrayList.clear();
        commentArrayList.addAll(crcdbh.getAllCurrentSessDataWithType(SessId, commentFor));

        commentAdapter.notifyDataSetChanged();

        /*commentArrayList = crcdbh.getAllCurrentSessDataWithType(SessId, commentFor);
        Common.DisplayLog("commentArrayList count", commentArrayList.size() + "");

        commentList.setAdapter(null);

        commentAdapter = new CommentAdapter(InboxCommentActivity.this, commentArrayList, "groupcomment");
        LinearLayoutManager lLayout = new LinearLayoutManager(getApplicationContext());
        lLayout.setStackFromEnd(true);
//        lLayout.setReverseLayout(true);
        commentList.setLayoutManager(lLayout);
        commentList.setAdapter(commentAdapter);*/

//        commentAdapter.notifyDataSetChanged();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        commentList.scrollToPosition(commentArrayList.size() - 1);
                    }
                },
                200);

    }

    public void SavedAddDataOnServer(ReminderComment rc) {

        if (Common.hasInternetConnection(getApplicationContext())) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<Reminder> callComment = apiService.addComment(rc.getSesssionId(), rc.getUserId(), rc.getComment(), "1", rc.gettCommentFor());
            callComment.enqueue(new Callback<Reminder>() {
                @Override
                public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                    int statusCode = response.code();
                    Common.DisplayLog("response.body()", response.body().toString());
                    boolean status = response.body().getStatus();
                    Common.DisplayLog("status", status + "");
                    Common.DisplayLog("getMessage", response.body().getMessage() + "");
                    if (status) {

//                        Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                    } else {
                        Common.ShowToast(getApplicationContext(), response.body().getMessage());

                    }
//                    pd.dismiss();

                }

                @Override
                public void onFailure(Call<Reminder> call, Throwable t) {
                    Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
//                    pd.dismiss();
                }


            });
        }
    }

    public void settabcontent(String tab) {


        contactViewLayout.setVisibility(View.GONE);
        selectcontlay.setVisibility(View.GONE);

        if (tab.equals("group")) {
            commentfor = "group";
            tabcommentfor = true;
            tab_gcomment.setBackgroundResource(R.drawable.btn_send);
            tab_gcomment.setTextColor(Color.parseColor("#ffffff"));
            tab_pcomment.setBackgroundResource(R.drawable.btn_device_bg);
            tab_pcomment.setTextColor(Color.parseColor("#1ba1e2"));

            tab_gwbcomment.setBackgroundResource(R.drawable.btn_send);
            tab_gwbcomment.setTextColor(Color.parseColor("#ffffff"));
            tab_pwbcomment.setBackgroundResource(R.drawable.btn_device_bg);
            tab_pwbcomment.setTextColor(Color.parseColor("#1ba1e2"));

            commentList.setVisibility(View.VISIBLE);


        } else {

//            commentfor = "group";
            tab_pcomment.setBackgroundResource(R.drawable.btn_device_blue);
            tab_pcomment.setTextColor(Color.parseColor("#ffffff"));
            tab_gcomment.setBackgroundResource(R.drawable.btn_send_bg);
            tab_gcomment.setTextColor(Color.parseColor("#1ba1e2"));

            tab_pwbcomment.setBackgroundResource(R.drawable.btn_device_blue);
            tab_pwbcomment.setTextColor(Color.parseColor("#ffffff"));
            tab_gwbcomment.setBackgroundResource(R.drawable.btn_send_bg);
            tab_gwbcomment.setTextColor(Color.parseColor("#1ba1e2"));

            contactViewLayout.setVisibility(View.GONE);
            selectcontlay.setVisibility(View.GONE);

            Common.DisplayLog("remindercontactuser_temp_selection", remindercontactuser_temp_selection.size() + "");
            if (remindercontactuser_temp_selection.size() > 0) {
                commentList.setVisibility(View.VISIBLE);
                loadSelectedContect();
            } else {
                commentList.setVisibility(View.GONE);
                selectcontlay.setVisibility(View.VISIBLE);

            }
        }
        getComment(SessionId, commentfor);
    }

    public void loadSelectedContect() {

        contactViewLayout.removeAllViews();
//        ArrayList<ReminderContactUser> remindercontactuser = crcdbh.getAllContactData();

        Common.DisplayLog("remindercontactuser_temp_selection", remindercontactuser_temp_selection.size() + "");
        String tempToText = "";
        int i = 0;
        for (i = 0; i < remindercontactuser_temp_selection.size(); i++) {


//            ReminderContactUser reminderContactUser = remindercontactuser_temp_selection.get(i);
//            ObjectMapper mapper = new ObjectMapper();
//
//            String json = mapper.writeValueAsString(user);
//            JSONObject jsonObj = new JSONObject(reminderContactUser);
////            System.out.println( jsonObj );
//            Common.DisplayLog("jsonObj", jsonObj.toString());

            LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = vi.inflate(R.layout.grpcommentcontact_view, null);

            if (i < 2) {
                tempToText += remindercontactuser_temp_selection.get(i).getName() + ",";
            }

            LinearLayout maincont = (LinearLayout) v.findViewById(R.id.maincont);

            if (selectednumber.equals(remindercontactuser_temp_selection.get(i).getNumber())) {
                maincont.setBackgroundResource(R.drawable.prcommentsel_a);
            }

            TextView textView = (TextView) v.findViewById(R.id.conName);
            textView.setText(remindercontactuser_temp_selection.get(i).getName());

            ImageView imageView = (ImageView) v.findViewById(R.id.con_image);

            String imgpath = ((remindercontactuser_temp_selection.get(i).getPhoto().equals("")) ? "defaultimg" : remindercontactuser_temp_selection.get(i).getPhoto()) + "";

            Picasso.with(InboxCommentActivity.this)
                    .load(imgpath)
                    .placeholder(R.drawable.defaultusericon)
                    .error(R.drawable.defaultusericon)
                    .into(imageView);


            final int finalI = i;
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    {


                        ArrayList<ReminderContactUser> remindercontactuser_temp_selection_temp = new ArrayList<ReminderContactUser>();
                        remindercontactuser_temp_selection_temp.addAll(remindercontactuser_temp_selection);

                        int index = remindercontactuser_temp_selection_temp.indexOf(remindercontactuser_temp_selection.get(finalI));
                        remindercontactuser_temp_selection_temp.remove(index);
                        remindercontactuser_temp_selection_temp.add(0, remindercontactuser_temp_selection.get(finalI));


                        selectednumber = remindercontactuser_temp_selection.get(finalI).getNumber();
                        commentfor = reminderContactUserToJson(remindercontactuser_temp_selection.get(finalI));

                        Common.DisplayLog("", commentfor);

                        remindercontactuser_temp_selection.clear();
                        remindercontactuser_temp_selection.addAll(remindercontactuser_temp_selection_temp);

                        loadSelectedContect();
                        selectcontlay.setVisibility(View.GONE);
                        commentList.setVisibility(View.VISIBLE);
                    }
                }
            });
            contactViewLayout.addView(v);
            contactViewLayout.setVisibility(View.VISIBLE);
        }

        LayoutInflater vi1 = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v1 = vi1.inflate(R.layout.grpcommentcontact_view, null);


        TextView textView = (TextView) v1.findViewById(R.id.conName);
        textView.setText("+ Select  ");

        CircleImageView imageView = (CircleImageView) v1.findViewById(R.id.con_image);
        imageView.setBorderWidth(0);

        Picasso.with(InboxCommentActivity.this)
                .load(R.drawable.commentselectcon)
                .placeholder(R.drawable.defaultusericon)
                .error(R.drawable.defaultusericon)
                .into(imageView);
        v1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentList.setVisibility(View.GONE);
                contactViewLayout.setVisibility(View.GONE);
                selectcontlay.setVisibility(View.VISIBLE);
            }
        });

        contactViewLayout.addView(v1);

        getComment(SessionId, commentfor);
    }

    public void keybordShowHide(String task) {
        if (task.equals("show")) {

            headerlayout.setVisibility(View.GONE);
            tabgpcontentlay.setVisibility(View.GONE);
            tabwithback.setVisibility(View.GONE);
            tabonlycomment.setVisibility(View.GONE);
            if (remindercontactuser_main.size() > 1) {
                tabwithback.setVisibility(View.VISIBLE);
            } else {
                tabonlycomment.setVisibility(View.VISIBLE);
            }

        } else {
            headerlayout.setVisibility(View.VISIBLE);


            tabgpcontentlay.setVisibility(View.GONE);
            tabwithback.setVisibility(View.GONE);
            tabonlycomment.setVisibility(View.GONE);
            if (remindercontactuser_main.size() > 1) {
                tabgpcontentlay.setVisibility(View.VISIBLE);
            } else {
                tabonlycomment.setVisibility(View.VISIBLE);
            }
        }

    }

    public void selectprivatcontact(int pos) {

        if (!remindercontactuser_temp_selection.contains(remindercontactuser_main.get(pos))) {
//            Common.ShowToast(getApplicationContext(), "add in array");
            remindercontactuser_temp_selection.add(remindercontactuser_main.get(pos));

        } else {

//            Common.ShowToast(getApplicationContext(), "already exist in array");
        }
        int index = remindercontactuser_temp_selection.indexOf(remindercontactuser_main.get(pos));
        remindercontactuser_temp_selection.remove(index);
        remindercontactuser_temp_selection.add(0, remindercontactuser_main.get(pos));


        selectednumber = remindercontactuser_main.get(pos).getNumber();
        commentfor = reminderContactUserToJson(remindercontactuser_main.get(pos));
        Common.DisplayLog("", commentfor);
        loadSelectedContect();
        selectcontlay.setVisibility(View.GONE);
        commentList.setVisibility(View.VISIBLE);
    }


    public String reminderContactUserToJson(ReminderContactUser reminderContactUser) {
        if (reminderContactUser != null) {
            JSONObject jo = new JSONObject();
            try {
                RUserDbHandler rudbh = new RUserDbHandler(InboxCommentActivity.this);
                ReminderUser ru = rudbh.getTableSingleDateWihtDao(Common.getSharedPreferences(getApplicationContext(), "userId", "0"));
                jo.put("id", reminderContactUser.getId());
                jo.put("sesssionId", reminderContactUser.getSesssionId());
                jo.put("iUserId", reminderContactUser.getiUserId());
                jo.put("name", reminderContactUser.getName());
                jo.put("number", reminderContactUser.getNumber());
                jo.put("photo", reminderContactUser.getPhoto());
                jo.put("foraction", reminderContactUser.getForaction());
                jo.put("consel", reminderContactUser.getConsel());
                jo.put("fromnumber", ru.getuNumber());
                jo.put("tonumber", reminderContactUser.getNumber());

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return jo.toString();
        }
        return "";
    }


    @Override
    public void onClick(View v) {

        int id = v.getId();
        if (id == R.id.tab_gcomment) {
            settabcontent("group");
        } else if (id == R.id.tab_gwbcomment) {
            settabcontent("group");
        } else if (id == R.id.tab_pcomment) {
            settabcontent("private");
        } else if (id == R.id.tab_pwbcomment) {
            settabcontent("private");
        }
    }

}
