package com.horizzon.reminderapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.utility.CommentItem;

import java.util.ArrayList;

/**
 * Created by horizzon on 12/17/2016.
 */
public class NotificationCustomAdapter extends BaseAdapter{
    Activity context;
    private ImageLoader imageLoader;
    ArrayList<CommentItem> list;

    public NotificationCustomAdapter(Activity context, ArrayList<CommentItem> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Holder holder;

        if(convertView==null)
        {
            holder = new Holder();

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.custom_noti_layout, null);

            holder.name = (TextView) convertView.findViewById(R.id.reminderName);
            holder.comment_by = (TextView) convertView.findViewById(R.id.reminderComtBy);
            holder.imgProfile = (ImageView) convertView.findViewById(R.id.imgProfile);
            holder.comment = (TextView) convertView.findViewById(R.id.reminderComt);
            holder.imgName = (TextView) convertView.findViewById(R.id.txtName);

            convertView.setTag(holder);
        }
        else
        {
            holder = (Holder) convertView.getTag();
        }

        CommentItem ud = list.get(position);
        holder.comment_by.setText(ud.getReminder_comment_by());
        return convertView;
    }

    public class Holder
    {
        TextView name;
        TextView comment_by;
        TextView comment;
        TextView imgName;
        ImageView imgProfile;
    }
}
