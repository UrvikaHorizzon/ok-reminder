package com.horizzon.reminderapp;

import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.ImageView;

import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.data.SessionIdentifierGenerator;
import com.horizzon.reminderapp.handler.CreateReminderDbHandler;

public class CreateReminderActivity extends AppCompatActivity {
    FragmentManager fm;
    FragmentTransaction ft;
    ImageView imgBack, imgSwap;
    GradientDrawable gd;
    public int rev_flag = 1;
    public static int btn_flag;
    String newrem;
    String SessionId,action;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_reminder);

        CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getApplicationContext());

        action = getIntent().getStringExtra("action");
        if (action == null || action.equals("")) {
            action = "add";
        }

        newrem = getIntent().getStringExtra("newrem");
        if (newrem != null && newrem.equals("yes")) {

            //Create session
            SessionIdentifierGenerator sig = new SessionIdentifierGenerator();
            SessionId = sig.nextSessionId();
            Common.createSharedPreferences(getApplicationContext(),"SessionId",SessionId);

            String[] COLUMN = {
                    "",
                    SessionId,
                    Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                    "",
                    "",
                    "send",
                    "temp",
                    "unlock",
                    "",
                    "",
                    ""
            };
           crdbh.addData(COLUMN);

        }

//        SessionId = Common.getSharedPreferences(getApplicationContext(),"SessionId","");


//        imgBack = (ImageView) findViewById(R.id.backBtn);
        /*imgSwap = (ImageView) findViewById(R.id.swapBtn);*/

        fm = getSupportFragmentManager();
        ft = fm.beginTransaction();
        CreateRemindBgFrag cf = new CreateRemindBgFrag();
        ft.replace(R.id.framefrag, cf);
        ft.commit();


//        imgBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (action.equals("edit")) {
            super.onBackPressed();
        } else {
            Intent in = null;
            in = new Intent(getApplicationContext(), HomeActivity.class);
            startActivity(in);
        }
    }
}
