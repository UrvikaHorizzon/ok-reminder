package com.horizzon.reminderapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.retrofit.model.location.Result;
import com.squareup.picasso.Picasso;

import java.util.List;


public class LocationSearchAdapter extends ArrayAdapter<Result> {

    private Context mContext;
    private List<Result> locationSearchList;
    private LayoutInflater mInflater;

    public LocationSearchAdapter(Context context, List<Result> locationSearch) {
        super(context,0, locationSearch);
        this.mContext = context;
        this.locationSearchList = locationSearch;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return locationSearchList.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.location_autoc_lay, null);
            holder = new ViewHolder();
            holder.mainText = (TextView) convertView.findViewById(R.id.mainText);
            holder.secondaryText = (TextView) convertView.findViewById(R.id.secondaryText);
            holder.description = (TextView) convertView.findViewById(R.id.description);
            holder.addtext = (ImageView) convertView.findViewById(R.id.addtext);
            holder.locico = (ImageView) convertView.findViewById(R.id.locico);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Result rs = locationSearchList.get(position);
        holder.mainText.setText(rs.getName());
        holder.secondaryText.setText(rs.getVicinity());
        holder.description.setText(rs.getVicinity());

        Picasso.with(mContext)
                .load(rs.getIcon())
//                .placeholder(R.drawable.profilepic)
//                .error(R.drawable.profilepic)
                .into(holder.locico);
        return convertView;
    }

    private static class ViewHolder {

        TextView mainText,secondaryText,description;
        ImageView addtext,locico;
    }

}
