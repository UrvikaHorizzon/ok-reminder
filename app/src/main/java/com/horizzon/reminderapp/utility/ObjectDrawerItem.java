package com.horizzon.reminderapp.utility;

/**
 * Created by Experior-2 on 21-10-2016.
 */
public class ObjectDrawerItem {

    public int icon;
    public String name;

    // Constructor.
    public ObjectDrawerItem(int icon, String name) {

        this.icon = icon;
        this.name = name;
    }
}