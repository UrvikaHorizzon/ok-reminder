package com.horizzon.reminderapp;

import android.Manifest;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.horizzon.reminderapp.adapter.DrawerItemCustomAdapter;
import com.horizzon.reminderapp.app.MyRegEditText;
import com.horizzon.reminderapp.app.MyRegTextView;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.RUserDbHandler;
import com.horizzon.reminderapp.helper.PrefManager;
import com.horizzon.reminderapp.model.FolderName;
import com.horizzon.reminderapp.retrofit.model.GetFolderNameList;
import com.horizzon.reminderapp.retrofit.model.ReminderCount;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;
import com.horizzon.reminderapp.service.ContactService;
import com.horizzon.reminderapp.service.FetchMyUsersService;
import com.horizzon.reminderapp.service.GetReminderService;
import com.horizzon.reminderapp.utility.ObjectDrawerItem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {
    private static final int PERMISSION_REQUEST_CODE = 1;
    ListView listView;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    protected ListView mDrawerList;
    ObjectDrawerItem[] drawerItem;
    ArrayList<String> values = new ArrayList<>();
    FloatingActionButton fab;
    AppCompatButton btn_no, btn_yes;
    String newFloderName;
    MyRegEditText txtAddFolderName;
    ImageView imgSearch, imgNoti, imgComment, imgOption;
    CircleImageView imgProfile;
    String comeForm = "";
    int PERMISSION_REQUEST_CONTACT = 20;
    RUserDbHandler rudbh;
    SharedPreferences sharedPreferences;
    MyRegTextView createFold;
    MyRegEditText edtSpaceTxt;
    String edtRemoveSpace, withOutSpace;
    String userImag;
    String UId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_setting);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        String[] PERMISSIONS = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_SMS, Manifest.permission.CAMERA, Manifest.permission.READ_CONTACTS};

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQUEST_CODE);
        }

        sharedPreferences = getSharedPreferences("UserInfo", MODE_PRIVATE);
        // MyFirebaseMessagingService.RegisterDeviceForFCM(HomeActivity.this,"");
        String token = sharedPreferences.getString("FCM_Token", "");
        sharedPreferences = getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        userImag = sharedPreferences.getString("userImage", "");
        UId = sharedPreferences.getString("UId", "");
        getFolderList();
        rudbh = new RUserDbHandler(getApplicationContext());
//        startService(new Intent(HomeActivity.this,ContactService.class));
        final Animation zoomin = AnimationUtils.loadAnimation(this, R.anim.zzomin);
        final Animation zoomout = AnimationUtils.loadAnimation(this, R.anim.zoomout);
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            firebaseUser.getIdToken(false).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    String idToken = task.getResult().getToken();
                    String usetId = Common.getSharedPreferences(getApplicationContext(), "userId", "0");
                    FetchMyUsersService.startMyUsersService(HomeActivity.this, usetId, idToken);
                }
            });
        }
//        getSupportActionBar().setDisplayShowHomeEnabled(false);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setAnimation(zoomin);
        fab.setAnimation(zoomout);
        listView = (ListView) findViewById(R.id.list);
        imgSearch = (ImageView) findViewById(R.id.imgSearch);
        imgComment = (ImageView) findViewById(R.id.imgComt);
        imgNoti = (ImageView) findViewById(R.id.imgNoti);
        imgProfile = findViewById(R.id.imgProfile);

        if (!userImag.equals("")) {
            String userPic = "https://www.hwpl.in/projects/reminderapp" + userImag.substring(2);
            Glide.with(this).load(userPic).into(imgProfile);
        }
        imgOption = (ImageView) findViewById(R.id.imgOpt);
        createFold = findViewById(R.id.createFold);
        edtSpaceTxt = findViewById(R.id.edtSpaceTxt);
        edtSpaceTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                withOutSpace = s.toString().trim();
                edtRemoveSpace = withOutSpace.replace(" ", ",");

                // TODO Auto-generated method stub
            }
        });
        createFold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* CustomCreateFloderDialog cdd = new CustomCreateFloderDialog(HomeActivity.this);
                cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                cdd.show();*/

                /*
                 * Dialog for add new folder
                 * */
                Dialog dlg = new Dialog(HomeActivity.this);
                dlg.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dlg.setContentView(R.layout.custom_add_floder);
                dlg.show();

                btn_yes = dlg.findViewById(R.id.btn_yes);
                btn_no = dlg.findViewById(R.id.btn_no);
                txtAddFolderName = dlg.findViewById(R.id.txtAddFolderName);
                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dlg.dismiss();
                    }
                });
                btn_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String folderName = txtAddFolderName.getText().toString();
                        if (!folderName.isEmpty() || !folderName.equals("")) {
                            newFloderName = folderName;
                            sendFolderNameOnServer(newFloderName);
                            //values.add(newFloderName);
                            dlg.dismiss();
                        } else {
                            Toast.makeText(getApplicationContext(), "Please enter folder name.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
        zoomin.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                fab.startAnimation(zoomout);
            }
        });

        zoomout.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                fab.startAnimation(zoomin);
            }
        });

        imgOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        values.add("ALL");
        values.add("INBOX");
        values.add("SHARED");
        values.add("SENT");
        //values.add("");
        // values.add("WORK");
        //  values.add("HOME");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.list_row, R.id.txt1, values);

        // Assign adapter to ListView
        listView.setAdapter(adapter);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // list the drawer items
        drawerItem = new ObjectDrawerItem[1];

        drawerItem[0] = new ObjectDrawerItem(R.drawable.settingicon, "Settings");
        // drawerItem[1] = new ObjectDrawerItem(R.drawable.syncicon, "Sync Now");

        // Pass the folderData to our ListView adapter
        final DrawerItemCustomAdapter adapter1 = new DrawerItemCustomAdapter(this, R.layout.list_item_row, drawerItem);

        // Set the adapter for the list view
        mDrawerList.setAdapter(adapter1);
        // set the item click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // for app icon control for nav drawer
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();

                Intent in = new Intent(HomeActivity.this, CreateReminderActivity.class);
                in.putExtra("newrem", "yes");
                startActivity(in);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent in = null;
                if (values.get(position).equals("ALL")) {
                    in = new Intent(HomeActivity.this, AllActivity.class);
                    startActivity(in);
                } else {
                    in = new Intent(HomeActivity.this, InboxActivity.class);
                    in.putExtra("comeForm", values.get(position));
                }
                /*else if (values.get(position).equals("INBOX") || values.get(position).equals("SHARED") || values.get(position).equals("SENT")) {
                    in = new Intent(HomeActivity.this, InboxActivity.class);
                    in.putExtra("comeForm", values.get(position));
                }*/
                /*
                 in values pass selected folder name in above condition Like :
                 values.get(position).equals("INBOX")

                */
                if (in != null) {
                    startActivity(in);
                }
                /*
                 * get list from server and then on that item click data pass as per click
                 * -- condition put in if else
                 * */


            }
        });

        imgComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(HomeActivity.this, CommentsActivity.class);
                startActivity(in);
            }
        });

        imgNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(HomeActivity.this, NotificationActivity.class);
                in.putExtra("comeForm", "NOTIFICATIONS");
                startActivity(in);
            }
        });

        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

       /* backGroundSevie = new BackGroundSevie();
        Intent  mServiceIntent = new Intent(this, backGroundSevie.getClass());
        if (!isMyServiceRunning(backGroundSevie.getClass())) {
            startService(mServiceIntent);
        }*/


    }


    public void sendFolderNameOnServer(String FolderName) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<com.horizzon.reminderapp.model.FolderName> call = apiService.createFolder(UId, FolderName);
        call.enqueue(new Callback<FolderName>() {
            @Override
            public void onResponse(Call<FolderName> call, Response<FolderName> response) {
                Toast.makeText(HomeActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                getFolderList();
            }

            @Override
            public void onFailure(Call<FolderName> call, Throwable t) {
                Common.DisplayLog("fail", t.getMessage().toString());
            }


        });
    }


    public void getFolderList() {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<GetFolderNameList> call = apiService.getFolderNameList(UId);
        call.enqueue(new Callback<GetFolderNameList>() {
            @Override
            public void onResponse(Call<GetFolderNameList> call, Response<GetFolderNameList> response) {
                GetFolderNameList getFolderNameList = response.body();
                if (getFolderNameList != null) {
                    List<GetFolderNameList.Datum> folderName = getFolderNameList.getData();
                    if (folderName.size() > 0) {
                        for (int i = 0; i < folderName.size(); i++) {
                            values.add(folderName.get(i).getVFoldername());
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                                R.layout.list_row, R.id.txt1, values);

                        // Assign adapter to ListView
                        listView.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<GetFolderNameList> call, Throwable t) {
                Common.DisplayLog("fail", t.getMessage().toString());
            }
        });
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("Service status", "Running");
                return true;
            }
        }
        Log.i("Service status", "Not running");
        return false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        askForContactPermission();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        loadProfile();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(HomeActivity.this, GetReminderService.class));
        loadProfile();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // startService(new Intent(HomeActivity.this, ContactService.class));
//                    Intent i = new Intent(SplashActivity.this, Activity_VideoHome.class);
//                    startActivity(i);
//                    finish();
//                    Toast.makeText(getApplicationContext(),"Permission Granted",Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public void askForContactPermission() {

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(HomeActivity.this,
                    Manifest.permission.READ_CONTACTS)) {
                //please confirm Contacts access
                askForContactPermission();
            } else {
                ActivityCompat.requestPermissions(HomeActivity.this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        PERMISSION_REQUEST_CONTACT);
            }
        } else {
            startService(new Intent(HomeActivity.this, ContactService.class));
        }

    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();

            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
            homeIntent.addCategory(Intent.CATEGORY_HOME);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent);
        }
    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            selectItem(position);
            if (position == 0) {
                Intent in = new Intent(HomeActivity.this, SettingsActivity.class);
                startActivity(in);
            }
        }
    }


    private void loadProfile() {

        new PrefManager(HomeActivity.this).checkLogin();

        /*ReminderUser reminderUser = rudbh.getTableSingleDateWihtDao(Common.getSharedPreferences(getApplicationContext(),"userId","0"));

        String imgpath = ((reminderUser.getuPhoto()==null || reminderUser.getuPhoto().equals("")) ? "defaultimg" : reminderUser.getuPhoto()) + "";

        Picasso.with(getApplicationContext())
                .load(imgpath)
                .placeholder(R.drawable.profilepic)
                .error(R.drawable.profilepic)
                .into(imgProfile);*/

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
