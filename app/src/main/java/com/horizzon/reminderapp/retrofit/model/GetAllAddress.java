package com.horizzon.reminderapp.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetAllAddress implements Serializable {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public class Data  implements Serializable {

        @SerializedName("home")
        @Expose
        private List<Home> home = null;
        @SerializedName("office")
        @Expose
        private List<Office> office = null;
        @SerializedName("favourite")
        @Expose
        private List<Favourite> favourite = null;

        public List<Home> getHome() {
            return home;
        }

        public void setHome(List<Home> home) {
            this.home = home;
        }

        public List<Office> getOffice() {
            return office;
        }

        public void setOffice(List<Office> office) {
            this.office = office;
        }

        public List<Favourite> getFavourite() {
            return favourite;
        }

        public void setFavourite(List<Favourite> favourite) {
            this.favourite = favourite;
        }

    }

    public class Favourite  implements Serializable {

        @SerializedName("Id")
        @Expose
        private String id;
        @SerializedName("iUserId")
        @Expose
        private String iUserId;
        @SerializedName("vAddressFor")
        @Expose
        private String vAddressFor;
        @SerializedName("tAddress")
        @Expose
        private String tAddress;
        @SerializedName("vLat")
        @Expose
        private String vLat;
        @SerializedName("vLong")
        @Expose
        private String vLong;
        @SerializedName("dCreatedDateTime")
        @Expose
        private String dCreatedDateTime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIUserId() {
            return iUserId;
        }

        public void setIUserId(String iUserId) {
            this.iUserId = iUserId;
        }

        public String getVAddressFor() {
            return vAddressFor;
        }

        public void setVAddressFor(String vAddressFor) {
            this.vAddressFor = vAddressFor;
        }

        public String getTAddress() {
            return tAddress;
        }

        public void setTAddress(String tAddress) {
            this.tAddress = tAddress;
        }

        public String getVLat() {
            return vLat;
        }

        public void setVLat(String vLat) {
            this.vLat = vLat;
        }

        public String getVLong() {
            return vLong;
        }

        public void setVLong(String vLong) {
            this.vLong = vLong;
        }

        public String getDCreatedDateTime() {
            return dCreatedDateTime;
        }

        public void setDCreatedDateTime(String dCreatedDateTime) {
            this.dCreatedDateTime = dCreatedDateTime;
        }

    }

    public class Home  implements Serializable {

        @SerializedName("Id")
        @Expose
        private String id;
        @SerializedName("iUserId")
        @Expose
        private String iUserId;
        @SerializedName("vAddressFor")
        @Expose
        private String vAddressFor;
        @SerializedName("tAddress")
        @Expose
        private String tAddress;
        @SerializedName("vLat")
        @Expose
        private String vLat;
        @SerializedName("vLong")
        @Expose
        private String vLong;
        @SerializedName("dCreatedDateTime")
        @Expose
        private String dCreatedDateTime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIUserId() {
            return iUserId;
        }

        public void setIUserId(String iUserId) {
            this.iUserId = iUserId;
        }

        public String getVAddressFor() {
            return vAddressFor;
        }

        public void setVAddressFor(String vAddressFor) {
            this.vAddressFor = vAddressFor;
        }

        public String getTAddress() {
            return tAddress;
        }

        public void setTAddress(String tAddress) {
            this.tAddress = tAddress;
        }

        public String getVLat() {
            return vLat;
        }

        public void setVLat(String vLat) {
            this.vLat = vLat;
        }

        public String getVLong() {
            return vLong;
        }

        public void setVLong(String vLong) {
            this.vLong = vLong;
        }

        public String getDCreatedDateTime() {
            return dCreatedDateTime;
        }

        public void setDCreatedDateTime(String dCreatedDateTime) {
            this.dCreatedDateTime = dCreatedDateTime;
        }

    }

    public class Office  implements Serializable {

        @SerializedName("Id")
        @Expose
        private String id;
        @SerializedName("iUserId")
        @Expose
        private String iUserId;
        @SerializedName("vAddressFor")
        @Expose
        private String vAddressFor;
        @SerializedName("tAddress")
        @Expose
        private String tAddress;
        @SerializedName("vLat")
        @Expose
        private String vLat;
        @SerializedName("vLong")
        @Expose
        private String vLong;
        @SerializedName("dCreatedDateTime")
        @Expose
        private String dCreatedDateTime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIUserId() {
            return iUserId;
        }

        public void setIUserId(String iUserId) {
            this.iUserId = iUserId;
        }

        public String getVAddressFor() {
            return vAddressFor;
        }

        public void setVAddressFor(String vAddressFor) {
            this.vAddressFor = vAddressFor;
        }

        public String getTAddress() {
            return tAddress;
        }

        public void setTAddress(String tAddress) {
            this.tAddress = tAddress;
        }

        public String getVLat() {
            return vLat;
        }

        public void setVLat(String vLat) {
            this.vLat = vLat;
        }

        public String getVLong() {
            return vLong;
        }

        public void setVLong(String vLong) {
            this.vLong = vLong;
        }

        public String getDCreatedDateTime() {
            return dCreatedDateTime;
        }

        public void setDCreatedDateTime(String dCreatedDateTime) {
            this.dCreatedDateTime = dCreatedDateTime;
        }

    }
}
