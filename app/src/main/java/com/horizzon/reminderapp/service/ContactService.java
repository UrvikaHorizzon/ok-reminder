package com.horizzon.reminderapp.service;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.provider.ContactsContract;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.RContactDbHandler;
import com.horizzon.reminderapp.retrofit.model.checkcontact.CheckContact;
import com.horizzon.reminderapp.retrofit.model.checkcontact.Datum;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;

import java.util.HashSet;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactService extends Service {
    public ContactService() {
    }

    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Common.DisplayLog("", "onStartCommand of Services");
        retrieveFullContactDetail();
        checkonData();
        return super.onStartCommand(intent, flags, startId);
    }

    private void retrieveFullContactDetail() {

        boolean foraction = true;
        RContactDbHandler rcdbh = new RContactDbHandler(getApplicationContext());
        Common.DisplayLog("", "retrieveFullContactDetail() = ok");

        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

        ContentResolver contactResolver = getContentResolver();

        final String[] PROJECTION = new String[]{
                "DISTINCT " + ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Email.DATA2,
                ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI
        };

//        Cursor allCursor = contactResolver.query(uri, null, null, null, null);
//        Cursor allCursor = contactResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        Cursor allCursor = contactResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,
                ContactsContract.Contacts.HAS_PHONE_NUMBER + "=1",
                null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        Common.DisplayLog("", "allCursor.getCount() = " + allCursor.getCount() + " > " + rcdbh.getDataCount());

//        if(rcdbh.getDataCount()<=0 || allCursor.getCount()> rcdbh.getDataCount()) {
        if (rcdbh.getDataCount() <= 0) {

            rcdbh.resetTable();
            HashSet<String> mobileNoSet = new HashSet<String>();

            if (allCursor != null && allCursor.moveToFirst()) {

                while (allCursor.moveToNext()) {
                    String id = allCursor.getString(allCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                    String name = allCursor.getString(allCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String tphoneNumber = allCursor.getString(allCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    String phoneNumber = allCursor.getString(allCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)).replace(" ", "").replace("-", "").replace("+", "").replace("+", "");

                    if (phoneNumber.length() > 9) {
                        phoneNumber = phoneNumber.substring(phoneNumber.length() - 10);
                    }
                    String EmailAddr = allCursor.getString(allCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA2));
                    String image_thumb = allCursor.getString(allCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI));
                    Common.DisplayLog("", "id = " + id + " name = " + name + " phoneNumber = " + phoneNumber + " EmailAddr = " + EmailAddr + " image_thumb = " + image_thumb);


                    String[] COLUMN = {
                            "",
                            "",
                            name,
                            phoneNumber,
                            image_thumb,
                            String.valueOf(foraction)
                    };
                    if (!name.equals(tphoneNumber) && !mobileNoSet.contains(phoneNumber)) {
                        rcdbh.addData(COLUMN);
                        mobileNoSet.add(phoneNumber);
                    }
                }

                mobileNoSet.clear();
                allCursor.close();

            }

        }
    }

    private void checkonData() {

        final RContactDbHandler rcdbh = new RContactDbHandler(getApplicationContext());
        String[] culist = rcdbh.getAllContactnumber();
        if (Common.hasInternetConnection(getApplicationContext(), false) && culist.length > 0) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<CheckContact> call = apiService.getCheckContact(culist);
            call.enqueue(new Callback<CheckContact>() {
                @Override
                public void onResponse(Call<CheckContact> call, Response<CheckContact> response) {
//
                    int statusCode = response.code();
                    Common.DisplayLog("response.body()", response.body().toString());
                    boolean status = response.body().getStatus();
                    Common.DisplayLog("status", status + "");
                    Common.DisplayLog("getMessage", response.body().getMessage() + "");

                    if (status) {
                        rcdbh.updataForActionAllFalse();
                        final List<Datum> datum = response.body().getData();
                        for (int i = 0; i < datum.size(); i++) {
                            rcdbh.updataForAction(datum.get(i).getVUNumber());
                            Common.DisplayLog("getVUNumber", datum.get(i).getVUNumber() + "");
                        }
                       /* Intent intent = new Intent("custom-event-name");
                        // You can also include some extra data.
                        intent.putExtra("message", "This is my message!");
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);*/

                    } else {
                        Common.DisplayLog("response.body().getMessage()", response.body().getMessage() + "");
                    }

                }

                @Override
                public void onFailure(Call<CheckContact> call, Throwable t) {
                    Common.DisplayLog("fail", t.getMessage());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
                }
            });
        }
    }
}
