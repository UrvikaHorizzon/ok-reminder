package com.horizzon.reminderapp.dao;

/**
 * Created by horizzon on 5/3/2017.
 */

public class LocationSearch {

    private String description, mainText, secondaryText;

    public LocationSearch() {
    }

    public LocationSearch(String description, String mainText, String secondaryText) {
        this.description = description;
        this.mainText = mainText;
        this.secondaryText = secondaryText;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMainText() {
        return mainText;
    }

    public void setMainText(String mainText) {
        this.mainText = mainText;
    }

    public String getSecondaryText() {
        return secondaryText;
    }

    public void setSecondaryText(String secondaryText) {
        this.secondaryText = secondaryText;
    }

    public String getFullText() {
        return mainText + ", " + secondaryText;
    }
}
