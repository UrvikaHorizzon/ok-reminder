
package com.horizzon.reminderapp.retrofit.model.getreminder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetAttachmentList {

    @SerializedName("iId")
    @Expose
    private String iId;
    @SerializedName("vSesssionId")
    @Expose
    private String vSesssionId;
    @SerializedName("iUserId")
    @Expose
    private String iUserId;
    @SerializedName("vAttachFor")
    @Expose
    private String vAttachFor;
    @SerializedName("tDetail")
    @Expose
    private String tDetail;
    @SerializedName("iStatus")
    @Expose
    private String iStatus;
    @SerializedName("dCreatedDateTime")
    @Expose
    private String dCreatedDateTime;
    @SerializedName("vImage")
    @Expose
    private String vImage;

    public String getIId() {
        return iId;
    }

    public void setIId(String iId) {
        this.iId = iId;
    }

    public String getVSesssionId() {
        return vSesssionId;
    }

    public void setVSesssionId(String vSesssionId) {
        this.vSesssionId = vSesssionId;
    }

    public String getIUserId() {
        return iUserId;
    }

    public void setIUserId(String iUserId) {
        this.iUserId = iUserId;
    }

    public String getVAttachFor() {
        return vAttachFor;
    }

    public void setVAttachFor(String vAttachFor) {
        this.vAttachFor = vAttachFor;
    }

    public String getTDetail() {
        return tDetail;
    }

    public void setTDetail(String tDetail) {
        this.tDetail = tDetail;
    }

    public String getIStatus() {
        return iStatus;
    }

    public void setIStatus(String iStatus) {
        this.iStatus = iStatus;
    }

    public String getDCreatedDateTime() {
        return dCreatedDateTime;
    }

    public void setDCreatedDateTime(String dCreatedDateTime) {
        this.dCreatedDateTime = dCreatedDateTime;
    }

    public String getVImage() {
        return vImage;
    }

    public void setVImage(String vImage) {
        this.vImage = vImage;
    }

}
