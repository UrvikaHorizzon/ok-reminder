package com.horizzon.reminderapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import androidx.core.app.ActivityOptionsCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.horizzon.reminderapp.adapter.CommentAdapter;
import com.horizzon.reminderapp.adapter.ContactBoxListAdapter;
import com.horizzon.reminderapp.adapter.ImageAttachmentAdapter;
import com.horizzon.reminderapp.dao.AttachmentTable;
import com.horizzon.reminderapp.dao.ReminderComment;
import com.horizzon.reminderapp.dao.ReminderContactUser;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.fragment.DetailAttachmentTabFragment;
import com.horizzon.reminderapp.fragment.DetailCommentTabFragment;
import com.horizzon.reminderapp.fragment.DetailTabFragment;
import com.horizzon.reminderapp.handler.CRAttachmentDbHandler;
import com.horizzon.reminderapp.handler.CRContactDbHandler;
import com.horizzon.reminderapp.helper.ExtendedViewPager;
import com.horizzon.reminderapp.helper.TouchImageAdapter;
import com.horizzon.reminderapp.model.Message;

import java.util.ArrayList;
import java.util.List;


public class DetailViewActivity extends AppCompatActivity {
    private static final int REQUEST_CODE_CHAT_FORWARD = 99;
    private ArrayList<Message> messageForwardList = new ArrayList<>();
    String SessionId, comeForm, inFrom, action;
    TextView conName, rSub, rDateTime, rday, commentcount, attachcount, userName;
    ImageView imgBack, imgHome, user_image, con_image2, con_image3, imgPriority1, imgPriority2, imgPriority3, tabcomment, tabattach, btn_reject, btn_edit, btn_accept;
    RecyclerView commentList;
    ArrayList<ReminderComment> commentArrayList;
    CommentAdapter commentAdapter;
    LinearLayout tabcommentcontent, tabattachcontent;
    ScrollView mainscroll;
    ContactBoxListAdapter contactboxlistadapter;
    LinearLayout subtaskLayout, maincontentlay, pafeviewerlay, notesLayout, imagesLayout, subtasklistLayout, notlistLayout, contactMainLayout, contactLayout;
    ArrayList<AttachmentTable> imagesList;
    ImageAttachmentAdapter imgAdapter;
    RecyclerView imglistView, participantslist;
    CRAttachmentDbHandler cradbh;
    ExtendedViewPager miimgpwEViewPager;
    public int backstate = 0;
    DetailAttachmentTabFragment activeFragment;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    ArrayList<ReminderContactUser> remindercontactuser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_view);

        action = getIntent().getStringExtra("action");
        if (action == null || action.equals("")) {
            action = "";
        }

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        SessionId = getIntent().getStringExtra("SessionId");
        Common.createSharedPreferences(getApplicationContext(), "SessionId", SessionId);
        if (savedInstanceState != null && (SessionId == null || SessionId.equals(""))) {
            SessionId = savedInstanceState.getString("SessionId");
        }

        inFrom = "done";
        if (getIntent().getStringExtra("comeForm") != null) {
            comeForm = getIntent().getStringExtra("comeForm");
        }

        cradbh = new CRAttachmentDbHandler(getApplicationContext());

        imgBack = (ImageView) findViewById(R.id.backBtn);
        imgHome = (ImageView) findViewById(R.id.homeBtn);

        pafeviewerlay = (LinearLayout) findViewById(R.id.pafeviewerlay);

        pafeviewerlay.setVisibility(View.GONE);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(DetailViewActivity.this, AllActivity.class);
                startActivity(in);

            }
        });

        imgHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(DetailViewActivity.this, HomeActivity.class);
                startActivity(in);

            }
        });

        getContactData();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("SessionId", getIntent().getStringExtra("SessionId"));
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    public void onBackPressed() {

        if (backstate > 0) {
            activeFragment.closeExtendedViewPager();
//            pafeviewerlay.setVisibility(View.GONE);
            backstate = 0;
        } else {
            super.onBackPressed();
        }
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new DetailTabFragment(), "Detail");
        adapter.addFragment(new DetailCommentTabFragment(), "Chat");
        adapter.addFragment(new DetailAttachmentTabFragment(DetailViewActivity.this), "Attachment");
        viewPager.setAdapter(adapter);
        activeFragment = (DetailAttachmentTabFragment) adapter.getItem(2);
        // openChat(DetailCommentTabFragment.newIntent(mContext, messageForwardList, chat), userImage);
    }

    private void openChat(Intent intent, View userImage) {
        ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, userImage, "userImage");
        startActivityForResult(intent, REQUEST_CODE_CHAT_FORWARD, activityOptionsCompat.toBundle());
    }

    public void getContactData() {
        CRContactDbHandler crcdbh = new CRContactDbHandler(getApplicationContext());
        remindercontactuser = crcdbh.getAllCurrentSessContactData(SessionId, "contact");
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (REQUEST_CODE_CHAT_FORWARD):
                if (resultCode == Activity.RESULT_OK) {
                    messageForwardList.clear();
                    ArrayList<Message> temp = data.getParcelableArrayListExtra("FORWARD_LIST");
                    messageForwardList.addAll(temp);
                }
                break;
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public void getExtendedViewPager(Activity _ac, int pos) {

        Common.DisplayLog("pos", "" + pos);
        ArrayList<AttachmentTable> imagesLists = cradbh.getAllCurrentSessDataWithType(SessionId, "image", inFrom);
        String[] imgLists = new String[imagesLists.size()];
        for (int i = 0; i < imagesLists.size(); i++) {
            imgLists[i] = imagesLists.get(i).getDetail();
            Common.DisplayLog("imgLists[" + i + "]", "" + imgLists[i]);
        }

        miimgpwEViewPager = (ExtendedViewPager) _ac.findViewById(R.id.miimgpwEViewPager);
        miimgpwEViewPager.setAdapter(new TouchImageAdapter(_ac, imgLists, 0));
        miimgpwEViewPager.setCurrentItem(pos);
        pafeviewerlay.setVisibility(View.VISIBLE);
        backstate = 1;
    }

}
