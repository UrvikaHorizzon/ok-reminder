package com.horizzon.reminderapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.StrictMode;

import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.horizzon.reminderapp.adapter.LocationSearchAdapter;
import com.horizzon.reminderapp.dao.LocationSearch;
import com.horizzon.reminderapp.dao.ReminderUserAddress;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.RUserAddressDbHandler;
import com.horizzon.reminderapp.helper.location.TrackGPS;
import com.horizzon.reminderapp.retrofit.model.AddAddress;
import com.horizzon.reminderapp.retrofit.model.DeleteAddress;
import com.horizzon.reminderapp.retrofit.model.UpdateAddress;
import com.horizzon.reminderapp.retrofit.model.location.Location;
import com.horizzon.reminderapp.retrofit.model.location.Nearbysearch;
import com.horizzon.reminderapp.retrofit.model.location.Result;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationAddressActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyBBVQzr3trsZgdfonAnK_Gai7GnVurmm_s";
    ImageView imgBack;
    String search_flag;
    TextView txtLayout;
    ListView searchlocationList;
    LinearLayout homeLayout, offcLayout, favLayout, favcontentLayout, homeofficecontentLayout, favouritelistLayout, favouriteaddLayout, doneContainer, btnDonelay, addressactionlay, favactionlay;
    Button saveandsel, deladd, btnTempDone, btnDone, savefav, delfav;
    private RUserAddressDbHandler ruadbh;
    String[] COLUMN;
    private ReminderUserAddress reminderUserAddress;
    private EditText autusingleaddress;
    boolean taskDone = true;
    String selectlastid = "";
    private TrackGPS gps;
    double longitude, latitude, sellongitude, sellatitude;
    boolean listback = false;
    boolean autoalow = false;
    String selId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_address);

        ruadbh = new RUserAddressDbHandler(getApplicationContext());

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        txtLayout = (TextView) findViewById(R.id.txtLayout);
        homeLayout = (LinearLayout) findViewById(R.id.homeLayout);
        offcLayout = (LinearLayout) findViewById(R.id.offcLayout);
        favLayout = (LinearLayout) findViewById(R.id.favLayout);
        favcontentLayout = (LinearLayout) findViewById(R.id.favcontentLayout);
        favouritelistLayout = (LinearLayout) findViewById(R.id.favouritelistLayout);
        favouriteaddLayout = (LinearLayout) findViewById(R.id.favouriteaddLayout);
        homeofficecontentLayout = (LinearLayout) findViewById(R.id.homeofficecontentLayout);
        doneContainer = (LinearLayout) findViewById(R.id.doneContainer);
        btnDonelay = (LinearLayout) findViewById(R.id.btnDonelay);
        addressactionlay = (LinearLayout) findViewById(R.id.addressactionlay);
        favactionlay = (LinearLayout) findViewById(R.id.favactionlay);

        saveandsel = (Button) findViewById(R.id.saveandsel);
        deladd = (Button) findViewById(R.id.deladd);
        btnTempDone = (Button) findViewById(R.id.btnTempDone);
        btnDone = (Button) findViewById(R.id.btnDone);
        savefav = (Button) findViewById(R.id.savefav);
        delfav = (Button) findViewById(R.id.delfav);

        autusingleaddress = (EditText) findViewById(R.id.autusingleaddress);
        searchlocationList = (ListView) findViewById(R.id.searchlocationList);
        searchlocationList.setVisibility(View.GONE);

        Intent in = getIntent();
        search_flag = in.getStringExtra("search_flag");

        saveandsel.setOnClickListener(this);
        deladd.setOnClickListener(this);
        if (search_flag.equals("home")) {
            txtLayout.setVisibility(View.GONE);
            homeLayout.setVisibility(View.VISIBLE);
            offcLayout.setVisibility(View.GONE);
            favLayout.setVisibility(View.GONE);
            btnDonelay.setVisibility(View.GONE);
            favcontentLayout.setVisibility(View.GONE);
            favactionlay.setVisibility(View.GONE);
            homeofficecontentLayout.setVisibility(View.VISIBLE);
            addressactionlay.setVisibility(View.VISIBLE);

            reminderUserAddress = ruadbh.getTableSingleTtpeDateWihtDao(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), search_flag, "0");
            if (reminderUserAddress != null) {
                autusingleaddress.setText(reminderUserAddress.getAddress());
            }
            autusingleaddress.requestFocus();
        } else if (search_flag.equals("office")) {
            txtLayout.setVisibility(View.GONE);
            homeLayout.setVisibility(View.GONE);
            offcLayout.setVisibility(View.VISIBLE);
            favLayout.setVisibility(View.GONE);
            btnDonelay.setVisibility(View.GONE);
            favcontentLayout.setVisibility(View.GONE);
            favactionlay.setVisibility(View.GONE);
            homeofficecontentLayout.setVisibility(View.VISIBLE);
            addressactionlay.setVisibility(View.VISIBLE);

            reminderUserAddress = ruadbh.getTableSingleTtpeDateWihtDao(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), search_flag, "0");
            if (reminderUserAddress != null) {
                autusingleaddress.setText(reminderUserAddress.getAddress());
            }
            autusingleaddress.requestFocus();
        } else if (search_flag.equals("favourite")) {
            txtLayout.setVisibility(View.GONE);
            homeLayout.setVisibility(View.GONE);
            offcLayout.setVisibility(View.GONE);
            favLayout.setVisibility(View.VISIBLE);
            btnDonelay.setVisibility(View.VISIBLE);
            favcontentLayout.setVisibility(View.VISIBLE);
            homeofficecontentLayout.setVisibility(View.GONE);
        } else {
            txtLayout.setVisibility(View.VISIBLE);
            homeLayout.setVisibility(View.GONE);
            offcLayout.setVisibility(View.GONE);
            favLayout.setVisibility(View.GONE);
            btnDonelay.setVisibility(View.GONE);
            favcontentLayout.setVisibility(View.GONE);
            homeofficecontentLayout.setVisibility(View.GONE);
        }

        favouriteaddLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (taskDone) {
                    btnTempDone.performClick();
                    favouritelistLayout.addView(createEditView(search_flag, ""));
                    favouriteaddLayout.setVisibility(View.VISIBLE);
                    taskDone = false;
                } else {
                    Common.ShowToast(getApplicationContext(), "Please complete your task");
                }
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TAG", "onClick: done btn :  ");
                if (!selectlastid.equals("")) {

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(autusingleaddress.getWindowToken(), 0);
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("selectlastid", selectlastid);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    Common.ShowToast(getApplicationContext(), "Please select address");
                }
            }
        });


        imgBack = (ImageView) findViewById(R.id.imgBack);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listback) {
                    Intent returnIntent = new Intent();
                    if (autusingleaddress.getText().toString().trim().equals("")) {

                        returnIntent.putExtra("goto", 2);
                    } else {

                        returnIntent.putExtra("goto", 1);
                    }

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(autusingleaddress.getWindowToken(), 0);

                    returnIntent.putExtra("address", autusingleaddress.getText().toString().trim());
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                    onBackPressed();
                } else {
                    /*InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(autusingleaddress.getWindowToken(), 0);
                    onBackPressed();*/
                    searchlocationList.setVisibility(View.GONE);
                    listback = true;
                }
                onBackPressed();
            }
        });

        /*autusingleaddress.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    autoalow = true;
                    autocompleteadd2(autusingleaddress, autusingleaddress.getText().toString());
                }
            }
        });*/
    /*    autusingleaddress.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                autoalow = true;
                autocompleteadd2(autusingleaddress, autusingleaddress.getText().toString());
                return false;
            }
        });*/
        autusingleaddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                /*
                 * Api calling temp off
                 * */

                /*if (search_flag.equals("favourite")) {
                    autocompleteadd2(autusingleaddress, s.toString());
                }*/
//                autusingleaddress.showDropDown();
            }
        });
        /*autusingleaddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String str = (String) parent.getItemAtPosition(position);
                autusingleaddress.setText(str);
                autusingleaddress.setSelection(autusingleaddress.getText().length());
            }
        });*/
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadSavedData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        gps = new TrackGPS(LocationAddressActivity.this);

        if (gps.canGetLocation()) {
            longitude = gps.getLongitude();
            latitude = gps.getLatitude();
            if (longitude <= 0 && latitude <= 0) {
                onResume();
            }
            Common.DisplayLog("Longitude-Latitude", "Longitude:" + Double.toString(longitude) + "\nLatitude:" + Double.toString(latitude));
        } else {
            gps.showSettingsAlert();
        }
    }

    @Override
    public void onBackPressed() {
        if (listback) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(autusingleaddress.getWindowToken(), 0);
            super.onBackPressed();
        } else {
            searchlocationList.setVisibility(View.GONE);
            listback = true;
        }
    }

    private View createEditView(final String attfor, String preId) {
//        mContainerView = (LinearLayout)findViewById(R.id.parentView);
        Common.DisplayLog("preId ", preId);

        if (preId.equals("")) {

            String[] COLUMN = {
                    "",
                    Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                    search_flag,
                    "",
                    ""
            };


            selId = String.valueOf(ruadbh.addData(COLUMN));
        } else {
            selId = preId;
        }
        final String lastid = selId;

        final ReminderUserAddress rud = ruadbh.getTableSingleTtpeDateWihtDao(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), search_flag, lastid);
        Common.DisplayLog("lastid ", lastid + "");
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View myView = inflater.inflate(R.layout.attachment_edit_row, null);
        myView.setTag(lastid);
        final CheckBox attselchk = (CheckBox) myView.findViewById(R.id.attselchk);


        attselchk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                attselchk.setChecked(false);
                if (isChecked) {
                    selectlastid = rud.getId().toString();
                } else {
                    selectlastid = "";
                }
            }
        });
        attselchk.setChecked(false);

        final LinearLayout editcontanor = myView.findViewById(R.id.editcontanor);
        final TextView texv = myView.findViewById(R.id.textChange);
        String st = rud.getAddress().replace("\\n", "\n").replace("\n", "\r\n");
//        texv.setVisibility(View.GONE);

        texv.setText(st);

//        ed.setText(preId);
        final Button textEdit = myView.findViewById(R.id.textEdit);
        final Button textDelete = myView.findViewById(R.id.textDelete);

        textEdit.setVisibility(View.VISIBLE);
        textDelete.setVisibility(View.GONE);

        tasksEdit(texv, editcontanor, textEdit, textDelete, lastid, attfor, true);
        if (preId.equals("")) {
//            textEdit.performClick();
            textEdit.performClick();
        }

        return myView;
    }

    private void animationForDelete(final Button textEdit, final Button textDelete) {
        final Animation RightSwipe = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.righttoleft);
        final Animation LeftSwipe = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.righttoleft);
        RightSwipe.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                textEdit.setVisibility(View.GONE);
                textDelete.startAnimation(LeftSwipe);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        LeftSwipe.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                textEdit.setVisibility(View.GONE);
                textDelete.setVisibility(View.VISIBLE);
                textDelete.clearAnimation();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
//                textDelete.setVisibility(View.GONE);
//                textEdit.startAnimation(LeftSwipe);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        textEdit.startAnimation(RightSwipe);
    }

    private void animationForEdit(final Button textEdit, final Button textDelete) {
        final Animation RightSwipe = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.righttoleft);
        final Animation LeftSwipe = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.righttoleft);
        RightSwipe.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
//                textDelete.setVisibility(View.GONE);
//                textEdit.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                textDelete.setVisibility(View.GONE);
                textEdit.startAnimation(LeftSwipe);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        LeftSwipe.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                textDelete.setVisibility(View.GONE);
                textEdit.setVisibility(View.VISIBLE);
                textEdit.clearAnimation();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
//                textDelete.setVisibility(View.GONE);
//                textEdit.startAnimation(LeftSwipe);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        textDelete.startAnimation(RightSwipe);
    }

    private void tasksEdit(final TextView texv, final LinearLayout editcontanor, final Button te, final Button td, final String lastid, final String attfor, final boolean preFacus) {


        /*create edit text*/

        te.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                btnTempDone.performClick();
                if (taskDone) {

                    String notesub = texv.getText().toString().trim();

                    homeofficecontentLayout.setVisibility(View.VISIBLE);
                    addressactionlay.setVisibility(View.GONE);
                    favcontentLayout.setVisibility(View.GONE);
                    favactionlay.setVisibility(View.VISIBLE);
                    autusingleaddress.setText(notesub);


                    Common.DisplayLog("edit notesub", notesub + "");
//                    final AutoCompleteTextView ed = new AutoCompleteTextView(getApplicationContext()); // Pass it an Activity or Context
//                    ed.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
//                    ed.setGravity(Gravity.CENTER | Gravity.LEFT);
//                    // ed.setBackgroundResource(0);
//                    ed.setBackgroundColor(Color.WHITE);
//                    ed.setHint("Type here..");
//                    final float scale = getResources().getDisplayMetrics().density;
//                    int lr = (int) (26 * scale + 0.5f);
//                    int tb = (int) (10 * scale + 0.5f);
//                    ed.setPadding(lr, tb, lr, tb);
//                    ed.setTextColor(Color.parseColor("#adadad"));
//                    ed.setHintTextColor(Color.parseColor("#adadad"));
//                    ed.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
//                    editcontanor.removeAllViews();
//                    editcontanor.addView(ed);
//
//                    ed.setText(notesub);
//                    ed.setSelection(ed.getText().length());
//                    ed.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                            Common.DisplayLog("actionId ", actionId + " done " + EditorInfo.IME_ACTION_DONE);
//                            if ((actionId == EditorInfo.IME_ACTION_DONE)) {
//                                Common.DisplayLog("", actionId + "");
//                                btnTempDone.performClick();
//                            }
//                            return false;
//                        }
//                    });
//
//
////                    ed.addTextChangedListener(new TextWatcher() {
////                        @Override
////                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
////
////                        }
////
////                        @Override
////                        public void onTextChanged(CharSequence s, int start, int before, int count) {
////
////                        }
////
////                        @Override
////                        public void afterTextChanged(Editable s) {
////                            autocompleteadd(ed, s.toString());
////                        }
////                    });
//                    ed.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                        @Override
//                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                            String str = (String) parent.getItemAtPosition(position);
//                            ed.setText(str);
//                            ed.setSelection(ed.getText().length());
//                        }
//                    });
                    animationForDelete(te, td);
//                    ed.setFocusable(true);
//                    ed.requestFocus();
//                    ed.setCursorVisible(true);
                    showDoneLayout(autusingleaddress, editcontanor, te, td, lastid, attfor, true);
                }
            }
        });

        /*end edit text*/

    }


    //    private void showDoneLayout(final AutoCompleteTextView edt, final LinearLayout editcontanor, final Button te, final Button td, final String lastid, final String attfor, final boolean preFacus) {
    private void showDoneLayout(final EditText edt, final LinearLayout editcontanor, final Button te, final Button td, final String lastid, final String attfor, final boolean preFacus) {
        if (preFacus) {
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            Common.DisplayLog("tag", "This'll run 300 milliseconds later");
                            edt.setSelection(edt.getText().length());
                            edt.setFocusable(true);
                            edt.requestFocus();
                            edt.setCursorVisible(true);
                            InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(edt, InputMethodManager.SHOW_IMPLICIT);


                            addressactionlay.setVisibility(View.GONE);
                            favactionlay.setVisibility(View.VISIBLE);
                            //doneContainer.setVisibility(View.VISIBLE);//done balt
                        }
                    },
                    500);
        }


//        edt.setSingleLine(false);
////            edt.setLines(3);
//        edt.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);


        td.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.DisplayLog("attfor", "textDelete clicked");
                taskDone = true;
                ruadbh.deleteSingleData(lastid);

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);

                LinearLayout linLayout = (LinearLayout) favouritelistLayout.findViewWithTag(lastid);

                favouritelistLayout.removeView(linLayout);

                //btnTempDone.setOnClickListener(null);
                savefav.setOnClickListener(null);
                doneContainer.setVisibility(View.GONE);

                loadSavedData();

                homeofficecontentLayout.setVisibility(View.GONE);
                favcontentLayout.setVisibility(View.VISIBLE);

            }
        });

        delfav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.DisplayLog("attfor", "textDelete clicked");
                taskDone = true;
                reminderUserAddress = ruadbh.getTableSingleTtpeDateWihtDao(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), search_flag, lastid);
                if (!reminderUserAddress.getHiddenId().equals("")) {
                    deleteAddress(reminderUserAddress.getHiddenId());
                }
                ruadbh.deleteSingleData(lastid);

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);

                LinearLayout linLayout = (LinearLayout) favouritelistLayout.findViewWithTag(lastid);

                favouritelistLayout.removeView(linLayout);

                //btnTempDone.setOnClickListener(null);
                savefav.setOnClickListener(null);
//                doneContainer.setVisibility(View.GONE);

                loadSavedData();

                homeofficecontentLayout.setVisibility(View.GONE);
                favcontentLayout.setVisibility(View.VISIBLE);

            }
        });


        savefav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String notesub = edt.getText().toString().trim();
                Common.DisplayLog("Done", notesub + "");
                if (!notesub.equals("")) {
                    Log.d("TAG", "onClick: notesub : " + notesub);
                    getLocationFormAdress(notesub);
                    reminderUserAddress = ruadbh.getTableSingleTtpeDateWihtDao(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), search_flag, lastid);
                    if (!reminderUserAddress.getHiddenId().equals("")) {
                        updateAddress(reminderUserAddress.getHiddenId(),
                                Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                                search_flag,
                                notesub,
                                sellatitude, sellongitude);
                        ruadbh.updataWithTypeandid(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), notesub.toString(), search_flag, lastid);

                    } else {
                        insertAddress(Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                                search_flag, notesub,
                                sellatitude, sellongitude);
                    }


                    edt.setCursorVisible(false);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                    //imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                    final TextView texv = new TextView(getApplicationContext()); // Pass it an Activity or Context
                    texv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)); // Pass two args; must be LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, or an integer pixel value.
                    texv.setGravity(Gravity.CENTER | Gravity.LEFT);
                    texv.setBackgroundResource(0);
                    final float scale = getResources().getDisplayMetrics().density;
                    int lr = (int) (26 * scale + 0.5f);
                    int tb = (int) (10 * scale + 0.5f);
                    texv.setPadding(lr, tb, lr, tb);
                    texv.setTextColor(Color.parseColor("#adadad"));
                    texv.setHintTextColor(Color.parseColor("#adadad"));
                    texv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                    editcontanor.removeAllViews();
                    editcontanor.addView(texv);

                    texv.setText(notesub);


                    /*create edit text*/
                    tasksEdit(texv, editcontanor, te, td, lastid, attfor, true);


                    /*end edit text*/
                    doneContainer.setVisibility(View.GONE);
                    animationForEdit(te, td);
                    loadSavedData();

                    homeofficecontentLayout.setVisibility(View.GONE);
                    favcontentLayout.setVisibility(View.VISIBLE);
                    taskDone = true;
//                    savefav.setOnClickListener(null);
                } else {
                    Common.ShowToast(getApplicationContext(), "Please enter some words.");
                }
                loadSavedData();
            }
        });

    }


    public void loadSavedData() {

        autoalow = false;

        //load Frvourite
        favouritelistLayout.removeAllViews();
        ArrayList<ReminderUserAddress> lodsubt = ruadbh.getAllTypeWiseDataWithType(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), search_flag);

        if (lodsubt.size() > 0) {
            favouriteaddLayout.setVisibility(View.VISIBLE);
            for (int i = 0; i < lodsubt.size(); i++) {
                if (taskDone) {
                    btnTempDone.performClick();
                    favouritelistLayout.addView(createEditView(lodsubt.get(i).getAddressFor(), lodsubt.get(i).getId()));
//                    taskDone = false;
                }
            }
        }
    }

    public void autocompleteadd(final AutoCompleteTextView autocomp, String input) {
        List<LocationSearch> resultList = null;
        List<String> resultList2 = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
//            sb.append("&sensor=false&types=geocode");
            sb.append("&sensor=false&types=geocode");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
            Common.DisplayLog("url", sb + "");

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
//            Log.e(LOG_TAG, "Error processing Places API URL", e);
        } catch (IOException e) {
//            Log.e(LOG_TAG, "Error connecting to Places API", e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            Common.DisplayLog("GoogleObject", jsonResults.toString());
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
//            resultList = new ArrayList(predsJsonArray.length());
            resultList = new ArrayList();
            resultList2 = new ArrayList();
            for (int i = 0; i < predsJsonArray.length(); i++) {
                Common.DisplayLog("", predsJsonArray.getJSONObject(i).getString("description"));
//                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
                resultList2.add(predsJsonArray.getJSONObject(i).getString("description"));

                JSONObject structured_formatting = predsJsonArray.getJSONObject(i).getJSONObject("structured_formatting");
                resultList.add(new LocationSearch(predsJsonArray.getJSONObject(i).getString("description"), structured_formatting.getString("main_text"), structured_formatting.getString("secondary_text")));
            }
        } catch (JSONException e) {
            Common.DisplayLog("ExceptionJSON", "Cannot process JSON results " + e);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(LocationAddressActivity.this, R.layout.location_autoc_lay2, R.id.mainText, resultList2);
//        LocationSearchAdapter adapter = new LocationSearchAdapter(LocationAddressActivity.this, resultList);
//        ArrayAdapter<LocationSearch> adapter = new LocationSearchAdapter(LocationAddressActivity.this, resultList);

        // Assign adapter to ListView
        autocomp.setAdapter(adapter);

        autocomp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String str = (String) parent.getItemAtPosition(position);
                autocomp.setText(str);
                autocomp.setSelection(autocomp.getText().length());
            }
        });

    }


    public void autocompleteadd2(final EditText autocomp, String input) {
        List<LocationSearch> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();

        StringBuilder sb = new StringBuilder(PLACES_API_BASE + "/nearbysearch" + OUT_JSON);
        sb.append("?key=" + API_KEY);
//            sb.append("&sensor=false&types=geocode");
        sb.append("&location=" + latitude + "," + longitude);
        try {
            sb.append("&rankby=distance&keyword=" + URLEncoder.encode(input, "utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Common.DisplayLog("url", sb + "");
        Common.DisplayLog("url", sb + "");


        ApiInterface apiService =
                ApiClient.getCustomClient(PLACES_API_BASE + "/").create(ApiInterface.class);

        Call<Nearbysearch> call = apiService.getNearbysearchDetails(API_KEY, latitude + "," + longitude, "distance", input);
        call.enqueue(new Callback<Nearbysearch>() {
            @Override
            public void onResponse(Call<Nearbysearch> call, Response<Nearbysearch> response) {
                int statusCode = response.code();
//                    List<Nearbysearch> movies = response.body().getResults();

                final List<Result> results = response.body().getResults();
                Common.DisplayLog("results.size()", results.size() + "");
                if (autoalow) {
                    searchlocationList.setVisibility(View.VISIBLE);
                } else {

                    results.clear();
                    autoalow = true;
                    searchlocationList.setVisibility(View.GONE);
                }
                LocationSearchAdapter adapter = new LocationSearchAdapter(LocationAddressActivity.this, results);

                // Assign adapter to ListView
                searchlocationList.setAdapter(adapter);
//                    searchlocationList.setVisibility(View.VISIBLE);

                searchlocationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        autoalow = false;
                        searchlocationList.setVisibility(View.GONE);
                        Result rs = results.get(position);
                        Location loc = rs.getGeometry().getLocation();
                        sellongitude = loc.getLng();
                        sellatitude = loc.getLat();
                        String str = rs.getVicinity().toString();
                        autocomp.setText(str);
                        autocomp.setSelection(autocomp.getText().length());
                        searchlocationList.setVisibility(View.GONE);
                    }
                });

            }

            @Override
            public void onFailure(Call<Nearbysearch> call, Throwable t) {
                Common.DisplayLog("fail", t.getMessage());
            }

        });
        /*

        LocationSearchAdapter adapter = new LocationSearchAdapter(LocationSearchActivity.this, resultList);

        // Assign adapter to ListView
        searchlocationList.setAdapter(adapter);

        final List<LocationSearch> finalResultList = resultList;
        searchlocationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                String str = (String) parent.getItemAtPosition(position);
                String str = finalResultList.get(position).getDescription().toString();
                autocomp.setText(str);
                autocomp.setSelection(autocomp.getText().length());
                gobacktocr();
            }
        });*/

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.saveandsel:
                Log.d("TAG", "onClick: save click : ");
                if (!autusingleaddress.getText().toString().trim().equals("")) {
                    reminderUserAddress = ruadbh.getTableSingleTtpeDateWihtDao(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), search_flag, "0");
                    getLocationFormAdress(autusingleaddress.getText().toString().trim());
                    if (reminderUserAddress != null) {
                        ruadbh.updataWithType(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), autusingleaddress.getText().toString().trim(), search_flag);
                        Log.d("TAG", "onClick: " + reminderUserAddress.getHiddenId());
                        updateAddress(reminderUserAddress.getHiddenId(),
                                Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                                search_flag,
                                autusingleaddress.getText().toString().trim(),
                                sellatitude, sellongitude);
                        /*
                         * Update address
                         * */

                    } else {


                        /*
                         * Insert Address
                         * */
                        insertAddress(Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                                search_flag, autusingleaddress.getText().toString().trim(),
                                sellatitude, sellongitude);
                    }

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(autusingleaddress.getWindowToken(), 0);
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("goto", 1);
                    returnIntent.putExtra("address", autusingleaddress.getText().toString().trim());
                    returnIntent.putExtra("sellongitude", sellongitude);
                    returnIntent.putExtra("sellatitude", sellatitude);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    Common.ShowToast(getApplicationContext(), "Please enter address");
                }
                break;
            case R.id.deladd:
                Log.d("TAG", "onClick: delete click : ");
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(autusingleaddress.getWindowToken(), 0);

                /*
                 * Delete Address
                 * */
                reminderUserAddress = ruadbh.getTableSingleTtpeDateWihtDao(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), search_flag, "0");
                deleteAddress(reminderUserAddress.getHiddenId());


                Intent returnIntent = new Intent();
                returnIntent.putExtra("goto", 2);
                returnIntent.putExtra("address", autusingleaddress.getText().toString().trim());
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
                break;
            default:
        }
    }


    private void getLocationFormAdress(String locationAddress) {
        Geocoder geocoder = new Geocoder(getApplicationContext());
        List<Address> addresses = null;
        //            addresses = geocoder.getFromLocationName(locationAddress, 1);
        addresses = getLocationFormAdresssearch(geocoder, locationAddress);
        if (addresses != null && addresses.size() > 0) {
            sellatitude = addresses.get(0).getLatitude();
            sellongitude = addresses.get(0).getLongitude();

            String address = "", city2 = "";
            if (addresses.get(0).getAddressLine(0) != null) {
                address = addresses.get(0).getAddressLine(0);
            }
            if (addresses.get(0).getLocality() != null) {
                city2 = addresses.get(0).getLocality();
            }
        }
    }

    private List<Address> getLocationFormAdresssearch(Geocoder geocoder, String locationAddress) {
        List<Address> addresses = new ArrayList<>();
        try {
            addresses = geocoder.getFromLocationName(locationAddress, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses == null || addresses.size() <= 0) {
            List<String> elephantList = Arrays.asList(locationAddress.split(","));
            String nwadd = "";
            for (int i = 1; i < elephantList.size(); i++) {
                nwadd += elephantList.get(i) + ",";
            }
            Common.DisplayLog("nwadd", nwadd + "");
            addresses = getLocationFormAdresssearch(geocoder, nwadd);
        }
        return addresses;
    }

    public void insertAddress(String uId, String addressType, String Address, double latt, double longg) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<AddAddress> callDateTime = apiService.addAddress(uId, addressType, Address, latt, longg);
        callDateTime.enqueue(new Callback<AddAddress>() {
            @Override
            public void onResponse(Call<AddAddress> call, Response<AddAddress> response) {
                Log.d("TAG", "onResponse: " + response.body());
                AddAddress addAddress = response.body();
                for (int i = 0; i < addAddress.getData().size(); i++) {
                    COLUMN = new String[]{
                            "",
                            Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                            search_flag,
                            Address,
                            addAddress.getData().get(i).getId()
                    };

                    String.valueOf(ruadbh.addData(COLUMN));
                }
            }

            @Override
            public void onFailure(Call<AddAddress> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.getMessage());
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void updateAddress(String hidenId, String uId, String addressType, String address, double latt, double longg) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<UpdateAddress> callDateTime = apiService.updateAddress(hidenId, uId, addressType, address, latt, longg);
        callDateTime.enqueue(new Callback<UpdateAddress>() {
            @Override
            public void onResponse(Call<UpdateAddress> call, Response<UpdateAddress> response) {
                UpdateAddress updateAddress = response.body();
            }

            @Override
            public void onFailure(Call<UpdateAddress> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.getMessage());
            }
        });
    }

    public void deleteAddress(String hiddenId) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<DeleteAddress> callDateTime = apiService.deleteAddress(hiddenId);
        callDateTime.enqueue(new Callback<DeleteAddress>() {
            @Override
            public void onResponse(Call<DeleteAddress> call, Response<DeleteAddress> response) {
                Toast.makeText(getApplicationContext(), response.message(), Toast.LENGTH_SHORT).show();

                if (response.isSuccessful()) {
                    ruadbh.deleteDataWithType(Common.getSharedPreferences(getApplicationContext(), "userId", "0"), search_flag);
                }
            }

            @Override
            public void onFailure(Call<DeleteAddress> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
