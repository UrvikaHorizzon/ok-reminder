package com.horizzon.reminderapp;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.horizzon.reminderapp.service.ContactService;


public class InstallDataActivity extends AppCompatActivity {
    private static final int PERMISSION_REQUEST_CODE = 1;
    int PERMISSION_REQUEST_CONTACT = 20;
    String[] PERMISSIONS = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_SMS, Manifest.permission.CAMERA, Manifest.permission.READ_CONTACTS};
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_install_data);
        pd = new ProgressDialog(InstallDataActivity.this);
        pd.setMessage("Loading...");
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        pd.show();
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(InstallDataActivity.this, PERMISSIONS, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        if (!hasPermissions(InstallDataActivity.this, PERMISSIONS)) {
                            ActivityCompat.requestPermissions(InstallDataActivity.this, PERMISSIONS, PERMISSION_REQUEST_CODE);
                        }
                        askForContactPermission();
//                        Intent intent = new Intent(InstallDataActivity.this, HomeActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        startActivity(intent);
//                        finish();
                    }
                },
                2000);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startService(new Intent(InstallDataActivity.this, ContactService.class));
//                    Intent i = new Intent(SplashActivity.this, Activity_VideoHome.class);
//                    startActivity(i);
//                    finish();
//                    Toast.makeText(getApplicationContext(),"Permission Granted",Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void askForContactPermission() {

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(InstallDataActivity.this,
                    Manifest.permission.READ_CONTACTS)) {
                //please confirm Contacts access
                askForContactPermission();
            } else {
                ActivityCompat.requestPermissions(InstallDataActivity.this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        PERMISSION_REQUEST_CONTACT);

            }
        } else {

            startService(new Intent(InstallDataActivity.this, ContactService.class));
            pd.hide();
            Intent intent = new Intent(InstallDataActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            /*new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            pd.hide();
                            Intent intent = new Intent(InstallDataActivity.this, HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    },
                    2000);*/
        }

    }

    @Override
    protected void onPause() {
        // Unregister since the activity is paused.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                mMessageReceiver);
        super.onPause();
    }

    @Override
    protected void onResume() {
        // Register to receive messages.
        // We are registering an observer (mMessageReceiver) to receive Intents
        // with actions named "custom-event-name".
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter("custom-event-name"));
        super.onResume();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            Log.d("receiver", "Got message: " + message);
            Intent i = new Intent(InstallDataActivity.this, HomeActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }
    };
}
