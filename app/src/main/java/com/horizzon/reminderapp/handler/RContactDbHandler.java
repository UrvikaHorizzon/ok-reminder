package com.horizzon.reminderapp.handler;

/**
 * Created by Abhijit Sojitra on 9/3/17.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.horizzon.reminderapp.dao.ContactUser;
import com.horizzon.reminderapp.data.Common;

import java.util.ArrayList;
import java.util.HashMap;

public class RContactDbHandler {


    // table name
    public static final String TABLE_NAME = "r_contact";

    public static final String[] COLUMNS = {
            "Id",
            "sesssionId",
            "name",
            "number",
            "photo",
            "foraction"
    };

    DatabaseHandler dbh;
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
            + "(" + COLUMNS[0] + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMNS[1] + " TEXT,"
            + COLUMNS[2] + " TEXT,"
            + COLUMNS[3] + " TEXT,"
            + COLUMNS[4] + " TEXT,"
            + COLUMNS[5] + " TEXT"
            + ");";

    public RContactDbHandler(Context context) {
        dbh = new DatabaseHandler(context);
    }


    public void addData(String[] c) {
        SQLiteDatabase db = dbh.getWritableDatabase();
        SQLiteDatabase db2 = dbh.getReadableDatabase();

        ContentValues values = new ContentValues();
        for (int j = 1; j < COLUMNS.length; j++) {
//            Common.DisplayLog("columnNames", COLUMNS[j]);
            values.put(COLUMNS[j], c[j]);
        }

//        long id = db.insert(TABLE_NAME, null, values);

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "='" + c[1] + "' AND " + COLUMNS[3] + "='" + c[3] + "' ORDER BY " + COLUMNS[0] + " DESC";
//        String query_select = "SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMNS[3] + "='" + c[3] + "'";
        Common.DisplayLog("query_select", query_select);
//        try {
        Cursor cursor = db2.rawQuery(query_select, null);
        if (cursor != null && cursor.getCount() <= 0) {
            long id = db.insert(TABLE_NAME, null, values);
        }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        db.close();
        db2.close();
    }

    public void updataData(String id, String sesid, String name, String number, String photo) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[2], name);
        values.put(COLUMNS[3], number);
        values.put(COLUMNS[4], photo);

        db.update(TABLE_NAME, values, COLUMNS[0] + " = ? AND " + COLUMNS[1] + " = ?", new String[]{id, sesid});

    }

    public void updataForAction(String sesid, String name) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[5], "true");

        db.update(TABLE_NAME, values, COLUMNS[1], new String[]{sesid});

    }

    public void updataForAction(String numberlist) {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[5], "false");

        db.update(TABLE_NAME, values, COLUMNS[3] + "=?", new String[]{numberlist});

    }

    public void updataForActionAllFalse() {
        SQLiteDatabase db = dbh.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[5], "true");

        db.update(TABLE_NAME, values, null, null);

    }


    public String[] getAllContactnumber() {
        SQLiteDatabase db = dbh.getReadableDatabase();

        String[] list = new String[0];
        ContactUser ContactUser;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " ORDER BY " + COLUMNS[0] + " ASC ";

        Cursor cursor = db.rawQuery(query_select, null);
        if (cursor.moveToFirst()) {
            list = new String[getDataCount() + 1];
            String[] columnNames = cursor.getColumnNames();
            int i = 0;
            do {
                ContactUser = new ContactUser(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), Boolean.valueOf(cursor.getString(5)));
                for (int j = 0; j < columnNames.length; j++) {
                    /*if (ConstantData.log_enable) {
                        Log.e("columnNames", columnNames[j]);
						Log.e("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
					}*/
                }
                String s = ContactUser.getNumber().replace(" ", "").replace("-", "");
                if (ContactUser.getNumber().length() > 9) {
                    s = s.substring(ContactUser.getNumber().length() - 10);
                }
                list[i] = s;
                Common.DisplayLog("list[i]", list[i] + "");
                i++;
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public ArrayList<ContactUser> getAllContactData() {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<ContactUser> list = new ArrayList<ContactUser>();
        ContactUser ContactUser;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " ORDER BY " + COLUMNS[0] + " ASC ";

        Cursor cursor = db.rawQuery(query_select, null);
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                ContactUser = new ContactUser(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), Boolean.valueOf(cursor.getString(5)));
                for (int j = 0; j < columnNames.length; j++) {
                    /*if (ConstantData.log_enable) {
                        Log.e("columnNames", columnNames[j]);
						Log.e("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
					}*/
                }
                list.add(ContactUser);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public Cursor getAllContactCursor() {
        SQLiteDatabase db = dbh.getReadableDatabase();

        String query_select = "SELECT  * FROM " + TABLE_NAME + " ORDER BY " + COLUMNS[0] + " ASC ";

        Cursor cursor = db.rawQuery(query_select, null);
        cursor.close();

        return cursor;
    }

    public ArrayList<HashMap<String, String>> getAllData(int id) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> hashmap;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " ORDER BY " + COLUMNS[0] + " DESC ";

        Cursor cursor = db.rawQuery(query_select, null);
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                hashmap = new HashMap<String, String>();
                for (int j = 0; j < columnNames.length; j++) {
                    /*if (ConstantData.log_enable) {
                        Log.e("columnNames", columnNames[j]);
						Log.e("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
					}*/
                    hashmap.put(COLUMNS[j], cursor.getString(j));
                }
                list.add(hashmap);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public HashMap<String, String> getTableSingleDate(String sesid) {
        SQLiteDatabase db = dbh.getReadableDatabase();

        HashMap<String, String> hashmap = null;

        String query_select = "SELECT  * FROM " + TABLE_NAME + " WHERE " + COLUMNS[1] + "=" + sesid + " ORDER BY " + COLUMNS[0] + " DESC";

        Cursor cursor = db.rawQuery(query_select, null);
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            do {
                hashmap = new HashMap<String, String>();
                for (int j = 0; j < columnNames.length; j++) {
                    /*if (ConstantData.log_enable) {
                        Log.e("columnNames", columnNames[j]);
						Log.e("", "columnNames   " + columnNames[j] + " => " + cursor.getString(j));
					}*/
                    hashmap.put(COLUMNS[j], cursor.getString(j));
                }

            } while (cursor.moveToNext());
        }
        cursor.close();

        return hashmap;
    }

    public void resetTempdata(String sessid) {
        SQLiteDatabase db = dbh.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.execSQL("delete from " + TABLE_NAME + "WHERE " + COLUMNS[1] + " = " + sessid);
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }


    public void resetTable() {
        SQLiteDatabase db = dbh.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.execSQL("delete from " + TABLE_NAME);
        db.execSQL("delete from sqlite_sequence where name='" + TABLE_NAME + "'");
        // db.execSQL("TRUNCATE table " + TABLE_NAME);
        db.close();
    }

    public int getDataCount() {
        SQLiteDatabase db = dbh.getReadableDatabase();

        String query_select = "SELECT  * FROM " + TABLE_NAME + " ORDER BY " + COLUMNS[0] + " ASC ";

        Cursor cursor = db.rawQuery(query_select, null);
        int totcount = cursor.getCount();
        cursor.close();

        return totcount;
    }

}
