package com.horizzon.reminderapp;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.horizzon.reminderapp.adapter.ImageAttachmentAdapter;
import com.horizzon.reminderapp.dao.AttachmentTable;
import com.horizzon.reminderapp.dao.CReminder;
import com.horizzon.reminderapp.data.Common;
import com.horizzon.reminderapp.handler.CRAttachmentDbHandler;
import com.horizzon.reminderapp.handler.CRContactDbHandler;
import com.horizzon.reminderapp.handler.CreateReminderDbHandler;
import com.horizzon.reminderapp.helper.Clickable;
import com.horizzon.reminderapp.helper.customgallery.CustomGalleryMultiPhotoSelectActivity;
import com.horizzon.reminderapp.retrofit.model.reminder.Reminder;
import com.horizzon.reminderapp.retrofit.rest.ApiClient;
import com.horizzon.reminderapp.retrofit.rest.ApiInterface;
import com.horizzon.reminderapp.utility.GridSpacingItemDecoration;
import com.horizzon.reminderapp.utility.RealPathUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AttachmentActivity extends AppCompatActivity implements Clickable {
    ImageView imgBack, imgHome;
    TextView txtItem;
    ImageView imgSubtask, imgNotes, imgGallery, imgCamera, imgContact, imgDropbox, imgOnenote, imgGDrive;
    LinearLayout subtaskLayout, addLayout, notesLayout, imagesLayout, doneContainer, subtasklistLayout, notlistLayout, contactMainLayout, contactLayout;
    ArrayList<AttachmentTable> imagesList;
    //    ImageGridCustomAdapter imgAdapter;
    ImageAttachmentAdapter imgAdapter;
    //    ExpandableHeightGridView imglistView;
    RecyclerView imglistView;
    Button btnDone, btnTempDone;
    Bitmap bitmap;
    public int item_count = 0;
    String SessionId;
    boolean taskDone = true;
    boolean notCount = true;
    String inFrom = "", action;
    ScrollView mainscroll;

    CRAttachmentDbHandler cradbh;
    ProgressDialog pd;
    boolean sattachment = false;
    private static final int REQUEST_WRITE_PERMISSION = 101;
    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attachment);
        requestPermission();
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        action = getIntent().getStringExtra("action");
        if (action == null || action.equals("")) {
            action = "add";
        }

        SessionId = Common.getSharedPreferences(getApplicationContext(), "SessionId", "");
        cradbh = new CRAttachmentDbHandler(getApplicationContext());


        if (getIntent().getStringExtra("inFrom") != null) {
            inFrom = (getIntent().getStringExtra("inFrom"));
            if (inFrom.equals("main")) {
                inFrom = "done";
                cradbh.resetTempStatusdata(SessionId, "false");
            } else {
                inFrom = "";
            }
        }
        Common.DisplayLog("inFrom", inFrom + "");

        mainscroll = (ScrollView) findViewById(R.id.mainscroll);
        imgBack = (ImageView) findViewById(R.id.backBtn);
        imgHome = (ImageView) findViewById(R.id.homeBtn);
        imgSubtask = (ImageView) findViewById(R.id.imgSubtask);
        imgNotes = (ImageView) findViewById(R.id.imgNotes);
        imgGallery = (ImageView) findViewById(R.id.imgGallery);
        imgCamera = (ImageView) findViewById(R.id.imgCamera);
        imgContact = (ImageView) findViewById(R.id.imgContact);
        imgDropbox = (ImageView) findViewById(R.id.imgDropbox);
        imgOnenote = (ImageView) findViewById(R.id.imgOnenote);
        imgGDrive = (ImageView) findViewById(R.id.imgGDrive);
        txtItem = (TextView) findViewById(R.id.txtItem);

        subtasklistLayout = (LinearLayout) findViewById(R.id.subtasklistLayout);
        notlistLayout = (LinearLayout) findViewById(R.id.notlistLayout);
        doneContainer = (LinearLayout) findViewById(R.id.doneContainer);
        contactMainLayout = (LinearLayout) findViewById(R.id.contactMainLayout);
        contactLayout = (LinearLayout) findViewById(R.id.contactLayout);

        subtaskLayout = (LinearLayout) findViewById(R.id.subtaskLayout);
        notesLayout = (LinearLayout) findViewById(R.id.notesLayout);
        imagesLayout = (LinearLayout) findViewById(R.id.imagesLayout);
        addLayout = (LinearLayout) findViewById(R.id.addLayout);
//        imglistView = (ExpandableHeightGridView) findViewById(R.id.imglistView);
        imglistView = (RecyclerView) findViewById(R.id.imglistView);


        btnDone = (Button) findViewById(R.id.btnDone);
        btnTempDone = (Button) findViewById(R.id.btnTempDone);


        getAttachImages();

//        imgAdapter = new ImageGridCustomAdapter(this, imagesList, this);
//        imglistView.setAdapter(imgAdapter);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (action.equals("edit")) {
                    onBackPressed();
                } else {
                    gobacktocr();
                }
            }
        });

        imgHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(AttachmentActivity.this, HomeActivity.class);
                startActivity(in);
            }
        });


        addLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (taskDone) {
                    btnTempDone.performClick();
                    subtasklistLayout.addView(createEditView("subtask", ""));
                    subtaskLayout.setVisibility(View.VISIBLE);
                    addLayout.setVisibility(View.VISIBLE);
                    taskDone = false;
                    item_count++;
                } else {
                    Common.ShowToast(getApplicationContext(), "Please complete your task");
                }
                reloadCount();

            }
        });

        imgSubtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addLayout.performClick();
                inFrom = "";
            }
        });

        imgNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notesLayout.setVisibility(View.VISIBLE);
                if (taskDone && notCount) {
                    Common.DisplayLog("", "imgNoteEdit clicked");
                    btnTempDone.performClick();
                    notlistLayout.addView(createEditView("note", ""));
                    taskDone = false;
                    notCount = false;
                } else if (!notCount) {
//                    Common.ShowToast(getApplicationContext(),"Please complete your task");
                } else {
                    Common.ShowToast(getApplicationContext(), "Please complete your task");
                }
                reloadCount();
                inFrom = "";
            }
        });

        imgGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                imagesLayout.setVisibility(View.VISIBLE);
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                /*{
                    Intent GalleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                    GalleryIntent.setType("image*//*");
                    GalleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    startActivityForResult(GalleryIntent, 2);
                }*/
                /*{
                    Intent GalleryIntent = new Intent(getApplicationContext(), CustomGalleryMultiPhotoSelectActivity.class);
                    startActivityForResult(GalleryIntent, 3);
                }*/
               /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
                } else {*/
                    /*Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 3);
                    inFrom = "";*/

                {
                    Intent GalleryIntent = new Intent(getApplicationContext(), CustomGalleryMultiPhotoSelectActivity.class);
                    startActivityForResult(GalleryIntent, 3);
                }
                inFrom = "";
                //}
            }

        });

        imgCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                imagesLayout.setVisibility(View.VISIBLE);
              /*  Intent CameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                CameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, MyFileContentProvider.CONTENT_URI);
                startActivityForResult(CameraIntent, 1);*/

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                } else {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, 1);
                }
                inFrom = "";
            }
        });

        imgDropbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        imgContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(AttachmentActivity.this, ContactActivity.class);
                in.putExtra("moveto", "2");
                startActivity(in);
                inFrom = "";
//                startActivityForResult(in, 2);
            }
        });

        imgOnenote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        imgGDrive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (action.equals("edit")) {
                    Common.DisplayLog("action : ", action);
                    SavedEditDataOnServer(SessionId);
                } else {
                    cradbh.updataStatus(SessionId, "done");
                    gobacktocr();
                }
            }
        });


    }


    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        } else if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 1);
            } else {
                Toast.makeText(AttachmentActivity.this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        Common.DisplayLog("onStart", "onStart");
        loadSavedData(SessionId);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Common.DisplayLog("onRestart", "onRestart");
        loadSavedData(SessionId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Common.DisplayLog("onResume", "onResume");
//        loadSavedData(SessionId);
    }

    private void gobacktocr() {
        Intent in = null;
        in = new Intent(getApplicationContext(), CreateReminderActivity.class);
        in.putExtra("newrem", "no");
        startActivity(in);

    }

    private View createEditView(final String attfor, String preId) {
//        mContainerView = (LinearLayout)findViewById(R.id.parentView);
        Common.DisplayLog("preId ", preId);
        String selId;
        if (preId.equals("")) {
            String[] COLUMN = {
                    "",
                    SessionId,
                    Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                    attfor,
                    "",
//                    "",
                    "false",
                    Common.getSQLDateTime()
            };
            selId = String.valueOf(cradbh.addData(COLUMN));
            Common.DisplayLog("if == selId ", selId);
        } else {

            selId = preId;
            Common.DisplayLog("else == selId ", selId);
        }
        final String lastid = selId;

        final AttachmentTable at = cradbh.getTableSingleDateWihtDao(SessionId, lastid);
        Common.DisplayLog("lastid ", lastid + "");
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View myView = inflater.inflate(R.layout.attachment_edit_row, null);
        myView.setTag(lastid);
        final CheckBox attselchk = (CheckBox) myView.findViewById(R.id.attselchk);


        attselchk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                attselchk.setChecked(false);
            }
        });
        attselchk.setChecked(false);
        if (attfor.equals("note")) {
            attselchk.setVisibility(View.GONE);
        }

        final LinearLayout editcontanor = (LinearLayout) myView.findViewById(R.id.editcontanor);
        final TextView texv = (TextView) myView.findViewById(R.id.textChange);
        String st = at.getDetail().replace("\\n", "\n").replace("\n", "\r\n");
//        texv.setVisibility(View.GONE);

        texv.setText(st);

//        ed.setText(preId);
        final Button textEdit = (Button) myView.findViewById(R.id.textEdit);
        final Button textDelete = (Button) myView.findViewById(R.id.textDelete);

        textEdit.setVisibility(View.VISIBLE);
        textDelete.setVisibility(View.GONE);

        tasksEdit(texv, editcontanor, textEdit, textDelete, lastid, attfor, true);
        if (preId.equals("")) {
//            textEdit.performClick();
            textEdit.performClick();
        } else if (attfor.equals("note")) {

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {

                            final EditText texv1 = new EditText(getApplicationContext()); // Pass it an Activity or Context
                            texv1.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                            texv1.setGravity(Gravity.CENTER | Gravity.LEFT);
                            texv1.setBackgroundResource(0);
                            final float scale = getResources().getDisplayMetrics().density;
                            int lr = (int) (26 * scale + 0.5f);
                            int tb = (int) (10 * scale + 0.5f);
                            texv1.setPadding(lr, tb, lr, tb);
                            texv1.setTextColor(Color.parseColor("#adadad"));
                            texv1.setHintTextColor(Color.parseColor("#adadad"));
                            texv1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                            editcontanor.removeAllViews();
                            editcontanor.addView(texv1);
                            texv1.setText(at.getDetail());
                            Common.DisplayLog("texv1", texv1.getText().toString());
                            /*text text*/

                            String edst = texv1.getText().toString().trim();
                            final TextView texv2 = new TextView(getApplicationContext()); // Pass it an Activity or Context
                            texv2.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                            texv2.setGravity(Gravity.CENTER | Gravity.LEFT);
                            texv2.setBackgroundResource(0);
                            texv2.setPadding(lr, tb, lr, tb);
                            texv2.setTextColor(Color.parseColor("#adadad"));
                            texv2.setHintTextColor(Color.parseColor("#adadad"));
                            texv2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                            editcontanor.removeAllViews();
                            editcontanor.addView(texv2);
                            texv2.setText(edst);

                        }
                    },
                    1000);
            /*textEdit.performClick();
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {

                            btnTempDone.performClick();
                        }
                    },
                    1000);*/
        }

        if (action.equals("edit") && !Common.getSharedPreferences(getApplicationContext(), "userId", "0").equals(at.getiUserId())) {
            textEdit.setOnClickListener(null);
            textEdit.setVisibility(View.GONE);
            textDelete.setOnClickListener(null);
            textDelete.setVisibility(View.GONE);
        }
//        showDoneLayout(ed, textEdit, textDelete, lastid, attfor, (preId.equals("")) ? true : false);

        return myView;
    }

    private void animationForDelete(final Button textEdit, final Button textDelete) {
        final Animation RightSwipe = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.righttoleft);
        final Animation LeftSwipe = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.righttoleft);
        RightSwipe.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                textEdit.setVisibility(View.GONE);
                textDelete.startAnimation(LeftSwipe);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        LeftSwipe.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                textEdit.setVisibility(View.GONE);
                textDelete.setVisibility(View.VISIBLE);
                textDelete.clearAnimation();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
//                textDelete.setVisibility(View.GONE);
//                textEdit.startAnimation(LeftSwipe);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        textEdit.startAnimation(RightSwipe);
    }

    private void animationForEdit(final Button textEdit, final Button textDelete) {
        final Animation RightSwipe = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.righttoleft);
        final Animation LeftSwipe = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.righttoleft);
        RightSwipe.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
//                textDelete.setVisibility(View.GONE);
//                textEdit.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                textDelete.setVisibility(View.GONE);
                textEdit.startAnimation(LeftSwipe);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        LeftSwipe.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                textDelete.setVisibility(View.GONE);
                textEdit.setVisibility(View.VISIBLE);
                textEdit.clearAnimation();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
//                textDelete.setVisibility(View.GONE);
//                textEdit.startAnimation(LeftSwipe);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        textDelete.startAnimation(RightSwipe);
    }

    private void tasksEdit(final TextView texv, final LinearLayout editcontanor, final Button te, final Button td, final String lastid, final String attfor, final boolean preFacus) {


        /*create edit text*/

        te.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                btnTempDone.performClick();
                if (taskDone) {


                    String notesub = texv.getText().toString().trim();
                    Common.DisplayLog("edit notesub", notesub + "");
                    EditText ed = new EditText(getApplicationContext()); // Pass it an Activity or Context
                    ed.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    ed.setGravity(Gravity.CENTER | Gravity.LEFT);
                    ed.setBackgroundResource(0);
                    ed.setHint("Type here..");
                    final float scale = getResources().getDisplayMetrics().density;
                    int lr = (int) (26 * scale + 0.5f);
                    int tb = (int) (10 * scale + 0.5f);
                    ed.setPadding(lr, tb, lr, tb);
                    ed.setTextColor(Color.parseColor("#adadad"));
                    ed.setHintTextColor(Color.parseColor("#adadad"));
                    ed.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                    editcontanor.removeAllViews();
                    editcontanor.addView(ed);

                    ed.setText(notesub);
                    ed.setSelection(ed.getText().length());
                    ed.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            Common.DisplayLog("actionId ", actionId + " done " + EditorInfo.IME_ACTION_DONE);
                            if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                                Common.DisplayLog("", actionId + "");
                                btnTempDone.performClick();
                            }
                            return false;
                        }
                    });

                    animationForDelete(te, td);

                    ed.setFocusable(true);
                    ed.requestFocus();
                    ed.setCursorVisible(true);
                    showDoneLayout(ed, editcontanor, te, td, lastid, attfor, true);
                }
            }
        });

        /*end edit text*/

    }


    private void tasksEdit2(final EditText texv, final LinearLayout editcontanor, final Button te, final Button td, final String lastid, final String attfor, final boolean preFacus) {


        /*create edit text*/

        te.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                btnTempDone.performClick();
                if (taskDone) {


                    String notesub = texv.getText().toString().trim();
                    Common.DisplayLog("edit notesub", notesub + "");
                    EditText ed = new EditText(getApplicationContext()); // Pass it an Activity or Context
                    ed.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    ed.setGravity(Gravity.CENTER | Gravity.LEFT);
                    ed.setBackgroundResource(0);
                    ed.setHint("Type here..");
                    final float scale = getResources().getDisplayMetrics().density;
                    int lr = (int) (26 * scale + 0.5f);
                    int tb = (int) (10 * scale + 0.5f);
                    ed.setPadding(lr, tb, lr, tb);
                    ed.setTextColor(Color.parseColor("#adadad"));
                    ed.setHintTextColor(Color.parseColor("#adadad"));
                    ed.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                    editcontanor.removeAllViews();
                    editcontanor.addView(ed);

                    ed.setText(notesub);
                    ed.setSelection(ed.getText().length());
                    ed.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            Common.DisplayLog("actionId ", actionId + " done " + EditorInfo.IME_ACTION_DONE);
                            if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                                Common.DisplayLog("", actionId + "");
                                btnTempDone.performClick();
                            }
                            return false;
                        }
                    });

                    animationForDelete(te, td);

                    ed.setFocusable(true);
                    ed.requestFocus();
                    ed.setCursorVisible(true);
                    showDoneLayout(ed, editcontanor, te, td, lastid, attfor, true);
                }
            }
        });

        /*end edit text*/

    }

    private void showDoneLayout(final EditText edt, final LinearLayout editcontanor, final Button te, final Button td, final String lastid, final String attfor, final boolean preFacus) {
        if (preFacus) {
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            Common.DisplayLog("tag", "This'll run 300 milliseconds later");
                            edt.setSelection(edt.getText().length());
                            edt.setFocusable(true);
                            edt.requestFocus();
                            edt.setCursorVisible(true);
                            InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(edt, InputMethodManager.SHOW_IMPLICIT);

                            doneContainer.setVisibility(View.VISIBLE);
                        }
                    },
                    500);
        }


        if (attfor.equals("subtask")) {

            edt.setSingleLine(true);
            edt.setImeOptions(EditorInfo.IME_ACTION_DONE);
        } else if (attfor.equals("note")) {
            edt.setSingleLine(false);
//            edt.setLines(3);
            edt.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
        }


        td.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.DisplayLog("attfor", "textDelete clicked");
                taskDone = true;
                cradbh.deleteSingleData(SessionId, lastid);
                if (action.equals("edit")) {
                    removeAttachmentDataOnServer(SessionId, lastid);
                }
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                if (attfor.equals("subtask")) {
                    LinearLayout linLayout = (LinearLayout) subtasklistLayout.findViewWithTag(lastid);

                    subtasklistLayout.removeView(linLayout);
                    item_count--;
                    if (item_count <= 0) {
                        subtaskLayout.setVisibility(View.GONE);
                    }
                } else if (attfor.equals("note")) {
                    Common.DisplayLog("attfor", "note");
                    LinearLayout linLayout = (LinearLayout) notlistLayout.findViewWithTag(lastid);
                    notlistLayout.removeView(linLayout);
                    notesLayout.setVisibility(View.GONE);
                    notCount = true;
                }
                cradbh.deleteSingleData(SessionId, lastid);
                btnTempDone.setOnClickListener(null);
                doneContainer.setVisibility(View.GONE);
                reloadCount();

            }
        });

        btnTempDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String notesub = edt.getText().toString().trim();
                Common.DisplayLog("Done", notesub + "");
                if (!notesub.equals("")) {


                    cradbh.updataData(lastid, SessionId, attfor, notesub.toString());


                    edt.setCursorVisible(false);

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
//                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                    final TextView texv = new TextView(getApplicationContext()); // Pass it an Activity or Context
                    texv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)); // Pass two args; must be LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, or an integer pixel value.
                    texv.setGravity(Gravity.CENTER | Gravity.LEFT);
                    texv.setBackgroundResource(0);
                    final float scale = getResources().getDisplayMetrics().density;
                    int lr = (int) (26 * scale + 0.5f);
                    int tb = (int) (10 * scale + 0.5f);
                    texv.setPadding(lr, tb, lr, tb);
                    texv.setTextColor(Color.parseColor("#adadad"));
                    texv.setHintTextColor(Color.parseColor("#adadad"));
                    texv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                    editcontanor.removeAllViews();
                    editcontanor.addView(texv);

                    texv.setText(notesub);


                    /*create edit text*/
                    tasksEdit(texv, editcontanor, te, td, lastid, attfor, true);
                    /*te.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                btnTempDone.performClick();
                            if (taskDone) {


                                String notesub = texv.getText().toString().trim();
                                Common.DisplayLog("edit notesub",notesub+"");
                                EditText ed = new EditText(getApplicationContext()); // Pass it an Activity or Context
                                ed.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                                ed.setGravity(Gravity.CENTER | Gravity.LEFT);
                                ed.setBackgroundResource(0);
                                ed.setHint("Type here..");
                                final float scale = getResources().getDisplayMetrics().density;
                                int lr = (int) (26 * scale + 0.5f);
                                int tb = (int) (10 * scale + 0.5f);
                                ed.setPadding(lr,tb,lr,tb);
                                ed.setTextColor(Color.parseColor("#adadad"));
                                ed.setHintTextColor(Color.parseColor("#adadad"));
                                ed.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                                editcontanor.removeAllViews();
                                editcontanor.addView(ed);

                                ed.setText(notesub);
                                ed.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                        Common.DisplayLog("actionId ", actionId + " done " + EditorInfo.IME_ACTION_DONE);
                                        if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                                            Common.DisplayLog("", actionId + "");
                                            btnTempDone.performClick();
                                        }
                                        return false;
                                    }
                                });

                                animationForDelete(te, td);

                                ed.setFocusable(true);
                                ed.requestFocus();
                                ed.setCursorVisible(true);
                                showDoneLayout(ed,editcontanor, te, td, lastid, attfor, true);
//                    ed.setFocusable(false);
                            }
                        }
                    });*/

                    /*end edit text*/


                    doneContainer.setVisibility(View.GONE);


                    animationForEdit(te, td);
                    taskDone = true;
                    btnTempDone.setOnClickListener(null);
                    reloadCount();
                } else {
                    Common.ShowToast(getApplicationContext(), "Please enter some words.");
                }
            }
        });

    }

    private void reloadCount() {
        txtItem.setText(cradbh.getDataCount(SessionId, inFrom) + " items");
    }

    private void getAttachImages() {

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        imagesList = cradbh.getAllCurrentSessDataWithType(SessionId, "image", inFrom);
                        Common.DisplayLog("imagesList count", imagesList.size() + "");
                        if (imagesList.size() > 0) {
                            imagesLayout.setVisibility(View.VISIBLE);
                        } else {
                            imagesLayout.setVisibility(View.GONE);
                        }
                        imglistView.setAdapter(null);
//                        imgAdapter = new ImageGridCustomAdapter(AttachmentActivity.this, imagesList, AttachmentActivity.this);
                        imgAdapter = new ImageAttachmentAdapter(AttachmentActivity.this, imagesList, AttachmentActivity.this, "AttachmentActivity", action);
                        GridLayoutManager lLayout = new GridLayoutManager(AttachmentActivity.this, 2);
                        imglistView.addItemDecoration(new GridSpacingItemDecoration(2, 5, false));
                        imglistView.setHasFixedSize(true);
                        imglistView.setLayoutManager(lLayout);
                        imglistView.setAdapter(imgAdapter);
//        imgAdapter.notifyDataSetChanged();

                    }
                },
                200);

    }

    private void getAttachContact() {
        contactMainLayout.setVisibility(View.GONE);

        contactLayout.removeAllViews();
        final CRContactDbHandler crcdbh = new CRContactDbHandler(getApplicationContext());
        ArrayList<AttachmentTable> remindercontactuser = cradbh.getAllCurrentSessDataWithType(SessionId, "contact", inFrom);


        Common.DisplayLog("remindercontactuser", remindercontactuser.size() + "");
        if (remindercontactuser.size() > 0) {
            contactMainLayout.setVisibility(View.VISIBLE);
            int i = 0;
            for (i = 0; i < remindercontactuser.size(); i++) {

                final AttachmentTable rcu = remindercontactuser.get(i);


                LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = vi.inflate(R.layout.attachcontact_view, null);

                TextView conName = (TextView) v.findViewById(R.id.conName);

                ImageView imageView = (ImageView) v.findViewById(R.id.con_image);
                LinearLayout btncall = (LinearLayout) v.findViewById(R.id.btncall);
                LinearLayout btntask = (LinearLayout) v.findViewById(R.id.btntask);
                LinearLayout btnsave = (LinearLayout) v.findViewById(R.id.btnsave);
                Button conDelete = (Button) v.findViewById(R.id.conDelete);

                try {
                    final JSONObject json = new JSONObject(rcu.getDetail());
                    Common.DisplayLog("json.toString()", json.toString() + "");
                    conName.setText(json.getString("name").toString());
//                    String imgpath = getRealPathFromURI(Uri.parse(((json.getString("photo").toString().equals("")) ? "defaultimg" : json.getString("photo").toString()) + ""));
                    String imgpath = ((json.getString("photo").toString().equals("")) ? "defaultimg" : json.getString("photo").toString()) + "";
                    final String number = json.getString("number").toString();
                    Common.DisplayLog("imgpath", imgpath + "");
                    Common.DisplayLog("json.getString('photo').toString()", json.getString("photo").toString() + "");
                    Picasso.with(getApplicationContext())
                            .load(imgpath)
                            .placeholder(R.drawable.defaultusericon)
                            .error(R.drawable.defaultusericon)
                            .into(imageView);

                    btncall.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            try {
                                intent.setData(Uri.parse("tel:" + json.getString("number").toString()));
                                startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    btntask.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                            try {
                                sendIntent.setData(Uri.parse("sms:" + json.getString("number").toString()));
                                startActivity(sendIntent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    btnsave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                            try {
                                Intent intent = new Intent(Intent.ACTION_INSERT);
                                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                                intent.putExtra(ContactsContract.Intents.Insert.NAME, json.getString("name").toString());
                                intent.putExtra(ContactsContract.Intents.Insert.PHONE, json.getString("number").toString());
                                int PICK_CONTACT = 100;
                                startActivityForResult(intent, PICK_CONTACT);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    });
                    conDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (action.equals("edit")) {
                                removeAttachmentDataOnServer(SessionId, rcu.getId());
                            }
                            cradbh.resetTempIddata(SessionId, rcu.getId());
                            crcdbh.deleteTempNumber(SessionId, number);
                            getAttachContact();
                        }
                    });

                    if (action.equals("edit") && !Common.getSharedPreferences(getApplicationContext(), "userId", "0").equals(rcu.getiUserId())) {
                        conDelete.setOnClickListener(null);
//                        conDelete.setVisibility(View.GONE);
                        conDelete.setBackgroundResource(0);
                    }

                    contactLayout.addView(v);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (action.equals("edit")) {
            super.onBackPressed();
        } else {
            gobacktocr();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
//        loadSavedData(SessionId);
        if (requestCode == 1 && data != null) {

            imagesLayout.setVisibility(View.VISIBLE);
//            Bundle extras = data.getExtras();
//            bitmap = (Bitmap) extras.get("data");
            Uri selectedImageUri;

            if (data.getData() != null) {
                selectedImageUri = data.getData();
            } else {
                Bundle extras = data.getExtras();
                bitmap = (Bitmap) extras.get("data");
                selectedImageUri = getImageUri(getApplicationContext(), bitmap);
            }
//            Uri selectedImageUri = data.getData();
            Common.DisplayLog("data.getData()", data.getExtras().get("data") + "");
            Common.DisplayLog("data.getData()", data.getData() + "");
            Common.DisplayLog("selectedImageUri", selectedImageUri + "");
            Common.DisplayLog("getRealPathFromURI(selectedImageUri)", getRealPathFromURI(selectedImageUri) + "");
            String[] COLUMN = {
                    "",
                    SessionId,
                    Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                    "image",
                    getRealPathFromURI(selectedImageUri),
                    "false", Common.getSQLDateTime()
            };
            final String lastid = String.valueOf(cradbh.addData(COLUMN));
            Common.DisplayLog("lastid ", lastid + "");
//            ud.setImg_attach(bitmap);
         //  getAttachImages();
        }

        if (requestCode == 2 && data != null) {
            Uri selectedImageUri;

            if (data.getClipData() != null) {
                ClipData mClipData = data.getClipData();
                ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                for (int i = 0; i < mClipData.getItemCount(); i++) {

                    ClipData.Item item = mClipData.getItemAt(i);
                    selectedImageUri = item.getUri();
                    mArrayUri.add(selectedImageUri);


                    Common.DisplayLog("mArrayUri selectedImageUri", selectedImageUri + "");
                    Common.DisplayLog("mArrayUri getRealPathFromURI(selectedImageUri)", getRealPathFromURI(selectedImageUri) + "");

                    String[] COLUMN = {
                            "",
                            SessionId,
                            Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                            "image",
                            getRealPathFromURI(selectedImageUri),
                            "false", Common.getSQLDateTime()
                    };
                    final String lastid = String.valueOf(cradbh.addData(COLUMN));
                    Common.DisplayLog("lastid ", lastid + "");


                }
                Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
            } else {

                if (data.getData() != null) {
                    selectedImageUri = data.getData();
                    Common.DisplayLog("data.getData()", data.getData() + "");
                } else {
                    Bundle extras = data.getExtras();
                    bitmap = (Bitmap) extras.get("data");
                    selectedImageUri = getImageUri(getApplicationContext(), bitmap);
                    Common.DisplayLog("data.getData()", data.getExtras().get("data") + "");
                }
                Common.DisplayLog("selectedImageUri", selectedImageUri + "");
                Common.DisplayLog("getRealPathFromURI(selectedImageUri)", getRealPathFromURI(selectedImageUri) + "");
                String[] COLUMN = {
                        "",
                        SessionId,
                        Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                        "image",
                        getRealPathFromURI(selectedImageUri),
                        "false", Common.getSQLDateTime()
                };
                final String lastid = String.valueOf(cradbh.addData(COLUMN));
                Common.DisplayLog("lastid ", lastid + "");
            }
//            getAttachImages();
        }

        if (requestCode == 3 && data != null) {

            ArrayList<String> selectedItems = data.getExtras().getStringArrayList("selectedItems");
            if (selectedItems != null && selectedItems.size() > 0) {
                for (int i = 0; i < selectedItems.size(); i++) {
                    String[] COLUMN = {
                            "",
                            SessionId,
                            Common.getSharedPreferences(getApplicationContext(), "userId", "0"),
                            "image",
                            selectedItems.get(i).toString(),
                            "false", Common.getSQLDateTime()
                    };
                    final String lastid = String.valueOf(cradbh.addData(COLUMN));
                    Common.DisplayLog("lastid ", lastid + "");
                }
            }


        }

        getAttachImages();
        reloadCount();


        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                    /*btnTempDone.performClick();
                    mainscroll.requestFocus();
                    View current = getCurrentFocus();
                    if(current !=null) {
                        current.clearFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(current.getWindowToken(), 0);
                    }*/

                        int[] imagesLayoutLocation = new int[2];
                        imagesLayout.getLocationOnScreen(imagesLayoutLocation);
                        int he = imagesLayout.getHeight();
                        Common.DisplayLog("imagesLayout.getTop()", "imagesLayoutLocation[0]==" + imagesLayoutLocation[0] + "    imagesLayoutLocation[1] ==" + imagesLayoutLocation[1]);
                        mainscroll.scrollTo(imagesLayoutLocation[0], (imagesLayoutLocation[1] + he));

                    }
                },
                500);

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        /*String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);*/


        /*String result;
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;*/
        String path = null;

        if (Build.VERSION.SDK_INT < 11) {
            path = RealPathUtils.getRealPathFromURI_BelowAPI11(getApplicationContext(), uri);
        } else if (Build.VERSION.SDK_INT < 19) {
            path = RealPathUtils.getRealPathFromURI_API11to18(getApplicationContext(), uri);
        } else {
            try {
                path = RealPathUtils.getRealPathFromURI_API19(getApplicationContext(), uri);
                Common.DisplayLog("getRealPathFromURI", "_API19");
            } catch (Exception ie) {

                try {
                    String[] projection = {MediaStore.Images.Media.DATA};

//                    Cursor cursor = managedQuery(uri, projection, null, null, null);
                    Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                    int column_index = cursor
                            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();
                    path = cursor.getString(column_index);
                    Common.DisplayLog("getRealPathFromURI", "column_index");
                } catch (Exception iee) {

                    String result;
                    Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                    if (cursor == null) { // Source is Dropbox or other similar local file path
                        path = uri.getPath();
                    } else {
                        cursor.moveToFirst();
                        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                        path = cursor.getString(idx);
                        cursor.close();
                    }
                    Common.DisplayLog("getRealPathFromURI", "idx");

                }
            }
        }

        return path;

    }

    @Override
    public void OnClick(int pos) {
        if (action.equals("edit")) {
            removeAttachmentDataOnServer(SessionId, String.valueOf(pos));
        }
        cradbh.deleteSingleData(SessionId, String.valueOf(pos));
        getAttachImages();
        if (imagesList.size() == 0) {
            imagesLayout.setVisibility(View.GONE);
        }
        reloadCount();
    }


    public void loadSavedData(String SessId) {


        Common.DisplayLog("", "loadSavedData " + SessId);

        //load Subtask
        subtasklistLayout.removeAllViews();
        ArrayList<AttachmentTable> lodsubt = cradbh.getAllCurrentSessDataWithType(SessId, "subtask", inFrom);

        if (lodsubt.size() > 0) {
            subtaskLayout.setVisibility(View.VISIBLE);
            addLayout.setVisibility(View.VISIBLE);
            for (int i = 0; i < lodsubt.size(); i++) {
                if (taskDone) {
                    btnTempDone.performClick();
                    subtasklistLayout.addView(createEditView(lodsubt.get(i).getAttachFor(), lodsubt.get(i).getId()));
//                    taskDone = false;
                }
            }

        }

        //load nots
        notlistLayout.removeAllViews();
        ArrayList<AttachmentTable> lodnote = cradbh.getAllCurrentSessDataWithType(SessId, "note", inFrom);
        if (lodnote.size() > 0) {
            notesLayout.setVisibility(View.VISIBLE);

            Common.DisplayLog("", "imgNoteEdit clicked");
            btnTempDone.performClick();
            notlistLayout.addView(createEditView(lodnote.get(0).getAttachFor(), lodnote.get(0).getId()));
//            taskDone = false;
            notCount = false;

        }

        //load image layout
        getAttachImages();


        //load Contact
        getAttachContact();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

//                    btnTempDone.performClick();
                        mainscroll.requestFocus();
//                    EditText current = (EditText) getCurrentFocus();
                    /*View current = getCurrentFocus();
                    if(current !=null) {
                        current.clearFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(current.getWindowToken(), 0);
                    }*/

                    }
                },
                500);


        reloadCount();

    }


    public void SavedEditDataOnServer(String SessId) {


        /***  Save main reminder ****/
        CreateReminderDbHandler crdbh = new CreateReminderDbHandler(getApplicationContext());
        CReminder cReminder = crdbh.getTableSingleDateWihtDao(SessId);

        if (Common.hasInternetConnection(getApplicationContext())) {
            crdbh.updataStatus(SessId, "done");
            Common.DisplayLog("", "loadSavedData " + SessId);

            cReminder = crdbh.getTableSingleDateWihtDao(SessId);

            Common.DisplayLog("SessId", SessId + "");
            pd = new ProgressDialog(AttachmentActivity.this);
            pd.setMessage("Loading...");
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


            /*** Attachment ****/
            CRAttachmentDbHandler cradbh = new CRAttachmentDbHandler(getApplicationContext());
            Common.DisplayLog("cradbh.getDataCount(SessionId)", cradbh.getDataCount(SessionId, "done") + "");


            ArrayList<AttachmentTable> lodsubt = cradbh.getAllCurrentSessDataWithoutType(SessId, "");
            Common.DisplayLog("lodsubt.size() ", lodsubt.size() + "");
            if (lodsubt.size() <= 0) {
                sattachment = true;
            }
            for (int j = 0; j < lodsubt.size(); j++) {
                sattachment = false;
                Common.DisplayLog("lodsubt.get(j).getStatus() ", lodsubt.get(j).getStatus());
                if (lodsubt.get(j).getStatus().equals("false")) {
                    if (lodsubt.get(j).getAttachFor().equals("image")) {

                        File file = new File(lodsubt.get(j).getDetail());

                        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
                        MultipartBody.Part Detail = MultipartBody.Part.createFormData("tDetail", file.getName(), reqFile);

                        RequestBody SesssionId = RequestBody.create(MediaType.parse("text/plain"), lodsubt.get(j).getSesssionId());
                        RequestBody iUserId = RequestBody.create(MediaType.parse("text/plain"), lodsubt.get(j).getSesssionId());
                        RequestBody AttachFor = RequestBody.create(MediaType.parse("text/plain"), lodsubt.get(j).getAttachFor());
                        RequestBody atStatus = RequestBody.create(MediaType.parse("text/plain"), lodsubt.get(j).getStatus());
                        Call<Reminder> callAttachment = apiService.addImageAttachment(SesssionId, iUserId, AttachFor, Detail, atStatus);
                        callAttachment.enqueue(new Callback<Reminder>() {
                            @Override
                            public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                                int statusCode = response.code();
                                Common.DisplayLog("response.body()", response.body().toString());
                                boolean status = response.body().getStatus();
                                Common.DisplayLog("status", status + "");
                                Common.DisplayLog("getMessage", response.body().getMessage() + "");
                                sattachment = true;
                                if (status) {

//                        Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                                } else {
                                    Common.ShowToast(getApplicationContext(), response.body().getMessage());

                                }
//                    pd.dismiss();

                            }

                            @Override
                            public void onFailure(Call<Reminder> call, Throwable t) {
                                sattachment = true;
                                Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
//                    pd.dismiss();
                            }


                        });
                    } else {
                        Call<Reminder> callAttachment = apiService.addAttachment(lodsubt.get(j).getSesssionId(), lodsubt.get(j).getiUserId(), lodsubt.get(j).getAttachFor(), lodsubt.get(j).getDetail(), lodsubt.get(j).getStatus());
                        callAttachment.enqueue(new Callback<Reminder>() {
                            @Override
                            public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                                int statusCode = response.code();
                                Common.DisplayLog("response.body()", response.body().toString());
                                boolean status = response.body().getStatus();
                                Common.DisplayLog("status", status + "");
                                Common.DisplayLog("getMessage", response.body().getMessage() + "");
                                sattachment = true;
                                if (status) {

//                        Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                                } else {
                                    Common.ShowToast(getApplicationContext(), response.body().getMessage());

                                }
//                    pd.dismiss();

                            }

                            @Override
                            public void onFailure(Call<Reminder> call, Throwable t) {
                                sattachment = true;
                                Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
//                    pd.dismiss();
                            }


                        });
                    }
                } else {
                    Call<Reminder> callAttachment = apiService.editAttachment(lodsubt.get(j).getId(), lodsubt.get(j).getSesssionId(), lodsubt.get(j).getiUserId(), lodsubt.get(j).getAttachFor(), lodsubt.get(j).getDetail(), lodsubt.get(j).getStatus());
                    callAttachment.enqueue(new Callback<Reminder>() {
                        @Override
                        public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                            int statusCode = response.code();
                            Common.DisplayLog("response.body()", response.body().toString());
                            boolean status = response.body().getStatus();
                            Common.DisplayLog("status", status + "");
                            Common.DisplayLog("getMessage", response.body().getMessage() + "");
                            sattachment = true;
                            if (status) {

//                        Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                            } else {
                                Common.ShowToast(getApplicationContext(), response.body().getMessage());

                            }
//                    pd.dismiss();

                        }

                        @Override
                        public void onFailure(Call<Reminder> call, Throwable t) {
                            sattachment = true;
                            Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
//                    pd.dismiss();
                        }


                    });
                }
            }


            clodeloding();
        }
    }

    public void removeAttachmentDataOnServer(String SessId, String Id) {


        /***  Save main reminder ****/

        if (Common.hasInternetConnection(getApplicationContext())) {

            Common.DisplayLog("SessId", SessId + " Id :" + Id);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            {
                Call<Reminder> callAttachment = apiService.removeAttachment(Id, SessId);
                callAttachment.enqueue(new Callback<Reminder>() {
                    @Override
                    public void onResponse(Call<Reminder> call, Response<Reminder> response) {
                        int statusCode = response.code();
                        Common.DisplayLog("response.body()", response.body().toString());
                        boolean status = response.body().getStatus();
                        Common.DisplayLog("status", status + "");
                        Common.DisplayLog("getMessage", response.body().getMessage() + "");
                        sattachment = true;
                        if (status) {

//                        Common.ShowToast(getActivity().getApplicationContext(), response.body().getMessage());
                        } else {
                            Common.ShowToast(getApplicationContext(), response.body().getMessage());

                        }
//                    pd.dismiss();

                    }

                    @Override
                    public void onFailure(Call<Reminder> call, Throwable t) {
                        sattachment = true;
                        Common.DisplayLog("fail", t.getMessage().toString());
//                        Common.ShowToast(getApplicationContext(), t.getMessage());
//                    pd.dismiss();
                    }


                });
            }

        }
    }

    public void clodeloding() {


        if (sattachment) {
            pd.dismiss();
//            crdbh.resetTempstatusdata();
            cradbh.updataStatus(SessionId, "done");
            if (action.equals("edit")) {
                super.onBackPressed();
            } else {

                Intent in = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(in);
            }
        } else {
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            clodeloding();
                        }
                    },
                    2000);
        }
    }

}
