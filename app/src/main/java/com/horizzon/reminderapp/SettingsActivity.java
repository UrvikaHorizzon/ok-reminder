package com.horizzon.reminderapp;

import android.content.Intent;
import android.os.Bundle;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.horizzon.reminderapp.adapter.DrawerItemCustomAdapter;
import com.horizzon.reminderapp.utility.ObjectDrawerItem;

public class SettingsActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    protected ListView mDrawerList;
    ObjectDrawerItem[] drawerItem;
    ImageView imgBack, imgOption, imgHome;
    TextView txtProfile, txtGeneral, txtHelp, txtAbout, txtUpgrade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        imgOption = (ImageView) findViewById(R.id.imgOpt);
        imgBack = (ImageView) findViewById(R.id.backBtn);
        imgHome = (ImageView) findViewById(R.id.homeBtn);

        txtProfile = (TextView) findViewById(R.id.txtProfile);
        txtGeneral = (TextView) findViewById(R.id.txtGeneral);
        txtHelp = (TextView) findViewById(R.id.txtHelp);
        txtAbout = (TextView) findViewById(R.id.txtAbout);
        txtUpgrade = (TextView) findViewById(R.id.txtUpgrade);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // list the drawer items
        drawerItem = new ObjectDrawerItem[1];

        drawerItem[0] = new ObjectDrawerItem(R.drawable.settingicon, "Settings");
     //   drawerItem[1] = new ObjectDrawerItem(R.drawable.syncicon, "Sync Now");

        // Pass the folderData to our ListView adapter
        final DrawerItemCustomAdapter adapter1 = new DrawerItemCustomAdapter(this, R.layout.list_item_row, drawerItem);

        // Set the adapter for the list view
        mDrawerList.setAdapter(adapter1);
        mDrawerList.setSelection(0);
        // set the item click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        imgOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imgHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(SettingsActivity.this, HomeActivity.class);
                startActivity(in);
            }
        });

        txtProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(SettingsActivity.this, ProfileActivity.class);
                startActivity(in);
            }
        });

    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            selectItem(position);
            if(position == 0) {
                Intent in = new Intent(SettingsActivity.this, SettingsActivity.class);
                startActivity(in);
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

}
