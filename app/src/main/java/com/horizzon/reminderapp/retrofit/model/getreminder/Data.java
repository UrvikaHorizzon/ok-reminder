
package com.horizzon.reminderapp.retrofit.model.getreminder;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("getReminderList")
    @Expose
    private List<GetReminderList> getReminderList = null;
    @SerializedName("getUsersboxsList")
    @Expose
    private List<GetUsersboxsList> getUsersboxsList = null;
    @SerializedName("getRFolderFilterList")
    @Expose
    private List<GetRFolderFilterList> getRFolderFilterList = null;
    @SerializedName("totalRecords")
    @Expose
    private Integer totalRecords;

    public List<GetReminderList> getGetReminderList() {
        return getReminderList;
    }

    public void setGetReminderList(List<GetReminderList> getReminderList) {
        this.getReminderList = getReminderList;
    }

    public List<GetUsersboxsList> getGetUsersboxsList() {
        return getUsersboxsList;
    }

    public void setGetUsersboxsList(List<GetUsersboxsList> getUsersboxsList) {
        this.getUsersboxsList = getUsersboxsList;
    }

    public List<GetRFolderFilterList> getGetRFolderFilterList() {
        return getRFolderFilterList;
    }

    public void setGetRFolderFilterList(List<GetRFolderFilterList> getRFolderFilterList) {
        this.getRFolderFilterList = getRFolderFilterList;
    }

    public Integer getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }

}
