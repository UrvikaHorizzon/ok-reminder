package com.horizzon.reminderapp.adapter;

import android.app.Activity;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horizzon.reminderapp.InboxCommentActivity;
import com.horizzon.reminderapp.R;
import com.horizzon.reminderapp.dao.ReminderContactUser;
import com.horizzon.reminderapp.data.Common;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by horizzon on 4/3/2017.
 */

public class ContactBoxListAdapter extends RecyclerView.Adapter<ContactBoxListAdapter.MyViewHolder> {

    Activity context;
    ArrayList<ReminderContactUser> list;
    String currdate = "", inFor;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout maincont;
        public ImageView con_image;
        public TextView conName;

        public MyViewHolder(View view) {
            super(view);
            maincont = (LinearLayout) view.findViewById(R.id.maincont);

            con_image = (ImageView) view.findViewById(R.id.con_image);

            conName = (TextView) view.findViewById(R.id.conName);
        }
    }

    public ContactBoxListAdapter(Activity context, ArrayList<ReminderContactUser> list, String inFor) {
        this.context = context;
        this.list = list;
        this.inFor = inFor;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grpcommentcontact_view, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ReminderContactUser cr = list.get(position);

        final String SessionId = cr.getSesssionId();

        holder.conName.setText(cr.getName());
        if (inFor.equals("InboxDetailActivity")) {
            holder.conName.setTextColor(Color.parseColor("#504c4c"));

        }
        if (inFor.equals("InboxCommentActivity")) {

            final InboxCommentActivity inboxCommentActivity = (InboxCommentActivity) context;
            holder.conName.setTextColor(Color.parseColor("#504c4c"));
            holder.maincont.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(context, "clicked :: " + position, Toast.LENGTH_LONG).show();
                    inboxCommentActivity.selectprivatcontact(position);
                }
            });

        }

        String imgpath = ((cr.getPhoto().equals("")) ? "defaultimg" : cr.getPhoto()) + "";
        Common.DisplayLog("imgpath", imgpath + "");
        Picasso.with(context)
                .load(imgpath)
                .placeholder(R.drawable.defaultusericon)
                .error(R.drawable.defaultusericon)
                .into(holder.con_image);


    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
